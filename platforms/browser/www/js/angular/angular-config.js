var downTimer = null;
var quizCursorCount = 0;
var mainView = {};
var preMoreInfoHgt = 0;
var preAddInfoHgt = 0;
//
function submitOnEnter(scope, element, attrs) {
	element.bind("keydown keypress", function (event) {
		if (event.which === 13) {
			scope.$apply(function () {
				scope.$eval(attrs.ngEnter);
			});
			event.preventDefault();
		}
	});
}
//
function userFunctionalities() {
	//
	var myApp = {};
	var rightView = {};
	var $$ = Dom7;
	var adjustableScrollHeight = 900;
	var adjustHeight = 0;
	var blurFlag = true;
	var isThisQuizToSRFlag = false;

	angular.module("userapp", [])

		.filter('nospace', function () {
			return function (value) {
				return (!value) ? '' : value.replace(/ /g, '');
			};
		})
		.directive('itemInit', function ($compile) {
			return function (scope, element, attrs) {
				scope.initItem(element);
			};
		})
		.directive('fallbackSrc', function () {
			var fallbackSrc = {
				link: function postLink(scope, iElement, iAttrs) {
					iElement.bind('error', function () {
						angular.element(this).attr("src", iAttrs.fallbackSrc);
					});
				}
			}
			return fallbackSrc;
		})
		//
		.directive('img', function () {
			return {
				link: function (scope, element, attrs) {
					attrs.$observe('ngSrc', function (val) {
						if (typeof (element.attr("data-rjs")) != "undefined") {
							setTimeout(function () {
								retinajs();
							}, 0)

						}
					});
				}
			};
		})
		//
		.directive('pasteAddressip', function () {
			return {
				restrict: 'A',
				link: function (scope, element, attrs) {
					element.on('paste', function () {
						setTimeout(function () {
							if (element[0].id == 'enqMsgIP' || element[0].id == 'enqMsg') { scope.showHideSendbutton(); }
							else if (element[0].id == 'postCodeIP') { scope.addXmark('postCodeIP', 'postCode_clsIP'); }
							else if (element[0].id == 'cityIP') { scope.addXmark('cityIP', 'city_clsIP'); }
							else if (element[0].id == 'addressLineIP') { scope.addXmark('addressLineIP', 'addressLine_clsIP'); }
							else if (element[0].id == 'buildingNameIP') { scope.addXmark('buildingNameIP', 'buildingName_clsIP'); }
							else { scope.addXmark(element[0].id, element[0].id + '_cls', element.val()); }
						}, 5);
					});
				}
			};
		})
		//
		.directive('scrollOnClick', function () {
			return {
				restrict: 'A',
				link: function (scope, $elm) {
					$elm.on('click', function () {
						$("body").animate({ scrollTop: $elm.offset().top }, "slow");
					});
				}
			}
		})
		//
		.directive('input1', function () {
			return {
				link: function (scope, el, attr) {
					if (Constants.devicePlatform == 'IOS') {
						el.bind('touchstart', function (e) {
							Keyboard.hideFormAccessoryBar(true);
						});
						el.bind('focus', function (e) {
							Keyboard.hideFormAccessoryBar(true);
						});
						el.bind('blur', function (e) {
							Keyboard.hideFormAccessoryBar(false);
						});
					}
					el.bind('keypress', function (e) {
						if (e.which === 13) {
							var next_id = el.parent('div').parent('li').next('li').find('input').attr('id');
							$$('#' + next_id).focus();
							$('#' + next_id).caret(-1);
						}
					});
				}
			};
		})
		//
		.directive('stopBlur', function () {
			return {
				link: function (scope, el, attr) {
					el.bind('touchstart', function (e) {
						blurFlag = false;
					});
					el.bind('touchend', function (e) {
						//blurFlag = true;
					});
				}
			};
		})
		//
		.directive('input2', function ($timeout) {
			return {
				link: function (scope, el, attr) {
					if (Constants.devicePlatform == 'IOS') {
						el.bind('touchstart', function (e) {
							Keyboard.hideFormAccessoryBar(true);
						});
						el.bind('focus', function (e) {
							Keyboard.hideFormAccessoryBar(false);
						});
						el.bind('blur', function (e) {
							Keyboard.hideFormAccessoryBar(true);
						});
					}
					el.bind('keydown', function (e) {
						if (e.which === 13) {
							var next_id = el.parent('div').parent('li').next('li').find('input').attr('id');
							if (next_id == "email")
								next_id = "dateOfBirth";
							if (Constants.devicePlatform == 'IOS') {
								scope.openDatePicker();
								//$$('#dateOfBirth').focus();
							} else {
								//cordova.plugins.Keyboard.close();
								Keyboard.close();
								var d = new Date();
								var mindate = parseInt(d.getFullYear()) - 17 + "-" + ("0" + (parseInt(d.getMonth()) + 1)).slice(-2) + "-" + ("0" + (parseInt(d.getDate()))).slice(-2);
								if ($$("#dateOfBirth").val() == "") {
									//$$("#dateOfBirth").val('2014-04-30');
									if (!isBlankOrNull(scope.userInfoList[0].dateOfBirth)) {
										$("#dateOfBirth").val(scope.userInfoList[0].dateOfBirth);
									} else {
										//$("#dateOfBirth").val("2002-01-01"); 
										$$("#dateOfBirth").val(mindate)

										var date = new Date(mindate);

										var day = ("0" + date.getDate()).slice(-2);
										var monthIndex = date.getMonth();
										var year = date.getFullYear();
										var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
										var inputDate = day + ' ' + monthNames[monthIndex] + ' ' + year;
										//$$("#date_txt").html(inputDate +"   "+test);
										var hiddenDate = parseInt(year) + "-" + ("0" + (parseInt(monthIndex) + 1)).slice(-2) + "-" + ("0" + (parseInt(day))).slice(-2);
										$$("#date_txt_1").val(hiddenDate);


										$$("#date_txt").html(inputDate);
									}
								}
								setTimeout(function () {
									cordova.plugins.Focus.focus($("#dateOfBirth"));
								}, 200);
							}
						}
					});
				}
			};
		})
		//
		.directive('select1', function () {
			return {
				link: function (scope, el, attr) {
					if (Constants.devicePlatform == 'IOS') {
						el.bind('touchstart focus', function (e) {
							Keyboard.hideFormAccessoryBar(false);
						});
						el.bind('blur', function (e) {
							Keyboard.hideFormAccessoryBar(true);
						});
						el.bind('keydown', function (e) {
							// alert(e.which)
						});
					}
				}
			};
		})
		//
		.directive('select2', function () {
			return {
				link: function (scope, el, attr) {
					if (Constants.devicePlatform == 'IOS') {
						el.bind('touchstart', function (e) {
							Keyboard.hideFormAccessoryBar(false);
						});
						el.bind('focus', function (e) {
							Keyboard.hideFormAccessoryBar(false);
						});
					}
				}
			};
		})
		//
		.directive('pasteAddress', function () {
			return {
				restrict: 'A',
				link: function (scope, element, attrs) {
					element.on('paste', function () {
						setTimeout(function () {
							if (element[0].id == 'messageToWhatuni') { scope.enableContactUs(); }
							else if (element[0].id == 'addressAjax') { scope.validatePostcode(); }
							else { scope.addXmark(element[0].id, element[0].id + '_cls', element.val()); }
						}, 5);
					});
				}
			};
		})
		//

		.directive('repeatDone', [function () {
			return {
				restrict: 'A',
				link: function (scope, element, iAttrs) {
					var parentScope = element.parent().scope();
					if (scope.$last && scope.removeFlag == 'Y') {
						// ng-repeat is completed and you now have access to 'myscroll' scope
						console.log("parentScope" + scope.$last);
						setTimeout(function () {
							scope.xtremePushTracking();
						}, 500)

					}
				}
			};
		}])

		.directive('repeatDoneHome', [function () {
			return {
				restrict: 'A',
				link: function (scope, element, iAttrs) {
					var parentScope = element.parent().scope();
					if (scope.$last) {
						// ng-repeat is completed and you now have access to 'myscroll' scope
						console.log("parentScope" + scope.$last);
						setTimeout(function () {
							//scope.xtremePushTracking();  
							scope.closeSearchPopup();
						}, 1000)

					}
				}
			};
		}])


		.run(function () {
			myApp = new Framework7({
				modalTitle: '',
				pushState: true,
				angular: true
				//allowDuplicateUrls:true
			});

			mainView = myApp.addView('.view-main', {
				animatePages: true,
				fastClicks: false,
				swipeBackPage: false
				//domCache: true
			});
			Constants.mainViewObj = mainView;
			Constants.myapp = myApp;
			// Option 1. Using page callback for page (for "about" page in this case) (recommended way):

			$(document).on('click','.covid_upt a', function (e) {
				//console.log(e)
				//e.preventDefault();
				//e.stopPropagation();
				var pageName = "";
				if (angular.element($$D('institutionhomeCtrl')).scope().checkAdvUniversity == 'N') {
					pageName = '/institution-profile/non-advertiser/' + localStorage.getItem('universityId');
				} else {
					pageName = '/institution-profile/advertiser/' + localStorage.getItem('universityId');
				}
				firebaseEventTrack('covid_banner',{'page_name':pageName});
				console.log($(this).attr("articlelink"));
				var url = $(this).attr("articlelink");
				goToProdiverPage(url);
				
			});
			
			$(document).on('click','.covid_infosect a', function (e) {
				//console.log(e)
				//e.preventDefault();
				//e.stopPropagation();
				firebaseEventTrack('covid_banner',{'page_name':'/home'});
				console.log($(this).attr("articlelink"));
				var url = $(this).attr("articlelink");
				goToProdiverPage(url);
				
			});

			//getLatLong();
			$(document).on('click', '.data_shr_cntr a', function (e) {
				//console.log(e)
				//e.preventDefault();
				//e.stopPropagation();
				console.log($(this).attr("privacylink"));
				var url = $(this).attr("privacylink");
				goToWhatuniPage(url);

			});


			myApp.onPageInit('index', function (page) {
				//myApp.alert ("Page rendered" +page);
				//
				//For Terms tooltip
				$$('.open-terms').on('click', function () {
					statusBarFontColor('black');
					myApp.popup('.popup-terms');
				});

				$$('.cls11').on('click', function () {
					statusBarFontColor('white');
					myApp.closeModal('.popup-terms');
				});

				//For Privacy tooltip
				$$('.open-privacy').on('click', function () {
					statusBarFontColor('black');
					myApp.popup('.popup-privacy');
				});
				//		
				$$('.cls12').on('click', function () {
					statusBarFontColor('white');
					myApp.closeModal('.popup-privacy');
				});
				//
				//For Alert
				$$('.fb_btn').on('click', function () {
					//if(Constants.devicePlatform == 'IOS') {
					myApp.modal({
						title: 'Whatuni wants to open facebook',
						buttons: [
							{
								text: 'Cancel'
							},
							{
								text: 'Open',
								onClick: function () {
									initApp();
								}
							}
						]

					});

					/*} else {
						myApp.modal({
							title:'Continue with facebook',
							text: '"Whatuni" wants to open "Facebook"',
							buttons: [
								{
									text:'Cancel'
								},
								{
									text: 'Open',
									onClick: function() {
										initApp();
									}
								}
							]
						
						});
					}*/
				});
				//			
				if ($("#main-index")) {
					console.log("came added class");
					$("#main-index").addClass('lgn_bdy')
				}
			});
			//
			myApp.onPageAfterAnimation('signup', function (page) {
				if ($$D('firstName') && ($$D('firstName').value).trim() == '') { $$D('firstName').focus(); }
				blurFlag = true;
			});
			//
			myApp.onPageAfterAnimation('signup2', function (page) {
				if ($$D('email') && ($$D('email').value).trim() == '') { $$D('email').focus(); }
				blurFlag = true;
			});
			//
			myApp.onPageAfterAnimation('forgotpwd', function (page) {
				var version = Constants.deviceVersion;
				if ((device.platform).toUpperCase() === 'IOS' && parseFloat(version) >= 12) {
					Keyboard.shrinkView(true);
				} else if ((device.platform).toUpperCase() === 'IOS') {
					Keyboard.shrinkView(false);
				}
				setTimeout(function () {
					$$D('emailId').focus();
				}, 400);
			});
			//
			myApp.onPageInit('versioning', function (page) {
				updateMessage('version');
			});
			//
			myApp.onPageInit('exception', function (page) {
				updateMessage('exception');
			});
			//
			myApp.onPageAfterAnimation('email-settings', function (page) {
				if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				if ($("#main-index")) { $("#main-index").addClass('quiz_page') }
				new WOW().init();
			});
			myApp.onPageInit('school-permission', function (page) {
				if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				if ($("#main-index")) { $("#main-index").addClass('quiz_page') }
				new WOW().init();
			});
			myApp.onPageAfterAnimation('clearing-settings', function (page) {
				if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				if ($("#main-index")) { $("#main-index").addClass('quiz_page') }
				new WOW().init();
			});

			myApp.onPageAfterAnimation('clearing-settings-home', function (page) {
				if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				if ($("#main-index")) { $("#main-index").addClass('quiz_page') }
				new WOW().init();
			});
			myApp.onPageBeforeAnimation('exception', function (page) {
				if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				if ($("#main-index")) { $("#main-index").removeClass('quiz_page') }
			});
			myApp.onPageBeforeAnimation('browse', function (page) {
				if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				if ($("#main-index")) { $("#main-index").removeClass('quiz_page') }
			});

			myApp.onPageBeforeAnimation('openday-provider-landing', function (page) {
				if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				if ($("#main-index")) { $("#main-index").removeClass('quiz_page') }
			});



			myApp.onPageAfterAnimation('school-permission', function (page) {
				var userID = localStorage.getItem('LOGGED_IN_USER_ID');
				firebaseEventTrack('screenview', { 'page_name': '/school-permission' });

			});

			myApp.onPageAfterAnimation('clearing-settings', function (page) {
				var userID = localStorage.getItem('LOGGED_IN_USER_ID');
				firebaseEventTrack('screenview', { 'page_name': '/clearing-settings' });
			});

			myApp.onPageAfterAnimation('email-settings', function (page) {
				var userID = localStorage.getItem('LOGGED_IN_USER_ID');
				firebaseEventTrack('screenview', { 'page_name': '/email-settings' });
			});

			myApp.onPageInit('preferences', function (page) {
				// do nothing
				new WOW().init();
			});
			//
			myApp.onPageInit('browse', function (page) {
				console.log('init');
				if ($$D('searchPopup')) {
					$$D('searchPopup').style.display = "none";
				}
				//retinajs();
				//Keyboard.hide();
				//$( ".content-block" ).addClass("animated bounceInUp");
				//$( ".crs_tiles ul li" ).addClass("animated flipInX");      

				/*
				$$('.page-content').scroll(function () {
					// do nothing
				});
				*/
				//
				function onDeviceReady() {
					returnKey("");
					//statusBarFontColor('black');
				}
				document.addEventListener("deviceready", onDeviceReady, false);
			});
			myApp.onPageInit('search-landing', function (page) {
				//if($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				/*
				Keyboard.shrinkView(true);
				Keyboard.disableScrollingInShrinkView(true);
				Keyboard.hideFormAccessoryBar(true);
				Keyboard.automaticScrollToTopOnHiding = true;
				window.addEventListener('keyboardWillShow', function () {
					if(document.getElementById('keyboard_hide')!==null) {
						$('#keyboard_hide').hide();
					}
				});
				window.addEventListener('keyboardWillHide', function () {
					if(document.getElementById('keyboard_hide')!==null) {
						$('#keyboard_hide').show();
					}
				});
				*/
				//googleTagManagerPageView('/search-landing');
				if (Constants.devicePlatform == 'IOS') {
					Keyboard.shrinkView(true);
					Keyboard.disableScrollingInShrinkView(true);
					Keyboard.hideFormAccessoryBar(true);
					returnKey("search");
				}
				//$$D('searchAjax').focus();
				function onDeviceReady() {

					//statusBarFontColor('black');
				}
				document.addEventListener("deviceready", onDeviceReady, false);
			});
			myApp.onPageAfterAnimation('search-landing', function (page) {
				$$D('searchAjax').focus();
				if (Constants.devicePlatform == 'IOS') {
					Keyboard.shrinkView(true);
				}
			});
			myApp.onPageAfterAnimation('browse', function (page) {
				setTimeout(function () {
					retinajs();
				}, 2000);
				//googleTagManagerPageView('/home');
			});

			myApp.onPageBack('login', function (page) {
				//Keyboard.hide();
				//$( ".crs_tiles ul li" ).addClass("animated flipInX");
			});
			myApp.onPageInit('Edit_profile', function (page) {
				//if($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				//console.log("Edit_profile")
				returnKey("next");
			});
			myApp.onPageInit('user-profile', function (page) {
				//if($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				console.log("user-profile")
				//returnKey("");
			});

			myApp.onPageAfterAnimation('Edit_profile', function (page) {
				//console.log("called");
				//console.log(document.getElementById('first-name'))
				//$$D('first-name').focus();
				if (Constants.devicePlatform == 'IOS') {
					Keyboard.shrinkView(true);
				}
			});

			//
			// Option 1. Using page callback for page (for "about" page in this case) (recommended way):
			myApp.onPageInit('searchResults', function (page) {
				//
				// --- SR popups ---
				//
				// Animate CSS
				//$( ".content-block" ).addClass("animated bounceInUp");
				//$( ".crs_tiles ul li" ).addClass("animated flipInX");    
				// Animate CSS
				//statusBarFontColor('white');
				var fromPage = page.fromPage;
				if (fromPage.name == 'quiz-page') {
					isThisQuizToSRFlag = true;
				} else {
					isThisQuizToSRFlag = false;
				}
				if (Constants.devicePlatform == 'IOS') {
					Keyboard.shrinkView(false);
				}
				$$('.open-index, #overHundResults').on('click', function () {
					populateToCurrentValuesInFilterHome('mySearchRootCtrl', '');
					myApp.popup('.popup-index');
				});
				$$('#viewSearchFilterResults').on('click', function () {
					myApp.closeModal('.popup-index');
				});
				//
				$$(document).on('click', '#mySearchRootCtrl .close-popup', function () {
					console.log("filter popup closed");
					statusBarFontColor('white');
				});
				//
				$$('.open-qual').on('click', function () {
					myApp.popup('.popup-qual');
					statusBarFontColor('white');
					//	$( ".crs_tiles ul li" ).addClass("animated flipInX"); 
				});

				$$('.cls1').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-qual');
					$('.popup-qual').removeClass('flip-out');
					$("input[value='" + $$D("qualificationCodeSelected").value + "|" + $$D("qualificationSelected").innerHTML + "']").prop('checked', true);
					if ($$D('totalCourseCount').value == "0") {
						$('.courseCount').hide();
					}
					$$D('totalCourseCount').value = $$D('oldTotalCourseCount').value;
					$('.courseCount').html($('#totalCourseCount').val() + ' courses');
					if ($$D('totalCourseCount').value != 0) {
						$('#viewSearchFilterResults').removeAttr('disabled');
					}
				});

				$$('#qualificationSavBtn').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-qual');
				});

				$$('.open-subject').on('click', function () {
					myApp.popup('.popup-subject');
					$(".popup-subject .crs_tiles ul li").removeClass("animated flipInX");
					$(".popup-subject .content-block").removeClass("animated bounceInUp");
					statusBarFontColor('white');
				});

				$$('#sujectFilterSaveBtn').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-subject');
				});
				$$('.cls2').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-subject');
					$("input[value='" + $$D("categoryCodeSelected").value + "|" + $$D("categoryIdSelected").value + "|" + $$D("subjectSelected").innerHTML + "']").prop('checked', true);
					if ($$D('totalCourseCount').value == "0") {
						$('.courseCount').hide();
					}
					$$D('totalCourseCount').value = $$D('oldTotalCourseCount').value;
					$('.courseCount').html($('#totalCourseCount').val() + ' courses');
					if ($$D('totalCourseCount').value != 0) {
						$('#viewSearchFilterResults').removeAttr('disabled');
					}
				});

				// For Location type
				$$('.open-ltype').on('click', function () {
					myApp.popup('.popup-ltype');
					statusBarFontColor('white');
				});
				$$('#locationTypeFilterSaveBtn').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-ltype');
				});

				$$('.cls4').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-ltype');
					$('input:checkbox').removeAttr('checked');
					var arr = !isBlankOrNull($$D("locationTypeNameSelected").innerHTML) ? $$D("locationTypeNameSelected").innerHTML.split(", ") : "";
					var arrId = !isBlankOrNull($$D("locationTypeIdSelected").value) ? $$D("locationTypeIdSelected").value.split(",") : "";
					for (var z = 0; z < arr.length; z++) {
						$("input[value='" + arrId[z] + "|" + arr[z] + "']").prop('checked', true);
					}
				});

				//For best match
				$$('.open-best').on('click', function () {
					statusBarFontColor('white');
					myApp.popup('.popup-best');
				});
				$$('#assessmentTypeFilterSaveBtn').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-best');
				});
				$$('.cls5').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-best');
					$('input[name=assessment-radio]').removeAttr('checked');
					$("input[value='" + $$D("assessmentTypeSelected").value + "|" + $$D("assessmentTypeSel").innerHTML + "']").prop('checked', true);
				});
				//For your preferences
				$$('.open-pref').on('click', function () {
					statusBarFontColor('white');
					myApp.popup('.popup-pref');
					var arrId = !isBlankOrNull($$D("reviewCategorySelected").value) ? $$D("reviewCategorySelected").value.split(",") : "";
					if (!isBlankOrNull(arrId)) {
						$("#reviewCategoryFilterSaveBtn").removeAttr('disabled');
					}
				});
				$$('#reviewCategoryFilterSaveBtn').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-pref');
				});
				$$('.cls13').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-pref');
					$('input[name=reviewCategory]').removeAttr('checked');
					//var arr = !isBlankOrNull($$D("reviewCategoryNameSelected").innerHTML) ? $$D("reviewCategoryNameSelected").innerHTML.split(", ") : "";
					//
					var reviewCategoryFilterDetails = angular.element($$D('mySearchRootCtrl')).scope().getReviewCategoryFilterDetails();
					var selectedArray = !isBlankOrNull($$D("reviewCategoryNameSelected").innerHTML) ? $$D("reviewCategoryNameSelected").innerHTML.split(", ") : "";
					var tempReviewCategoryNameArray = [];
					angular.forEach(reviewCategoryFilterDetails, function (value, index) {
						if (selectedArray.indexOf(value.reviewCategoryDisplayName.trim()) >= 0) {
							tempReviewCategoryNameArray.push(value.reviewCategoryName);
						}
					});
					var arr = tempReviewCategoryNameArray;
					//
					var arrId = !isBlankOrNull($$D("reviewCategorySelected").value) ? $$D("reviewCategorySelected").value.split(",") : "";
					for (var z = 0; z < arr.length; z++) {
						$("input[value='" + arrId[z] + "|" + arr[z] + "']").prop('checked', true);
					}
				});
				//For Previous study
				$$('.open-pstudy').on('click', function () {
					myApp.popup('.popup-pstudy');
					statusBarFontColor('white');
					$('#previousQualErrMsg').hide();
				});
				$$('#previousQualSavBtn').on('click', function () {
					if (validateSelectedGrades("closePopUp", '', '', '')) { statusBarFontColor('black'); myApp.closeModal('.popup-pstudy'); }
				});
				$$('.cls6').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-pstudy');
					if (!isBlankOrNull($$D("pqSavedValue").value)) { $("#selectGrades").val($$D("pqSavedValue").value); }
					else { $("#selectGrades").val($$D("pqResetValue").value); }
					onChageQualGrades($$D("selectGrades").value, 'preselect', '', 'mySearchRootCtrl');
					$("#previousQualErrMsg").hide();
					if ($$D('totalCourseCount').value == "0") {
						$('.courseCount').hide();
					}
					$$D('totalCourseCount').value = $$D('oldTotalCourseCount').value;
					$('.courseCount').html($('#totalCourseCount').val() + ' courses');
					if ($$D('totalCourseCount').value != 0) {
						$('#viewSearchFilterResults').removeAttr('disabled');
					}
				});
				//For How you study
				$$('.open-howstudy').on('click', function () {
					myApp.popup('.popup-howstudy');
					statusBarFontColor('white');
				});
				$$('#studyModeFilterSaveBtn').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-howstudy');
				});
				$$('.cls8').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-howstudy');
					$('input[name=studyMode-radio]').removeAttr('checked');
					if ($$D("defaultStudyMode")) { $("input[value='" + $$D("defaultStudyMode").value + "']").prop('checked', true); }
					if (!isBlankOrNull($$D("studyModeNameSelected").innerHTML)) { $("input[value='" + $$D("smTextKeySelected").value + "|" + $$D("studyModeNameSelected").innerHTML + "']").prop('checked', true); }
				});
				//For Location
				$$('.open-location').on('click', function () {
					myApp.popup('.popup-location');
					statusBarFontColor('white');
				});
				$$('#locationfilterSaveBtn').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-location');
				});
				$$('.cls9').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('.popup-location');
					if ($("input[name='location']:checked").val() != 'All UK' && $$D("selectedLocation").innerHTML == '') {
						$('input[name=sublocation]').removeAttr('checked');
						$('input[name=location]').removeAttr('checked');
					}
					if (!isBlankOrNull($$D("selectedLocation").innerHTML)) {
						$("input[value='" + $$D("selectedLocation").innerHTML + "']").prop('checked', true);
					} else { $("input[value='All UK']").prop('checked', true); }
					var id = $('input:radio').filter(function () { return this.value === $$D("selectedLocation").innerHTML; }).prop('id');
					if (!isBlankOrNull(id) && id.indexOf('sublocation') > -1) {
						$("input[value='All England']").prop('checked', true);
						$(".city_list").show();
					} else { $(".city_list").hide(); }
				});
				/*For Qualification tooltip
				$$('.open-qualttip').on('click', function (event) {
					myApp.popup('.popup-qualttip');
					$( ".popup-qualttip .crs_tiles ul li" ).removeClass("animated flipInX"); 
					$( ".popup-qualttip .content-block" ).removeClass("animated bounceInUp");
					event.target.id.addClass('flip');
				});
				$$('.cls10').on('click', function(){
					myApp.closeModal('.popup-qualttip');
				});*/
				$$('.clk_im1').on('click', function () {
					myApp.popup('.popup-howttip');
					$(".popup-howstudy").addClass("hover");
				});
				$$('.cls11').on('click', function () {
					//myApp.popup('#myProviderRootCtrl .popup-howttip');
					$(".popup-howstudy").removeClass("hover");
				});
				$$('.clk_im').on('click', function () {
					myApp.popup('.popup-howttip');
					$(".popup-qual").addClass("hover");
				});
				$$('.cls81').on('click', function () {
					//myApp.popup('#myProviderRootCtrl .popup-howttip');
					$(".popup-qual").removeClass("hover");
				});
				/*For Qualification tooltip
				$$('.open-howttip').on('click', function () {
					myApp.popup('.popup-howttip');
					$( ".popup-howttip .content-block" ).removeClass("animated bounceInUp");
				});
				$$('.cls11').on('click', function(){
					myApp.closeModal('.popup-howttip');
				});	
				*/
				$$('#mySearchRootCtrl .page-content').scroll(function () {
					scroll = $('#searchResultsView').scrollTop() + 10;
					//var stickyOffset = $('#sticky').offset().top;
					var cr_sti = $('#sticky').outerHeight();
					//t1= stickyOffset + cr_sti;

					if (scroll > 200 && $('#mbstik').html() == "") {
						$('#mbstik').append($("#sticky"));
						$('#mbstik .profile').addClass('mobsec');
						$('#prepend').addClass('dv1');
					} else if (scroll < 200 && $('#mbstik').html() != "") {
						$('#prepend').append($("#sticky"));
						$('#sticky').removeClass('mobsec');
						$('#prepend').removeClass('dv1');
						//console.log('profile > '+($('#mbstik .profile').attr('class')));
						//$('.sti_pod').slideUp();
						// $('#mbstik').css('display', 'none');
					}
					//$('#sticky').css('top' , scroll);
				});
			});
			/*
			$$(document).on('pageInit', function(e) {
	
				var page = e.detail.page;
				if (page.name === 'searchResults') {
					// Find script tags
					console.log('page.name > '+page.name);
					$$(page.container).find('script').each(function(el){
						console.log('src > '+$(this).attr('src'));
						if ($(this).attr('src')) {
							jQuery.getScript($(this).attr('src'));
						}
						else {
							console.log('text > '+$(this).text());
							// Evaluate js
							eval($(this).text());
						}
					});
				}
				
			});
			*/
			/*
			$$(document).on('pageInit', function(e) {
				var page = e.detail.page;
				if (page.name === 'browse') {
					// Find script tags
					console.log('page.name > '+page.name);
					//retinajs();
					$$(page.container).find('script').each(function(el){
						console.log('src > '+$(this).attr('src'));
						if ($(this).attr('src')) {
							jQuery.getScript($(this).attr('src'));
						}
						else {
							console.log('text > '+$(this).text());
							// Evaluate js
							eval($(this).text());
						}
					});
				}
			});
			*/

			myApp.onPageInit('openday-provider-landing', function (page) {
				$$('#opendayproviderlandingCtrl .close-popup').on('click', function () {
					statusBarFontColor('white');
					if (document.getElementById("stk_app") != null) {
						if ($('#stk_app').html() == '') {
							statusBarFontColor('white');
						} else {
							statusBarFontColor('black');
						}
					}
					//myApp.closeModal('#myProviderRootCtrl .popup-index');
				});
				$(".popup-openday").on("open", function () {
					statusBarFontColor('black');
				});
			});
			
			myApp.onPageInit('providerResults', function (page) {
				localStorage.setItem('PRV', "Yes");
				//Animate CSS
				//	$( ".content-block" ).addClass("animated bounceInUp");
				//	$( ".crs_tiles ul li" ).addClass("animated flipInX");
				//Animate CSS			
				$$('#myProviderRootCtrl .open-index, #myProviderRootCtrl #overHundResults').on('click', function () {
					populateToCurrentValuesInFilterHome('myProviderRootCtrl', 'PR');
					myApp.popup('#myProviderRootCtrl .popup-index');
				});
				$$('#myProviderRootCtrl #viewSearchFilterResultsPR').on('click', function () {
					myApp.closeModal('#myProviderRootCtrl .popup-index');
				});
				$$('#myProviderRootCtrl .close-popup').on('click', function () {
					statusBarFontColor('white');
					if (document.getElementById("stk_app") != null) {
						if ($('#stk_app').html() == '') {
							statusBarFontColor('white');
						} else {
							statusBarFontColor('black');
						}
					}
					//myApp.closeModal('#myProviderRootCtrl .popup-index');
				});
				$$('#myProviderRootCtrl .open-qual').on('click', function () {
					myApp.popup('#myProviderRootCtrl .popup-qual');
					statusBarFontColor('white');
				});

				$$('#myProviderRootCtrl .cls1').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-qual');
					$('.popup-qual').removeClass('flip-out');
					$("input[value='" + $$D("qualificationCodeSelectedPR").value + "|" + $$D("qualificationSelectedPR").innerHTML + "']").prop('checked', true); if ($$D('totalCourseCountPR').value == "0") {
						$('.courseCountPR').hide();
					}
					$$D('totalCourseCountPR').value = $$D('oldTotalCourseCountPR').value;
					$('.courseCountPR').html($('#totalCourseCountPR').val() + ' courses');
					if ($$D('totalCourseCountPR').value != 0) {
						$('#viewSearchFilterResultsPR').removeAttr('disabled');
					}
				});

				$$('#myProviderRootCtrl #qualificationSavBtnPR').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-qual');
				});

				$$('#myProviderRootCtrl .open-subject').on('click', function () {
					myApp.popup('#myProviderRootCtrl .popup-subject');
					statusBarFontColor('white');
					$(".popup-subject .crs_tiles ul li").removeClass("animated flipInX");
					$(".popup-subject .content-block").removeClass("animated bounceInUp");
				});

				$$('#myProviderRootCtrl #sujectFilterSaveBtnPR').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-subject');
				});
				$$('#myProviderRootCtrl .cls2').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-subject');
					$("input[value='" + $$D("categoryCodeSelectedPR").value + "|" + $$D("categoryIdSelectedPR").value + "|" + $$D("subjectSelectedPR").innerHTML + "']").prop('checked', true);
					if ($$D('totalCourseCountPR').value == "0") {
						$('.courseCountPR').hide();
					}
					$$D('totalCourseCountPR').value = $$D('oldTotalCourseCountPR').value;
					$('.courseCountPR').html($('#totalCourseCountPR').val() + ' courses');
					if ($$D('totalCourseCountPR').value != 0) {
						$('#viewSearchFilterResultsPR').removeAttr('disabled');
					}
				});

				// For Location type
				$$('#myProviderRootCtrl .open-ltype').on('click', function () {
					myApp.popup('#myProviderRootCtrl .popup-ltype');
				});
				$$('#locationTypeFilterSaveBtn').on('click', function () {
					myApp.closeModal('#myProviderRootCtrl .popup-ltype');
				});

				$$('#myProviderRootCtrl .cls4').on('click', function () {
					myApp.closeModal('#myProviderRootCtrl .popup-ltype');
					$('input:checkbox').removeAttr('checked');
					var arr = !isBlankOrNull($$D("locationTypeNameSelectedPR").innerHTML) ? $$D("locationTypeNameSelectedPR").innerHTML.split(", ") : "";
					var arrId = !isBlankOrNull($$D("locationTypeIdSelectedPR").value) ? $$D("locationTypeIdSelectedPR").value.split(",") : "";
					for (var z = 0; z < arr.length; z++) {
						$("input[value='" + arrId[z] + "|" + arr[z] + "']").prop('checked', true);
					}
				});

				//For best match
				$$('#myProviderRootCtrl .open-best').on('click', function () {
					statusBarFontColor('white');
					myApp.popup('#myProviderRootCtrl .popup-best');
				});
				$$('#myProviderRootCtrl #assessmentTypeFilterSaveBtn').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-best');
				});
				$$('#myProviderRootCtrl .cls5').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-best');
					$('input[name=assessment-radioPR]').removeAttr('checked');
					$("input[value='" + $$D("assessmentTypeSelectedPR").value + "|" + $$D("assessmentTypeSelPR").innerHTML + "']").prop('checked', true);
				});
				//For Previous study
				$$('#myProviderRootCtrl .open-pstudy').on('click', function () {
					myApp.popup('#myProviderRootCtrl .popup-pstudy');
					statusBarFontColor('white');
				});
				$$('#myProviderRootCtrl #previousQualSavBtnPR').on('click', function () {
					if (validateSelectedGrades("closePopUp", '', 'PR', '')) { statusBarFontColor('black'); myApp.closeModal('#myProviderRootCtrl .popup-pstudy'); }
				});
				$$('#myProviderRootCtrl .cls6').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-pstudy');
					if (!isBlankOrNull($$D("pqSavedValuePR").value)) { $("#selectGrades").val($$D("pqSavedValuePR").value); }
					else { $("#selectGradesPR").val($$D("pqResetValuePR").value); }
					onChageQualGrades($$D("selectGradesPR").value, 'preselect', 'PR', 'myProviderRootCtrl');
					$("#previousQualErrMsgPR").hide();
					if ($$D('totalCourseCountPR').value == "0") {
						$('.courseCountPR').hide();
					}
					$$D('totalCourseCountPR').value = $$D('oldTotalCourseCountPR').value;
					$('.courseCountPR').html($('#totalCourseCountPR').val() + ' courses');
					if ($$D('totalCourseCountPR').value != 0) {
						$('#viewSearchFilterResultsPR').removeAttr('disabled');
					}
				});

				//For How you study
				$$('#myProviderRootCtrl .open-howstudy').on('click', function () {
					myApp.popup('#myProviderRootCtrl .popup-howstudy');
					statusBarFontColor('white');
				});
				$$('#myProviderRootCtrl #studyModeFilterSaveBtnPR').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-howstudy');
				});
				$$('#myProviderRootCtrl .cls8').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-howstudy');
					$('.popup-howstudy').removeClass('flip-out');
					$('input[name=studyMode-radio]').removeAttr('checked');
					if (!isBlankOrNull($$D("studyModeNameSelected").innerHTML)) { $("input[value='" + $$D("smTextKeySelected").value + "|" + $$D("studyModeNameSelected").innerHTML + "']").prop('checked', true); }
				});
				//For Location
				$$('#myProviderRootCtrl .open-location').on('click', function () {
					myApp.popup('#myProviderRootCtrl .popup-location');
				});
				$$('#myProviderRootCtrl #locationfilterSaveBtn').on('click', function () {
					myApp.closeModal('#myProviderRootCtrl .popup-location');
				});
				$$('#myProviderRootCtrl .cls9').on('click', function () {
					myApp.closeModal('#myProviderRootCtrl .popup-location');
					if ($("input[name='location']:checked").val() != 'All UK' && $$D("selectedLocation").innerHTML == '') {
						$('input[name=sublocation]').removeAttr('checked');
						$('input[name=location]').removeAttr('checked');
					}
					$("input[value='" + $$D("selectedLocation").innerHTML + "']").prop('checked', true);
					var id = $('input:radio').filter(function () { return this.value === $$D("selectedLocation").innerHTML; }).prop('id');
					if (!isBlankOrNull(id) && id.indexOf('sublocation') > -1) {
						$("input[value='All England']").prop('checked', true);
						$(".city_list").show();
					} else { $(".city_list").hide(); }
				});
				//For Qualification tooltip
				/*
				$$('#myProviderRootCtrl .open-qualttip').on('click', function (event) {
					myApp.popup('#myProviderRootCtrl .popup-qualttip');
					$( ".popup-qualttip .content-block" ).removeClass("animated bounceInUp"); 
					event.target.id.addClass('flip');
				});
				$$('#myProviderRootCtrl .cls10').on('click', function(){
					myApp.closeModal('#myProviderRootCtrl .popup-qualttip');
					$('.popup-qual').removeClass('flip-in')
					$('.popup-qual').addClass('flip-out')
				});
				*/
				//Your preferences
				$$('#myProviderRootCtrl .open-pref').on('click', function () {
					statusBarFontColor('white');
					myApp.popup('#myProviderRootCtrl .popup-pref');
					var arrId = !isBlankOrNull($$D("reviewCategorySelectedPR").value) ? $$D("reviewCategorySelectedPR").value.split(",") : "";
					if (!isBlankOrNull(arrId)) {
						$("#reviewCategoryFilterSaveBtnPR").removeAttr('disabled');
					}
				});
				$$('#reviewCategoryFilterSaveBtnPR').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-pref');
				});
				$$('#myProviderRootCtrl .cls13').on('click', function () {
					statusBarFontColor('black');
					myApp.closeModal('#myProviderRootCtrl .popup-pref');
					//
					$('input[name=reviewCategoryPR]').removeAttr('checked');
					//var arr = !isBlankOrNull($$D("reviewCategoryNameSelectedPR").innerHTML) ? $$D("reviewCategoryNameSelectedPR").innerHTML.split(", ") : "";
					//
					var reviewCategoryFilterDetails = angular.element($$D('myProviderRootCtrl')).scope().getReviewCategoryFilterDetails();
					var selectedArray = !isBlankOrNull($$D("reviewCategoryNameSelectedPR").innerHTML) ? $$D("reviewCategoryNameSelectedPR").innerHTML.split(", ") : "";
					var tempReviewCategoryNameArray = [];
					angular.forEach(reviewCategoryFilterDetails, function (value, index) {
						if (selectedArray.indexOf(value.reviewCategoryDisplayName.trim()) >= 0) {
							tempReviewCategoryNameArray.push(value.reviewCategoryName);
						}
					});
					var arr = tempReviewCategoryNameArray;
					//
					var arrId = !isBlankOrNull($$D("reviewCategorySelectedPR").value) ? $$D("reviewCategorySelectedPR").value.split(",") : "";
					for (var z = 0; z < arr.length; z++) {
						$("input[value='" + arrId[z] + "|" + arr[z] + "']").prop('checked', true);
					}
					if (isBlankOrNull($$D("reviewCategoryNameSelectedPR").innerHTML)) { $("#reviewCategoryFilterSaveBtnPR").attr('disabled', 'disabled'); }
					//
				});
				//For Qualification tooltip
				/*
				$$('#myProviderRootCtrl .open-howttip').on('click', function () {
					myApp.popup('#myProviderRootCtrl .popup-howttip');
					$( ".popup-howttip .content-block" ).removeClass("animated bounceInUp");
				});
				$$('#myProviderRootCtrl .cls11').on('click', function(){
					myApp.closeModal('#myProviderRootCtrl .popup-howttip');
					$('.popup-howstudy').removeClass('flip-in')
					$('.popup-howstudy').addClass('flip-out')
				});
				*/
				//
				$$('#myProviderRootCtrl .page-content').scroll(function () {
					scroll = $('#myProviderRootCtrl .page-content').scrollTop();
					//var cr_sti = $$('.stk_div').outerHeight();
					if (scroll > 170 && $('#myProviderRootCtrl #stk_app').html() == "") {
						$('#myProviderRootCtrl #stk_app').append($("#myProviderRootCtrl .stk_div"));
						$('#myProviderRootCtrl #stk_app').addClass('stk_fix');
						$('#myProviderRootCtrl .stk_div').addClass('mobsec');
						statusBarFontColor('black');
						// $$('.page-content').css('position','static');
					} else if (scroll < 170 && $('#myProviderRootCtrl #stk_app').html() != "") {
						$('#myProviderRootCtrl #stk_div').prepend($$("#myProviderRootCtrl .stk_div"));
						$('#myProviderRootCtrl .stk_div').removeClass('mobsec');
						$('#myProviderRootCtrl #stk_app').removeClass('stk_fix');
						statusBarFontColor('white');
						//  	$$('.page-content').css('position','relative');	
					}
				});
				$$('#myProviderRootCtrl .clk_im1').on('click', function () {
					myApp.popup('#myProviderRootCtrl .popup-howttip');
					$("#myProviderRootCtrl .popup-howstudy").addClass("hover");
				});
				$$('#myProviderRootCtrl .cls11').on('click', function () {
					//myApp.popup('#myProviderRootCtrl .popup-howttip');
					$("#myProviderRootCtrl .popup-howstudy").removeClass("hover");
				});
				$$('#myProviderRootCtrl .clk_im').on('click', function () {
					myApp.popup('#myProviderRootCtrl .popup-howttip');
					$("#myProviderRootCtrl .popup-qual").addClass("hover");
				});
				$$('#myProviderRootCtrl .cls81').on('click', function () {
					//myApp.popup('#myProviderRootCtrl .popup-howttip');
					$("#myProviderRootCtrl .popup-qual").removeClass("hover");
				});
				//googleTagManagerPageView('/provider-results/'+localStorage.getItem('institutionId'));
			});
			//
			myApp.onPageInit('institution-profile', function (page) {
				console.log('page - ip');
				//
				returnKey("next");
				//
				if (Constants.devicePlatform == 'IOS') {
					Keyboard.shrinkView(true);
				}
				//
				$("#institutionhomeCtrl input").keyup(function (e) {
					if (e.keyCode == 13) {
						var next_id = $(this).parent('div').parent('div').parent('li').next('li').find('input').attr('id');
						// alert(next_id)
						$$('#' + next_id).focus();
					}
				});
				//
				$$('#tab3').on('tab:show', function () {
					console.log('tab:show > tab3');
					if ($$D('addressAjax')) {
						$$D('addressAjax').focus();
					}
				});
				$$('#tab4').on('tab:show', function () {
					console.log('tab:show > tab4');
					if ($$D('buildingName')) {
						setTimeout(function () {
							$$D('buildingName').focus();
						}, 10);
					}
				});
				$$('#tab6').on('tab:show', function () {
					console.log('tab:show > tab6');
					if ($$D('enqMsg')) {
						$$D('enqMsg').focus();
					}
				});
				//
			});

			myApp.onPageInit('course-details', function (page) {
				returnKey("next");
				//
				if (Constants.devicePlatform == 'IOS') {
					Keyboard.shrinkView(true);
				}
				//
				$("#courseDetailsRootCtrl input").keyup(function (e) {
					if (e.keyCode == 13) {
						var next_id = $(this).parent('div').parent('div').parent('li').next('li').find('input').attr('id');
						// alert(next_id)
						$$('#' + next_id).focus();
					}
				});
				//
				$$('#tab3').on('tab:show', function () {
					console.log('tab:show > tab3');
					if ($$D('addressAjax')) {
						$$D('addressAjax').focus();
					}
				});
				$$('#tab4').on('tab:show', function () {
					console.log('tab:show > tab4');
					if ($$D('buildingName')) {
						setTimeout(function () {
							$$D('buildingName').focus();
						}, 10);
					}
				});
				$$('#tab6').on('tab:show', function () {
					console.log('tab:show > tab6');
					if ($$D('enqMsg')) {
						$$D('enqMsg').focus();
					}
				});

			});
			//
			$$(document).on('pageInit', function (e) {
				var page = e.detail.page;
				console.log('page name ' + page.name);
				if (page.name === 'login' || page.name === 'signup' || page.name === 'signup2' || page.name === 'forgotpwd' || page.name === 'index') {
					is_error = false;
					$('.page-content').removeClass('frg_err');
					$('.err_msg').hide();
				}
			});
			$$(document).on('pageAfterBack', function (e) {
				var page = e.detail.page;
				var pageName = $('.page-from-left-to-center').attr('data-page');
				if (pageName == 'search-landing' || pageName == 'browse' || pageName == 'quiz-page') {
					clearFilter();
				}
				if (pageName == 'browse') {
					angular.element($$D('browseHomeCntrl')).scope().closeSearchPopup();
				}
			});
			$$(document).on('pageAfterAnimation', function (e) {
				//
				var page = e.detail.page;
				console.log('pageAfterAnimation ' + page.name);
				var PageArray = ["versioning", "user-profile", "browse", "quiz-page", "final-five", "searchResults", "providerResults", "course-details", "institution-profile"];
				console.log('index ' + PageArray.indexOf(page.name));
				// alert(page.name);
				//
				if (Constants.devicePlatform == 'IOS') {
					if (Keyboard.isVisible && PageArray.indexOf(page.name) >= 0) {
						if (Constants.devicePlatform == 'IOS') {
							Keyboard.hide();
						}
					}
				}
				//
				if (page.name == "browse") {
					statusBarFontColor('black');
				}
				if (page.name == "user-profile") {
					if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				}

				if (page.name == "searchResults" && Constants.devicePlatform == 'IOS') {
					Keyboard.shrinkView(false);
				}
				if (page.name == "user-profile" && Constants.devicePlatform == 'IOS') {
					Keyboard.shrinkView(true);
				}
				//
				if (page.name == "school-permission" && Constants.devicePlatform == 'IOS') {
					Keyboard.shrinkView(true);
				}
				//
				var accessoryHidePage = ["user-profile", "browse", "quiz-page", "final-five", "institution-profile", "index"];
				if (accessoryHidePage.indexOf(page.name) >= 0 && Constants.devicePlatform == 'IOS') {
					Keyboard.hideFormAccessoryBar(true);
				}
				//

				var statusBarWhitePageArray = ["index", "signup", "signup2", "login", "forgotpwd", "searchResults", "providerResults","openday-provider-landing"];
				var statusBarBlackPageArray = ["browse", "search-landing", "final-five", "user-profile", "Edit_profile", "email-settings", "preferences", "school-permission", "clearing-settings", "clearing-settings-home","openday-search-landing","openday-search-result"];
				if (statusBarWhitePageArray.indexOf(page.name) >= 0) {
					statusBarFontColor("white");
				}
				if (statusBarBlackPageArray.indexOf(page.name) >= 0) {
					statusBarFontColor("black");
				}
				//

				//
				var keyboardFixArray = ["index", "signup", "signup2", "login", "forgotpwd"];
				if (keyboardFixArray.indexOf(page.name) >= 0) {
					$$("body").addClass("body_fixed");
				} else {
					$$("body").removeClass("body_fixed");
				}
				//

				var removeBlueClassArray = ["searchResults", "providerResults", "browse", "search-landing", "final-five", "user-profile", "Edit_profile", "institution-profile", "course-details"];
				if (removeBlueClassArray.indexOf(page.name) >= 0) {
					if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				}

				//
				var keyboardArray = ["index"];
				if (keyboardArray.indexOf(page.name) >= 0) {
					console.log("came keyboardArray");
					if (Constants.devicePlatform === 'IOS' && parseFloat(Constants.deviceVersion) >= 12) {
						Keyboard.shrinkView(true);
					} else if (Constants.devicePlatform === 'IOS') {
						Keyboard.shrinkView(false);
					}
				}
				//


			});
			$$(document).on('pageBack', function (e) {
				var page = e.detail.page;
				var pageName = page.name;

				console.log('pageName > ' + pageName);
				if (pageName == 'searchResults') {
					clearFilter();
				}
				if (pageName == 'signup2') {
					$('.err_msg').hide();
				}
			});
			myApp.onPageInit('login', function (page) {
				console.log(' condi' + (!$('#browseHomeCntrl') && !$('#browseHomeCntrl').hasClass('page crs_page ng-scope page-on-center')));
				if (!$('#browseHomeCntrl') && !$('#browseHomeCntrl').hasClass('page crs_page ng-scope page-on-center')) {
					$("input").focus(function () {
						console.log('focus');
						$('body').bind('touchmove', function (e) { e.preventDefault() });
					});
					//
					$("input").blur(function () {
						console.log('blur');
						$('body').unbind('touchmove');
					});
				}
				//  myApp.alert ("Page rendered")
				closeErrMsgFunc('userLoginErrMsg');
				//statusBarFontColor('white');
			});

			myApp.onPageInit('signup', function (page) {
				$("input").focus(function () {
					$('body').bind('touchmove', function (e) { e.preventDefault() });

				});
				$("input").blur(function () {
					$('body').unbind('touchmove');

				});
				statusBarFontColor('white');
			});

			myApp.onPageInit('signup2', function (page) {
				closeErrMsgFunc('userSignUpErrMsg');
				$("input").focus(function () {
					$('body').bind('touchmove', function (e) { e.preventDefault() });

				});
				$("input").blur(function () {
					$('body').unbind('touchmove');

				});
				//statusBarFontColor('white');
			});

			myApp.onPageInit('forgotpwd', function (page) {
				closeErrMsgFunc('recoverPwdErrMsg');
				$("input").focus(function () {
					$('body').bind('touchmove', function (e) { e.preventDefault() });

				});
				$("input").blur(function () {
					$('body').unbind('touchmove');
				});
				if ($("#main-index")) { $("#main-index").addClass('lgn_bdy') }
				//statusBarFontColor('white');
			});

			myApp.onPageInit('user-profile', function (page) {
				//if($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				//Animate CSS
				//$( ".usr_pfl .content-block" ).addClass("animated bounceInUp");
				//$( ".usr_pfl .crs_tiles ul li" ).addClass("animated flipInX");  
				//Animate CSS
				//
				$$('.first_page').on('click', function () {
					myApp.popup('.popup-first_page');
				});
				//It will open popup when you click the link with class 'second_page'
				$$('.second_page').on('click', function () {
					myApp.popup('.popup-second_page');
				});
				//
				//For Privacy tooltip
				$$('.open-privacy').on('click', function () {
					myApp.popup('.privacy_pg');
				});
				//
				$(document).ready(function () {
					$("select").change(function () {
						if ($(this).val() == "") $(this).css({ color: "#90A4AE" });
						else $(this).css({ color: "#37474F" });
					});
				});
				//statusBarFontColor('black');
			});

			myApp.onPageInit('quiz-page', function (page) {
				if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				if ($("#main-index")) { $("#main-index").removeClass('quiz_page') }
				$(document).ready(function () {
					$('.srt_list .fav_ico').click(function () {
						$(this).toggleClass('animated flash fav_fill');
						if ($(this).hasClass('animated')) {
							$('.add_fav').addClass('rev');
						}
						else {
							$('.add_fav').removeClass('rev');
						}
					});
				})
				if (Constants.devicePlatform == 'IOS') {
					Keyboard.shrinkView(true);
					//Keyboard.shrinkView(false);
				}
				statusBarFontColor('black');
			});
			myApp.onPageAfterAnimation('ajax-address', function (page) {
				if ($('#searchSchool #addressAjax')) {
					$('#searchSchool #addressAjax').focus();
				}
				if ($('#searchSchool #schoolAjax')) {
					$('#searchSchool #schoolAjax').focus();
				}
			});
			myApp.onPageAfterAnimation('#ajax-manual-address', function (page) {
				if ($('#searchAddressManual #buildingName')) {
					$('##searchAddressManual #buildingName').focus();
				}
			});
			//
			myApp.onPageInit('quiz', function (page) {
				updateMessage();
			});
			//
			myApp.onPageInit('final-five', function (page) {
				//if($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				//switchHighLightTab('favoriteSubjects');
				//updateMessage();
				statusBarFontColor('black');

				$(document).on('touchstart', '.slct_f5', function (e) {
					if (downTimer != null)
						clearTimeout(downTimer);
					_this = $(this);
					var _e = e;
					console.log("touchstart");
					downTimer = setTimeout(function () {
						if (touch == false && $('.final15').scrollTop() >= 0) {
							_this.addClass('highlight');
							console.log("sortable enable");
							$('#sortable-1').sortable('enable');

							touch = true;
							_this.trigger(_e);

						} else {
							clearTimeout(downTimer);
						}

					}, 300);

				});
				$(document).on('touchend', '.slct_f5', function (e) {
					clearTimeout(downTimer);
					$('.slct_f5').removeClass('highlight');
					$('#sortable-1').sortable('disable');
					touch = false;
				});
				$(document).on('touchmove', '.slct_f5', function (e) {
					clearTimeout(downTimer);
					touch = false;
				});

			});
			//

			/*var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
			//$('input[type="date"]').change(function(){
			$(document).on('change','input[type="date"]',function() {
			$(document).on('change','#dateOfBirth',function() {
				//alert(this.value);         //Date in full format alert(new Date(this.value));
				//var inputDate = new Date(this.value);
				//01-Jan-2002
				
				if(this.value=="") {
					//$$("#searchFieldText_1").val('2014-04-30');
					$$("#date_txt").html("Enter date");
				} else {
					$$("#date_txt").html(this.value);
					var datePickerValue = this.value;
					var datePickerArray = datePickerValue.split("-");
					var datePickerYear = datePickerArray[0];
					
					//var date = new Date(this.value);
					var date = new Date(datePickerValue);
	
					var day = ("0" + date.getDate()).slice(-2) ;
					var monthIndex = date.getMonth();
					var year = date.getFullYear();
					var inputDate = day + ' ' + monthNames[monthIndex] + ' ' + year;
					//$$("#date_txt").html(inputDate +"   "+test);
					$$("#date_txt").html(inputDate);
				} 
			});*/
		})

		.factory('DataService', ['$document', '$http', function ($document, $http) {
			'use strict';

			var pub = {},
				eventListeners = {
					'nextClicked': []
				};

			pub.addEventListener = function (eventName, callback) {
				eventListeners[eventName].push(callback);
			}

			pub.nextClicked = function (firstName, lastName) {
				for (var i = 0; i < eventListeners.nextClicked.length; i++) {
					eventListeners.nextClicked[i](firstName, lastName);
				}
			};

			return pub;

		}])

		.config(function () {
			//$resourceProvider.defaults.stripTrailingSlashes = false;
			//landing screen
			// mainView.router.loadPage("home.html")
			localStorage.setItem('PRV', "No")
			if (!isBlankOrNull(localStorage.getItem('LOGGED_IN_USER_ID'))) {
				window.location.hash = "#!/html/home/homeLanding.html";
				$('#main-index').removeClass('pages').addClass('pages lgn_bdy');
			} else {
				window.location.hash = "#!/html/user/home.html";
			}
		})

		.controller("RootCtrl", ["$scope", "$compile", "$rootScope", "$http", function ($scope, $compile, $rootScope, $http) {
			$scope.title = "Framework7(a)";

			$scope.clearingCheckForGuest = function () {
				var stringJson = {
					//"flagName":"APP_CLEARING_FLAG",									
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/guest-user/"; 
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$$D("clearingCheckLoader").style.display = "block";
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('clearingCheckLoader').style.display = 'none';
					localStorage.setItem('LOGGED_IN_USER_ID', response.guestUserId);
					sessionStorage.setItem("appClearingurl", response.appClearingurl);
					localStorage.setItem('GUEST_USER', "YES");
					if (response.appClearingBanner == "ON") {
						mainView.router.loadPage('html/user/clearing-permission.html');
					} else {
						mainView.router.loadPage('html/quiz/quiz.html');
					}					
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}

		}])

		.controller("homeCntrl", function ($scope, $http) {

			

			$scope.isGuestUser = localStorage.getItem("GUEST_USER");
			Constants.homeScopeObj = $scope;
			$scope.checkUserStatus = function () {
				if (!isBlankOrNull(localStorage.getItem('email')) && checkValidEmail(localStorage.getItem('email'))) {
					var inputJson = {
						"firstName": localStorage.getItem('firstName'),
						"lastName": localStorage.getItem('lastName'),
						"email": localStorage.getItem('email'),
						"userIp": "",
						"userAgent": "",
						"ageRange": localStorage.getItem('fbAgeRange'),
						"locale": localStorage.getItem('fbUserLocale'),
						"fbUserId": localStorage.getItem('userId'),
						"accessToken": "246",
						"affiliateId": "220703",
						"appVersion": Constants.appVersion
					};
					var jsonData = JSON.stringify(inputJson);
					var req = {
						method: 'POST',
						url: 'https://mtest.whatuni.com/wuappws/user/check-existed/',
						headers: { 'Content-Type': 'application/json' },
						data: jsonData
					}
					$http(req).then(function (httpresponse) {
						var response = httpresponse.data;
						/*
						if(response.versionDetails.length > 0) {
						   var exceptionTemplateURL = "../../html/versioning/updateVersion.html";
						   checkVersionDetails(response.versionDetails[0].versionName , response.versionDetails[0].status, response.versionDetails[0].message,exceptionTemplateURL);			
						}
						*/

						if ((response.userExistFlag == "Y")) {
							localStorage.setItem('LOGGED_IN_USER_ID', response.userId);
							localStorage.setItem('LOGGED_IN_USER_FLAG', "Y");
							localStorage.setItem('LOGGED_IN_USER_EMAIL', localStorage.getItem('email'));
							localStorage.setItem('LOGGED_IN_USER_PASSWORD', response.password);
							localStorage.removeItem('firstName');
							localStorage.removeItem('lastName');
							localStorage.removeItem('email');
							//window.location.replace('html/home/homeLanding.html');

							if (localStorage.getItem('GUEST_USER') == "YES") {
								localStorage.setItem('GUEST_USER', "NO");
								mainView.history[mainView.history.length - 2] = mainView.history[mainView.history.length - 2] + "?request=login"

								//
								var action_obj = {}
								if (localStorage.getItem('action') != "") {
									action_obj = { action: localStorage.getItem('action'), action_id: localStorage.getItem('action_id') }
								}
								mainView.router.back({
									'url': mainView.history[mainView.history.length - 2],
									'force': true,
									'ignoreCache': true,
									'pushState': false,
									'reload': true,
									'animatePages': false,
									'query': action_obj
								});
								//
							} else {
								localStorage.setItem('GUEST_USER', "NO");
								mainView.router.loadPage('html/home/homeLanding.html');
							}
						} else {
							localStorage.setItem('SIGNUP_FROM_FACEBOOK', 'Y');
							mainView.router.loadPage('html/user/signUp.html');
						}
					}, function (httpresponse, status) {
						var response = httpresponse.data;
						localStorage.setItem('exception', response.exception);
						localStorage.setItem('status', response.status);
						localStorage.setItem('message', response.message);
						//window.location.replace('html/exception/exception.html');						
						//window.location.href='html/exception/exception.html';	
						mainView.router.loadPage('html/exception/exception.html');
					});
				} else {
					mainView.router.loadPage('html/user/signUp.html');
				}
			}
			$scope.signupAsGuset = function () {
				
				angular.element($$D('RootCtrl')).scope().clearingCheckForGuest();

			}
		})

		.controller("signUpCtrl", function ($scope, $http, DataService) {
			firebaseEventTrack('screenview', { 'page_name': '/sign-up' });

			blurFlag = true;
			//Below code for hide and show the message
			$scope.hideShowUserDetails = function (name) {
				showEmailPage($scope.firstName, $scope.lastName);
			};
			//
			console.log('blurFlag > ' + blurFlag);
			$scope.validateAndShowFirstName = function (divId, val, o) { if (blurFlag) { validateAndShowFirstName(divId, val); } };
			//
			$scope.changeErrorPage = function (divId, successDiv) {
				if ($$D(divId) && $("#" + divId).hasClass("frg_err") && (!$("#" + successDiv).hasClass("inp_tick"))) {
					changeErrorPage(divId);
				}
			};
			//
			$scope.validateFirstName = function (firstName, firstSuccess, lastSuccess) {
				if (!isEmpty($scope.firstName) && validateUserName($scope.firstName)) {
					var tempName = $scope.firstName.charAt(0).toUpperCase() + $scope.firstName.substring(1);
					$scope.firstName = tempName;
					$('#' + firstSuccess).addClass("inp_tick");
					hideShowSuccess(firstName, firstSuccess, lastSuccess, 'nextButton', 'nameSuceessMsg');
					$("#bodyContent").removeClass("frg_err");
					$(".err_msg").hide();
				} else {
					hideSuccess(firstSuccess, 'nameSuceessMsg', 'nextButton');
				}
			};
			//
			$scope.validateLastName = function (lastName, firstSuccess, lastSuccess) {
				if (typeof $scope.lastName != 'undefined') {
					if (!isEmpty($scope.lastName) && validateUserName($scope.lastName)) {
						var tempName = $scope.lastName.charAt(0).toUpperCase() + $scope.lastName.substring(1);
						$scope.lastName = tempName;
						$('#' + lastSuccess).addClass("inp_tick");
						hideShowSuccess(lastName, firstSuccess, lastSuccess, 'nextButton', 'nameSuceessMsg');
					} else {
						hideSuccess(lastSuccess, 'nameSuceessMsg', 'nextButton');
					}
				}
			};
			//
			$scope.onItemClicked = function () {
				localStorage.setItem('firstName', $scope.firstName);
				localStorage.setItem('lastName', $scope.lastName);
			}
			//
			$scope.preLoadFbDetails = function () {
				var userId = localStorage.getItem('userId');
				var firstName = localStorage.getItem('firstName');
				var lastName = localStorage.getItem('lastName');
				if (!isBlankOrNull(userId) && !isBlankOrNull(firstName) && !isBlankOrNull(lastName)) {
					$scope.firstName = firstName;
					$scope.lastName = lastName;
					$('#nextButton').removeAttr("disabled");
					$('#inp_tick_firstname').addClass("inp_tick");
					$('#inp_tick_lastname').addClass("inp_tick");
					localStorage.removeItem('firstName');
					localStorage.removeItem('lastName');
				} else {
					//if($$D('firstName')) { $$D('firstName').focus(); }
				}
			}
			$scope.preLoadFbDetails();
		})

		.controller("signUp2Ctrl", function ($scope, $http, DataService) {
			//Below code for hide and show the message
			blurFlag = true;
			$scope.hideShowUserDetails = function (name) {
				showEmailPage($scope.firstName, $scope.lastName);
			};
			//		
			$scope.changeErrorPage = function (divId, successDiv) {
				if ($$D(divId) && $("#" + divId).hasClass("frg_err") && (!$("#" + successDiv).hasClass("inp_tick"))) {
					changeErrorPage(divId);
				}
			};
			//
			$scope.checkAndShowValidEmail = function (divId, errorMsgDiv) { if (blurFlag) { checkAndShowValidEmail(divId, errorMsgDiv); } };
			//
			DataService.addEventListener('nextClicked', function (firstName, lastName) {
				sessionStorage.setItem('firstName', firstName);
				$scope.firstName = firstName;
				console.log(sessionStorage.getItem('firstName'))
				sessionStorage.setItem('lastName', lastName);
				$scope.lastName = lastName;
				console.log(sessionStorage.getItem('lastName'));
			});
			//
			$scope.validateEmail = function (email, mailSuccess, passSuccess) {
				//console.log("scope email" + $scope.email)
				if (!isEmpty($$D("email").value) && checkValidEmail($$D("email").value)) {
					//if(!isEmpty($scope.email) && checkValidEmail($scope.email)){					
					showSuccess(email, mailSuccess, 'userSignUpErrMsg2', 'bodyContent2');
					hideShowSuccess(email, mailSuccess, passSuccess, 'signUpButton', 'mailSuceessMsg');
					$("#bodyContent2").removeClass("frg_err");
					$(".err_msg").hide();
				} else {
					hideSuccess(mailSuccess, 'mailSuceessMsg', 'signUpButton');
				}
			};
			//
			$scope.validatePassword = function (password, mailSuccess, passSuccess) {
				if (typeof $scope.password != 'undefined') {
					if (!isEmpty($scope.password) && $scope.password.length >= 6) {
						$('#' + passSuccess).addClass("inp_tick");
						hideShowSuccess(password, mailSuccess, passSuccess, 'signUpButton', 'mailSuceessMsg')
					} else {
						hideSuccess(passSuccess, 'mailSuceessMsg', 'signUpButton');

					}
				}
			};
			//
			$scope.preLoadFbDetails = function () {
				var userId = localStorage.getItem('userId');
				var email = localStorage.getItem('email');
				var ageRange = localStorage.getItem('fbAgeRange');
				var locale = localStorage.getItem('fbUserLocale');
				if (!isBlankOrNull(userId) && !isBlankOrNull(email)) {
					$scope.email = email;
					$scope.fbAgeRange = ageRange;
					$scope.fbUserLocale = locale;
					$scope.fbUserId = userId;
					$('#inp_tick_email').addClass("inp_tick");
					localStorage.removeItem('email');
					localStorage.removeItem('fbAgeRange');
					localStorage.removeItem('fbUserLocale');
				} else {
					//if($$D('email')) { $$D('email').focus(); }
				}
			}
			$scope.preLoadFbDetails();
			//		
			//Below code for submit the sign up page
			$scope.submitSignupPage = function () {
				$$D('loader_img-sign_up').style.display = 'block';
				$$D('sign-up').style.display = 'none';
				if (!isEmpty($scope.email) && !isEmpty($scope.password) && checkValidEmail($scope.email)) {
					var inputJson = {
						"firstName": localStorage.getItem('firstName'),
						"lastName": localStorage.getItem('lastName'),
						"email": $scope.email,
						"password": $scope.password,
						"userIp": "",
						"userAgent": navigator.userAgent,
						"ageRange": $scope.fbAgeRange,
						"locale": $scope.fbUserLocale,
						"fbUserId": $scope.fbUserId,
						"accessToken": "246",
						"affiliateId": "220703",
						"appVersion": Constants.appVersion,
						"latitude": latitude,
						"longitude": longtitude
					};
					//localStorage.removeItem('firstName');
					//localStorage.removeItem('lastName');
					var jsonData = JSON.stringify(inputJson);
					var req = {
						method: 'POST',
						url: 'https://mtest.whatuni.com/wuappws/user/registration/',
						headers: { 'Content-Type': 'application/json' },
						data: jsonData
					}
					$http(req).then(function (httpresponse) {
						var response = httpresponse.data;
						$$D('loader_img-sign_up').style.display = 'none';
						$$D('sign-up').style.display = 'block';
						/*
						if(response.versionDetails.length > 0) {
						   var exceptionTemplateURL = "../../html/versioning/updateVersion.html";
						   checkVersionDetails(response.versionDetails[0].versionName , response.versionDetails[0].status, response.versionDetails[0].message,exceptionTemplateURL);			
						}
						*/
						$scope.userId = response.userId;
						if ($scope.userId > 0) {
							localStorage.setItem('LOGGED_IN_USER_ID', $scope.userId);
							localStorage.setItem('LOGGED_IN_USER_FLAG', "Y");
						}
						localStorage.setItem('LOGGED_IN_USER_EMAIL', $scope.email);
						localStorage.setItem('LOGGED_IN_USER_PASSWORD', $scope.password);
						localStorage.setItem('LOGGED_IN_USER_FIRST_NAME', localStorage.getItem('firstName'));
						localStorage.setItem('LOGGED_IN_USER_LAST_NAME', localStorage.getItem('lastName'));

						$scope.errorMessage = response.responseCode;
						if ($scope.errorMessage == "REGISTRATION_SUCCESS") {
							sessionStorage.setItem("appClearingurl", response.appClearingurl);
							
							firebaseEventTrack('signup', { method: 'email' });
							$(".err_msg").hide();
							//mainView.router.loadPage('html/quiz/quiz.html');
							localStorage.removeItem('firstName');
							localStorage.removeItem('lastName');
							// mainView.router.loadPage('html/user/emailSettingNotification.html');
							// mainView.router.loadPage('html/user/school-permission.html');


							if (localStorage.getItem('GUEST_USER') == "YES") {
								localStorage.setItem('GUEST_USER', "NO");
								mainView.history[mainView.history.length - 4] = mainView.history[mainView.history.length - 4] + "?request=login"

								//
								var action_obj = {}
								if (localStorage.getItem('action') != "") {
									action_obj = { action: localStorage.getItem('action'), action_id: localStorage.getItem('action_id') }
								}
								mainView.router.back({
									'url': mainView.history[mainView.history.length - 4],
									'force': true,
									'ignoreCache': true,
									'pushState': false,
									'reload': true,
									'animatePages': false,
									'query': action_obj
								});
								//
							} else {
								localStorage.setItem('GUEST_USER', "NO");
								if (response.appClearingBanner == "ON") {
									mainView.router.loadPage('html/user/clearing-permission.html');
								} else {
									mainView.router.loadPage('html/user/school-permission.html');
								}
							}


						} else {
							displayErrMg($scope.errorMessage);
							$("#userSignUpErrMsg2").show();
							$("#bodyContent2").addClass("frg_err");
						}
					}, function (httpresponse, status) {
						var response = httpresponse.data;
						localStorage.setItem('exception', response.exception);
						localStorage.setItem('status', response.status);
						localStorage.setItem('message', response.message);
						//window.location.replace('html/exception/exception.html');						
						mainView.router.loadPage('html/exception/exception.html');
					});
				} else {
					$(".err_msg").show();
					$$D('loginErrMsg').innerHTML = ErrorMessage.error_signup_mail;
					showSuccessMessage('mailSuceessMsg', 'none');
					$('#signUpButton').attr("disabled", "disabled");
				}
			};
			//Below code for go back function
			$scope.goBack = function () {
				$(".err_msg").hide();
				if ($$D('userMail').style.display == 'block') {
					$$D('userName').style.display = 'block';
					$$D('userMail').style.display = 'none';
				} else {
					window.history.back();
				}
			};
			//				
		})
		//
		.controller("clearingPermissionCtrl", function ($scope, $http, $element) {

			$scope.setClearingFlag = function (flag) {

				if (flag == "ON") {
					
					xtremePushTag("Clearing_Courses", "Yes");
					//googleTagManagerEventTrack(null, 'Clearing Courses Popup', 'Clearing Courses Popup', 'yes', '0');
					//mainView.router.loadPage('html/user/clearing-whatuni-go.html');
					if (isGuestUser()) {
						mainView.router.loadPage('html/home/homeLanding.html');
					} else {
						mainView.router.loadPage('html/user/school-permission.html');
					}
					goToWhatuniPage(sessionStorage.getItem("appClearingurl"));
				} else {
					
					xtremePushTag("Clearing_Courses", "No");
					//googleTagManagerEventTrack(null, 'Clearing Courses Popup', 'Clearing Courses Popup', 'no', '0');
					if (isGuestUser()) {
						mainView.router.loadPage('html/quiz/quiz.html');
					} else {
						mainView.router.loadPage('html/user/school-permission.html');
					}
				}



			}
		})
		.controller("schoolpermissionCtrl", function ($scope, $http, $element) {
			//
			$scope.userSchoolName = "";
			$scope.userSchoolId = ""
			$scope.saveSchoolForm = function () {
				if ($scope.userSchoolName.trim() == "Non UK / not on list") {
					//googleTagManagerEventTrack(null, 'What is your school', 'Confirm', $scope.userSchoolName, '0');
					mainView.router.loadPage('html/user/emailSettingNotification.html');
				} else {
					var firstName = localStorage.getItem('LOGGED_IN_USER_FIRST_NAME');
					var lastName = localStorage.getItem('LOGGED_IN_USER_LAST_NAME');
					var email = localStorage.getItem('LOGGED_IN_USER_EMAIL');
					var password = localStorage.getItem('LOGGED_IN_USER_PASSWORD');
					var dateOfBirth = "";
					var nationality = "";
					var addressLineOne = "";
					var addressLineTwo = "";
					var city = "";
					var countyState = "";
					var postCode = "";
					var startDate = "";
					var studyLevel = "";

					var userSchoolName = "";

					var userSchoolId = "";
					//if(!isBlankOrNull($scope.userSchoolName))
					//userSchoolName = $scope.userSchoolName;

					//if(!isBlankOrNull($scope.userSchoolId))
					//userSchoolId = $scope.userSchoolId;

					//localStorage.setItem('SCHOOL_NAME', userSchoolName);	
					//localStorage.setItem('SCHOOL_URN', userSchoolId);	
					var preQualType = "";
					var gradeText = "";

					var nonUkSchool = !isBlankOrNull($('#nonUKSchoolNameFlag').val()) ? $('#nonUKSchoolNameFlag').val() : "N";
					//var nonUkSchool = "N";

					var userSchoolName = !isBlankOrNull($$D("userSchoolName").innerHTML) ? $$D("userSchoolName").innerHTML : "";
					var userSchoolId = !isBlankOrNull($$D("userSchoolId").value) ? $$D("userSchoolId").value : "";

					localStorage.setItem('SCHOOL_NAME', userSchoolName);
					localStorage.setItem('SCHOOL_URN', userSchoolId);
					//googleTagManagerEventTrack(null, 'What is your school', 'Confirm', userSchoolName, '0');
					if (nonUkSchool == "N") { userSchoolName = userSchoolId; }
					var stringJson = {
						"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
						"firstName": firstName,
						"lastName": lastName,
						"email": email,
						"password": password,
						"dateOfBirth": dateOfBirth,
						"nationality": nationality,
						"addressLineOne": addressLineOne,
						"addressLineTwo": addressLineTwo,
						"city": city,
						"countyState": countyState,
						"postCode": postCode,
						"yearOfEntry": startDate,
						"studyLevel": studyLevel,
						"school": userSchoolName,
						"nonUkSchool": nonUkSchool,
						"preQualType": preQualType,
						"preQualGrade": gradeText,
						"accessToken": "246",
						"affiliateId": "220703",
						"appVersion": Constants.appVersion
					};
					var jsonData = JSON.stringify(stringJson);
					console.log(">>>>>>>>>>>>>");
					console.log(jsonData);
					console.log(">>>>>>>>>>>>>");
					//mainView.router.loadPage('html/user/emailSettingNotification.html');	

					var req = {
						method: 'POST',
						url: 'https://mtest.whatuni.com/wuappws/profile/save-user-profile/',
						headers: { 'Content-Type': 'application/json' },
						data: jsonData
					}
					$http(req).then(function (httpresponse) {
						var response = httpresponse.data;
						var status = response.successMsg;
						if (status == "SUCCESS") {
							mainView.router.loadPage('html/user/emailSettingNotification.html');
						}
					}, function (httpresponse, status) {
						var response = httpresponse.data;
						localStorage.setItem('exception', response.exception);
						localStorage.setItem('status', response.status);
						localStorage.setItem('message', response.message);
						mainView.router.loadPage('html/exception/exception.html');
					});

				}

			}
			//
			$scope.goToSchoolAjax = function () {
				var window_height = $(window).height();
				//alert(window_height*.97);


				$(".school_list .email-content").css('height', window_height - 59.5 + "px");
				$('#schoolPermissionNonUKCheck').attr('checked', false);
				$(".scl_ajax ul li").css("background", "#fff");
				$$D('xmark').className = '';
				myApp.showTab('#tab2');
				setTimeout(function () {
					$$D('schoolPermissionAjax').focus();
				}, 500)
			}
			//
			$scope.getSchool = function () {

				var nonUKCheck = $('#schoolPermissionNonUKCheck').prop('checked');
				if ($$D('schoolPermissionAjax') && !nonUKCheck) {
					if ($$D('schoolPermissionAjax').value.length > 0) {
						$$D('xmark').className = 'xmark';
						var school = $$D('schoolPermissionAjax').value;
						if (school.length > 2) {
							$$D('editSchoolLoader').style.display = 'block';
							var stringJson = {
								"school": school,
								"appVersion": Constants.appVersion,
								"affiliateId": "220703",
								"accessToken": "246"
							};
							var jsonData = JSON.stringify(stringJson);
							var url = "https://mtest.whatuni.com/wuappws/profile/school-list-ajax/";
							var req = {
								method: 'POST',
								url: url,
								headers: { 'Content-Type': 'application/json' },
								data: jsonData
							}
							$http(req).then(function (httpresponse) {
								var response = httpresponse.data;
								$$D('editSchoolLoader').style.display = 'none';
								if (!($('#schoolPermissionNonUKCheck').prop('checked'))) { $scope.schoolList = response.schoolList; }
								if (response.schoolList != null && response.schoolList.length > 0 && $$D('schoolPermissionAjax').value.length > 2) {
									$('#schoolPermissionAjax').css('display', 'block');
								} else {
									$("#saveUserSchoolName").addClass("gry"); $("#saveUserSchoolName").attr("disabled", "disabled");
									$scope.schoolList = null;
								}
							}, function (httpresponse, status) {
								var response = httpresponse.data;
								localStorage.setItem('exception', response.exception);
								localStorage.setItem('status', response.status);
								localStorage.setItem('message', response.message);
								mainView.router.loadPage('html/exception/exception.html');
							});
						} else {
							$("#saveUserSchoolName").addClass("gry"); $("#saveUserSchoolName").attr("disabled", "disabled");
							$scope.schoolList = null;
						}
					} else {
						$$D('xmark').className = '';
					}
				} else {
					/*var school = $$D('schoolPermissionAjax').value;
					if(school.length >= 1){ $("#userSchoolName").html(school);
					$("#saveUserSchoolName").removeClass("gry"); $("#saveUserSchoolName").removeAttr("disabled", "disabled"); }
					else { $("#userSchoolName").html($("#oldUserSchoolName").val());
					$("#saveUserSchoolName").addClass("gry"); $("#saveUserSchoolName").attr("disabled", "disabled"); }
					*/
				}
			}
			//

			$scope.clearSearch = function () {
				if ($$D('schoolPermissionAjax')) {
					$$D('schoolPermissionAjax').value = "";
					$$D('xmark').className = '';
					$scope.userSchoolName = "";
					$scope.userSchoolId = "";
					$scope.schoolList = null;
				}
			}
			//
			$scope.saveSchool = function (index) {
				if ($$D('school-' + index)) {
					var school = $$D('school-' + index).value.split("|");
					var schoolId = school[0];
					var schoolName = school[1];

					$("#userSchoolName").html(schoolName);
					$("#userSchoolId").val(schoolId);
					$("#schoolPermissionAjax").val(schoolName);
					//$("#saveUserSchoolName").removeClass("gry");
					//$("#saveUserSchoolName").removeAttr("disabled", "disabled");
					$scope.schoolList = null;
					$scope.saveUserSchoolAjax();
				}
			}
			//
			$scope.closeAndClearSchool = function () {
				if (!isBlankOrNull($("#oldUserSchoolName").val())) {
					$("#userSchoolName").html($("#oldUserSchoolName").val());
					$scope.userSchoolName = $("#oldUserSchoolName").val();
					$("#userSchoolName").removeClass("color_grey");
				} else {
					$scope.userSchoolName = "";
					if ($("#userSchoolName").html().trim() == "" || $("#userSchoolName").html().trim() == "Not selected") {
						$("#userSchoolName").html("Not selected");
						$("#userSchoolName").addClass("color_grey");
					} else {
						$("#userSchoolName").html("Non UK / not on list");
						$scope.userSchoolName = "Non UK / not on list";
					}

				}
				$("#schoolPermissionAjax").val("");
				$("#schoolPermissionAjax").attr("placeholder", "Enter school name");
				$("input[id='schoolPermissionNonUKCheck']").prop('checked', false);
				$scope.schoolList = null;
				//$("#saveUserSchoolName").addClass("gry"); $("#saveUserSchoolName").attr("disabled", "disabled");
				$(".school_list .email-content").removeAttr('style');
				//$(".school_list .email-content").css('height',"97%");
				myApp.showTab('#tab1');
			}
			//
			$scope.saveUserSchoolAjax = function () {
				$("#oldUserSchoolName").val($("#userSchoolName").html());
				$("#nonUKSchoolNameFlag").val($('#schoolPermissionNonUKCheck').prop('checked') ? "Y" : "N");
				$scope.closeAndClearSchool();
			}
			//
			$scope.checkNonUkFlag = function () {
				$('#schoolPermissionNonUKCheck').attr('checked', true);
				$("#schoolPermissionAjax").val("");
				$("#schoolPermissionAjax").attr("placeholder", "Enter school name");
				$scope.schoolList = null;
				$("#oldUserSchoolName").val('');
				$("#userSchoolName").html('Non UK / not on list');
				//$scope.userSchoolName = "";
				$scope.userSchoolName = "Non UK / not on list";
				$(".school_list .email-content").removeAttr('style');
				//$(".school_list .email-content").css('height',"97%");
				$("#userSchoolName").removeClass("color_grey");
				myApp.showTab('#tab1');
			}
			//


		})
		//controller for emailSettingNotification.html
		.controller("emailSettingNotificationCtrl", function ($scope) {

			//redirecting the home page based on the clearing flag to avoid showing the chatbot in clearing journey 	 
			$scope.redirectionBasedOnUserClearingFlag = function () {
				// if (localStorage.getItem("USER_CLEARING_FLAG") == "ON") {
				// 	if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
				// 	if ($("#main-index")) { $("#main-index").removeClass('quiz_page') }
				// 	mainView.router.loadPage('html/home/homeLanding.html');
				// } else {
					mainView.router.loadPage('html/quiz/quiz.html');
				// }
			}
			//	   
		})
		//
		.directive('ngEnter', function () {
			return function (scope, element, attrs) {
				submitOnEnter(scope, element, attrs);
			};
		})
		//
		.controller("userLoginCtrl", function ($scope, $http) {
			firebaseEventTrack('screenview', { 'page_name': '/sign-in' });

			if (localStorage.getItem('FROM_PROFILE_PAGE') == "LOGIN") {
				mainView.router.loadPage('html/user/forgotPassword.html');
				localStorage.setItem('FROM_PROFILE_PAGE', "FORGOT");
			}
			if ((localStorage.getItem('LOGGED_IN_USER_FLAG') == 'N') && !isBlankOrNull(localStorage.getItem('LOGGED_IN_USER_EMAIL'))) {
				$scope.email = localStorage.getItem('LOGGED_IN_USER_EMAIL');
				$('#inp_tick_email').addClass("inp_tick");
				$scope.password = "";
				if (!isBlankOrNull(localStorage.getItem('LOGGED_IN_USER_PASSWORD'))) {
					$scope.password = localStorage.getItem('LOGGED_IN_USER_PASSWORD');
					$('#inp_tick_pwd').addClass("inp_tick");
					$('#logInButton').removeAttr("disabled");
				}
				$("#showHidePwd").hide();
			} else {
				//if($$D('email') && !$('#browseHomeCntrl')) { $$D('email').focus(); }
			}
			//		
			$scope.changeErrorPage = function (divId, successDiv) {
				if ($$D(divId) && $("#" + divId).hasClass("frg_err") && (!$("#" + successDiv).hasClass("inp_tick"))) {
					changeErrorPage(divId);
				}
			};
			//
			$scope.validateUserLogin = function () {
				if (validateUserLogin()) {
					$scope.login();
				}
			};
			$scope.checkAndShowValidEmail = function (divId, errorMsgDiv) { checkAndShowValidEmail(divId, errorMsgDiv); };
			$scope.login = function () {
				$$D('loader_img_log_in').style.display = 'block';
				$$D('logIn').style.display = 'none';
				var stringJson = {
					"email": $scope.email,
					"password": $scope.password,
					"appVersion": Constants.appVersion,
					"accessToken": "246",
					"affiliateId": "220703"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/user/login/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('loader_img_log_in').style.display = 'none';
					$$D('logIn').style.display = 'block';
					/*
					if(response.versionDetails.length > 0) {
					   var exceptionTemplateURL = "../../html/versioning/updateVersion.html";
					   checkVersionDetails(response.versionDetails[0].versionName , response.versionDetails[0].status, response.versionDetails[0].message, exceptionTemplateURL);			
					}
					*/
					$scope.userId = response.userId;
					if (localStorage.getItem('LOGGED_IN_USER_ID') != $scope.userId && $scope.userId > 0) {
						localStorage.setItem('LOGGED_IN_USER_ID', $scope.userId);
						localStorage.setItem('LOGGED_IN_USER_FLAG', "Y");
						localStorage.setItem('LOGGED_IN_USER_EMAIL', $scope.email);
						localStorage.setItem('LOGGED_IN_USER_PASSWORD', $scope.password);
					}
					$scope.userDetails = response.userDetails;
					localStorage.setItem('LOGGED_IN_USER_FIRST_NAME', $scope.userDetails[0].forename);
					localStorage.setItem('LOGGED_IN_USER_LAST_NAME', $scope.userDetails[0].surname);
					localStorage.setItem('LOGGED_IN_USER_IMAGE', $scope.userDetails[0].userImage);
					localStorage.setItem('LOGGED_IN_USER_FACEBOOK_ID', $scope.userDetails[0].facebookId);
					localStorage.setItem('SCHOOL_NAME', $scope.userDetails[0].schoolName);
					localStorage.setItem('SCHOOL_URN', $scope.userDetails[0].schoolURN);
					localStorage.setItem('USER_MODE', $scope.userDetails[0].userSource);



					//
					$scope.errorMessage = response.errorMessage;
					if ($scope.errorMessage == "SIGNIN_SUCCESS") {
						$("#loginSucErrBlk").removeClass("frg_err");
						$("#userLoginErrMsg").hide();
						localStorage.setItem('latitude', latitude);
						localStorage.setItem('longitude', longtitude);
						localStorage.setItem('QUIZ_COMPLETE', "No");
						if (localStorage.getItem('GUEST_USER') == "YES") {
							localStorage.setItem('GUEST_USER', "NO");
							mainView.history[mainView.history.length - 3] = mainView.history[mainView.history.length - 3] + "?request=login"

							//
							var action_obj = {}
							if (localStorage.getItem('action') != "") {
								action_obj = { action: localStorage.getItem('action'), action_id: localStorage.getItem('action_id') }
							}
							mainView.router.back({
								'url': mainView.history[mainView.history.length - 3],
								'force': true,
								'ignoreCache': true,
								'pushState': false,
								'reload': true,
								'animatePages': false,
								'query': action_obj
							});
							//
						} else {
							localStorage.setItem('GUEST_USER', "NO");
							mainView.router.loadPage('html/home/homeLanding.html');
						}

					} else {
						displayErrMg($scope.errorMessage);
						$("#loginSucErrBlk").addClass("frg_err");
						$("#userLoginErrMsg").show();
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					//window.location.replace('html/exception/exception.html');						
					mainView.router.loadPage('html/exception/exception.html');
				});
			};
		})

		.controller("forgotPwdCtrl", function ($scope, $http) {
			firebaseEventTrack('screenview', { 'page_name': '/forgot-password' });

			if (localStorage.getItem('FROM_PROFILE_PAGE') == "FORGOT") {
				setTimeout(function () { $("#emailId").val(localStorage.getItem('LOGGED_IN_USER_EMAIL')); }, 500);
				localStorage.removeItem('FROM_PROFILE_PAGE');
				$("#recoverPwdButton").removeAttr("disabled");
				$("#forgotResetPwdTxt").html("Reset password?");
				$("#recoverPassword").html("Reset password");
				$("#resetPswdHeadText").html("Enter email address to reset your password");
			}
			$scope.validateUserLogin = function () {
				if (validateForgotPwdUserLogin()) {
					$scope.login();
				}
			};
			//		  
			$scope.login = function () {
				$$D('loader_img_forgot').style.display = 'block';
				$$D('recoverPassword').style.display = 'none';
				var stringJson = {
					"email": $scope.email, //"prabha@idea.com",							
					"appVersion": Constants.appVersion,
					"accessToken": "246",
					"affiliateId": "220703"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/user/forgot-password/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('loader_img_forgot').style.display = 'none';
					$$D('recoverPassword').style.display = 'block';
					/*
					if(response.versionDetails.length > 0) {
					   var exceptionTemplateURL = "../../html/versioning/updateVersion.html";
					   checkVersionDetails(response.versionDetails[0].versionName , response.versionDetails[0].status, response.versionDetails[0].message, exceptionTemplateURL);			
					}
					*/
					$scope.responseCode = response.responseCode;
					$("#recoverPwdSuccMsg").show();
					$("#recoverPwdSucErrBlk").addClass("frg_sucss");
					$("#recoverPwdSucErrBlk").removeClass("frg_err");
					$("#recoverPwdErrMsg").hide();
					/*
					if($scope.responseCode == "0") {
						$("#recoverPwdSuccMsg").show();
						$("#recoverPwdSucErrBlk").addClass("frg_sucss");
						$("#recoverPwdSucErrBlk").removeClass("frg_err");				
						$("#recoverPwdErrMsg").hide();
					} else {                
						$$D("recoverErrMsg").innerHTML = ErrorMessage.error_email_nofound; 
						$("#recoverPwdSucErrBlk").addClass("frg_err");
						$("#recoverPwdErrMsg").show();
					}
					*/
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					//window.location.replace('html/exception/exception.html');						
					mainView.router.loadPage('html/exception/exception.html');
				});
			};
		})

		.controller("browseHomeCtrl", function ($scope, $http, $element, $sce) {
			//Below method for clear filter

			$scope.appInteractionLogging = function (activity, collegeId, extraTextURL) {
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var collegeId = collegeId;
				var courseId = null;
				var userAgent = navigator.userAgent;
				var userIp = null;
				var suborderItemId = null;
				var profileId = null;
				var typeName = null;
				var activity = activity;
				var extraText = null;
				if (!isBlankOrNull(extraTextURL)) {
					extraText = extraTextURL;
				}
				var stringJson = {
					"userId": userId,
					"collegeId": collegeId,
					"courseId": courseId,
					"userIp": userIp,
					"userAgent": userAgent,
					"jsLog": 'Y',
					"suborderItemId": suborderItemId,
					"profileId": profileId,
					"typeName": typeName,
					"activity": activity,
					"extraText": extraText,
					"latitude": latitude,
					"longitude": longtitude,
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/interaction/db-stats/logging/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					//$$D('loaderCourse').style.display = 'none';
					var logStatusId = response.logStatusId;
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					// do nothing				
				});
			}
			function onDeviceReady() {

				if (!isBlankOrNull(localStorage.getItem('LOGGED_IN_USER_ID'))) {
					var userID = localStorage.getItem('LOGGED_IN_USER_ID');
				} else {
					var userID = "";
				}
				firebaseEventTrack('screenview', { 'page_name': '/home' });

			}
			$scope.getShortListFlag = function (institutionId, removeFrom, addTo, rootCtrl, pageFlag) {
				getShortListRootCtrl(institutionId, removeFrom, addTo, $scope, $http, rootCtrl, pageFlag);
			}

			//featured provider open link function by Sangeeth.S
			$scope.openLinkAndStatsLog = function (collegeId, url, uniName) {
				$scope.appInteractionLogging('HOTCOURSES: CLEARING: FEATURED PROVIDER', collegeId, url);
				if (!isBlankOrNull(uniName)) {
					uniName = uniName.toLowerCase();
				}
				//googleTagManagerEventTrack(null, 'Interaction', 'Webclick', uniName, encodeURI(url));	
				window.open(encodeURI(url), '_system');
			}
			//
			document.addEventListener("deviceready", onDeviceReady, false);
			browseScopeObject = $scope;
			var currentSlideCount = 0;
			var galleryTop = null;
			var galleryThumbs = null;
			//$('#main-index').removeClass('pages lgn_bdy').addClass('pages');
			$scope.gotowhatuniclearing = function() {
				goToWhatuniPage($scope.appClearingurl);
			}
			$scope.getBrowseList = function (pullFlag) {
				if (!isBlankOrNull(localStorage.getItem('latitude')) && !isBlankOrNull(localStorage.getItem('longitude'))) {
					latitude = localStorage.getItem('latitude');
					longtitude = localStorage.getItem('longitude');
					localStorage.removeItem("latitude");
					localStorage.removeItem("longitude");
				}
				console.log("pullFlag >", pullFlag)
				var locationMode = ($('#locationMode') && !isBlankOrNull($('#locationMode').val()) && !(pullFlag == 'pullFlag')) ? $('#locationMode').val() : "CLOSE_TO_YOU";
				var jsonData = JSON.stringify(getJson(pullFlag));
				if ($$D('searchPopup')) { $$D('searchPopup').style.display = "none"; }
				if (pullFlag == '' || pullFlag == 'location_change') {
					$$D('loaderCourse').style.display = "block";
				}
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/home-browse/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				var scrolling_array = [0, 0];
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					//var response = response1.data;
					$$D('arrow_hide').style.display = "block";
					$scope.guestUserFlag = response.guestUserFlag;
					if (pullFlag == '' || pullFlag == 'location_change') {
						$$D('loaderCourse').style.display = "none";
					}

					//
					// - Clearing condition check
					//
					$scope.userClearingFlag = (response.appClearingBanner == "Y") ? "ON" : "OFF";
					$scope.appClearingurl = response.appClearingurl;
					
					// if (!isBlankOrNull(localStorage.getItem('DEFAULT_SHOW_CLEARING_SWITCH_FLAG'))) {
					// 	localStorage.setItem("USER_CLEARING_FLAG", "ON");
					// 	$scope.userClearingFlag = "ON";
					// } else {
					// 	localStorage.setItem("USER_CLEARING_FLAG", $scope.userClearingFlag);
					// }

					if (response.versionDetails.length > 0) {
						if (response.versionDetails[0].versionName > Constants.appVersion && sessionStorage.getItem('VERSION_INFORMED') != 'true') {
							sessionStorage.setItem('VERSION_INFORMED', 'true');
							localStorage.setItem('updateVersionMessage', response.versionDetails[0].message);
							localStorage.setItem('updateVersion', response.versionDetails[0].versionName);
							localStorage.setItem('updateVersionStaus', response.versionDetails[0].status);
							setTimeout(function () {
								mainView.router.loadPage('html/versioning/updateVersion.html');
							}, 500);
						}
					}

					$scope.currentYearOFEntry = response.currentYear;
					//

					//$scope.postcode = response.postcode;
					$scope.postcode = response.address[0].postcode;
					$scope.obselected = !isBlankOrNull(locationMode) ? locationMode : "CLOSE_TO_YOU";
					//
					$scope.otherCourseList = response.otherSubjects;
					$scope.recentCourseList = response.recentSubjects;
					$scope.userName = response.userName;
					$scope.subjects = Constants.subjects;
					$scope.tempOpenDayInstitutionsList = response.institutionsWithUpCommingOpendays;
					$scope.tempNearByInstitutionsList = response.institutionsCloseToYou;
					$scope.featuredProviderList = response.featuredProviderList; // Added for featured provider pod by Sangeeth.S
					//Below code for displaying random images
					angular.forEach($scope.tempOpenDayInstitutionsList, function (value, index) {
						if (isBlankOrNull($scope.tempOpenDayInstitutionsList[index].tileMediaPath)) {
							$scope.tempOpenDayInstitutionsList[index].tileMediaPath = UniRandomImage();
						}
					});
					//
					angular.forEach($scope.tempNearByInstitutionsList, function (value, index) {
						if (isBlankOrNull($scope.tempNearByInstitutionsList[index].tileMediaPath)) {
							$scope.tempNearByInstitutionsList[index].tileMediaPath = UniRandomImage();
						}
					});
					//		
					$scope.openDayInstitutionsList = $scope.tempOpenDayInstitutionsList;
					$scope.nearByInstitutionsList = $scope.tempNearByInstitutionsList;
					$scope.institutionBrowseLogo = Constants.institutionBrowseLogo;

					//
					$scope.featuredUniversity = response.featuredUniversity;
					$scope.featuredCourse = response.featuredCourse;
					$scope.featuredOpenDay = response.featuredOpenDay;


					//Latest News
					$scope.articleFlag = response.articleFlag;
					$scope.articleContent = response.articleContent;
					
					setTimeout(function () {
						$('#close_to_you_tick').hide();
						$('#you_home_tick').hide();
						$('#you_home_tick').parent('li').removeClass('op5');
						$('#close_to_you_tick').parent('li').removeClass('op5');
						if (isBlankOrNull(locationMode) || locationMode == 'CLOSE_TO_YOU') {
							$('#locationModeText').html('Close to you');
							$('#close_to_you_tick').show();
							$('#you_home_tick').parent('li').addClass('op5');
							if (isBlankOrNull(response.address[0].postcode)) {
								$scope.postcode = null;
							} else {
								$scope.postcode = response.address[0].postcode;

								$("#address_display").html(response.address[0].addressLine1 + " " + ((response.address[0].addressLine2 != null) ? response.address[0].addressLine2 : "") + "<br>" + $scope.postcode);
								//$("#address_display").html($scope.postcode);
							}
						} else {
							$('#locationModeText').html('Your Home');
							$('#you_home_tick').show();
							$('#close_to_you_tick').parent('li').addClass('op5');
						}
						$scope.locationPermissionDisabled = isLocationPermissionDisabled;
					}, 500)

					setTimeout(function () {

						galleryTop = new Swiper('#browseHomeCntrl .tabs-top', {
							nextButton: '.swiper-button-next',
							prevButton: '.swiper-button-prev',
							spaceBetween: 0,
							autoHeight: 'true',
							onSlideChangeStart: function (swiper) {
								//$('.gallery-thumbs  .swiper-wrapper').css('transform','translate3d(0px, 0px, 0px)');
								//alert("top Called");
								//scrolling_array[currentSlideCount]=$('#browseHomeCntrl .page-content').scrollTop();
								$(".tabs-top .swiper-wrapper").css('height', $("#browseCont .gallery_nav-" + swiper.activeIndex).height() + "px");

								$("#browseCont").css('height', $("#browseCont .gallery_nav-" + swiper.activeIndex).height() + "px");
							},
							onSlideChangeEnd: function (swiper) {
							}
						});


						galleryThumbs = new Swiper('#browseHomeCntrl .gallery-thumbs', {
							spaceBetween: 0,
							centeredSlides: true,
							slidesPerView: 'auto',
							touchRatio: 0.5,
							slideToClickedSlide: true,
							onSlideChangeStart: function (swiper) {
								var sindex = 0;
								if (swiper.activeIndex == 1) {
									sindex = 0;
								} else {
									sindex = 1;
								}
								scrolling_array[sindex] = $('#browseHomeCntrl .page-content').scrollTop();
								$(".tabs-top .swiper-wrapper").css('height', $("#browseCont .gallery_nav-" + swiper.activeIndex).height() + "px");
								$("#browseCont").css('height', $("#browseCont .gallery_nav-" + swiper.activeIndex).height() + "px");
								//$('.gallery-thumbs  .swiper-wrapper').css('transform','translate3d(0px, 0px, 0px)');
								$(".gallery-thumbs .swiper-slide").removeClass('active');
								$(".gallery-thumbs .thumbs-" + swiper.activeIndex).addClass('active');
								setTimeout(function () {
									if (scrolling_array[swiper.activeIndex] != undefined) {
										$$('#browseHomeCntrl .page-content').scrollTop(scrolling_array[swiper.activeIndex], 0);
									} else {
										$$('#browseHomeCntrl .page-content').scrollTop(0, 0);
									}
								}, 0)
								currentSlideCount = swiper.activeIndex;
							},
							onSlideChangeEnd: function (swiper) {
							}
						});

						galleryTop.params.control = galleryThumbs;
						galleryThumbs.params.control = galleryTop;
						$(".tabs-top .swiper-wrapper").css('height', $("#browseCont .gallery_nav-" + currentSlideCount).height() + "px");
						$("#browseCont").css('height', $("#browseCont .gallery_nav-" + currentSlideCount).height() + "px");


						$('.gallery-thumbs  .swiper-wrapper').css('transform', 'translate3d(0px, 0px, 0px)');
						galleryThumbs.slideTo(currentSlideCount);

						if ($('.page-on-center').attr('data-page') == 'browse') {
							$scope.closeSearchPopup();
						}

						//$scope.closeSearchPopup();
					}, 500)

					var page = $$('.page[data-page="browse"]')[0].f7PageData;
					if (Object.keys(page.query).length !== 0 && pullFlag == '') {
						setTimeout(function () {
							if (page.query.action == "shortlist") {
								galleryThumbs.slideTo(1);
								if (!$("#" + page.query.action_id).hasClass('fav_fill'))
									$("#" + page.query.action_id).trigger('click');
							} else if (page.query.action == "addAddress") {
								galleryThumbs.slideTo(1);
								$("#locationModeText").trigger('click');
								$("#" + page.query.action_id).trigger('click');
							} else {
								$("#" + page.query.action_id).trigger('click');
							}
						}, 1000);
					}

				}, function (httpresponse, status) {
					var response = httpresponse.data;
					console.log("response");
					console.log(response);
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					//window.location.replace('../../html/exception/exception.html');	 			
					mainView.router.loadPage('html/exception/exception.html');
				});
				$scope.searchResultsPageURL = function (subjectName, categoryCode, categoryId) {
					//googleTagManagerEventTrack(null, 'Tile', 'Courses', subjectName, '0');

					firebaseEventTrack('viewitem', { origin: 'tile', item_category: "not set", item_name: subjectName.toLowerCase(), item_id: "not set", search_term: "na" });

					localStorage.setItem('subject', subjectName);
					localStorage.setItem('categoryCode', categoryCode);
					localStorage.setItem('categoryId', categoryId);
					localStorage.setItem('FROM_PAGE', 'BROWSE_HOME');
					//window.location.href = '../../html/search/searchResults.html';
					mainView.router.loadPage('html/search/searchResults.html');
				}
			};
			$scope.getBrowseList('');
			/*
			$scope.locationModeChange = function(pullFlag) {
				console.log(">>>>> locationModeChange");
				if(!isBlankOrNull(localStorage.getItem('latitude')) && !isBlankOrNull(localStorage.getItem('longitude'))) {
					latitude = localStorage.getItem('latitude');
					longtitude = localStorage.getItem('longitude');
					localStorage.removeItem("latitude");
					localStorage.removeItem("longitude");
				}
				var locationMode = ($('#locationMode') && !isBlankOrNull($('#locationMode').val()) && !(pullFlag == 'pullFlag') ) ? $('#locationMode').val() : "CLOSE_TO_YOU";
				var jsonData = JSON.stringify(getJson());
				
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/home-browse/',
					headers: {'Content-Type': 'application/json'},
					data: jsonData
				}	
				$http(req).then(function(httpresponse) {
					var response = httpresponse.data;
					console.log('postcode check > '+response.postcode);
					
					//
					$scope.tempNearByInstitutionsList = response.institutionsCloseToYou;
	
					//
					angular.forEach($scope.tempNearByInstitutionsList, function (value, index) {					
						if(isBlankOrNull($scope.tempNearByInstitutionsList[index].tileMediaPath)){
							$scope.tempNearByInstitutionsList[index].tileMediaPath = UniRandomImage();
						}
					});
					//		
					$scope.nearByInstitutionsList = $scope.tempNearByInstitutionsList;
				},function(httpresponse, status) {
					var response = httpresponse.data;
				});
			};
			*/
			$scope.addYourAddressPopup = function () {
				if (isGuestUser()) {
					localStorage.setItem('action', "addAddress");
					localStorage.setItem('action_id', "addYourAddressPopup");
					Constants.mainViewObj.router.loadPage({ url: "html/user/home.html", pushState: false });
				} else {
					myApp.popup('#searchAddress');
				}

			}
			$scope.goBackToSearchAddr = function (id) {
				myApp.popup('#searchAddress');
				myApp.closeModal('#searchAddressManual');
			}
			//
			$scope.backToEditPage = function () {
				myApp.closeModal('.location_add_address');
				if ($$D('addressAjax')) {
					$$D('addressAjax').value = '';
					$$D('xmarkAddress').className = '';
					$('#ajaxList').css('display', 'none');
				}
			}
			//
			$scope.validatePostcode = function () {
				if ($$D('addressAjax')) {
					if ($$D('addressAjax').value.length > 0) {
						$$D('xmarkAddress').className = 'searchbar-clear';
						$('#ajaxList').css('display', 'none');
						var postcode = $$D('addressAjax').value;
						postcode = checkPostCode(postcode);
						if (checkPostCode(postcode)) {
							$$D('editAddrLoader').style.display = 'block';
							var stringJson = {
								"postCode": postcode.toUpperCase(),
								"appVersion": Constants.appVersion,
								"affiliateId": "220703",
								"accessToken": "246"
							};
							var jsonData = JSON.stringify(stringJson);
							var url = "https://mtest.whatuni.com/wuappws/get-ajax-address/";
							var req = {
								method: 'POST',
								url: url,
								headers: { 'Content-Type': 'application/json' },
								data: jsonData
							}
							$http(req).then(function (httpresponse) {
								var response = httpresponse.data;
								$scope.ajaxAddressList = response.ajaxAddressList;
								if (response.ajaxAddressList != null && response.ajaxAddressList.length > 0) {
									setTimeout(function () {
										$('#ajaxList').css('display', 'block');
										$$D('editAddrLoader').style.display = 'none';
									}, 300);
								}
							}, function (httpresponse, status) {
								var response = httpresponse.data;
								localStorage.setItem('exception', response.exception);
								localStorage.setItem('status', response.status);
								localStorage.setItem('message', response.message);
								mainView.router.loadPage('html/exception/exception.html');
							});
						}
					} else {
						$$D('xmarkAddress').className = '';
						$('#ajaxList').css('display', 'none');
					}
				}
			}
			//
			$scope.getAddressList = function (index) {
				if ($$D('address-' + index)) {
					var address = $$D('address-' + index).value.split("###");
					var length = address.length;
					var postcode = address[length - 1];
					$scope.postcode = postcode;
					//var countyState = address[length - 2] ;
					var city = address[length - 2];
					var addressLineTwo = address[length - 3];
					var addressLineOne = address[length - 4];

					$("#userAddressOne").val(addressLineOne);
					$("#userAddressTwo").val(addressLineTwo);
					$("#userCityHidden").val(city);
					$("#userPostcodeHidden").val(postcode);
					setTimeout(function () {
						$("#address_display").html(addressLineOne + " " + addressLineTwo + "<br>" + postcode);
					}, 100);
					$scope.saveUserDetails("ajax");

				}
			}
			//
			$scope.saveUserAddress = function (buildingName, addressLine, city, postcode) {
				if ($$D(buildingName) && $$D(addressLine) && $$D(city) && $$D(postcode)) {
					if ($('#saveButtonLP').hasClass('btn_blu')) {
						var buildingName = $$D(buildingName).value.length > 0 ? $$D(buildingName).value : '';
						var addressLine = $$D(addressLine).value.length > 0 ? $$D(addressLine).value : '';
						var city = $$D(city).value.length > 0 ? $$D(city).value : '';
						var postcode = $$D(postcode).value.length > 0 ? $$D(postcode).value : '';
						$scope.postcode = postcode;
						$("#userAddress").html(buildingName + " " + addressLine);

						$("#userAddressOne").val(buildingName);
						$("#userAddressTwo").val(addressLine);
						$("#userCityHidden").val(city);
						$("#userPostcodeHidden").val(postcode);
						setTimeout(function () {
							$("#address_display").html(buildingName + " " + addressLine + "<br>" + postcode);
						}, 100);
						$scope.saveUserDetails("manually");
					}
				}
			}


			$(".bdy_ovrly").click(function () {
				if ($("#close_to_you_tick").css('display') != "none" || $("#you_home_tick").css('display') != "none") {
					myApp.closeModal('.add_address');
				}
			});

			//
			$scope.location_popup = function () {
				myApp.pickerModal('.add_address');
			}
			//
			$scope.location_close_popup = function (mode, e) {
				e.stopPropagation();
				if (mode != "Edit") {
					$("#locationMode").val(mode);
					if ((mode == "CLOSE_TO_YOU" && $("#close_to_you_tick").css('display') == "none") || (mode == "YOUR_HOME" && $("#you_home_tick").css('display') == "none")) {
						$scope.getBrowseList('location_change');
					}
					myApp.closeModal('.add_address');
				}
				if (mode == "Edit") {
					myApp.popup('.location_add_address');
				}
			}
			//
			$(".add_address").on("open", function () {
				$(".bdy_ovrly").addClass("add_view")
			});
			//
			$(".add_address").on("close", function () {
				$(".bdy_ovrly").removeClass("add_view")
			});
			//
			$(".location_add_address").on("open", function () {
				setTimeout(function () {
					$$D('addressAjax').focus();
				}, 500);
			});
			//
			$(".location_add_address").on("close", function () {
				$('#addressAjax').val('');
				$('#xmarkAddress').removeClass('srclear').addClass('');
				$('#ajaxList').css('display', 'none');
				$$D('xmarkAddress').className = '';
			});
			//
			$(".search_land").on("open", function () {
				setTimeout(function () {
					$$D('buildingNameLP').focus();
				}, 500)
			});
			//
			$(".search_land").on("close", function () {
				$('#buildingNameLP').val('');
				$('#addressLineLP').val('');
				$('#cityLP').val('');
				$('#postCodeLP').val('');
				$('#buildingName_clsLP').removeClass("hd_cls");
				$('#addressLine_clsLP').removeClass("hd_cls");
				$('#city_clsLP').removeClass("hd_cls");
				$('#postCode_clsLP').removeClass("hd_cls");
				$('#saveButtonLP').removeClass("btn_blu");
				$(".search_address .pr_dtl").scrollTop(0, 0);
			});
			//
			$scope.validateUserAddress = function () {
				console.log("ng blur called");
				if ($$D('buildingName_clsLP') && $$D('addressLine_clsLP') && $$D('city_clsLP') && $$D('postCode_clsLP')) {
					console.log("ng blur 2");
					if ($('#buildingName_clsLP').hasClass('hd_cls') && $('#city_clsLP').hasClass('hd_cls') && $('#postCode_clsLP').hasClass('hd_cls')) {
						$('#saveButtonLP').addClass('btn_blu');
					} else {
						$('#saveButtonLP').removeClass('btn_blu');
					}
				}
			}
			//
			$scope.addXmark = function (id, xmarkId) {
				if ($$D(id) && $$D(xmarkId)) {
					if ($$D(id).value) {
						$$D(xmarkId).className = 'hd_cls';
						var count = $('.hd_cls')
						if (count.length == 4) {
							$('#saveButtonLP').addClass('btn_blu');
						} else {
							if (count.length == 3) {
								if ($$D('addressLine_clsLP') && isBlankOrNull($$D('addressLine_clsLP').className)) {
									$('#saveButtonLP').addClass('btn_blu');
								}
							} else {
								$('#saveButtonLP').removeClass('btn_blu');
							}
						}
					} else {
						$$D(xmarkId).className = '';
						$('#saveButtonLP').removeClass('btn_blu');
					}
				}
			}
			//
			$scope.clearField = function (id, xmarkId) {
				if ($$D(id)) {
					$$D(id).value = '';
					$$D(xmarkId).className = '';
					$('#saveButtonLP').removeClass('btn_blu');
				}

			}

			//
			$scope.saveUserDetails = function (mode) {
				var firstName = localStorage.getItem('LOGGED_IN_USER_FIRST_NAME');
				var lastName = localStorage.getItem('LOGGED_IN_USER_LAST_NAME');
				var email = "";
				var dateOfBirth = "";
				var nationality = "";
				var studyLevel = "";
				var startDate = "";
				var userSchoolName = "";
				var userSchoolId = "";
				var gradeText = "";
				var addressLineOne = !isBlankOrNull($("#userAddressOne").val()) ? $("#userAddressOne").val() : "";
				var addressLineTwo = !isBlankOrNull($("#userAddressTwo").val()) ? $("#userAddressTwo").val() : "";
				var city = !isBlankOrNull($('#userCityHidden').val()) ? $('#userCityHidden').val() : "";
				var postCode = !isBlankOrNull($('#userPostcodeHidden').val()) ? $('#userPostcodeHidden').val() : "";
				var countyState = "";
				var password = "";
				var preQualType = "";
				var nonUkSchool = "Y";

				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"firstName": firstName,
					"lastName": lastName,
					"email": email,
					"password": password,
					"dateOfBirth": dateOfBirth,
					"nationality": nationality,
					"addressLineOne": addressLineOne,
					"addressLineTwo": addressLineTwo,
					"city": city,
					"countyState": countyState,
					"postCode": postCode,
					"yearOfEntry": startDate,
					"studyLevel": studyLevel,
					"school": userSchoolName,
					"nonUkSchool": nonUkSchool,
					"preQualType": preQualType,
					"preQualGrade": gradeText,
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/profile/save-user-profile/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$("[id='editAddrLoader']").show();
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					var status = response.successMsg;
					console.log(response);

					$("[id='editAddrLoader']").hide();
					$("#you_home_tick").hide();
					$('#you_home_tick').parent('li').addClass('op5');
					//
					myApp.closeModal('.location_add_address');
					myApp.pickerModal('.add_address');
					if (mode == "manually") {
						setTimeout(function () {
							myApp.closeModal('.search_land');
						}, 200)
					}
					//
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});

			}
			//
			$scope.clearSearch = function () {
				if ($$D('addressAjax')) {
					$$D('addressAjax').value = '';
					$$D('xmarkAddress').className = '';
					$('#ajaxList').css('display', 'none');
				}
			}

			//
			var ptrContent = $$('#browseHomeCntrl .pull-to-refresh-content');
			ptrContent.on('refresh', function (e) {
				// Emulate 2s loading
				setTimeout(function () {
					browseScopeObject.getBrowseList('pullFlag');
					myApp.pullToRefreshDone();
				}, 3000);
				//
			});
			$scope.closeSearchPopup = function (flag) {
				if (flag == "NO" || localStorage.getItem("SHOW_SEARCH_POPUP") == "NO") {
					localStorage.setItem("SHOW_SEARCH_POPUP", "NO");
					$scope.showSearchPopup = "NO";
					$('#searchPopup').hide();
				} else {
					$scope.showSearchPopup = "YES";
					$('#searchPopup').show();
					console.log("brower-id", $("#browseCont .swiper-slide-active").attr('tabindex'));
					var activeTabIndex = $("#browseCont .swiper-slide-active").attr('tabindex')
					$("#browseCont").css('height', $("#browseCont .gallery_nav-" + activeTabIndex).height() + "px");
				}
			}

			$scope.saleSortInstitutionPageURL = function ($event) {
				var university_id = document.getElementById($event.currentTarget.id).getAttribute('university_id');
				var university_name = document.getElementById($event.currentTarget.id).getAttribute('university_name');
				console.log(university_id);
				console.log(university_name);
				if (!isBlankOrNull(university_name) && !isBlankOrNull(university_id)) {
					localStorage.setItem('universityId', university_id);
					localStorage.setItem('universityName', university_name);
				}
				firebaseEventTrack('viewitem', { origin: 'featured uni', item_category: university_name.toLowerCase(), item_name: "not set", item_id: university_id, search_term: "na" });

				Constants.mainViewObj.router.loadPage('html/institution/ins_profile.html');
			}
			$scope.saleSortCoursePageURL = function ($event) {
				var university_id = document.getElementById($event.currentTarget.id).getAttribute('university_id');
				var course_id = document.getElementById($event.currentTarget.id).getAttribute('course_id');
				var university_name = document.getElementById($event.currentTarget.id).getAttribute('university_name');
				console.log(course_id);
				console.log(university_id);
				console.log(university_name);
				if (!isBlankOrNull(university_name) && !isBlankOrNull(university_id)) {
					localStorage.setItem('collegeId', university_id);
					localStorage.setItem('courseId', course_id);
					//localStorage.setItem('universityName', university_name);			
				}
				firebaseEventTrack('viewitem', { origin: 'featured course', item_category: university_name.toLowerCase(), item_name: "not set", item_id: university_id, search_term: "na" });
				Constants.mainViewObj.router.loadPage('html/coursedetails/courseDetails.html');
			}

			$scope.saleSortOpendayPageURL = function ($event) {
				var university_id = document.getElementById($event.currentTarget.id).getAttribute('university_id');
				var university_name = document.getElementById($event.currentTarget.id).getAttribute('university_name');
				console.log(university_id);
				console.log(university_name);
				if (!isBlankOrNull(university_name) && !isBlankOrNull(university_id)) {
					localStorage.setItem('universityId', university_id);
					localStorage.setItem('universityName', university_name);
				}
				firebaseEventTrack('viewitem', { origin: 'featured open day', item_category: university_name.toLowerCase(), item_name: "not set", item_id: university_id, search_term: "na" });

				Constants.mainViewObj.router.loadPage('html/institution/ins_profile.html');
			}

			$scope.institutionPageURL = function ($event, page) {
				console.log($event);
				console.log($event.currentTarget.id);
				var university_id = document.getElementById($event.currentTarget.id).getAttribute('university_id');
				var university_name = document.getElementById($event.currentTarget.id).getAttribute('university_name');
				console.log(university_id);
				console.log(university_name);
				var eventCategory = 'Tile';
				if (page == 'SL') {
					eventCategory = 'Search';
				}
				//googleTagManagerEventTrack(null, 'Search', 'University', university_name, '0');
				firebaseEventTrack('viewitem', { origin: 'tile', item_category: university_name.toLowerCase(), item_name: "not set", item_id: university_id, search_term: "na" });
				var tile_media_path = document.getElementById($event.currentTarget.id).getAttribute('tile_media_path');
				if (!isBlankOrNull(university_name) && !isBlankOrNull(university_id)) {
					localStorage.setItem('universityId', university_id);
					localStorage.setItem('universityName', university_name);
				}
				if (!isBlankOrNull(tile_media_path)) {
					localStorage.setItem('tileMediaPath', tile_media_path);
				} else {
					localStorage.setItem('tileMediaPath', "");
				}
				Constants.mainViewObj.router.loadPage('html/institution/ins_profile.html');
			}
			$scope.getSafeHtml = function (x) {
				return $sce.trustAsHtml(x);
			};

		})

		//Below controller for search landing
		.controller("searchLandingCtrl", function ($scope, $http, $compile) {
			//
			var currentSlideCount = 0;

			var userID = localStorage.getItem('LOGGED_IN_USER_ID');
			firebaseEventTrack('screenview', { 'page_name': '/search-landing' });
			$scope.getSearchFormLists = function () {
				//$$D('searchAjax').focus();
				//$$D('searchUniTab').className = 'button tab-link';
				//$$D('searchCourseTab').className = 'button tab-link active';
				$$D('searchCont').style.display = "none";
				$$D('loaderSearch').style.display = "block";
				$scope.dynamicFlag;
				if (localStorage.getItem('latitude') != null && localStorage.getItem('longitude') != null) {
					latitude = localStorage.getItem('latitude');
					longtitude = localStorage.getItem('longitude');
					localStorage.removeItem("latitude");
					localStorage.removeItem("longitude");
				}
				var inputJson = {
					"latitude": latitude,
					"longitude": longtitude,
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"accessToken": "246",
					"affiliateId": "220703",
					"qualificationCode": "M",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(inputJson);
				//
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/search-form/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$scope.guestUserFlag = response.guestUserFlag;
					if (response.versionDetails.length > 0) {
						var exceptionTemplateURL = "html/versioning/updateVersion.html";
						checkVersionDetails(response.versionDetails[0].versionName, response.versionDetails[0].status, response.versionDetails[0].message, exceptionTemplateURL);
					}
					$scope.recentCourseList = response.recentSubjects;
					$scope.nearByCourseList = response.nearSubjects;
					$scope.nearByInstitutionsList = response.recentInstitutions;
					$scope.closToYouInstitutionsList = response.institutionsCloseToYou;
					$scope.dynamicFlag = true;
					$scope.ajaxFlag;
					//$scope.noResultCourses=false;
					//$scope.noResultUnis=false;


					$$D('dynamic-courses').style.display = "block";
					$$D('dynamic-unis').style.display = "block";
					$$D('ajax-courses').style.display = "none";
					$$D('ajax-unis').style.display = "none";
					$$D('searchCont').style.display = "flex";
					$$D('loaderSearch').style.display = "none";

					//$$D('noResultCourses').style.display = "none";
					//$$D('noResultUnis').style.display = "none";



					/*if(($scope.recentCourseList == null || $scope.recentCourseList.length==0) && ($scope.nearByCourseList == null || $scope.nearByCourseList.length==0)) {
						$$D('noResultCourses').style.display = "block";
						$scope.noResultCourses=true;
					}
					
					if(($scope.nearByInstitutionsList == null || $scope.nearByInstitutionsList.length==0) && ($scope.closToYouInstitutionsList == null || $scope.closToYouInstitutionsList.length==0)) {
						$$D('noResultUnis').style.display = "block";
						$scope.noResultUnis=true;
					}*/
					console.log('dynamic list');

					$scope.swiperSlide($scope.noResultCourses, $scope.noResultUnis);
					var page = $$('.page[data-page="search-landing"]')[0].f7PageData;
					if (Object.keys(page.query).length !== 0) {
						setTimeout(function () {
							$("#" + page.query.action_id).trigger('click');
						}, 1000);
					}


				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					//window.location.replace('../../html/exception/exception.html');	 			
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			$scope.swiperSlide = function (noResultCourses, noResultUnis) {
				var galleryTop = null;
				var galleryThumbs = null;
				setTimeout(function () {
					console.log('thumbs check');
					galleryTop = new Swiper('.tabs-top-1', {
						nextButton: '.swiper-button-next',
						prevButton: '.swiper-button-prev',
						spaceBetween: 0,
						autoHeight: 'true',
						onSlideChangeStart: function (swiper) {
							$('.gallery-thumbs-1  .swiper-wrapper').css('transform', 'translate3d(0px, 0px, 0px)');
						},
						onSlideChangeEnd: function (swiper) {
						}
					});

					var scrolling_array = [];
					galleryThumbs = new Swiper('.gallery-thumbs-1', {
						spaceBetween: 0,
						centeredSlides: true,
						slidesPerView: 'auto',
						touchRatio: 0.5,
						slideToClickedSlide: true,
						onSlideChangeStart: function (swiper) {
							scrolling_array[currentSlideCount] = $('#searchForm .page-content').scrollTop();
							console.log('swiper.activeIndex > ' + swiper.activeIndex);
							$(".tabs-top-1 .swiper-wrapper").css('height', $("#searchCont .gallery_nav-" + swiper.activeIndex).height() + "px");
							$('.gallery-thumbs-1 .swiper-wrapper').css('transform', 'translate3d(0px, 0px, 0px)');
							$(".gallery-thumbs-1 .swiper-slide").removeClass('active');
							$(".gallery-thumbs-1 .swiper-slide").removeClass('current');
							$(".gallery-thumbs-1 .thumbs-" + swiper.activeIndex).addClass('active');
							$(".gallery-thumbs-1 .thumbs-" + swiper.activeIndex).addClass('current');
							setTimeout(function () {
								if (scrolling_array[swiper.activeIndex] != undefined) {
									$$('#searchForm .page-content').scrollTop(scrolling_array[swiper.activeIndex], 0);
								} else {
									$$('#searchForm .page-content').scrollTop(0, 0);
								}
							}, 0)
							currentSlideCount = swiper.activeIndex;
						},
						onSlideChangeEnd: function (swiper) {
						}
					});
					galleryTop.params.control = galleryThumbs;
					galleryThumbs.params.control = galleryTop;


					$('.gallery-thumbs-1 .swiper-wrapper').css('transform', 'translate3d(0px, 0px, 0px)');
					galleryThumbs.slideTo(currentSlideCount);
				}, 300)
			}
			//
			$scope.getSearchFormLists();
			//
			$scope.getSearchAjaxLists = function () {
				
				$scope.ajaxFlag;
				var searchKeyword = localStorage.getItem('searchKeyword');
						
				var inputJson = {
					"latitude": latitude,
					"longitude": longtitude,
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"searchKeyword": searchKeyword,
					"qualificationCode": "M",
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(inputJson);
				//
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/ajax-search/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$scope.guestUserFlag = response.guestUserFlag;
					if (response.versionDetails.length > 0) {
						var exceptionTemplateURL = "html/versioning/updateVersion.html";
						checkVersionDetails(response.versionDetails[0].versionName, response.versionDetails[0].status, response.versionDetails[0].message, exceptionTemplateURL);
					}
					$scope.ajaxRecentCourseList = response.recentSubjects;
					$scope.ajaxSuggestedCourseList = response.suggestedSubjects;
					$scope.ajaxRecentInstitutionsList = response.recentInstitutions;
					$scope.ajaxSuggestedInstitutionsList = response.suggestedInstitutions;
					$scope.ajaxFlag = true;
					$scope.dynamicFlag;
					if (isSearchAjaxNotNull()) {
						$$D('ajax-courses').style.display = "block";
						$$D('ajax-unis').style.display = "block";
						$$D('dynamic-courses').style.display = "none";
						$$D('dynamic-unis').style.display = "none";
					} else {
						$$D('ajax-courses').style.display = "none";
						$$D('ajax-unis').style.display = "none";
						$$D('dynamic-courses').style.display = "block";
						$$D('dynamic-unis').style.display = "block";
					}
					$$D('searchCont').style.display = "flex";
					$$D('loaderSearch').style.display = "none";

					console.log('ajax list');
					$scope.swiperSlide($scope.noResultCourses, $scope.noResultUnis);
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					//window.location.replace('../../html/exception/exception.html');	 			
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//
			$scope.searchResultsPageURL = function (subjectName, categoryCode, categoryId, nearYouFlag) {
				localStorage.setItem('subject', subjectName);
				localStorage.setItem('categoryCode', categoryCode);
				localStorage.setItem('categoryId', categoryId);
				localStorage.setItem('latitude', latitude);
				localStorage.setItem('longitude', longtitude);
				localStorage.setItem('nearYouFlag', nearYouFlag);
				localStorage.setItem('FROM_PAGE', 'SEARCH_LANDING');
				//googleTagManagerEventTrack(null, 'Search', 'Courses', subjectName, '0');

				firebaseEventTrack('viewitem', { origin: 'top nav', item_category: "not set", item_name: subjectName.toLowerCase(), item_id: "not set", search_term: "na" });

				//window.location.href = '../../html/search/searchResults.html';
				mainView.router.loadPage('html/search/searchResults.html');
			}
			//
			$scope.goBack = function () {
				//window.history.back();
				mainView.router.back({ 'pushState': false });
			}
			//
			$scope.clearSearch = function () {
				if ($$D('searchAjax')) {
					$$D('searchAjax').value = "";
					$$D('xmark').className = '';
					//$scope.getSearchFormLists();
					$$D('searchCont').style.display = "none";
					$$D('loaderSearch').style.display = "block";
					$$D('dynamic-courses').style.display = "block";
					$$D('dynamic-unis').style.display = "block";
					$$D('ajax-courses').style.display = "none";
					$$D('ajax-unis').style.display = "none";
					$$D('searchCont').style.display = "flex";
					$$D('loaderSearch').style.display = "none";
				}
			}
			//
			$scope.searchAjaxAutoComplete = function (keyCode) {
				$$D('searchCont').style.display = "none";
				$$D('loaderSearch').style.display = "block";
				if ($$D('searchAjax')) {
					if (($$D('searchAjax').value).trim().length > 0) {
						$$D('xmark').className = 'srclear';
						if (keyCode == 13 && $$D('searchUniTab').className.indexOf('current') < 0) {
							//googleTagManagerEventTrack(null, 'Search', 'Courses', $$D('searchAjax').value, '0'); 
							firebaseEventTrack('viewitem', { origin: 'top nav', item_category: "not set", item_name: "not set", item_id: "not set", search_term: $$D('searchAjax').value });
							$scope.searchResultsPageKeywordSearchURL($$D('searchAjax').value);
						}
					} else {
						$$D('xmark').className = '';
					}
					if (!isSearchAjaxNotNull()) {
						/*
						if($$D('searchCourseTab').className == 'button tab-link active' && $$D('searchUniTab').className == 'button tab-link'){
							window.location.href = '#ajax-search-course';
						}else if($$D('searchUniTab').className == 'button tab-link active' && $$D('searchCourseTab').className == 'button tab-link'){
							window.location.href = '#ajax-search-university';
						}
						*/
						$$D('dynamic-courses').style.display = "block";
						$$D('dynamic-unis').style.display = "block";
						$$D('ajax-courses').style.display = "none";
						$$D('ajax-unis').style.display = "none";
						$$D('searchCont').style.display = "flex";
						$$D('loaderSearch').style.display = "none";
						//$$D('noResultCourses').style.display = "none";
						//$$D('noResultUnis').style.display = "none";
						//$('#noresultbg').removeClass('nr_bg');
						//$scope.getSearchFormLists();
						console.log('search - dynamic list');
					} else if (isSearchAjaxNotNull()) {
						localStorage.setItem('searchKeyword', $$D('searchAjax').value);
						/*
						if($$D('searchCourseTab').className == 'button tab-link active' && $$D('searchUniTab').className == 'button tab-link'){
							window.location.href = '#search-course';
						}else if($$D('searchUniTab').className == 'button tab-link active' && $$D('searchCourseTab').className == 'button tab-link'){
							window.location.href = '#search-university';
						}
						*/
						$scope.getSearchAjaxLists();
						//console.log('search - ajax list');

					}
				}
			}
			$scope.searchResultsPageKeywordSearchURL = function (subjectName) {
				localStorage.setItem('subject', convertToSeoFrientlyText(subjectName));
				localStorage.removeItem('categoryId');
				localStorage.removeItem('categoryCode');
				localStorage.setItem('FROM_PAGE', 'SEARCH_LANDING');
				//window.location.href = '../../html/search/searchResults.html';
				mainView.router.loadPage('html/search/searchResults.html');
			}
		})

		.controller("SearchRootCtrl", ["$scope", "$http", "$rootScope", function ($scope, $http, $rootScope, DataService) {

			scopeObject = $scope;
			$scope.title = "Framework7(a)";
			//
			$scope.openPercentagePopUp = function () {
				statusBarFontColor('black');
				myApp.popup('.match-popup');
			}
			//statusBarFontColor('black');
			//

			var userID = localStorage.getItem('LOGGED_IN_USER_ID');
			firebaseEventTrack('screenview', { 'page_name': '/search-results' });
			//
			var xtremepushDateFormat = xtremePushCurrentDate();
			xtremePushTag("User_Has_Received_A_SR_Page", xtremepushDateFormat);

			$scope.searchResultsDetails = function (fromPage, toPage, filterSearchFlag) {
				var listType = fromPage;
				if (fromPage == 'dynamicList' && (parseInt($$D("totalRecordsLoaded").value) < parseInt($$D("totalUniversityCount").value))) {
					var totalUniCourseCnt = parseInt($$D("totalUniversityCount").value);
					var nextSet = parseInt($$D("nextSetResults").value);
					if (nextSet >= 6) { fromPage = nextSet + 1; toPage = nextSet + 10; $$D("nextSetResults").value = nextSet + 10; }
					if (parseInt($$D("nextSetResults").value) >= totalUniCourseCnt) { $$D("viewMoreResults").style.display = "none"; }
				}
				//	
				var jsonFilterFlag = "";
				var categoryId = ""; var categoryCode = ""; var subjectName = ""; var orderBy = ""; var quizFlag = "";
				if (!isBlankOrNull(localStorage.getItem("recentSearchCriteria")) && filterSearchFlag != "Filter") {
					if (isThisQuizToSRFlag && filterSearchFlag != 'pull') { quizFlag = "QUIZ"; }
					jsonFilterFlag = "TRUE";
					var json = JSON.parse(localStorage.getItem("recentSearchCriteria"));
					$$D("selectedCountryValue").value = json.region;
					$$D("selectedLocationFlag").value = json.regionFlag;
					$$D("locationTypeIdSelected").value = json.locationType;
					$$D("locationTypeNameSelected").innerHTML = json.locationTypeName;
					$$D("previousQualSelected").value = json.previousQual;
					$$D("previousQualGradesSelected").value = json.previousQualGrade;
					$$D("previousQualSel").innerHTML = json.previousQualGradeName;
					$$D("categoryIdSelected").value = json.searchCategoryId;
					$$D("categoryCodeSelected").value = json.searchCategoryCode;
					$("#subjectSelected").html(json.keywordSearch);
					$("#orderByKeyValue").val(json.orderBy);
					$("#selectedOrderByVal").html(json.selectedOrderByVal);
					$$D("qualificationCodeSelected").value = json.qualification;
					$$D("qualificationSelected").innerHTML = json.qualificationName;
					$$D("assessmentTypeSelected").value = json.assessmentType;
					$$D("assessmentTypeSel").innerHTML = json.assessmentTypeName;
					$$D("smTextKeySelected").value = json.studyMode;
					$$D("studyModeNameSelected").innerHTML = json.studyModeTextName;
					$$D("reviewCategorySelected").value = json.reviewCategory;
					$$D("reviewCategoryNameSelected").innerHTML = json.reviewCategoryName;
					$("#jacsCode").val(json.jacsCode);
				}
				var qualificationCode = "";
				var locationTypeId = "";
				var locationTypeName = "";
				var region = "";
				var regionFlag = "";
				var studyModeTextKey = "";
				var studyModeTextName = "";
				var previousQualification = "";
				var previousQualificationGrade = "";
				var previousQualificationGradeName = "";
				var assessmentType = "";
				var assessmentTypeName = "";
				var reviewCategory = "";
				var reviewCategoryName = "";
				var jacsCode = !isBlankOrNull($("#jacsCode").val()) ? $("#jacsCode").val() : "";
				var hideSRtitle = "";
				if (!isBlankOrNull(localStorage.getItem("recentSearchCriteria"))) {
					//$('.flip_dv').show();																
					if (!isBlankOrNull(locationTypeId) || !isBlankOrNull(region) || !isBlankOrNull(regionFlag) || !isBlankOrNull(studyModeTextKey) || !isBlankOrNull(previousQualification) || !isBlankOrNull(previousQualificationGrade)) {
						hideSRtitle = 'hide';
					}
				} else {
					$('.flip_dv').hide();
				}
				var selectedCountryValue = !isBlankOrNull($$D("selectedCountryValue").value) ? $$D("selectedCountryValue").value : "";
				$("#selectedLocation").html(selectedCountryValue);
				//
				// --- GTM event log for filters ---         
				//
				if (filterSearchFlag == "Filter") {
					var json = JSON.parse(localStorage.getItem("recentSearchCriteria"));
					if (json.qualificationName != "Not chosen") {
						if (!isBlankOrNull($('#qualificationSelected').html()) && $('#qualificationSelected').html() != json.qualificationName) {
							//googleTagManagerEventTrack(null, 'Search', 'Qualification', $('#qualificationSelected').html(), '0');
						}
						if (!isBlankOrNull($('#subjectSelected').html()) && $('#subjectSelected').html() != json.keywordSearch) {
							//googleTagManagerEventTrack(null, 'Search', 'Subject', $('#subjectSelected').html(), '0');
							xtremePushTag("Category_Code", json.searchCategoryCode);
						}
					}
					if (!isBlankOrNull($('#selectedLocation').html()) && $('#selectedLocation').html() != json.region) {
						//googleTagManagerEventTrack(null, 'Search', 'Country', $('#selectedLocation').html(), '0');
					}
					if (!isBlankOrNull($('#locationTypeNameSelected').html()) && $('#locationTypeNameSelected').html() != json.locationTypeName) {
						//googleTagManagerEventTrack(null, 'Search', 'Location Type', $('#locationTypeNameSelected').html(), '0');
					}
					if (!isBlankOrNull($('#studyModeNameSelected').html()) && $('#studyModeNameSelected').html() != json.studyModeTextName) {
						//googleTagManagerEventTrack(null, 'Search', 'Study Mode', $('#studyModeNameSelected').html(), '0');
					}
					if (!isBlankOrNull($('#previousQualSel').html()) && $('#previousQualSel').html() != json.previousQualGradeName) {
						//googleTagManagerEventTrack(null, 'Search', 'Previous Qualification', $('#previousQualSel').html(), '0');
					}
					if (!isBlankOrNull($('#assessmentTypeSel').html()) && $('#assessmentTypeSel').html() != json.assessmentTypeName) {
						//googleTagManagerEventTrack(null, 'Search', 'Assessment Type', $('#assessmentTypeSel').html(), '0');
					}
					if (!isBlankOrNull($('#reviewCategoryNameSelected').html()) && $('#reviewCategoryNameSelected').html() != json.reviewCategoryName) {
						//
						var reviewCategoryFilterDetails = $scope.getReviewCategoryFilterDetails();
						var selectedArray = !isBlankOrNull($$D("reviewCategoryNameSelected").innerHTML) ? $$D("reviewCategoryNameSelected").innerHTML.split(", ") : "";
						var tempReviewCategoryNameArray = [];
						angular.forEach(reviewCategoryFilterDetails, function (value, index) {
							if (selectedArray.indexOf(value.reviewCategoryDisplayName.trim()) >= 0) {
								tempReviewCategoryNameArray.push(value.reviewCategoryName);
							}
						});
						//
						//googleTagManagerEventTrack(null, 'Search', 'Your Preferences', tempReviewCategoryNameArray.join(", "), '0');
					}
				}
				//
				if (filterSearchFlag == "orderby") {
					var json = JSON.parse(localStorage.getItem("recentSearchCriteria"));
					if (!isBlankOrNull($('#selectedOrderByValueHidden').val()) && $('#selectedOrderByValueHidden').val() != json.orderBy) {
						//googleTagManagerEventTrack(null, 'Search', 'Order By', $('#selectedOrderByValueHidden').val(), '0');
					}
				}
				//
				region = !isBlankOrNull($("#selectedLocation").html()) ? $("#selectedLocation").html() : region;
				regionFlag = !isBlankOrNull($$D("selectedLocationFlag").value) ? $$D("selectedLocationFlag").value : regionFlag;
				if (filterSearchFlag == "Filter" || jsonFilterFlag == "TRUE") {
					$$D("oldCountryValue").value = !isBlankOrNull($("#selectedLocation").html()) ? $("#selectedLocation").html() : region;
					$$D("oldCountryFlagValue").value = regionFlag;
				}
				//
				locationTypeId = !isBlankOrNull($$D("locationTypeIdSelected").value) ? $$D("locationTypeIdSelected").value : locationTypeId;
				locationTypeName = !isBlankOrNull($$D("locationTypeNameSelected").innerHTML) ? $$D("locationTypeNameSelected").innerHTML : locationTypeName;
				if (filterSearchFlag == "Filter" || jsonFilterFlag == "TRUE") {
					$$D("oldLocationTypeNameValue").value = $$D("locationTypeNameSelected").innerHTML;
					$$D("oldLocationTypeIdValue").value = locationTypeId;
				}
				//
				studyModeTextKey = !isBlankOrNull($$D("smTextKeySelected").value) ? $$D("smTextKeySelected").value : studyModeTextKey;
				studyModeTextName = !isBlankOrNull($$D("studyModeNameSelected").innerHTML) ? $$D("studyModeNameSelected").innerHTML : studyModeTextName;
				if (filterSearchFlag == "Filter" || jsonFilterFlag == "TRUE") {
					$$D("oldStudyModeNameValue").value = $$D("studyModeNameSelected").innerHTML;
					$$D("oldsmTextKeyValue").value = studyModeTextKey;
				}
				//
				previousQualification = !isBlankOrNull($$D("previousQualSelected").value) ? $$D("previousQualSelected").value : previousQualification;
				if (filterSearchFlag == "Filter" || jsonFilterFlag == "TRUE") {
					$$D("oldPreviousQualValue").value = previousQualification;
				}
				//
				previousQualificationGrade = !isBlankOrNull($$D("previousQualGradesSelected").value) ? $$D("previousQualGradesSelected").value : previousQualificationGrade;
				previousQualificationGradeName = !isBlankOrNull($$D("previousQualSel").innerHTML) ? $$D("previousQualSel").innerHTML : previousQualificationGradeName;
				if (filterSearchFlag == "Filter" || jsonFilterFlag == "TRUE") {
					$$D("oldPreviousQualGradesValue").value = previousQualificationGrade;
					$$D("oldPreviousQualText").value = $$D("previousQualSel").innerHTML;
				}
				//
				assessmentType = !isBlankOrNull($$D("assessmentTypeSelected").value) ? $$D("assessmentTypeSelected").value : assessmentType;
				assessmentTypeName = !isBlankOrNull($$D("assessmentTypeSel").innerHTML) ? $$D("assessmentTypeSel").innerHTML : assessmentTypeName;
				if (filterSearchFlag == "Filter" || jsonFilterFlag == "TRUE") {
					$$D("oldAssessmentTypeSelected").value = assessmentType;
					$$D("oldAssessmentTypeNameSelected").value = $$D("assessmentTypeSel").innerHTML;
				}
				//
				reviewCategory = !isBlankOrNull($$D("reviewCategorySelected").value) ? $$D("reviewCategorySelected").value : reviewCategory;
				reviewCategoryName = !isBlankOrNull($$D("reviewCategoryNameSelected").innerHTML) ? $$D("reviewCategoryNameSelected").innerHTML : reviewCategoryName;
				if (filterSearchFlag == "Filter" || jsonFilterFlag == "TRUE") {
					$$D("oldReviewCategorySelected").value = reviewCategory;
					$$D("oldReviewCategoryNameSelected").value = $$D("reviewCategoryNameSelected").innerHTML;
				}
				//			
				if (!isBlankOrNull(localStorage.getItem('latitude')) && !isBlankOrNull(localStorage.getItem('longitude'))) {
					latitude = localStorage.getItem('latitude');
					longtitude = localStorage.getItem('longitude');
					localStorage.removeItem("latitude");
					localStorage.removeItem("longitude");
				}
				//
				if (filterSearchFlag == "Filter" || jsonFilterFlag == "TRUE") {
					categoryId = $$D("categoryIdSelected").value;
					categoryCode = $$D("categoryCodeSelected").value;
					$$D("oldCategoryIdSelected").value = categoryId;
					$$D("oldCategoryCodeSelected").value = categoryCode;
					$$D("oldCategoryText").value = $("#subjectSelected").html();
					subjectName = $("#subjectSelected").html();
					orderBy = !isBlankOrNull($("#orderByKeyValue").val()) ? $$D("orderByKeyValue").value : "";
					if (!(isBlankOrNull(region) && isBlankOrNull(locationTypeId) && isBlankOrNull(studyModeTextKey) && isBlankOrNull(previousQualification) && isBlankOrNull(assessmentType) && isBlankOrNull(reviewCategory))) {
						if ($$D("oldOrderByKeyValue").value == $$D("orderByKeyValue").value) {
							orderBy = "BEST_MATCH"; $("#selectedOrderByVal").html("Best match");
							console.log('orderby 1 > ' + orderBy);
						}
					} else {
						orderBy = !isBlankOrNull($("#selectedOrderByValueHidden").val()) ? $("#selectedOrderByValueHidden").val().toUpperCase() : "";
					}
					$$D("oldOrderByKeyValue").value = orderBy;
					$$D("oldOrderByValue").value = $("#selectedOrderByVal").html();
					localStorage.setItem('categoryId', categoryId);
					localStorage.setItem('categoryCode', categoryCode);
					localStorage.setItem('subject', subjectName);
				} else {
					categoryId = isBlankOrNull(localStorage.getItem('categoryId')) ? categoryId : localStorage.getItem('categoryId');
					categoryCode = isBlankOrNull(localStorage.getItem('categoryCode')) ? categoryCode : localStorage.getItem('categoryCode');
					subjectName = isBlankOrNull(localStorage.getItem('subject')) ? subjectName : localStorage.getItem('subject');
					if (listType != 'dynamicList' && !isBlankOrNull(localStorage.getItem('nearYouFlag'))) {
						orderBy = localStorage.getItem('nearYouFlag');
						$$D("oldOrderByKeyValue").value = orderBy;
						localStorage.removeItem("nearYouFlag");
					}
					xtremePushTag("Category_Code", categoryCode);
				}
				//			
				$$D("categoryIdSelected").value = categoryId;
				$$D("categoryCodeSelected").value = categoryCode;
				$("#subjectSelected").html(subjectName);
				//			
				qualificationCode = !isBlankOrNull($$D("qualificationCodeSelected").value) ? $$D("qualificationCodeSelected").value : "M";
				$$D("oldQualificationValue").value = $$D("qualificationSelected").innerHTML;
				$$D("oldQualificationCodeValue").value = qualificationCode;
				//		
				if (listType == 'dynamicList') {
					categoryId = localStorage.getItem('categoryId');
					categoryCode = localStorage.getItem('categoryCode');
					subjectName = localStorage.getItem('subject');
					orderBy = $$D("oldOrderByKeyValue").value;
					regionFlag = $$D("oldCountryFlagValue").value;
					region = $$D("oldCountryValue").value;
					locationTypeId = $$D("oldLocationTypeIdValue").value;
					studyModeTextKey = $$D("oldsmTextKeyValue").value;
					previousQualification = $$D("oldPreviousQualValue").value;
					previousQualificationGrade = $$D("oldPreviousQualGradesValue").value;
					assessmentType = $$D("oldAssessmentTypeSelected").value;
					reviewCategory = $$D("oldReviewCategorySelected").value;
				}
				if (!isBlankOrNull($("#selectedOrderByValueHidden").val()) && filterSearchFlag != 'Filter') {
					//console.log("test"+$("#selectedOrderByValueHidden").val());
					orderBy = !isBlankOrNull($("#selectedOrderByValueHidden").val()) ? $("#selectedOrderByValueHidden").val().toUpperCase() : "";
				}
				$("#selectedOrderByValueHidden").val(orderBy);
				//
				categoryId = !isBlankOrNull(categoryId) ? categoryId : "";
				categoryCode = !isBlankOrNull(categoryCode) ? categoryCode : "";
				subjectName = !isBlankOrNull(subjectName) ? subjectName : "";
				orderBy = !isBlankOrNull(orderBy) ? orderBy : "";
				//
				fromPage = (isBlankOrNull(fromPage) || fromPage == 'dynamicList') ? "1" : fromPage;
				toPage = isBlankOrNull(toPage) ? "6" : toPage;
				//
				var reviewSubjectOne = ""; var reviewSubjectTwo = ""; var reviewSubjectThree = "";
				if (!isBlankOrNull(reviewCategory)) {
					reviewCategoryArray = reviewCategory.split(",");
					if (reviewCategoryArray.length == 3) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; reviewSubjectThree = reviewCategoryArray[2]; }
					else if (reviewCategoryArray.length == 2) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; }
					else { reviewSubjectOne = reviewCategoryArray[0]; }
				}
				//
				if (!isBlankOrNull(locationTypeId) || !isBlankOrNull(region) || !isBlankOrNull(regionFlag) || !isBlankOrNull(studyModeTextKey) || !isBlankOrNull(previousQualification) || !isBlankOrNull(previousQualificationGrade) || !isBlankOrNull(assessmentType) || !isBlankOrNull(reviewSubjectOne) || !isBlankOrNull(reviewSubjectTwo) || !isBlankOrNull(reviewSubjectThree)) {
					hideSRtitle = 'hide';
				}
				//	
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"categoryCode": categoryCode,
					"categoryId": categoryId,
					"qualification": qualificationCode,
					"fromPageNo": fromPage,
					"toPageNo": toPage,
					"keywordSearch": subjectName,
					"appVersion": Constants.appVersion,
					"accessToken": "246",
					"affiliateId": "220703",
					"userAgent": "",
					"searchUrl": "/degree-courses/search?subject=" + localStorage.getItem('subject'),
					"orderBy": orderBy,
					"latitude": latitude,
					"longtitude": longtitude,
					"locationType": locationTypeId,
					"region": region,
					"regionFlag": regionFlag,
					"studyMode": studyModeTextKey,
					"previousQualification": previousQualification,
					"previousQualificationGrade": previousQualificationGrade,
					"assessmentType": assessmentType,
					"reviewSubjectOne": reviewSubjectOne,
					"reviewSubjectTwo": reviewSubjectTwo,
					"reviewSubjectThree": reviewSubjectThree,
					"jacsCode": jacsCode
				};
				console.log("fromPage", fromPage);
				var jsonData = JSON.stringify(stringJson);
				$$D('loaderCourseSR').style.display = "block";
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/course/search-results/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					//
					// Maintain recentSearchCriteria Start
					//
					//statusBarFontColor('white');



					//$$D("reviewCategoryNameSelected").innerHTML = json.reviewCategoryName;
					//
					if (!isBlankOrNull(localStorage.getItem("recentSearchCriteria")) && filterSearchFlag != "Filter") { //
						var json = JSON.parse(localStorage.getItem("recentSearchCriteria"));
						var selectedArray = json.reviewCategory.split(",");
						var tempReviewCategoryArray = [];
						var tempReviewCategoryNameArray = [];
						angular.forEach(response.reviewCategoryFilterDetails, function (value, index) {
							if (selectedArray.indexOf(value.textKey.trim()) >= 0) {
								tempReviewCategoryArray.push(value.reviewCategoryName);
								tempReviewCategoryNameArray.push(value.reviewCategoryDisplayName);
							}
						});
						reviewCategoryName = tempReviewCategoryArray.join(", ");
						reviewCategoryNameText = tempReviewCategoryNameArray.join(", ");
						$$D("reviewCategoryNameSelected").innerHTML = reviewCategoryNameText;
						$$D("oldReviewCategoryNameSelected").value = reviewCategoryName;

					}
					if (filterSearchFlag == "Filter") {
						var selectedArray = $$D("oldReviewCategoryNameSelected").value.split(", ");
						var tempReviewCategoryArray = [];
						angular.forEach(response.reviewCategoryFilterDetails, function (value, index) {
							if (selectedArray.indexOf(value.reviewCategoryDisplayName.trim()) >= 0) {
								tempReviewCategoryArray.push(value.reviewCategoryName);
							}
						});
						reviewCategoryName = tempReviewCategoryArray.join(", ");
						$$D("oldReviewCategoryNameSelected").value = reviewCategoryName;
					}
					//

					$scope.getReviewCategoryFilterDetails = function () {
						return response.reviewCategoryFilterDetails;
					}
					var searchCriteria = {
						"searchCategoryCode": categoryCode,
						"searchCategoryId": categoryId,
						"keywordSearch": subjectName,
						"qualification": qualificationCode,
						"qualificationName": $$D("qualificationSelected").innerHTML,
						"orderBy": orderBy,
						"selectedOrderByVal": $("#selectedOrderByVal").html(),
						"locationType": locationTypeId,
						"locationTypeName": locationTypeName,
						"region": region,
						"regionFlag": regionFlag,
						"studyMode": studyModeTextKey,
						"studyModeTextName": studyModeTextName,
						"previousQual": previousQualification,
						"previousQualGrade": previousQualificationGrade,
						"previousQualGradeName": previousQualificationGradeName,
						"assessmentType": assessmentType,
						"assessmentTypeName": assessmentTypeName,
						"reviewCategory": reviewCategory,
						"reviewCategoryName": reviewCategoryName,
						"jacsCode": jacsCode
					}
					localStorage.setItem('recentSearchCriteria', JSON.stringify(searchCriteria));
					//	                
					
					$$D('searchResultsView').style.display = "block";
					if (response.versionDetails != null && response.versionDetails.length > 0) {
						var exceptionTemplateURL = "html/versioning/updateVersion.html";
						checkVersionDetails(response.versionDetails[0].versionName, response.versionDetails[0].status, response.versionDetails[0].message, exceptionTemplateURL);
					}
					$scope.totalCourseCount = response.totalCourseCount;
					var totalCourseCount = $scope.totalCourseCount;
					$$D("totalCourseCount").value = totalCourseCount;
					$$D("oldTotalCourseCount").value = totalCourseCount;
					$(".courseCount").html(totalCourseCount + " courses");
					if (totalCourseCount <= 1) { $(".courseCount").html(totalCourseCount + " course"); }
					if (hideSRtitle == 'hide') {
						$("#overFiveHundResults").hide();
					} else {
						if (totalCourseCount > 100 && (hideSRtitle != 'hide')) { $("#overFiveHundResults").show(); }
						else { $("#overFiveHundResults").hide(); }
					}
					//
					if (localStorage.getItem("quizQualMessage") == "0_COURSE") {
						$("#overHundResults").html('<span class="lt_bld">No Results found for your selection.</span><br>We are displaying results for undergraduate courses.');
						$("#overFiveHundResults").show(); localStorage.removeItem("quizQualMessage");
					} else {
						$("#overHundResults").html('<span class="lt_bld">Over 100 results found</span> <br>Narrow your search to find the perfect course');
					}
					//
					$scope.totalUniversityCount = response.totalUniversityCount;
					$$D("totalUniversityCount").value = $scope.totalUniversityCount;
					$scope.tempSearchResultsList = response.searchResults;
					//Below code for displaying random images
					angular.forEach($scope.tempSearchResultsList, function (value, index) {
						if (isBlankOrNull($scope.tempSearchResultsList[index].tileMediaPath)) {
							$scope.tempSearchResultsList[index].tileMediaPath = UniRandomImage();
						}
					});
					//
					if (fromPage == 1) {
						$$D("totalRecordsLoaded").value = "0";
						if (filterSearchFlag == "Filter" || filterSearchFlag == 'orderby') {
							setTimeout(function () { $('#searchResultsView').scrollTop(0); }, 500);
							$scope.searchResultsListDym = $scope.tempSearchResultsList;
							$$D("totalRecordsLoaded").value = parseInt($$D("totalRecordsLoaded").value) + parseInt($scope.searchResultsListDym.length);
							setTimeout(function () { $$D('loaderCourseSR').style.display = "none"; $("#searchResultsDym").html($$D("ajaxDynamicList").innerHTML); }, 500);
						} else {
							$scope.searchResultsList = $scope.tempSearchResultsList;
							$$D("totalRecordsLoaded").value = parseInt($$D("totalRecordsLoaded").value) + parseInt($scope.searchResultsList.length);
							$$D('loaderCourseSR').style.display = "none";
							if (quizFlag == "QUIZ") {
								$("#goBackDiv").hide();
								$('#searchAgainSearchresult').hide();
								$$D('searchResultsFilter').style.display = "block";
							}
						}
						setTimeout(function () {
							if (parseInt($$D("totalRecordsLoaded").value) < toPage || $scope.totalUniversityCount == parseInt($$D("totalRecordsLoaded").value)) { $("#viewMoreResults").hide(); }
							else { $("#viewMoreResults").show(); }
							$$D('loaderCourseSR').style.display = "none";
						}, 600);
					} else {
						$scope.searchResultsListDym = $scope.tempSearchResultsList;
						$$D("totalRecordsLoaded").value = parseInt($$D("totalRecordsLoaded").value) + parseInt($scope.searchResultsListDym.length);
						setTimeout(function () { $$D('loaderCourseSR').style.display = "none"; $("#searchResultsDym").append($$D("ajaxDynamicList").innerHTML); }, 500);
					}
					if (parseInt($$D("totalRecordsLoaded").value) > 0) { $$D('searchResultsFilter').style.display = "block"; }
					$scope.subjectTileMediaPath = response.subjectTileMediaPath;
					$scope.subjectTileMediaId = response.subjectTileMediaId;
					console.log('$scope.subjectTileMediaId > ' + $scope.subjectTileMediaId);
					$scope.subjectTitle = replaceAll(response.subjectTitle, '-', ' ');
					$scope.institutionBrowseLogo = Constants.institutionBrowseLogo;
					$scope.subjects = Constants.subjects;
					/*Qualification Filter*/
					$scope.qualificationFilters = response.qualificationFilters;
					angular.forEach($scope.qualificationFilters, function (value, index) {
						if ($scope.qualificationFilters[index].flag == "Y") {
							$$D("oldQualificationValue").value = $scope.qualificationFilters[index].qualificaiton;
							$$D("oldQualificationCodeValue").value = $scope.qualificationFilters[index].qualificationCode;
						}
					});
					setTimeout(function () {
						if (!isBlankOrNull($("#oldQualificationValue").val())) { $("#qualificationSelected").html($$D("oldQualificationValue").value); $("#qualificationAddChangeTxt").html("Change"); } else { $("#qualificationSelected").html("Not chosen"); $("#qualificationAddChangeTxt").html("+ Add"); }
						if (!isBlankOrNull($$D("oldQualificationCodeValue").value)) { $("#qualificationCodeSelected").val($$D("oldQualificationCodeValue").value); } else { $("#qualificationCodeSelected").val(""); }
					}, 500);
					/*Subject Filter*/
					$scope.subjectfilters = response.subjectfilters;
					angular.forEach($scope.subjectfilters, function (value, index) {
						if ($scope.subjectfilters[index].flag == "Y") {
							$("#subjectSelected").html($scope.subjectfilters[index].subjectName);
							$$D("categoryCodeSelected").value = $scope.subjectfilters[index].categoryCode;
							$$D("categoryIdSelected").value = $scope.subjectfilters[index].searchCategoryId;
							$$D("oldCategoryCodeSelected").value = $scope.subjectfilters[index].categoryCode;
							$$D("oldCategoryIdSelected").value = $scope.subjectfilters[index].searchCategoryId;
							$$D("oldCategoryText").value = $("#subjectSelected").html();
							$$D("subjectResetValue").value = $$D("categoryCodeSelected").value + "|" + $$D("categoryIdSelected").value + "|" + $("#subjectSelected").html();
						}
					});
					/*Location Type Filter*/
					$scope.locationTypeFilterResults = response.locationTypeFilters;
					$scope.locationTypeSubFilterResults = [];
					angular.forEach($scope.locationTypeFilterResults, function (value, index) {
						if ($scope.locationTypeFilterResults[index].locationTypeName.toUpperCase() == "COUNTRYSIDE") {
							$scope.locationTypeSubFilterResults.push({ "locationTypeClassName": "cntry", "locationTypeName": $scope.locationTypeFilterResults[index].locationTypeName, "locationTypeId": $scope.locationTypeFilterResults[index].locationTypeId });
						} else if ($scope.locationTypeFilterResults[index].locationTypeName.toUpperCase() == "TOWN") {
							$scope.locationTypeSubFilterResults.push({ "locationTypeClassName": "town", "locationTypeName": $scope.locationTypeFilterResults[index].locationTypeName, "locationTypeId": $scope.locationTypeFilterResults[index].locationTypeId });
						} else if ($scope.locationTypeFilterResults[index].locationTypeName.toUpperCase() == "BIG CITY") {
							$scope.locationTypeSubFilterResults.push({ "locationTypeClassName": "big_city", "locationTypeName": $scope.locationTypeFilterResults[index].locationTypeName, "locationTypeId": $scope.locationTypeFilterResults[index].locationTypeId });
						} else if ($scope.locationTypeFilterResults[index].locationTypeName.toUpperCase() == "SEASIDE") {
							$scope.locationTypeSubFilterResults.push({ "locationTypeClassName": "sea", "locationTypeName": $scope.locationTypeFilterResults[index].locationTypeName, "locationTypeId": $scope.locationTypeFilterResults[index].locationTypeId });
						}
						if ($scope.locationTypeFilterResults[index].flag == "Y") {
							setTimeout(function () { $("input[value='" + $scope.locationTypeFilterResults[index].locationTypeId + '|' + $scope.locationTypeFilterResults[index].locationTypeName + "']").prop('checked', true); }, 500);
						}
					});
					setTimeout(function () {
						if (!isBlankOrNull($$D("oldLocationTypeNameValue").value)) { $("#locationTypeNameSelected").html($$D("oldLocationTypeNameValue").value); } else { $("#locationTypeNameSelected").html("Not chosen"); }
						if (!isBlankOrNull($$D("oldLocationTypeIdValue").value)) { $("#locationTypeIdSelected").val($$D("oldLocationTypeIdValue").value); $("#locationTypeAddChangeTxt").html("Change"); } else { $("#locationTypeIdSelected").val(""); $("#locationTypeAddChangeTxt").html("+ Add"); }
					}, 500);
					/*Location Filter*/
					$scope.locationFilterResults = response.locationfilters;
					$scope.locationSubFilterResults = [];
					angular.forEach($scope.locationFilterResults, function (value, index) {
						if ($scope.locationFilterResults[index].region != $scope.locationFilterResults[index].topRegion) {
							$scope.locationSubFilterResults.push({ "region": $scope.locationFilterResults[index].region, "topRegion": $scope.locationFilterResults[index].topRegion, "flag": $scope.locationFilterResults[index].flag });
						}
						if ($scope.locationFilterResults[index].flag == "Y") {
							setTimeout(function () { $(':radio[value="' + $scope.locationFilterResults[index].topRegion + '"]').attr('checked', 'checked'); }, 500);
							if ($scope.locationFilterResults[index].region != $scope.locationFilterResults[index].topRegion) {
								setTimeout(function () { $(".city_list").show(); }, 500);
							}
						}
					});
					setTimeout(function () {
						if (!isBlankOrNull($$D("oldCountryValue").value)) { $("#selectedLocation").html($$D("oldCountryValue").value); $("#locationAddChangeTxt").html("Change"); } else { $("#selectedLocation").html("Not chosen"); $("#locationAddChangeTxt").html("+ Add"); }
						if (!isBlankOrNull($$D("oldCountryFlagValue").value)) { $("#selectedLocationFlag").val($$D("oldCountryFlagValue").value); } else { $("#selectedLocationFlag").val(""); }
					}, 500);
					/*Study mode filter*/
					$scope.studyModeFiltersResults = response.studyModeFilters;
					setTimeout(function () {
						if (!isBlankOrNull($$D("oldStudyModeNameValue").value)) { $("#studyModeNameSelected").html($$D("oldStudyModeNameValue").value); $("#studyModeAddChangeTxt").html("Change"); } else { $("#studyModeNameSelected").html("Not chosen"); $("#studyModeAddChangeTxt").html("+ Add"); }
						if (!isBlankOrNull($$D("oldsmTextKeyValue").value)) { $("#smTextKeySelected").val($$D("oldsmTextKeyValue").value); } else { $("#smTextKeySelected").val(""); }
					}, 500);
					angular.forEach($scope.studyModeFiltersResults, function (value, index) {
						if (!isBlankOrNull($scope.studyModeFiltersResults[index].flag == 'Y')) {
							setTimeout(function () { $("input[value='" + $scope.studyModeFiltersResults[index].textKey + "|" + $scope.studyModeFiltersResults[index].studyMode + "']").prop('checked', true); }, 1000);
						}
					});
					/*Previous qualification filter*/
					$scope.previousQualificationfilters = response.previousQualificationfilters;
					$scope.qualselected = '1|' + $scope.previousQualificationfilters[0].gradeLevel + '|' + $scope.previousQualificationfilters[0].textKey;
					angular.forEach($scope.previousQualificationfilters, function (value, index) {
						if (!isBlankOrNull($scope.previousQualificationfilters[index].previousQualSelected)) {
							$scope.qualselected = index + 1 + '|' + $scope.previousQualificationfilters[index].gradeLevel + '|' + $scope.previousQualificationfilters[index].textKey;
							if ($scope.previousQualificationfilters[index].textKey == "TARIFF_POINTS") {
								$scope.qualselected = index + 1 + '||' + $scope.previousQualificationfilters[index].textKey;
								setTimeout(function () { $$D("pqTariffSavedValue").value = $scope.previousQualificationfilters[index].previousQualSelected; }, 500);
							}
						}
					});
					setTimeout(function () {
						$$D("pqResetValue").value = $scope.qualselected; onChageQualGrades($scope.qualselected, 'preselect', '', 'mySearchRootCtrl');
						if (!isBlankOrNull($$D("oldPreviousQualText").value)) { $("#previousQualSel").html($$D("oldPreviousQualText").value); } else { $("#previousQualSel").html("Not chosen"); }
						if (!isBlankOrNull($$D("oldPreviousQualGradesValue").value)) { $("#previousQualGradesSelected").val($$D("oldPreviousQualGradesValue").value); } else { $("#previousQualGradesSelected").val(""); }
						if (!isBlankOrNull($$D("oldPreviousQualValue").value)) { $("#previousQualSelected").val($$D("oldPreviousQualValue").value); } else { $("#previousQualSelected").val(""); }
						if (filterSearchFlag == "Filter" || jsonFilterFlag == "TRUE") { $("#selectGrades").val($scope.qualselected); }
					}, 500);
					/*Order by*/
					$scope.orderBy = response.orderByName;
					var nonSelectedFlag = true;
					angular.forEach($scope.orderBy, function (value, index) {
						if ($scope.orderBy[index].orderBySelected == "Y") {
							$scope.obselected = $scope.orderBy[index].orderByName;
							setTimeout(function () {
								$$D("orderByKeyValue").value = $scope.obselected;
								$$D('currentOrderBy').innerHTML = ($scope.orderBy[index].orderBydisplayName).toUpperCase();
								$("#orderBySelectBox").val($scope.obselected);
							}, 500);
							nonSelectedFlag = false;
						}
					});
					if (nonSelectedFlag) {
						setTimeout(function () {
							$scope.obselected = "MOST_INFO";
							$$D("orderByKeyValue").value = "MOST_INFO";
							$$D("oldOrderByKeyValue").value = "MOST_INFO";
							$$D('currentOrderBy').innerHTML = 'MOST INFO';
						}, 500);
					}
					setTimeout(function () {
						$("#orderBySelectBox").val($scope.obselected);
						if ((filterSearchFlag == "Filter" || jsonFilterFlag == "TRUE") && !isBlankOrNull($scope.obselected)) {
							$("#orderBySelectBox").val($scope.obselected);
						}
					}, 500);
					//
					/*Review category*/
					$scope.reviewCategoryFilterResults = response.reviewCategoryFilterDetails;
					$scope.reviewCategorySubFilterResults = [];
					angular.forEach($scope.reviewCategoryFilterResults, function (value, index) {
						console.log('$scope.reviewCategoryFilterResults[' + index + '].reviewCategoryName > ' + $scope.reviewCategoryFilterResults[index].reviewCategoryName + '> ' + $scope.reviewCategoryFilterResults[index].selectedText);
						if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "ACCOMMODATION") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_acm", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "CITY LIFE") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_cl", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "CLUBS AND SOCIETIES") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_cas", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "COURSE AND LECTURERS") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_cal", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "GIVING BACK") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_gb", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "INTERNATIONAL EXPERIENCE") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_int", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "STUDENT SUPPORT") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_ss", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "STUDENT UNION") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_su", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "UNI FACILITIES") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_uf", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "JOB PROSPECTS") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_jp", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						}

						if ($scope.reviewCategoryFilterResults[index].selectedText == "Y") {
							setTimeout(function () { $("input[value='" + $scope.reviewCategoryFilterResults[index].textKey + '|' + $scope.reviewCategoryFilterResults[index].reviewCategoryName + "']").prop('checked', true); }, 500);
						}
					});
					setTimeout(function () {
						if (!isBlankOrNull($$D("oldReviewCategoryNameSelected").value)) {
							$("#reviewCategoryNameSelected").html($$D("oldReviewCategoryNameSelected").value);
						} else {
							$("#reviewCategoryNameSelected").html("Not chosen");
						}
						if (!isBlankOrNull($$D("oldReviewCategorySelected").value)) {
							$("#reviewCategorySelected").val($$D("oldReviewCategorySelected").value);
							$("#reviewCategoryAddChangeTxt").html("Change");
						} else {
							$("#reviewCategorySelected").val(""); $("#reviewCategoryAddChangeTxt").html("+ Add");
						}
					}, 500);

					/*Assessment Type Filter*/
					$scope.assessmentTypeFilterResults = response.assessmentTypeFilterDetails;
					$scope.assessmentTypeSubFilterResults = [];
					angular.forEach($scope.assessmentTypeFilterResults, function (value, index) {
						if ($scope.assessmentTypeFilterResults[index].assesmentTypeName.toUpperCase() == "EXAM") {
							$scope.assessmentTypeSubFilterResults.push({ "assesmentTypeClassName": "cntry", "assesmentTypeName": $scope.assessmentTypeFilterResults[index].assesmentTypeName, "assesmentTypeId": $scope.assessmentTypeFilterResults[index].assesmentTypeId });
						} else if ($scope.assessmentTypeFilterResults[index].assesmentTypeName.toUpperCase() == "COURSEWORK") {
							$scope.assessmentTypeSubFilterResults.push({ "assesmentTypeClassName": "town", "assesmentTypeName": $scope.assessmentTypeFilterResults[index].assesmentTypeName, "assesmentTypeId": $scope.assessmentTypeFilterResults[index].assesmentTypeId });
						} else if ($scope.assessmentTypeFilterResults[index].assesmentTypeName.toUpperCase() == "PRACTICAL") {
							$scope.assessmentTypeSubFilterResults.push({ "assesmentTypeClassName": "big_city", "assesmentTypeName": $scope.assessmentTypeFilterResults[index].assesmentTypeName, "assesmentTypeId": $scope.assessmentTypeFilterResults[index].assesmentTypeId });
						}
						if ($scope.assessmentTypeFilterResults[index].selectedText == "Y") {
							setTimeout(function () { $("input[value='" + $scope.assessmentTypeFilterResults[index].assesmentTypeId + '|' + $scope.assessmentTypeFilterResults[index].assesmentTypeName + "']").prop('checked', true); }, 500);
						}
					});
					setTimeout(function () {
						if (!isBlankOrNull($$D("oldAssessmentTypeNameSelected").value)) {
							$("#assessmentTypeSel").html($$D("oldAssessmentTypeNameSelected").value);
						} else {
							$("#assessmentTypeSel").html("Not chosen");
						}
						if (!isBlankOrNull($$D("oldAssessmentTypeSelected").value)) {
							$("#assessmentTypeSelected").val($$D("oldAssessmentTypeSelected").value);
							$("#assessmentTypeChangeTxt").html("Change");
						} else {
							$("#assessmentTypeSelected").val(""); $("#assessmentTypeChangeTxt").html("+ Add");
						}
					}, 500);
					/*Assessment Filter*/
					if (isBlankOrNull(region) && isBlankOrNull(locationTypeId) && isBlankOrNull(studyModeTextKey) && isBlankOrNull(previousQualification) && isBlankOrNull(assessmentType) && isBlankOrNull(reviewCategory)) {
						setTimeout(function () { $(".flip_dv").hide(); }, 500);
					} else {
						setTimeout(function () { $(".flip_dv").show(); }, 500);
					}
					//
					if ($('.page-on-left').attr('data-page') == 'searchResults' && ($('.page-on-center').attr('data-page') == 'search-landing' || $('.page-on-center').attr('data-page') == 'browse')) {
						clearFilter();
					}

					//
					var page = $$('.page[data-page="searchResults"]')[0].f7PageData;
					if (Object.keys(page.query).length !== 0 && filterSearchFlag == undefined && listType !== 'dynamicList') {
						setTimeout(function () {
							console.log("SR page trigger called");
							if (page.query.action == "shortlist") {
								$('#mySearchRootCtrl .page-content').scrollTop($("#" + page.query.action_id).offset().top - 130, 100);
								if (!$("#" + page.query.action_id).hasClass('fav_fill'))
									$("#" + page.query.action_id).trigger('click');

							} else {
								$("#" + page.query.action_id).trigger('click');
							}
						}, 1000);
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					//window.location.replace('../../html/exception/exception.html');						
					mainView.router.loadPage('html/exception/exception.html');
				});
			};
			var page = $$('.page[data-page="searchResults"]')[0].f7PageData;
			if (Object.keys(page.query).length !== 0) {
				console.log("my call listType");
				$("#nextSetResults").val(sessionStorage.getItem("SR_Pagination"));
				$("#selectedOrderByValueHidden").val(sessionStorage.getItem("SR_OrderBySelectBox"));
				//selectedOrderByValueHidden
				$scope.searchResultsDetails(1, sessionStorage.getItem("SR_Pagination"));

			} else {
				$scope.searchResultsDetails(1, 6);
			}

			$scope.goBack = function () {
				/*
				localStorage.setItem('latitude', latitude);
				localStorage.setItem('longitude', longtitude);
				var fromPage = localStorage.getItem('FROM_PAGE');
				var URL = "../../html/home/homeLanding.html";
				if(fromPage == 'SEARCH_LANDING') {
					URL = "../../html/search/searchLanding.html";
				}
				window.history.back();
				//window.location.replace(URL);
				//window.location.href=URL;
				*/
			}
			//
			$scope.providerResultsPageURL = function (name, index, universityId, universityName, e, tileMediaPath) {
				e.stopPropagation();
				if (name == 'dynm' || name == 'uni_name' || name == 'uni-name') {
					localStorage.setItem('institutionId', universityId);
					localStorage.setItem('categoryId', localStorage.getItem('categoryId'));
					localStorage.setItem('categoryCode', localStorage.getItem('categoryCode'));
					localStorage.setItem('subject', localStorage.getItem('subject'));
					localStorage.setItem('nearYouFlag', 'NEAR YOU');
					localStorage.setItem('FROM_PAGE', 'search-results');
					localStorage.setItem('tileMediaPath', tileMediaPath);
					mainView.router.loadPage('html/institution/providerResults.html');
				} else if (name == 'uni_logo' || name == 'uni-logo') {
					localStorage.setItem('universityId', universityId);
					localStorage.setItem('universityName', universityName);
					localStorage.setItem('tileMediaPath', tileMediaPath);
					mainView.router.loadPage('html/institution/ins_profile.html');
				} else if (name == 'fl-span') {
					$("#percentageMatchUni").html($("#uni_name-" + index).html());
					$("#percentageMatchBlock").html($('#back-' + index).html());
					$(".reviewCatOne").hide(); $(".reviewCatTwo").hide(); $(".reviewCatThree").hide();
					if (!isBlankOrNull($("#oldReviewCategoryNameSelected").val())) {
						var reviewArr = $("#oldReviewCategoryNameSelected").val().split(", ");
						//
						var reviewCategoryFilterDetails = angular.element($$D('mySearchRootCtrl')).scope().getReviewCategoryFilterDetails();
						var selectedArray = $$D("oldReviewCategoryNameSelected").value.split(", ");
						var tempReviewCategoryArray = [];
						angular.forEach(reviewCategoryFilterDetails, function (value, index) {
							if (selectedArray.indexOf(value.reviewCategoryName.trim()) >= 0) {
								tempReviewCategoryArray.push(value.reviewCategoryDisplayName);
							}
						});
						//
						for (var lp = 0; lp < reviewArr.length; lp++) {
							if (lp == 0 && !isBlankOrNull(reviewArr[0])) { $(".reviewCatOne").html("<span>" + tempReviewCategoryArray[0] + "</span>"); $(".reviewCatOne").show(); }
							else if (lp == 1 && !isBlankOrNull(reviewArr[1])) { $(".reviewCatTwo").html("<span>" + tempReviewCategoryArray[1] + "</span>"); $(".reviewCatTwo").show(); }
							else if (lp == 2 && !isBlankOrNull(reviewArr[2])) { $(".reviewCatThree").html("<span>" + tempReviewCategoryArray[2] + "</span>"); $(".reviewCatThree").show(); }
						}
					}
					statusBarFontColor('black');
					myApp.popup('.match-popup');
				}
			}
			//Location filter
			var selectedLocationVal = ""; var selectedLocationFlag = "";
			$scope.selectLocation = function (radioId) {
				$('input[name=sublocation]').removeAttr('checked');
				selectedLocationVal = $$D("location-" + radioId) ? $$D("location-" + radioId).value : selectedLocationVal;
				selectedLocationFlag = "N";
				$(".city_list").hide();
				if ($$D("subLocationList_" + radioId)) {
					$$D("subLocationList_" + radioId).style.display = "block";
				}
			}
			//
			$scope.selectSubLocation = function (radioId) {
				selectedLocationVal = $$D("sublocation-" + radioId) ? $$D("sublocation-" + radioId).value : selectedLocationVal;
				selectedLocationFlag = "Y";
			}
			//
			$scope.saveLocationFilter = function () {
				if (isBlankOrNull(selectedLocationVal) && !isBlankOrNull($("input[name='location']:checked").val())) {
					$("#selectedLocation").html($("input[name='location']:checked").val());
					$$D("selectedLocationFlag").value = $("input[name='location']:checked").attr('id');
					$("#locationAddChangeTxt").html("Change");
				} else if (!isBlankOrNull(selectedLocationVal)) {
					$("#selectedLocation").html(selectedLocationVal);
					$$D("selectedLocationFlag").value = selectedLocationFlag;
					$$D("selectedCountryValue").value = selectedLocationVal;
					$("#locationAddChangeTxt").html("Change");
				} else {
					$("#selectedLocation").html("Not chosen"); $("#locationAddChangeTxt").html("+ Add");
				}
			}
			//Location type filter	
			var locationTypeId = ""; var locationTypeName = "";
			$scope.selectLocationType = function (locationType) {
				var strSelectedVals = $("input[name='locationType']:checked").map(function () { return $(this).val(); }).get().join(',');
				var locType = strSelectedVals.split(",");
				locationTypeId = ""; locationTypeName = "";
				for (var z = 0; z < locType.length; z++) {
					var array = locType[z].split("|");
					if (!isBlankOrNull(array[0]) && !isBlankOrNull(array[1])) {
						locationTypeId += array[0] + ",";
						locationTypeName += array[1] + ", ";
					}
				}
			}
			//
			$scope.saveLocationTypeSel = function () {
				$scope.selectLocationType();
				if (!isBlankOrNull(locationTypeId)) {
					$$D("locationTypeIdSelected").value = locationTypeId.substr(0, locationTypeId.length - 1);
					$("#locationTypeNameSelected").html(locationTypeName.substr(0, locationTypeName.length - 2));
					$("#locationTypeAddChangeTxt").html("Change");
				} else {
					$$D("locationTypeIdSelected").value = "";
					$("#locationTypeNameSelected").html("Not chosen"); $("#locationTypeAddChangeTxt").html("+ Add");
				}
				locationTypeId = ""; locationTypeName = "";
			}
			//Review category
			var reviewCategory = ""; var reviewCategoryName = "";
			$scope.selectReviewCategory = function (index) {
				var strSelectedVals = $("input[name='reviewCategory']:checked").map(function () { return $(this).val(); }).get().join(',');
				var reviewCategoryIds = strSelectedVals.split(",");
				reviewCategory = ""; reviewCategoryName = "";
				if (!isBlankOrNull(reviewCategoryIds) && (reviewCategoryIds.length > 0 && reviewCategoryIds.length <= 3)) {
					for (var lp = 0; lp < reviewCategoryIds.length; lp++) {
						var array = reviewCategoryIds[lp].split("|");
						if (!isBlankOrNull(array[0]) && !isBlankOrNull(array[1])) {
							reviewCategory += array[0] + ",";
							reviewCategoryName += array[1] + ", ";
						}
					}
					$('#reviewCategoryFilterSaveBtn').removeAttr("disabled");
				} else {
					lastId = "reviewCategory_" + index;
					$('#' + lastId).prop("checked", false);
					if (isBlankOrNull(reviewCategoryIds)) {
						$('#reviewCategoryFilterSaveBtn').attr("disabled", "disabled");
					}
				}
			}
			$scope.saveReviewCategory = function () {
				$scope.selectReviewCategory();
				if (!isBlankOrNull(reviewCategory)) {
					$$D("reviewCategorySelected").value = reviewCategory.substr(0, reviewCategory.length - 1);
					//$("#reviewCategoryNameSelected").html(reviewCategoryName.substr(0, reviewCategoryName.length - 2));
					//
					var reviewCategoryFilterDetails = $scope.reviewCategoryFilterResults;
					var selectedArray = reviewCategoryName.substr(0, reviewCategoryName.length - 2).split(", ");
					var tempReviewCategoryNameArray = [];
					angular.forEach(reviewCategoryFilterDetails, function (value, index) {
						console.log(selectedArray.indexOf(value.reviewCategoryName.trim()))
						if (selectedArray.indexOf(value.reviewCategoryName.trim()) >= 0) {
							tempReviewCategoryNameArray.push(value.reviewCategoryDisplayName);
						}
					});
					reviewCategoryNameText = tempReviewCategoryNameArray.join(", ");
					//
					$("#reviewCategoryNameSelected").html(reviewCategoryNameText);
					$("#reviewCategoryAddChangeTxt").html("Change");
				} else {
					$$D("reviewCategorySelected").value = "";
					$("#reviewCategoryNameSelected").html("Not chosen"); $("#reviewCategoryAddChangeTxt").html("+ Add");
				}
				locationTypeId = ""; locationTypeName = "";
			}
			//Save assessment type 
			$scope.saveAssessmentTypeSel = function () {
				var assessmentType = $('input:radio[name=assessment-radio]:checked').map(function () { return this.value; }).get();
				if (!isBlankOrNull(assessmentType)) {
					var assessmentArray = assessmentType[0].split("|");
				}
				$$D("assessmentTypeSelected").value = assessmentArray[0];
				$$D("assessmentTypeSel").innerHTML = assessmentArray[1];
				if (parseInt($$D("totalCourseCount").value) == 0) { $('#viewSearchFilterResults').attr("disabled", "disabled"); }
				else { $('#viewSearchFilterResults').removeAttr('disabled'); }
				$("#assessmentTypeChangeTxt").html("Change");
			}

			//Save selected qualification
			$scope.saveQualificationSel = function () {
				var qualification = $('input:radio[name=qualificaiton-radio]:checked').map(function () { return this.value; }).get();
				var array = qualification[0].split("|");
				$$D("qualificationCodeSelected").value = array[0];
				$("#qualificationSelected").html(array[1]);
				$(".courseCount").html($("#showQualificationCount").html());
				if (parseInt($$D("totalCourseCount").value) == 0) { $('#viewSearchFilterResults').attr("disabled", "disabled"); }
				else { $('#viewSearchFilterResults').removeAttr('disabled'); }
				$("#qualificationAddChangeTxt").html("Change");
			}
			//Save selected subject
			$scope.saveSubjectSel = function () {
				var subject = $('input:radio[name=subject-radio]:checked').map(function () { return this.value; }).get();
				var array = subject[0].split("|");
				$$D("categoryCodeSelected").value = array[0];
				$$D("categoryIdSelected").value = array[1];
				$("#subjectSelected").html(array[2]);
				$(".courseCount").html($("#showSubjectCount").html());
				if (parseInt($$D("totalCourseCount").value) == 0) { $('#viewSearchFilterResults').attr("disabled", "disabled"); }
				else { $('#viewSearchFilterResults').removeAttr('disabled'); }
			}
			//To get course count
			$scope.getSubjectQualificationCourseCount = function (id, flag) {
				if ($$D(id)) {
					var codeAndIdArr = $$D(id).value.split("|");
				}
				var categoryCode = ""; var categoryId = ""; var subject = ""; var qualificationId = "";
				previousQualificationGrade = "";
				previousQualification = "";
				if (flag == "subject") {
					subject = codeAndIdArr[2];
					categoryCode = codeAndIdArr[0];
					categoryId = codeAndIdArr[1];
					qualificationId = !isBlankOrNull($$D("qualificationCodeSelected").value) ? $$D("qualificationCodeSelected").value : "";
					previousQualificationGrade = !isBlankOrNull($$D("previousQualGradesSelectedforcount").value) ? $$D("previousQualGradesSelectedforcount").value : "";
					previousQualification = !isBlankOrNull($$D("previousQualSelectedforcount").value) ? $$D("previousQualSelectedforcount").value : "";
				} else if (flag == "qualification") {
					subject = $$D("subjectSelected").innerHTML;
					categoryCode = !isBlankOrNull($$D("categoryCodeSelected").value) ? $$D("categoryCodeSelected").value : "";
					categoryId = !isBlankOrNull($$D("categoryIdSelected").value) ? $$D("categoryIdSelected").value : "";
					qualificationId = codeAndIdArr[0];
					previousQualificationGrade = !isBlankOrNull($$D("previousQualGradesSelectedforcount").value) ? $$D("previousQualGradesSelectedforcount").value : "";
					previousQualification = !isBlankOrNull($$D("previousQualSelectedforcount").value) ? $$D("previousQualSelectedforcount").value : "";
				} else if (flag == "pre-qualification") {
					subject = $$D("subjectSelected").innerHTML;
					categoryCode = !isBlankOrNull($$D("categoryCodeSelected").value) ? $$D("categoryCodeSelected").value : "";
					categoryId = !isBlankOrNull($$D("categoryIdSelected").value) ? $$D("categoryIdSelected").value : "";
					qualificationId = !isBlankOrNull($$D("qualificationCodeSelected").value) ? $$D("qualificationCodeSelected").value : "";
					previousQualificationGrade = !isBlankOrNull($$D("previousQualGradesSelectedforcount").value) ? $$D("previousQualGradesSelectedforcount").value : "";
					previousQualification = !isBlankOrNull($$D("previousQualSelectedforcount").value) ? $$D("previousQualSelectedforcount").value : "";
				}
				//$$D('loaderCourse').style.display = 'block';
				var stringJson = {
					"institutionId": '',
					"categoryCode": categoryCode,
					"categoryId": categoryId,
					"qualification": qualificationId,
					"keywordSearch": subject,
					"previousQualification": previousQualification,
					"previousQualificationGrade": previousQualificationGrade,
					"accessToken": "246",
					"affiliateId": "220703"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/course/course-count/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					//$$D('loaderCourse').style.display = 'none';										
					var totalCourseCount = response.totalCourseCount;
					//if(id != "subjectResetValue") { $$D("oldTotalCourseCount").value = $$D("totalCourseCount").value; }
					$$D("totalCourseCount").value = totalCourseCount;
					var totalCourse = totalCourseCount + " courses";
					if (totalCourseCount <= 1) { totalCourse = totalCourseCount + " course"; }
					if (parseInt($$D("totalCourseCount").value) == 0) { $('#viewSearchFilterResults').attr("disabled", "disabled"); }
					else { $('#viewSearchFilterResults').removeAttr('disabled'); }
					if (flag == "qualification") { $("#showQualificationCount").html(totalCourse); if (totalCourseCount == 0) { $("#showQualificationCount").show(); $('#showPreviousQualificationCount').show(); } else { $("#showQualificationCount").hide(); $('#showPreviousQualificationCount').hide(); } }
					else if (flag == "subject") { $("#showSubjectCount").html(totalCourse); if (totalCourseCount == 0) { $("#showSubjectCount").show(); $('#showPreviousQualificationCount').show() } else { $("#showSubjectCount").hide(); $('#showPreviousQualificationCount').hide(); } }
					else if (flag == "pre-qualification") { $("#showSubjectCount").html(totalCourse); if (totalCourseCount == 0) { $("#showSubjectCount").show(); $('#showPreviousQualificationCount').show() } else { $("#showSubjectCount").hide(); $('#showPreviousQualificationCount').hide(); } }
					if (id == "subjectResetValue" || id == "prevQualCount") { if (totalCourseCount == 0) { $(".courseCount").html(totalCourse); $(".courseCount").show(); } else { $(".courseCount").hide(); } }
					if (id == 'prevQualCount' && totalCourseCount > 0) { $('#previousQualSavBtn').removeAttr("disabled"); }
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					//window.location.replace('../../html/exception/exception.html');						
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//Previous qualification		
			$scope.saveUCASTariff = function () {
				validateSelectedGrades('', '', '', '');
			}
			//Study mode
			$scope.saveStudyModeFilter = function () {
				var studyMode = $('input:radio[name=studyMode-radio]:checked').map(function () { return this.value; }).get();
				if (!isBlankOrNull(studyMode)) {
					var array = studyMode[0].split("|");
					$$D("smTextKeySelected").value = array[0];
					$("#studyModeNameSelected").html(array[1]);
					$("#studyModeAddChangeTxt").html("Change");
				} else {
					$("#studyModeNameSelected").html("Not chosen"); $("#studyModeAddChangeTxt").html("+ Add");
				}
			}
			//Submit search filters
			$scope.submitSearchFilters = function () {
				statusBarFontColor('white');
				$scope.searchResultsDetails(1, 6, 'Filter');
			}
			//Below code for pull to refresh
			var searchResultsContent = $$('#mySearchRootCtrl .pull-to-refresh-content');
			searchResultsContent.on('refresh', function (e) {
				// Emulate 2s loading
				setTimeout(function () {
					scopeObject.searchResultsDetails(1, 6, 'pull');
					myApp.pullToRefreshDone();
				}, 3000);
				//
			});
			//
			//Arul for Flip
			/*
			$$('.clk_im, .clk_im1').on('click', function(){
			$(this).parents('.popup-qual, .popup-howstudy ').removeClass('flip-out');
			$(this).parents('.popup-qual, .popup-howstudy ').addClass('flip-in');
			});
	
			$$('.popup-qualttip .back,.popup-howttip .back').on('click', function(){
			$(this).parents('body').find('.popup-qual,.popup-howstudy').removeClass('flip-in');
			$(this).parents('body').find('.popup-qual').addClass('flip-out');
			});
			*/
			$scope.getShortListFlag = function (institutionId, removeFrom, addTo, rootCtrl, pageFlag) {
				getShortListRootCtrl(institutionId, removeFrom, addTo, $scope, $http, rootCtrl, pageFlag);
			}

		}])

		.controller("providerRootCtrl", ["$scope", "$http", "$rootScope", function ($scope, $http, $rootScope, DataService) {
			var customDimensionUniversityName;
			$scope.title = "Framework7(a)";
			//
			scopeObject = $scope;
			//
			$scope.openPercentagePopUp = function () {
				statusBarFontColor('black');
				myApp.popup('#myProviderRootCtrl .match-popup');
			}
			//
			$scope.appInteractionLogging = function (activity) {
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var collegeId = localStorage.getItem('institutionId');
				var courseId = null;
				var userAgent = navigator.userAgent;
				var userIp = null;
				var suborderItemId = null;
				var profileId = null;
				var typeName = null;
				var activity = activity;
				var extraText = null;
				var stringJson = {
					"userId": userId,
					"collegeId": collegeId,
					"courseId": courseId,
					"userIp": userIp,
					"userAgent": userAgent,
					"jsLog": 'Y',
					"suborderItemId": suborderItemId,
					"profileId": profileId,
					"typeName": typeName,
					"activity": activity,
					"extraText": extraText,
					"latitude": latitude,
					"longitude": longtitude,
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/interaction/db-stats/logging/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					//$$D('loaderCourse').style.display = 'none';										
					var logStatusId = response.logStatusId;
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					// do nothing				
				});
			}
			//			
			$scope.providerResultsDetails = function (fromPage, toPage, filterSearchFlag, pullFlag) {
				$$D('loaderProvider').style.display = 'block';
				var listType = fromPage;
				if (fromPage == 'dynamicList' && (parseInt($$D("totalRecordsLoadedPR").value) < parseInt($$D("overAllCourseCountPR").value))) {
					var totalUniCourseCnt = parseInt($$D("overAllCourseCountPR").value);
					var nextSet = parseInt($$D("nextSetResultsPR").value);
					if (nextSet >= 6) { fromPage = nextSet + 1; toPage = nextSet + 10; $$D("nextSetResultsPR").value = nextSet + 10; }
					if (parseInt($$D("nextSetResultsPR").value) >= totalUniCourseCnt) { $$D("viewMoreResultsPR").style.display = "none"; }
					localStorage.setItem('fromPage', fromPage);
					localStorage.setItem('toPage', toPage);
				}
				var categoryId = ""; var categoryCode = ""; var subjectName = ""; var orderBy = "";
				var qualificationCode = "";
				var qualificationName = "";
				var locationTypeId = "";
				var locationTypeName = "";
				var region = "";
				var regionFlag = "";
				var studyModeTextKey = "";
				var studyModeTextName = "";
				var previousQualification = "";
				var previousQualificationGrade = "";
				var previousQualificationGradeName = "";
				var assessmentType = "";
				var assessmentTypeName = "";
				var reviewCategory = "";
				var reviewCategoryName = "";
				var selectedOrderByVal = "";
				var jacsCode = "";
				//
				if (!isBlankOrNull(localStorage.recentSearchCriteria) && (isBlankOrNull(filterSearchFlag) || filterSearchFlag != "Filter") && isBlankOrNull(localStorage.getItem('VIEW_COURSES_FLAG'))) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					categoryCode = !isBlankOrNull(recentSearchCriteria.searchCategoryCode) ? recentSearchCriteria.searchCategoryCode : "";
					categoryId = !isBlankOrNull(recentSearchCriteria.searchCategoryId) ? recentSearchCriteria.searchCategoryId : "";
					subjectName = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "";
					qualificationCode = !isBlankOrNull(recentSearchCriteria.qualification) ? recentSearchCriteria.qualification : "";
					orderBy = !isBlankOrNull(recentSearchCriteria.orderBy) ? recentSearchCriteria.orderBy : "";
					locationTypeId = !isBlankOrNull(recentSearchCriteria.locationType) ? recentSearchCriteria.locationType : "";
					locationTypeName = !isBlankOrNull(recentSearchCriteria.locationTypeName) ? recentSearchCriteria.locationTypeName : "";
					region = !isBlankOrNull(recentSearchCriteria.region) ? recentSearchCriteria.region : "";
					regionFlag = !isBlankOrNull(recentSearchCriteria.regionFlag) ? recentSearchCriteria.regionFlag : "";
					studyModeTextKey = !isBlankOrNull(recentSearchCriteria.studyMode) ? recentSearchCriteria.studyMode : "";
					studyModeTextName = !isBlankOrNull(recentSearchCriteria.studyModeTextName) ? recentSearchCriteria.studyModeTextName : "";
					previousQualification = !isBlankOrNull(recentSearchCriteria.previousQual) ? recentSearchCriteria.previousQual : "";
					previousQualificationGrade = !isBlankOrNull(recentSearchCriteria.previousQualGrade) ? recentSearchCriteria.previousQualGrade : "";
					previousQualificationGradeName = !isBlankOrNull(recentSearchCriteria.previousQualGradeName) ? recentSearchCriteria.previousQualGradeName : "";
					assessmentType = !isBlankOrNull(recentSearchCriteria.assessmentType) ? recentSearchCriteria.assessmentType : "";
					assessmentTypeName = !isBlankOrNull(recentSearchCriteria.assessmentTypeName) ? recentSearchCriteria.assessmentTypeName : "";
					reviewCategory = !isBlankOrNull(recentSearchCriteria.reviewCategory) ? recentSearchCriteria.reviewCategory : "";
					reviewCategoryName = !isBlankOrNull(recentSearchCriteria.reviewCategoryName) ? recentSearchCriteria.reviewCategoryName : "";
					jacsCode = !isBlankOrNull(recentSearchCriteria.jacsCode) ? recentSearchCriteria.jacsCode : "";
					//
					//Study mode				
					$$D("oldStudyModeNameValuePR").value = studyModeTextName;
					$$D("oldsmTextKeyValuePR").value = studyModeTextKey;
					$$D("smTextKeySelectedPR").value = studyModeTextKey;
					$$D("studyModeNameSelectedPR").innerHTML = studyModeTextName;
					//Previous qualification
					$$D("oldPreviousQualValuePR").value = previousQualification;
					$$D("previousQualSelectedPR").value = previousQualification;
					//Previous qualification grade
					$$D("oldPreviousQualGradesValuePR").value = previousQualificationGrade;
					$$D("oldPreviousQualTextPR").value = previousQualificationGradeName;
					$$D("previousQualGradesSelectedPR").value = previousQualificationGrade;
					$$D("previousQualSelPR").innerHTML = previousQualificationGradeName;
					//Assessment type
					$$D("oldAssessmentTypeSelectedPR").value = assessmentType;
					$$D("oldAssessmentTypeNameSelectedPR").value = assessmentTypeName;
					$$D("assessmentTypeSelectedPR").value = assessmentType;
					$$D("assessmentTypeSelPR").innerHTML = assessmentTypeName;
					//Your preference				
					$("#oldReviewCategoryIdSelectedPR").val(reviewCategory);
					$("#oldReviewCategoryNameSelectedPR").val(reviewCategoryName);
					$("#reviewCategorySelectedPR").val(reviewCategory);
					$("#reviewCategoryNameSelectedPR").html(reviewCategoryName);
					$$D("oldReviewCategorySelectedPR").value = reviewCategory;
				}
				//
				// --- GTM event log for filters ---         
				/*
				if(filterSearchFlag == "Filter") {
					if(!isBlankOrNull($('#qualificationSelectedPR').html())) {
						googleTagManagerEventTrack(null, 'Search', 'Qualification', $('#qualificationSelectedPR').html(), '0');
					}
					if(!isBlankOrNull($('#subjectSelectedPR').html())) {
						googleTagManagerEventTrack(null, 'Search', 'Subject', $('#subjectSelectedPR').html(), '0');
					}
					if(!isBlankOrNull($('#studyModeNameSelectedPR').html())) {
						googleTagManagerEventTrack(null, 'Search', 'Studymode', $('#studyModeNameSelectedPR').html(), '0');
					}
					if(!isBlankOrNull($('#previousQualSelPR').html())) {
						googleTagManagerEventTrack(null, 'Search', 'Previous Qualification', $('#previousQualSelPR').html(), '0');
					}
					if(!isBlankOrNull($('#assessmentTypeSelPR').html())) {
						googleTagManagerEventTrack(null, 'Search', 'Assessment Type', $('#assessmentTypeSelPR').html(), '0');
					}
				}
				*/
				//
				studyModeTextKey = !isBlankOrNull($$D("smTextKeySelectedPR").value) ? $$D("smTextKeySelectedPR").value : studyModeTextKey;
				studyModeTextName = !isBlankOrNull($$D("studyModeNameSelectedPR").innerHTML) ? $$D("studyModeNameSelectedPR").innerHTML : studyModeTextName;
				if (filterSearchFlag == "Filter") {
					$$D("oldStudyModeNameValuePR").value = $$D("studyModeNameSelectedPR").innerHTML;
					$$D("oldsmTextKeyValuePR").value = studyModeTextKey;
				}
				//
				previousQualification = !isBlankOrNull($$D("previousQualSelectedPR").value) ? $$D("previousQualSelectedPR").value : previousQualification;
				if (filterSearchFlag == "Filter") {
					$$D("oldPreviousQualValuePR").value = previousQualification;
				}
				//
				previousQualificationGrade = !isBlankOrNull($$D("previousQualGradesSelectedPR").value) ? $$D("previousQualGradesSelectedPR").value : previousQualificationGrade;
				previousQualificationGradeName = !isBlankOrNull($$D("previousQualSelPR").innerHTML) ? $$D("previousQualSelPR").innerHTML : previousQualificationGradeName;
				if (filterSearchFlag == "Filter") {
					$$D("oldPreviousQualGradesValuePR").value = previousQualificationGrade;
					$$D("oldPreviousQualTextPR").value = $$D("previousQualSelPR").innerHTML;
				}
				assessmentType = !isBlankOrNull($$D("assessmentTypeSelectedPR").value) ? $$D("assessmentTypeSelectedPR").value : assessmentType;
				assessmentTypeName = !isBlankOrNull($$D("assessmentTypeSelPR").innerHTML) ? $$D("assessmentTypeSelPR").innerHTML : assessmentTypeName;
				if (filterSearchFlag == "Filter") {
					$$D("oldAssessmentTypeSelectedPR").value = assessmentType;
					$$D("oldAssessmentTypeNameSelectedPR").value = $$D("assessmentTypeSelPR").innerHTML;
				}
				//			
				if (!isBlankOrNull(localStorage.getItem('latitude')) && !isBlankOrNull(localStorage.getItem('longitude'))) {
					latitude = localStorage.getItem('latitude');
					longtitude = localStorage.getItem('longitude');
					localStorage.removeItem("latitude");
					localStorage.removeItem("longitude");
				}
				//
				if (filterSearchFlag == "Filter") {
					reviewCategory = !isBlankOrNull($$D("reviewCategorySelectedPR").value) ? $$D("reviewCategorySelectedPR").value : reviewCategory;
					reviewCategoryName = !isBlankOrNull($$D("reviewCategoryNameSelectedPR").innerHTML) ? $$D("reviewCategoryNameSelectedPR").innerHTML : reviewCategoryName;
					$$D("oldReviewCategorySelectedPR").value = reviewCategory;
					$$D("oldReviewCategoryNameSelectedPR").value = $$D("reviewCategoryNameSelectedPR").innerHTML;
				}
				//		
				if (filterSearchFlag == "Filter") {
					categoryId = $$D("categoryIdSelectedPR").value;
					categoryCode = $$D("categoryCodeSelectedPR").value;
					$$D("oldCategoryIdSelectedPR").value = categoryId;
					$$D("oldCategoryCodeSelectedPR").value = categoryCode;
					$$D("oldCategoryTextPR").value = $("#subjectSelectedPR").html();
					subjectName = $("#subjectSelectedPR").html();
					//orderBy = !isBlankOrNull($("#orderByKeyValue").val()) ? $$D("orderByKeyValue").value : ""; 
					/*if(!(isBlankOrNull(region) && isBlankOrNull(locationTypeId) && isBlankOrNull(studyModeTextKey) && isBlankOrNull(previousQualification))){
						if($$D("oldOrderByKeyValue").value == $$D("orderByKeyValue").value) { orderBy = "BEST_MATCH"; $("#selectedOrderByVal").html("Best match");  }
					}*/
					//$$D("oldOrderByKeyValue").value = orderBy;
					//$$D("oldOrderByValue").value = $("#selectedOrderByVal").html();
					localStorage.setItem('categoryId', categoryId);
					localStorage.setItem('categoryCode', categoryCode);
					localStorage.setItem('subject', subjectName);
				} else {
					institutionId = localStorage.getItem('institutionId');
					institutionName = localStorage.getItem('institutionName');
					categoryId = localStorage.getItem('categoryId');
					categoryCode = localStorage.getItem('categoryCode');
					subjectName = localStorage.getItem('subject');
					if (listType != 'dynamicList') {
						//orderBy = localStorage.getItem('nearYouFlag'); 
						//$$D("oldOrderByKeyValue").value = orderBy; 
						localStorage.removeItem("nearYouFlag");
					}
				}
				//			
				$$D("categoryIdSelectedPR").value = categoryId;
				$$D("categoryCodeSelectedPR").value = categoryCode;
				$("#subjectSelectedPR").html(subjectName);
				//			
				var qualificationCode = !isBlankOrNull($$D("qualificationCodeSelectedPR").value) ? $$D("qualificationCodeSelectedPR").value : qualificationCode;
				$$D("oldQualificationValuePR").value = $$D("qualificationSelectedPR").innerHTML;
				$$D("oldQualificationCodeValuePR").value = qualificationCode;
				//		
				if (listType == 'dynamicList') {
					categoryId = localStorage.getItem('categoryId');
					categoryCode = localStorage.getItem('categoryCode');
					subjectName = localStorage.getItem('subject');
					studyModeTextKey = $$D("oldsmTextKeyValuePR").value;
					previousQualification = $$D("oldPreviousQualValuePR").value;
					previousQualificationGrade = $$D("oldPreviousQualGradesValuePR").value;
					assessmentType = $$D("oldAssessmentTypeSelectedPR").value;
				}
				//
				//
				institutionId = !isBlankOrNull(institutionId) ? institutionId : "";
				institutionName = !isBlankOrNull(institutionName) ? institutionName : "";
				var FROM_PAGE = localStorage.getItem('FROM_PAGE');
				//
				fromPage = (isBlankOrNull(fromPage) || fromPage == 'dynamicList') ? "1" : fromPage;
				toPage = isBlankOrNull(toPage) ? "6" : toPage;
				localStorage.setItem('fromPage', fromPage);
				localStorage.setItem('toPage', toPage);
				//
				if (subjectName == 'All Subjects') {
					subjectName = '';
				}
				//
				//googleTagManagerPageView('/provider-results/'+localStorage.getItem('institutionId'));
				//
				//document.addEventListener("deviceready", onDeviceReady, false);
				//
				var reviewSubjectOne = ""; var reviewSubjectTwo = ""; var reviewSubjectThree = "";
				if (!isBlankOrNull(reviewCategory)) {
					reviewCategoryArray = reviewCategory.split(",");
					if (reviewCategoryArray.length == 3) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; reviewSubjectThree = reviewCategoryArray[2]; }
					else if (reviewCategoryArray.length == 2) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; }
					else { reviewSubjectOne = reviewCategoryArray[0]; }
				}
				//
				var dbCatergoryCode = !isBlankOrNull(categoryCode) ? categoryCode : "";
				var dbCategoryId = !isBlankOrNull(categoryId) ? categoryId : "";
				var dbSubjectName = !isBlankOrNull(subjectName) ? subjectName : "";
				if ((!isBlankOrNull(localStorage.getItem('VIEW_COURSES_FLAG')) && localStorage.getItem('VIEW_COURSES_FLAG') == 'VIEW_COURSES_FLAG')) {
					dbCatergoryCode = "";
					dbCategoryId = "";
					dbSubjectName = "";
					//localStorage.removeItem('VIEW_COURSES_FLAG');
				}
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"phraseSearch": dbSubjectName,
					"institutionId": institutionId,
					"qualification": qualificationCode,
					"city": "",
					"postCode": "",
					"studyMode": studyModeTextKey,
					"locationType": locationTypeId,
					"region": region,
					"regionFlag": regionFlag,
					"searchHow": dbSubjectName,
					"searchCategory": dbCatergoryCode,
					"searchCategoryId": dbCategoryId,
					"userAgent": navigator.userAgent,
					"fromPage": fromPage,
					"toPage": toPage,
					"entryLevel": previousQualification,
					"entryPoints": previousQualificationGrade,
					"assessmentType": assessmentType,
					"universityLocationType": "",
					"reviewSubjectOne": reviewSubjectOne,
					"reviewSubjectTwo": reviewSubjectTwo,
					"reviewSubjectThree": reviewSubjectThree,
					"jacsCode": jacsCode,
					"searchUrl": "",
					"trackSessionId": "",
					"networkId": "",
					"appVersion": Constants.appVersion,
					"accessToken": "246",
					"affiliateId": "220703",
					"latitude": latitude,
					"longitude": longtitude
					//Added for clearing 
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/institution/provider-results',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
						var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
						orderBy = !isBlankOrNull(recentSearchCriteria.orderBy) ? recentSearchCriteria.orderBy : "";
						locationTypeId = !isBlankOrNull(recentSearchCriteria.locationType) ? recentSearchCriteria.locationType : "";
						locationTypeName = !isBlankOrNull(recentSearchCriteria.locationTypeName) ? recentSearchCriteria.locationTypeName : "";
						region = !isBlankOrNull(recentSearchCriteria.region) ? recentSearchCriteria.region : "";
						regionFlag = !isBlankOrNull(recentSearchCriteria.regionFlag) ? recentSearchCriteria.regionFlag : "";
						qualificationName = !isBlankOrNull(recentSearchCriteria.qualificationName) ? recentSearchCriteria.qualificationName : "";
						selectedOrderByVal = !isBlankOrNull(recentSearchCriteria.selectedOrderByVal) ? recentSearchCriteria.selectedOrderByVal : "";
					}
					//
					$scope.getReviewCategoryFilterDetails = function () {
						return response.reviewCategoryFilterDetails;
					}
					//
					// Maintain recentSearchCriteria Start
					//
					var searchCriteria = {
						"searchCategoryCode": categoryCode,
						"searchCategoryId": categoryId,
						"keywordSearch": subjectName,
						"qualification": qualificationCode,
						"qualificationName": qualificationName,
						"orderBy": orderBy,
						"selectedOrderByVal": selectedOrderByVal,
						"locationType": locationTypeId,
						"locationTypeName": locationTypeName,
						"region": region,
						"regionFlag": regionFlag,
						"studyMode": studyModeTextKey,
						"studyModeTextName": studyModeTextName,
						"previousQual": previousQualification,
						"previousQualGrade": previousQualificationGrade,
						"previousQualGradeName": previousQualificationGradeName,
						"assessmentType": assessmentType,
						"assessmentTypeName": assessmentTypeName,
						"reviewCategory": reviewCategory,
						"reviewCategoryName": reviewCategoryName,
						"jacsCode": jacsCode
					}
					localStorage.setItem('recentSearchCriteria', JSON.stringify(searchCriteria));
					$$D('providerResultsView').style.display = 'block';
					$$D('providerResultsFilter').style.display = 'block';
					$scope.institutionDetails = response.institutionDetails;
					$scope.tempInstitutionDetails = [];
					//Below code for random images
					angular.forEach($scope.institutionDetails, function (value, index) {
						if (isBlankOrNull($scope.institutionDetails[index].mediaPath)) {
							$scope.tempInstitutionDetails.push({ "backgroundImageClassName": "usr_sec", "displayName": $scope.institutionDetails[index].displayName, "id": $scope.institutionDetails[index].id, "mediaPath": !isBlankOrNull(localStorage.getItem('tileMediaPath')) ? localStorage.getItem('tileMediaPath') : UniRandomImage(), "logo": $scope.institutionDetails[index].logo, "logoName": $scope.institutionDetails[index].logoName, "reviewRatingDisplay": $scope.institutionDetails[index].reviewRatingDisplay, "reviewCount": $scope.institutionDetails[index].reviewCount, "overallRating": $scope.institutionDetails[index].overallRating, "shortListFlag": $scope.institutionDetails[index].shortListFlag });
						} else {
							$scope.tempInstitutionDetails.push({ "backgroundImageClassName": "usr_sec pr_grdnt", "displayName": $scope.institutionDetails[index].displayName, "id": $scope.institutionDetails[index].id, "mediaPath": $scope.institutionDetails[index].mediaPath, "logo": $scope.institutionDetails[index].logo, "logoName": $scope.institutionDetails[index].logoName, "reviewRatingDisplay": $scope.institutionDetails[index].reviewRatingDisplay, "reviewCount": $scope.institutionDetails[index].reviewCount, "overallRating": $scope.institutionDetails[index].overallRating, "shortListFlag": $scope.institutionDetails[index].shortListFlag });
						}
						//$scope.institutionDetails[index].mediaPath = !isBlankOrNull(localStorage.getItem('tileMediaPath')) ? localStorage.getItem('tileMediaPath') : UniRandomImage();
					});
					$scope.institutionDetails = $scope.tempInstitutionDetails;
					//
					var userID = localStorage.getItem('LOGGED_IN_USER_ID');
					customDimensionUniversityName = response.institutionDetails[0].displayName;

					firebaseEventTrack('screenview', { 'page_name': '/provider-results/' + localStorage.getItem('institutionId') });

					//	
					//			
					if (subjectName != 'All Subjects') {
						//$scope.subjectName = keywordSubjectFormat(dbSubjectName.replace('All', ''));
						$scope.subjectName = dbSubjectName.replace('All', '');
						$scope.subjectName = replaceAll($scope.subjectName, '-', ' ');
					}
					//
					if (response.versionDetails != null && response.versionDetails.length > 0) {
						var exceptionTemplateURL = "html/versioning/updateVersion.html";
						checkVersionDetails(response.versionDetails[0].versionName, response.versionDetails[0].status, response.versionDetails[0].message, exceptionTemplateURL);
					}
					//
					$scope.overAllCourseCount = response.overAllCourseCount;
					var overAllCourseCount = $scope.overAllCourseCount;
					$$D("totalCourseCountPR").value = overAllCourseCount;
					$$D("oldTotalCourseCountPR").value = overAllCourseCount;
					$(".courseCountPR").html(overAllCourseCount + " courses");
					if (overAllCourseCount <= 1) { $(".courseCountPR").html(overAllCourseCount + " course"); }
					//
					$$D("overAllCourseCountPR").value = $scope.overAllCourseCount;
					if (fromPage == 1) {
						$$D("totalRecordsLoadedPR").value = "0";
						if (filterSearchFlag == "Filter") {
							setTimeout(function () { $('#searchResultsView').scrollTop(0); }, 500);
							$scope.providerResultsListDym = response.searchResults;
							$$D("totalRecordsLoadedPR").value = parseInt($$D("totalRecordsLoadedPR").value) + parseInt($scope.providerResultsListDym.length);
							setTimeout(function () { $("#providerResultsDym").html($$D("ajaxDynamicListPR").innerHTML); }, 500);
						} else {
							$scope.providerResults = response.searchResults;
							$$D("totalRecordsLoadedPR").value = parseInt($$D("totalRecordsLoadedPR").value) + parseInt($scope.providerResults.length);
							//$$D('loaderProvider').style.display = "none";					
						}
						setTimeout(function () {
							if (parseInt($$D("totalRecordsLoadedPR").value) < toPage || $scope.overAllCourseCount == parseInt($$D("totalRecordsLoadedPR").value)) { $("#viewMoreResultsPR").hide(); }
							else { $("#viewMoreResultsPR").show(); }
							//$$D('loaderProvider').style.display = "none";
						}, 600);
					} else {
						$scope.providerResultsListDym = response.searchResults;
						$$D("totalRecordsLoadedPR").value = parseInt($$D("totalRecordsLoadedPR").value) + parseInt($scope.providerResultsListDym.length);
						setTimeout(function () { //$$D('loaderProvider').style.display = "none"; 
							$("#providerResultsDym").append($$D("ajaxDynamicListPR").innerHTML);
						}, 500);
					}
					//
					if (parseInt($$D("totalRecordsLoadedPR").value) > 0) { $$D('providerResultsFilter').style.display = "block"; }
					$scope.institutionBrowseLogo = Constants.institutionBrowseLogo;
					$scope.subjects = Constants.subjects;
					//
					/*Qualification Filter*/
					$scope.qualificationFilters = response.qualificationFilters;
					angular.forEach($scope.qualificationFilters, function (value, index) {
						if ($scope.qualificationFilters[index].flag == "Y") {
							$$D("oldQualificationValuePR").value = $scope.qualificationFilters[index].qualificaiton;
							$$D("oldQualificationCodeValuePR").value = $scope.qualificationFilters[index].qualificationCode;
						}
					});
					setTimeout(function () {
						if (!isBlankOrNull($("#oldQualificationValuePR").val())) { $("#qualificationSelectedPR").html($$D("oldQualificationValuePR").value); $("#qualificationAddChangeTxtPR").html("Change"); } else { $("#qualificationSelectedPR").html("Not chosen"); $("#qualificationAddChangeTxtPR").html("+ Add"); }
						if (!isBlankOrNull($$D("oldQualificationCodeValuePR").value)) { $("#qualificationCodeSelectedPR").val($$D("oldQualificationCodeValuePR").value); } else { $("#qualificationCodeSelectedPR").val(""); }
					}, 500);
					/*Subject Filter*/
					$scope.subjectfilters = response.subjectfilters;
					angular.forEach($scope.subjectfilters, function (value, index) {
						if ($scope.subjectfilters[index].flag == "Y") {
							$("#subjectSelectedPR").html($scope.subjectfilters[index].subjectName);
							$$D("categoryCodeSelectedPR").value = $scope.subjectfilters[index].categoryCode;
							$$D("categoryIdSelectedPR").value = $scope.subjectfilters[index].searchCategoryId;
							$$D("oldCategoryCodeSelectedPR").value = $scope.subjectfilters[index].categoryCode;
							$$D("oldCategoryIdSelectedPR").value = $scope.subjectfilters[index].searchCategoryId;
							$$D("oldCategoryTextPR").value = $("#subjectSelectedPR").html();
							$$D("subjectResetValuePR").value = $$D("categoryCodeSelectedPR").value + "|" + $$D("categoryIdSelectedPR").value + "|" + $("#subjectSelectedPR").html();
						}
					});
					/*Study mode filter*/
					$scope.studyModeFiltersResults = response.studyModeFilters;
					setTimeout(function () {
						if (!isBlankOrNull($$D("oldStudyModeNameValuePR").value)) { $("#studyModeNameSelectedPR").html($$D("oldStudyModeNameValuePR").value); $("#studyModeAddChangeTxtPR").html("Change"); } else { $("#studyModeNameSelectedPR").html("Not chosen"); $("#studyModeAddChangeTxtPR").html("+ Add"); }
						if (!isBlankOrNull($$D("oldsmTextKeyValuePR").value)) { $("#smTextKeySelectedPR").val($$D("oldsmTextKeyValuePR").value); } else { $("#smTextKeySelectedPR").val(""); }
					}, 500);
					/*Previous qualification filter*/
					$scope.previousQualificationfilters = response.previousQualificationfilters;
					$scope.qualselected = '1|' + $scope.previousQualificationfilters[0].gradeLevel + '|' + $scope.previousQualificationfilters[0].textKey;
					$$D("pqResetValuePR").value = $scope.qualselected;
					angular.forEach($scope.previousQualificationfilters, function (value, index) {
						if (!isBlankOrNull($scope.previousQualificationfilters[index].previousQualSelected)) {
							$scope.qualselected = index + 1 + '|' + $scope.previousQualificationfilters[index].gradeLevel + '|' + $scope.previousQualificationfilters[index].textKey;
						}
					});
					setTimeout(function () { onChageQualGrades($scope.qualselected, 'preselect', 'PR', 'myProviderRootCtrl'); }, 500);
					/*Previous qualification filter*/
					$scope.previousQualificationfilters = response.previousQualificationfilters;
					$scope.qualselected = '1|' + $scope.previousQualificationfilters[0].gradeLevel + '|' + $scope.previousQualificationfilters[0].textKey;
					angular.forEach($scope.previousQualificationfilters, function (value, index) {
						if (!isBlankOrNull($scope.previousQualificationfilters[index].previousQualSelected)) {
							$scope.qualselected = index + 1 + '|' + $scope.previousQualificationfilters[index].gradeLevel + '|' + $scope.previousQualificationfilters[index].textKey;
							if ($scope.previousQualificationfilters[index].textKey == "TARIFF_POINTS") {
								$scope.qualselected = index + 1 + '||' + $scope.previousQualificationfilters[index].textKey;
								setTimeout(function () { $$D("pqTariffSavedValuePR").value = $scope.previousQualificationfilters[index].previousQualSelected; }, 500);
							}
						}
					});
					setTimeout(function () {
						$$D("pqResetValuePR").value = $scope.qualselected; onChageQualGrades($scope.qualselected, 'preselect', 'PR', 'myProviderRootCtrl');

						if (!isBlankOrNull($$D("oldPreviousQualTextPR").value)) { $("#previousQualSelPR").html($$D("oldPreviousQualTextPR").value); } else { $("#previousQualSelPR").html("Not chosen"); }
						if (!isBlankOrNull($$D("oldPreviousQualGradesValuePR").value)) { $("#previousQualGradesSelectedPR").val($$D("oldPreviousQualGradesValuePR").value); } else { $("#previousQualGradesSelectedPR").val(""); }
						if (!isBlankOrNull($$D("oldPreviousQualValuePR").value)) { $("#previousQualSelectedPR").val($$D("oldPreviousQualValuePR").value); } else { $("#previousQualSelectedPR").val(""); }
						if (filterSearchFlag == "Filter") { $("#selectGradesPR").val($scope.qualselected); }
					}, 500);
					//
					if (isBlankOrNull(studyModeTextKey) && isBlankOrNull(previousQualification) && isBlankOrNull(assessmentType) && isBlankOrNull(locationTypeId) && isBlankOrNull(region) && isBlankOrNull(reviewCategory) || (!isBlankOrNull(localStorage.getItem('VIEW_COURSES_FLAG')) && localStorage.getItem('VIEW_COURSES_FLAG') == 'VIEW_COURSES_FLAG')) {
						setTimeout(function () { $(".pr_span").hide(); }, 500);
					} else {
						setTimeout(function () { $(".pr_span").show(); }, 500);
					}
					//			
					/*Assessment Type Filter*/
					$scope.assessmentTypeFilterResults = response.assessmentTypeFilterDetails;
					$scope.assessmentTypeSubFilterResults = [];
					angular.forEach($scope.assessmentTypeFilterResults, function (value, index) {
						if ($scope.assessmentTypeFilterResults[index].assesmentTypeName.toUpperCase() == "EXAM") {
							$scope.assessmentTypeSubFilterResults.push({ "assesmentTypeClassName": "cntry", "assesmentTypeName": $scope.assessmentTypeFilterResults[index].assesmentTypeName, "assesmentTypeId": $scope.assessmentTypeFilterResults[index].assesmentTypeId });
						} else if ($scope.assessmentTypeFilterResults[index].assesmentTypeName.toUpperCase() == "COURSEWORK") {
							$scope.assessmentTypeSubFilterResults.push({ "assesmentTypeClassName": "town", "assesmentTypeName": $scope.assessmentTypeFilterResults[index].assesmentTypeName, "assesmentTypeId": $scope.assessmentTypeFilterResults[index].assesmentTypeId });
						} else if ($scope.assessmentTypeFilterResults[index].assesmentTypeName.toUpperCase() == "PRACTICAL") {
							$scope.assessmentTypeSubFilterResults.push({ "assesmentTypeClassName": "big_city", "assesmentTypeName": $scope.assessmentTypeFilterResults[index].assesmentTypeName, "assesmentTypeId": $scope.assessmentTypeFilterResults[index].assesmentTypeId });
						}
						if ($scope.assessmentTypeFilterResults[index].selectedText == "Y") {
							setTimeout(function () { $("input[value='" + $scope.assessmentTypeFilterResults[index].assesmentTypeId + '|' + $scope.assessmentTypeFilterResults[index].assesmentTypeName + "']").prop('checked', true); }, 500);
						}
					});
					setTimeout(function () {
						if (!isBlankOrNull($$D("oldAssessmentTypeNameSelectedPR").value)) {
							$("#assessmentTypeSelPR").html($$D("oldAssessmentTypeNameSelectedPR").value);
						} else {
							$("#assessmentTypeSelPR").html("Not chosen");
						}
						if (!isBlankOrNull($$D("oldAssessmentTypeSelectedPR").value)) {
							$("#assessmentTypeSelectedPR").val($$D("oldAssessmentTypeSelectedPR").value);
							$("#assessmentTypeChangeTxtPR").html("Change");
						} else {
							$("#assessmentTypeSelectedPR").val(""); $("#assessmentTypeChangeTxtPR").html("+ Add");
						}
					}, 500);
					/*Assessment Filter*/
					/*Review category*/
					$scope.reviewCategoryFilterResults = response.reviewCategoryFilterDetails;
					$scope.reviewCategorySubFilterResults = [];
					angular.forEach($scope.reviewCategoryFilterResults, function (value, index) {
						if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "ACCOMMODATION") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_acm", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "CITY LIFE") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_cl", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "CLUBS AND SOCIETIES") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_cas", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "COURSE AND LECTURERS") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_cal", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "GIVING BACK") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_gb", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "INTERNATIONAL EXPERIENCE") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_int", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "STUDENT SUPPORT") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_ss", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "STUDENT UNION") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_su", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "UNI FACILITIES") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_uf", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						} else if ($scope.reviewCategoryFilterResults[index].reviewCategoryName.toUpperCase() == "JOB PROSPECTS") {
							$scope.reviewCategorySubFilterResults.push({ "reviewCategoryClassName": "pref_jp", "reviewCategoryName": $scope.reviewCategoryFilterResults[index].reviewCategoryName, "textKey": $scope.reviewCategoryFilterResults[index].textKey, "mediaId": $scope.reviewCategoryFilterResults[index].mediaId, "reviewCategoryDisplayName": $scope.reviewCategoryFilterResults[index].reviewCategoryDisplayName });
						}
						if ($scope.reviewCategoryFilterResults[index].selectedText == "Y") {
							setTimeout(function () { $("input[value='" + $scope.reviewCategoryFilterResults[index].textKey + '|' + $scope.reviewCategoryFilterResults[index].reviewCategoryName + "']").prop('checked', true); }, 500);
						}
					});
					setTimeout(function () {
						if (!isBlankOrNull($$D("oldReviewCategoryNameSelectedPR").value)) {
							$("#reviewCategoryNameSelectedPR").html($$D("oldReviewCategoryNameSelectedPR").value);
						} else {
							$("#reviewCategoryNameSelectedPR").html("Not chosen");
						}
						if (!isBlankOrNull($$D("oldReviewCategorySelectedPR").value)) {
							$("#reviewCategorySelectedPR").val($$D("oldReviewCategorySelectedPR").value);
							$("#reviewCategoryAddChangeTxtPR").html("Change");
						} else {
							$("#reviewCategorySelectedPR").val(""); $("#reviewCategoryAddChangeTxtPR").html("+ Add");
						}
					}, 500);

					// --- PAGE VIEW STATS
					
					setTimeout(function () { $$D('loaderProvider').style.display = 'none'; }, 500);
					//
					var page = $$('.page[data-page="providerResults"]')[0].f7PageData;
					if (Object.keys(page.query).length !== 0 && filterSearchFlag == undefined) {
						setTimeout(function () {
							console.log("trigger called");
							if (page.query.action == "shortlist") {
								if (!$("#" + page.query.action_id).hasClass('fav_fill'))
									$("#" + page.query.action_id).trigger('click');
							} else {
								$("#" + page.query.action_id).trigger('click');
							}
						}, 1000);
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					//window.location.replace('../../html/exception/exception.html');						
					mainView.router.loadPage('html/exception/exception.html');
				});
			};
			//
			$scope.goBack = function (event) {
				event.stopPropagation();
				localStorage.setItem('latitude', latitude);
				localStorage.setItem('longitude', longtitude);
				window.history.back();
			}
			//Save assessment type 
			$scope.saveAssessmentTypeSel = function () {
				var assessmentType = $('input:radio[name=assessment-radioPR]:checked').map(function () { return this.value; }).get();
				if (!isBlankOrNull(assessmentType)) {
					var assessmentArray = assessmentType[0].split("|");
				}
				$$D("assessmentTypeSelectedPR").value = assessmentArray[0];
				$$D("assessmentTypeSelPR").innerHTML = assessmentArray[1];
				if (parseInt($$D("totalCourseCountPR").value) == 0) { $('#viewSearchFilterResultsPR').attr("disabled", "disabled"); }
				else { $('#viewSearchFilterResultsPR').removeAttr('disabled'); }
				$("#assessmentTypeChangeTxtPR").html("Change");
			}
			//Save selected qualification
			$scope.saveQualificationSel = function () {
				var qualification = $('input:radio[name=qualificaiton-radioPR]:checked').map(function () { return this.value; }).get();
				var array = qualification[0].split("|");
				$$D("qualificationCodeSelectedPR").value = array[0];
				$("#qualificationSelectedPR").html(array[1]);
				$(".courseCountPR").html($("#showQualificationCountPR").html());
				if (parseInt($$D("totalCourseCountPR").value) == 0) { $('#viewSearchFilterResultsPR').attr("disabled", "disabled"); }
				else { $('#viewSearchFilterResultsPR').removeAttr('disabled'); }
				$("#qualificationAddChangeTxtPR").html("Change");
			}
			//Save selected subject
			$scope.saveSubjectSel = function () {
				var subject = $('input:radio[name=subject-radioPR]:checked').map(function () { return this.value; }).get();
				var array = subject[0].split("|");
				$$D("categoryCodeSelectedPR").value = array[0];
				$$D("categoryIdSelectedPR").value = array[1];
				$("#subjectSelectedPR").html(array[2]);
				$(".courseCountPR").html($("#showSubjectCountPR").html());
				if (parseInt($$D("totalCourseCountPR").value) == 0) { $('#viewSearchFilterResultsPR').attr("disabled", "disabled"); }
				else { $('#viewSearchFilterResultsPR').removeAttr('disabled'); }
			}
			//To get course count
			$scope.getSubjectQualificationCourseCount = function (id, flag) {
				if ($$D(id)) {
					//var codeAndIdArr = $$D(id).value.split("|");
					var codeAndIdArr = $("#myProviderRootCtrl #" + id).val().split("|");
				}
				var categoryCode = ""; var categoryId = ""; var subject = ""; var qualificationId = "";
				var previousQualificationGrade = "";
				var previousQualification = "";
				if (flag == "subject") {
					subject = codeAndIdArr[2];
					categoryCode = codeAndIdArr[0];
					categoryId = codeAndIdArr[1];
					qualificationId = !isBlankOrNull($$D("qualificationCodeSelectedPR").value) ? $$D("qualificationCodeSelectedPR").value : "";
					previousQualificationGrade = !isBlankOrNull($$D("previousQualGradesSelectedforcountPR").value) ? $$D("previousQualGradesSelectedforcountPR").value : "";
					previousQualification = !isBlankOrNull($$D("previousQualSelectedforcountPR").value) ? $$D("previousQualSelectedforcountPR").value : "";
				} else if (flag == "qualification") {
					subject = $$D("subjectSelectedPR").innerHTML;
					categoryCode = !isBlankOrNull($$D("categoryCodeSelectedPR").value) ? $$D("categoryCodeSelectedPR").value : "";;
					categoryId = !isBlankOrNull($$D("categoryIdSelectedPR").value) ? $$D("categoryIdSelectedPR").value : "";
					qualificationId = codeAndIdArr[0];
					previousQualificationGrade = !isBlankOrNull($$D("previousQualGradesSelectedforcountPR").value) ? $$D("previousQualGradesSelectedforcountPR").value : "";
					previousQualification = !isBlankOrNull($$D("previousQualSelectedforcountPR").value) ? $$D("previousQualSelectedforcountPR").value : "";
				} else if (flag == "pre-qualification") {

					subject = $$D("subjectSelectedPR").innerHTML;
					categoryCode = !isBlankOrNull($$D("categoryCodeSelectedPR").value) ? $$D("categoryCodeSelectedPR").value : "";
					categoryId = !isBlankOrNull($$D("categoryIdSelectedPR").value) ? $$D("categoryIdSelectedPR").value : "";
					qualificationId = !isBlankOrNull($$D("qualificationCodeSelectedPR").value) ? $$D("qualificationCodeSelectedPR").value : "";
					previousQualificationGrade = !isBlankOrNull($$D("previousQualGradesSelectedforcountPR").value) ? $$D("previousQualGradesSelectedforcountPR").value : "";
					previousQualification = !isBlankOrNull($$D("previousQualSelectedforcountPR").value) ? $$D("previousQualSelectedforcountPR").value : "";
				}
				if (subject == 'All Subjects') {
					subject = '';
				}
				//$$D('loaderCourse').style.display = 'block';
				var stringJson = {
					"institutionId": institutionId,
					"categoryCode": categoryCode,
					"categoryId": categoryId,
					"qualification": qualificationId,
					"keywordSearch": subject,
					"previousQualification": previousQualification,
					"previousQualificationGrade": previousQualificationGrade,
					"accessToken": "246",
					"affiliateId": "220703"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/course/course-count/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					//$$D('loaderCourse').style.display = 'none';										
					var totalCourseCount = response.totalCourseCount;
					//if(id != "subjectResetValuePR") { $$D("oldTotalCourseCount").value = $$D("totalCourseCount").value; }
					$$D("totalCourseCountPR").value = totalCourseCount;
					var totalCourse = totalCourseCount + " courses";
					if (totalCourseCount <= 1) { totalCourse = totalCourseCount + " course"; }
					if (parseInt($$D("totalCourseCountPR").value) == 0) { $('#viewSearchFilterResultsPR').attr("disabled", "disabled"); }
					else { $('#viewSearchFilterResultsPR').removeAttr('disabled'); }
					if (flag == "qualification") { $("#showQualificationCountPR").html(totalCourse); }
					else if (flag == "subject") { $("#showSubjectCountPR").html(totalCourse); if (totalCourseCount == 0) { $("#showSubjectCountPR").show(); $('#showPreviousQualificationCountPR').show() } else { $("#showSubjectCountPR").hide(); $('#showPreviousQualificationCountPR').hide(); } }
					else if (flag == "pre-qualification") { $("#showSubjectCountPR").html(totalCourse); if (totalCourseCount == 0) { $("#showSubjectCountPR").show(); $('#showPreviousQualificationCountPR').show() } else { $("#showSubjectCountPR").hide(); $('#showPreviousQualificationCountPR').hide(); } }
					if (id == "subjectResetValue" || id == "prevQualCount") { if (totalCourseCount == 0) { $(".courseCountPR").html(totalCourse); $(".courseCountPR").show(); } else { $(".courseCountPR").hide(); } }
					if (id == 'prevQualCount' && totalCourseCount > 0) { $('#previousQualSavBtnPR').removeAttr("disabled"); } else { $('#previousQualSavBtnPR').attr("disabled", "disabled"); }
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					//window.location.replace('../../html/exception/exception.html');						
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//Previous qualification		
			$scope.saveUCASTariff = function () {
				validateSelectedGrades('', '', 'PR', 'myProviderRootCtrl');
			}
			//Study mode
			$scope.saveStudyModeFilter = function () {
				var studyMode = $('input:radio[name=studyMode-radio]:checked').map(function () { return this.value; }).get();
				if (!isBlankOrNull(studyMode)) {
					var array = studyMode[0].split("|");
					$$D("smTextKeySelectedPR").value = array[0];
					$("#studyModeNameSelectedPR").html(array[1]);
					$("#studyModeAddChangeTxtPR").html("Change");
				} else {
					$("#studyModeNameSelectedPR").html("Not chosen"); $("#studyModeAddChangeTxtPR").html("+ Add");
				}
			}
			//Review category
			var reviewCategory = ""; var reviewCategoryName = "";
			$scope.selectReviewCategory = function (index) {
				var strSelectedVals = $("input[name='reviewCategoryPR']:checked").map(function () { return $(this).val(); }).get().join(',');
				var reviewCategoryIds = strSelectedVals.split(",");
				reviewCategory = ""; reviewCategoryName = "";
				if (!isBlankOrNull(reviewCategoryIds) && (reviewCategoryIds.length > 0 && reviewCategoryIds.length <= 3)) {
					for (var lp = 0; lp < reviewCategoryIds.length; lp++) {
						var array = reviewCategoryIds[lp].split("|");
						if (!isBlankOrNull(array[0]) && !isBlankOrNull(array[1])) {
							reviewCategory += array[0] + ",";
							reviewCategoryName += array[1] + ", ";
						}
					}
					$('#reviewCategoryFilterSaveBtnPR').removeAttr("disabled");
				} else {
					lastId = "reviewCategoryPR_" + index;
					$('#' + lastId).prop("checked", false);
					if (isBlankOrNull(reviewCategoryIds)) {
						$('#reviewCategoryFilterSaveBtnPR').attr("disabled", "disabled");
					}
				}
			}
			$scope.saveReviewCategory = function () {
				$scope.selectReviewCategory();
				if (!isBlankOrNull(reviewCategory)) {
					$$D("reviewCategorySelectedPR").value = reviewCategory.substr(0, reviewCategory.length - 1);
					//$("#reviewCategoryNameSelectedPR").html(reviewCategoryName.substr(0, reviewCategoryName.length - 2));
					//
					var reviewCategoryFilterDetails = $scope.reviewCategoryFilterResults;
					var selectedArray = reviewCategoryName.substr(0, reviewCategoryName.length - 2).split(", ");
					var tempReviewCategoryNameArray = [];
					angular.forEach(reviewCategoryFilterDetails, function (value, index) {
						console.log(selectedArray.indexOf(value.reviewCategoryName.trim()))
						if (selectedArray.indexOf(value.reviewCategoryName.trim()) >= 0) {
							tempReviewCategoryNameArray.push(value.reviewCategoryDisplayName);
						}
					});
					reviewCategoryNameText = tempReviewCategoryNameArray.join(", ");
					//
					$("#reviewCategoryNameSelectedPR").html(reviewCategoryNameText);
					$("#reviewCategoryAddChangeTxtPR").html("Change");
				} else {
					$$D("reviewCategorySelectedPR").value = "";
					$("#reviewCategoryNameSelectedPR").html("Not chosen"); $("#reviewCategoryAddChangeTxtPR").html("+ Add");
				}
			}
			//Submit search filters
			$scope.submitSearchFilters = function () {
				if (document.getElementById("stk_app") != null) {
					if ($('#stk_app').html() == '') {
						statusBarFontColor('white');
					} else {
						statusBarFontColor('black');
					}
				}
				removeViewAllFlag();
				$scope.providerResultsDetails(1, 6, 'Filter');
			}
			//
			$scope.providerResultsDetails(1, 6);
			//Below code for navigate to institution profile page
			$scope.goToInstitutionProfilePage = function (institutionId) {//window.location.replace('ins_profile.html');
				localStorage.setItem('universityId', institutionId);
				removeViewAllFlag();
				mainView.router.loadPage('html/institution/ins_profile.html');
			}
			//Below code for open flip content
			$scope.flipFlop = function (id, index, event) {
				if (id == 'flip') {
					$("#percentageMatchPRBlock").html($('#target-' + index).html());
					$("#percentageMatchPRUni").html($("#course-PR-" + index).val());
					$(".reviewCatOnePR").hide(); $(".reviewCatTwoPR").hide(); $(".reviewCatThreePR").hide();
					if (!isBlankOrNull($("#reviewCategoryNameSelectedPR").html())) {
						var reviewArr = $("#reviewCategoryNameSelectedPR").html().split(", ");
						//
						var reviewCategoryFilterDetails = angular.element($$D('myProviderRootCtrl')).scope().getReviewCategoryFilterDetails();
						var selectedArray = $$D("oldReviewCategoryNameSelectedPR").value.split(", ");
						var tempReviewCategoryArray = [];
						angular.forEach(reviewCategoryFilterDetails, function (value, index) {
							if (selectedArray.indexOf(value.reviewCategoryName.trim()) >= 0) {
								tempReviewCategoryArray.push(value.reviewCategoryDisplayName);
							}
						});
						//
						for (var lp = 0; lp < reviewArr.length; lp++) {
							if (lp == 0 && !isBlankOrNull(reviewArr[0])) { $(".reviewCatOnePR").html("<span>" + tempReviewCategoryArray[0] + "</span>"); $(".reviewCatOnePR").show(); }
							else if (lp == 1 && !isBlankOrNull(reviewArr[1])) { $(".reviewCatTwoPR").html("<span>" + tempReviewCategoryArray[1] + "</span>"); $(".reviewCatTwoPR").show(); }
							else if (lp == 2 && !isBlankOrNull(reviewArr[2])) { $(".reviewCatThreePR").html("<span>" + tempReviewCategoryArray[2] + "</span>"); $(".reviewCatThreePR").show(); }
						}
					}
					statusBarFontColor('black');
					myApp.popup('#myProviderRootCtrl .match-popup');
					event.stopPropagation();
					return false;
				} else if (id == 'course-details') {
					//window.location.replace('courseDetails.html');
					mainView.router.loadPage('html/coursedetails/courseDetails.html');
				}
			}
			//Below code for close flip content
			$scope.closeFlipFlop = function (id, index, event) {
				event.stopPropagation();
				if (id == 'flip') {
					$('#target-' + index).slideUp('fast');
					$('#cont-' + index).removeClass("pr_bg");
				} else if (id == 'course-details') {
					//window.location.replace('courseDetails.html');
					mainView.router.loadPage('html/coursedetails/courseDetails.html');
				}
			}
			//Below code for pull to refresh
			var providerContent = $$('#myProviderRootCtrl .pull-to-refresh-content');
			providerContent.on('refresh', function (e) {
				// Emulate 2s loading
				setTimeout(function () {
					//scopeObject.providerResultsDetails('1', '6', 'Filter', '');
					$scope.providerResultsDetails('1', '6', 'Filter', '');
					myApp.pullToRefreshDone();
				}, 3000);
				//
			});
			//
			$scope.courseDetailsPageURL = function (courseId, collegeId, courseName, e) {
				e.stopPropagation();
				localStorage.setItem('courseId', courseId);
				localStorage.setItem('courseName', courseName);
				localStorage.setItem('collegeId', collegeId);
				removeViewAllFlag();
				//window.location.href='../../html/coursedetails/courseDetails.html';
				mainView.router.loadPage('html/coursedetails/courseDetails.html');
			}
			//
			$scope.getShortListFlag = function (institutionId, removeFrom, addTo, rootCtrl, pageFlag) {
				getShortListRootCtrl(institutionId, removeFrom, addTo, $scope, $http, rootCtrl, pageFlag);
			}
			/*
			$$('.clk_im, .clk_im1').on('click', function(){
			$(this).parents('.popup-qual, .popup-howstudy ').removeClass('flip-out');
			$(this).parents('.popup-qual, .popup-howstudy ').addClass('flip-in');
			});
			//
			$$('.popup-qualttip .back,.popup-howttip .back').on('click', function(){
			$(this).parents('body').find('.popup-qual,.popup-howstudy').removeClass('flip-in');
			$(this).parents('body').find('.popup-qual').addClass('flip-out');
			});
			*/
		}])

		.controller("institutionhomeCtrl", function ($scope, $http, $sce, $timeout, $compile) {
			$scope.emailClientConsentFlagIP = "N";
			$scope.prospectusClientConsentFlagIP = "N";
			$scope.currentSlideIndex = 0;

			var customDimensionUniversityName;
			// XtremePush Event and Tag Tracking
			//

			xtremePushTag("User_Views_An_IP_Page", localStorage.getItem('universityId'));
			var xtremePushIPVisit = "IP - " + localStorage.getItem('universityId');
			xtremePushEvent(xtremePushIPVisit);
			//
			function onDeviceReady() {
				statusBarFontColor('white');
			}
			document.addEventListener("deviceready", onDeviceReady, false);
			$scope.getSafeHtml = function (x) {
				return $sce.trustAsHtml(x);
			};
			$scope.urlFormat = function (name) {
				name = name.replace(/ /g, "_").toLowerCase();
				return name;
			}
			$scope.isEmpty = function (obj) {
				for (var prop in obj) {
					if (obj.hasOwnProperty(prop))
						return false;
				}
				return true;
			}
			$scope.viewAllRating = function () {
				if ($scope.ratelimit == 3) {
					$scope.ratelimit = $scope.reviewStarRating.length;
					$$D("viewMoreContent").innerHTML = "- View less";
					var height = parseInt($('.gallery-top .swiper-wrapper').css('height').replace("px", "")) + 200;
					$('.gallery-top .swiper-wrapper').css('height', height + "px");
				} else {
					$scope.ratelimit = 3;
					$$D("viewMoreContent").innerHTML = "+ View more";
					var height = parseInt($('.gallery-top .swiper-wrapper').css('height').replace("px", "")) - 200;
					$('.gallery-top .swiper-wrapper').css('height', height + "px");
				}
			}
			$scope.initItem = function (el) {
				$timeout(function () {
					angular.element(el).addClass('visible');
				}, 0);
			}
			$scope.universityGallery = function () {
				//googleTagManagerEventTrack(null, 'Provider Nav', 'Image Gallery', $scope.university.name, '0');
				var myPhotoBrowserStandalone = myApp.photoBrowser({
					photos: $scope.photos,
					theme: 'dark',
					swipeToClose: false
				});
				if ($scope.photos.length > 0) {
					statusBarFontColor('white');
					myPhotoBrowserStandalone.open();
				} else {
					myApp.alert("No Photos Available")
				}
				//
				applyIconOnPause();
				//
			}
			$scope.ipvisitWebsite = function () {
				$scope.appInteractionLogging('HOTCOURSES: WEBSITE CLICK', encodeURI($scope.visitWebsite));
				var uniName = $scope.university.name;
				if (!isBlankOrNull(uniName)) {
					uniName = uniName.toLowerCase();
				}

				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}
				var interactionPagename = ""
				if ($scope.checkAdvUniversity == 'N') {
					interactionPagename = '/institution-profile/non-advertiser/' + localStorage.getItem('universityId');
				} else {
					interactionPagename = '/institution-profile/advertiser/' + localStorage.getItem('universityId');
				}
				var collegeId = localStorage.getItem('universityId');
				firebaseEventTrack('ecommercepurchase', { coupon: 'interaction_webclick', currency: "GBP", value: 0, transaction_id: collegeId, location: uniName.toLowerCase(), item_name: firebaseSubject, interaction_pagename: interactionPagename  });

				//googleTagManagerEventTrack(null, 'Interaction', 'Webclick', uniName, encodeURI($scope.visitWebsite));
				window.open(encodeURI($scope.visitWebsite), '_system');
			}
			$scope.goBack = function () {
				localStorage.setItem('latitude', latitude);
				localStorage.setItem('longitude', longtitude);
				//window.history.back();
				mainView.router.back({ 'pushState': false });
			};
			$scope.reviewcheck = function (name1, name2) {
				if (name1 != undefined && name2 != undefined) {
					name1 = name1.replace("&", "and");
					name2 = name2.replace("&", "and");
					if (name1.toLocaleLowerCase().indexOf(name2.toLocaleLowerCase()) != -1) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			}

			$scope.reviewcheckSection = function (section) {
				if (section != undefined) {
					//name1=name1.replace("&", "and");
					section = section.replace("&", "and");
					section = section.replace("Uni ", "");
					if ($scope.reviewSection.indexOf(section.toLocaleLowerCase()) != -1) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
			$scope.reviewUrlWebsiteFn = function () {
				//googleTagManagerEventTrack(null, 'Link', 'Reviews', '', encodeURI($scope.reviewUrlWebsite));
				window.open(encodeURI($scope.reviewUrlWebsite), '_system');
			}
			$scope.googleMap = function () {
				var platform = device.platform.toLowerCase();
				var scheme = "";
				if (platform == "android") {
					scheme = 'com.google.android.apps.maps';
				} else if (platform == "ios") {
					// scheme = 'comgooglemaps://';
					scheme = 'maps://';
				}
				var addressLongLat = $scope.university.latitude + ',' + $scope.university.longitude;
				var universityName = $scope.university.name;
				//window.open("http://maps.google.com/?q="+addressLongLat, '_system');

				var mapOption = '';
				if (Constants.devicePlatform == 'IOS') {
					mapOption = 'clearcache=yes,location=no,hardwareback=no,closebuttoncaption=close,hidenavigationbuttons=yes,toolbarposition=top';
				} else {
					//option = 'clearcache=yes,location=no,hardwareback=no,closebuttoncaption=close,footer=yes,footercolor=#333333';
					mapOption = 'clearcache=yes,location=yes,hardwareback=no,closebuttoncaption=close,footer=no,hidenavigationbuttons=yes,hideurlbar=yes,toolbarcolor=#333333';
				}
				window.open("http://maps.google.com/?q=" + addressLongLat, '_blank', mapOption);

				/*
				appAvailability.check(
					scheme,
					function() {
						if(platform == "android") {
							window.open("geo:"+addressLongLat+"?q="+universityName+'@'+addressLongLat, "_system");
						} else if(platform == "ios") {
							var mapLocationUrl = 'http://maps.apple.com/?q='+universityName+'@'+addressLongLat+"&ll="+addressLongLat+"&z=1";
							window.open(encodeURI(mapLocationUrl), '_system');
						}
					},
					function() {
						window.open("http://maps.google.com/?q="+addressLongLat, '_system');
					}
				);*/
			}
			$scope.universityClick = function (collegeId, obj) {
				localStorage.setItem('universityId', collegeId);
				//window.reload();
				//location.reload();

				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}

				firebaseEventTrack('viewitem', { origin: 'recommended', item_category: obj.collegeName.toLowerCase(), item_name: firebaseSubject.toLowerCase(), item_id: obj.collegeId, search_term: "na" });

				mainView.router.reloadPage('html/institution/ins_profile.html');
			}
			$scope.emailuni = function (num) {
				if ($scope.enquiryDetails.emailWebform != null) {
					$scope.appInteractionLogging('HOTCOURSES: EMAIL SENT: WEBSITE CLICK', encodeURI($scope.enquiryDetails.emailWebform));
					window.open(encodeURI($scope.enquiryDetails.emailWebform), '_system');
					myApp.closeModal('.second_page');
				} else {

					var userID = localStorage.getItem('LOGGED_IN_USER_ID');

					firebaseEventTrack('screenview', { 'page_name': '/email/' + localStorage.getItem('universityId') });

					emailUniPopup(num);
					//window.location.href='../../html/institution/emailuni.html';
				}
			}
			$scope.emailCheckGuestUser = function () {
				if (isGuestUser()) {
					localStorage.setItem('action', "ipEmailuni");
					localStorage.setItem('action_id', "ip_page_email_uni");
					Constants.mainViewObj.router.loadPage({ url: "html/user/home.html", pushState: false });
				} else {
					$scope.emailClientConsentFlagIP = "N";
					$("#emailClientConsentFlagIP").prop('checked', false);
					myApp.popup('#ip_page_email_uni_popup');
					$scope.interactionEventLogging('EMAIL');
					$scope.emailuni(11);

				}
			}

			$scope.prospectusCheckGuestUser = function () {
				if (isGuestUser()) {
					localStorage.setItem('action', "ipProspectus");
					localStorage.setItem('action_id', "ip_req_props");
					Constants.mainViewObj.router.loadPage({ url: "html/user/home.html", pushState: false });
				} else {
					$scope.prospectusClientConsentFlagIP = "N";
					$("#prospectusClientConsentFlagIP").prop('checked', false);
					myApp.popup('#ip_req_props_popup');
					$scope.interactionEventLogging('PROSPECTUS');
					$scope.prospectus(7);

				}
			}

			$scope.prospectus = function (num) {
				if ($scope.enquiryDetails.prospectusWebform != null) {
					$scope.appInteractionLogging('HOTCOURSES: EMAIL SENT: REQUEST PROSPECTUS: WEBSITE CLICK', encodeURI($scope.enquiryDetails.prospectusWebform));
					window.open(encodeURI($scope.enquiryDetails.prospectusWebform), '_system');
					myApp.closeModal('.req_props');
				} else {

					var userID = localStorage.getItem('LOGGED_IN_USER_ID');

					firebaseEventTrack('screenview', { 'page_name': '/prospectus/' + localStorage.getItem('universityId') });

					if ($$D('prospectusSubBtnIP')) {
						if ($('#prospectusSubBtnIP').is(':disabled')) {
							$("#prospectusSubBtnIP").removeAttr('disabled');
						}
					}
					prospectusPopup(num);
					//window.location.href='../../html/institution/prospectus.html';
				}
			}
			//
			$scope.opendayCountCheck = function() {
				if($scope.moreOpendayList.length===1) {
					var url = $scope.opendayList[0].bookingUrl;
					console.log("openday url",url);
					$scope.reservePlaceLogging();
					//$scope.reservePlaceActionLog();
					inAppBrowser(url);
				} else {
					localStorage.setItem('openday_college_id', localStorage.getItem('universityId'));
					localStorage.setItem('openday_event_id', "");
					mainView.router.loadPage('html/openday/openDays.html');
				}
			}
			//
			$scope.reservePlace = function (opendayEventId, opendate) {
				collegeId = !isBlankOrNull(localStorage.getItem('universityId')) ? localStorage.getItem('universityId') : "";

				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"accessToken": "246",
					"affiliateId": "220703",
					"eventId": opendayEventId,
					"collegeId": collegeId,
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/enquiry/reserve-open-days/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;

					//googleTagManagerEventTrack(null, 'interaction', 'Webclick', response.opendayList[0].collegeName, '100');

					//googleTagManagerEventTrack(null, 'open-days', 'reserve-place', response.opendayList[0].collegeName, '1');				

					$scope.opendayList = response.opendayList;
					$scope.moreOpendayList = response.moreOpendayList;
					if ($scope.moreOpendayList.length > 1) {
						angular.forEach($scope.moreOpendayList, function (value, index) {
							if ($scope.moreOpendayList[index].selectedEvent == "Y") {
								//$("#multipleOppDetail").show();
								$scope.obselected = $scope.moreOpendayList[index].eventId;
								//$("#moreOpendays").html($scope.moreOpendayList[index].opportunity); 
							}
						});
					}
					//reservePlacePopup();
				});
			}
			$scope.goToBookingUrl = function () {
				window.open($scope.opendayList[0].bookingUrl, '_system');
			}
			//
			$scope.reservePlaceLogging = function (interationType) {
				var uniName = $scope.university.name;
				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}
				var collegeId = localStorage.getItem('universityId');
				var interactionPagename = ""
				if ($scope.checkAdvUniversity == 'N') {
					interactionPagename = '/institution-profile/non-advertiser/' + localStorage.getItem('universityId');
				} else {
					interactionPagename = '/institution-profile/advertiser/' + localStorage.getItem('universityId');
				}
				firebaseEventTrack('ecommercepurchase', { coupon: 'interaction_reserve_a_place', currency: "GBP", value: 0, transaction_id: collegeId, location: uniName.toLowerCase(), item_name: firebaseSubject, interaction_pagename: interactionPagename  });
				//window.open($scope.opendayList[0].bookingUrl, '_system');
				//googleTagManagerEventTrack(null, 'Interaction', 'Reserve a Place', uniName, $scope.opendayList[0].bookingUrl);
				$scope.reservePlaceStats();
			}
			//			
			$scope.reservePlaceStats = function () {
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var collegeId = localStorage.getItem('universityId');
				var suborderItemId = null;
				suborderItemId = $scope.opendayList[0].subOrderItemId;
				var extraText = null;
				extraText = encodeURI($scope.opendayList[0].bookingUrl);
				var stringJson = {
					"userId": userId,
					"collegeId": collegeId,
					"courseId": "0",
					"userIp": "",
					"userAgent": navigator.userAgent,
					"jsLog": 'Y',
					"suborderItemId": suborderItemId,
					"profileId": "",
					"typeName": "",
					"activity": "HOTCOURSES: OPEN DAYS: WEBSITE CLICK",
					"extraText": extraText,
					"latitude": latitude,
					"longitude": longtitude,
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/interaction/db-stats/logging/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					//$$D('loaderCourse').style.display = 'none';
					var logStatusId = response.logStatusId;
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					// do nothing				
				});
			}

			$scope.reservePlaceActionLog = function () {
				console.log("opendate-->" + $scope.opendayList[0].odMonthYear);
				var date = getCurrentDate();
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"actionType": "OD",
					"actionDate": "",
					"actionStatus": "C",
					"actionUrl": "",
					"actionKeyId": localStorage.getItem('universityId'),
					"actionDetails": $scope.opendayList[0].odMonthYear,
					"trackSessionId": "",
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/save-user-actions/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;

				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}

			$scope.reserveopenday = function () {
				//var collegeId  = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";
				var collegeId = !isBlankOrNull(localStorage.getItem('universityId')) ? localStorage.getItem('universityId') : "";
				firebaseEventTrack('screenview', { 'page_name': '/open-day/' + collegeId });

				reserveopendayPopup();

			}
			$scope.openMoreDates = function () {
				if (Constants.devicePlatform == 'IOS')
					Keyboard.hideFormAccessoryBar(false);
				setTimeout(function () {
					if (Constants.devicePlatform == 'IOS') {
						$("#ipmoreOpendays").focus();
					}
				}, 50);

			}
			//

			$scope.appInteractionLogging = function (activity, extraTextURL, sectionProfileId, sectionTypeName, tabIndex = 0) {
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var collegeId = localStorage.getItem('universityId');
				var courseId = null;
				var userAgent = navigator.userAgent;
				var userIp = null;
				var suborderItemId = null;
				var profileId = null;
				if ($scope.universityTabs.length > 0) {
					console.log("tabIndex > ", tabIndex)
					suborderItemId = $scope.universityTabs[tabIndex].suborderItemId;
				}

				if (activity == "HOTCOURSES: CLEARING: WEBSITE CLICK" || activity == "HOTCOURSES: WEBSITE CLICK") {
					suborderItemId = $scope.universityTabs[$scope.currentSlideIndex].suborderItemId;
				}

				if (!isBlankOrNull(sectionProfileId)) {
					profileId = sectionProfileId;
				}
				var typeName = null;
				if (!isBlankOrNull(sectionTypeName)) {
					typeName = sectionTypeName;
				}
				var activity = activity;
				var extraText = null;
				if (!isBlankOrNull(extraTextURL)) {
					extraText = extraTextURL;
				}
				var stringJson = {
					"userId": userId,
					"collegeId": collegeId,
					"courseId": courseId,
					"userIp": userIp,
					"userAgent": userAgent,
					"jsLog": 'Y',
					"suborderItemId": suborderItemId,
					"profileId": profileId,
					"typeName": typeName,
					"activity": activity,
					"extraText": extraText,
					"latitude": latitude,
					"longitude": longtitude,
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/interaction/db-stats/logging/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					//$$D('loaderCourse').style.display = 'none';
					var logStatusId = response.logStatusId;
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					// do nothing				
				});
			}

			$scope.university = [];
			$scope.keyStatsInfo = {};
			$scope.accommodationCost = {};
			$scope.opendayDetails = {};
			$scope.studentReview = {}
			$scope.photos = [];
			$scope.competitorDetails = [];
			$scope.ratelimit = 0;
			$scope.enquiryDetails = {};
			$scope.institutionBrowseLogo = Constants.institutionBrowseLogo;
			$scope.matchPecenatgeFlag;

			var searchCategoryCode = "";
			var searchCategoryId = "";
			var keywordSearch = "";
			var qualification = "";
			var orderBy = "";
			var locationType = "";
			var region = "";
			var regionFlag = "";
			var studyMode = "";
			var previousQual = "";
			var previousQualGrade = "";
			var assessmentType = "";
			var reviewCategory = "";
			var reviewCategoryName = "";
			var jacsCode = "";
			//
			if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
				var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);

				searchCategoryCode = !isBlankOrNull(recentSearchCriteria.searchCategoryCode) ? recentSearchCriteria.searchCategoryCode : "";
				searchCategoryId = !isBlankOrNull(recentSearchCriteria.searchCategoryId) ? recentSearchCriteria.searchCategoryId : "";
				keywordSearch = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "";
				qualification = !isBlankOrNull(recentSearchCriteria.qualification) ? recentSearchCriteria.qualification : "";
				orderBy = !isBlankOrNull(recentSearchCriteria.orderBy) ? recentSearchCriteria.orderBy : "";
				locationType = !isBlankOrNull(recentSearchCriteria.locationType) ? recentSearchCriteria.locationType : "";
				region = !isBlankOrNull(recentSearchCriteria.region) ? recentSearchCriteria.region : "";
				regionFlag = !isBlankOrNull(recentSearchCriteria.regionFlag) ? recentSearchCriteria.regionFlag : "";
				studyMode = !isBlankOrNull(recentSearchCriteria.studyMode) ? recentSearchCriteria.studyMode : "";
				previousQual = !isBlankOrNull(recentSearchCriteria.previousQual) ? recentSearchCriteria.previousQual : "";
				previousQualGrade = !isBlankOrNull(recentSearchCriteria.previousQualGrade) ? recentSearchCriteria.previousQualGrade : "";
				assessmentType = !isBlankOrNull(recentSearchCriteria.assessmentType) ? recentSearchCriteria.assessmentType : "";
				reviewCategory = !isBlankOrNull(recentSearchCriteria.reviewCategory) ? recentSearchCriteria.reviewCategory : "";
				reviewCategoryName = !isBlankOrNull(recentSearchCriteria.reviewCategoryName) ? recentSearchCriteria.reviewCategoryName : "";
				jacsCode = !isBlankOrNull(recentSearchCriteria.jacsCode) ? recentSearchCriteria.jacsCode : "";
				//
				if (!isBlankOrNull(region) || !isBlankOrNull(locationType) || !isBlankOrNull(studyMode) || !isBlankOrNull(previousQual) || !isBlankOrNull(assessmentType) || !isBlankOrNull(reviewCategory)) {
					$scope.matchPecenatgeFlag = true;
				}
			}
			//	
			var reviewSubjectOne = ""; var reviewSubjectTwo = ""; var reviewSubjectThree = "";
			if (!isBlankOrNull(reviewCategory)) {
				reviewCategoryArray = reviewCategory.split(",");
				if (reviewCategoryArray.length == 3) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; reviewSubjectThree = reviewCategoryArray[2]; }
				else if (reviewCategoryArray.length == 2) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; }
				else { reviewSubjectOne = reviewCategoryArray[0]; }
			}
			//	
			var stringJson = {
				"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
				"collegeId": localStorage.getItem('universityId'),
				// "collegeId":3737,
				"userIp": "000.000.000.000",
				"userAgent": navigator.userAgent,
				"accessToken": "246",
				"affiliateId": "220703",
				"appVersion": Constants.appVersion,
				"searchCategoryCode": searchCategoryCode,
				"searchCategoryId": searchCategoryId,
				"keywordSearch": keywordSearch,
				"qualification": qualification,
				"orderBy": orderBy,
				"locationType": locationType,
				"region": region,
				"regionFlag": regionFlag,
				"studyMode": studyMode,
				"previousQual": previousQual,
				"previousQualGrade": previousQualGrade,
				"assessmentType": assessmentType,
				"reviewSubjectOne": reviewSubjectOne,
				"reviewSubjectTwo": reviewSubjectTwo,
				"reviewSubjectThree": reviewSubjectThree,
				"jacsCode": jacsCode
			}


			var jsonData = JSON.stringify(stringJson);
			var url = "https://mtest.whatuni.com/wuappws/profile/uni-info/";
			var req = {
				method: 'POST',
				url: url,
				headers: { 'Content-Type': 'application/json' },
				data: jsonData
			}

			// $scope.goOpendayProviderLanding = function () {
			// 	var id = localStorage.getItem('universityId')
			// 	localStorage.setItem('openday_college_id', id);
			// 	mainView.router.loadPage('html/openday/openDays.html');
			// }

			var galleryNextSlide = null;
			//var galleryThumbs=null;
			$scope.getUniversity = function (arg) {
				//$scope.universityTabs=[];
				//$$D('loaderInstitution').style.display = 'block';
				//$$D('loaderInstitutionView').style.display = 'none';
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('loaderInstitution').style.display = 'none';
					//$scope.landingOdFlag = response.landingOdFlag;

					//Latest News
					$scope.articleFlag = response.articleFlag;
					$scope.articleContent = response.articleContent;
					$scope.mapBoxKey = response.mapBoxKey;


					angular.forEach(response.loadYearList, function (value, key) {
						if (value.loadYearName == "KIS_LOAD_YEAR") {
							Constants.kisLoadYear = value.loadYearData;
						}
						if (value.loadYearName == "HESA_LOAD_YEAR") {
							Constants.hesaLoadYear = value.loadYearData;
						}
						if (value.loadYearName == "HC_LOAD_YEAR") {
							Constants.hcLoadYear = value.loadYearData;
						}
						if (value.loadYearName == "ASSESSED_LOAD_YEAR") {
							Constants.assessedLoadYear = value.loadYearData;
						}
					});

					$scope.checkAdvUniversity = response.advertiserFlag;
					$scope.opendayFlag = response.opendayFlag;
					if ($scope.opendayFlag == "Y") {
						$scope.reservePlace(null, null);
					}
					checkAdvUniversity = response.advertiserFlag;
					if ($scope.checkAdvUniversity == 'N') {
						statusBarFontColor('white');
					}
					//$scope.checkAdvUniversity = 'Y';
					//myApp.alert($scope.checkAdvUniversity);

					//Commented by Keerthika - for maintain the non adv background images consistent throughout the journey
					var background = (!isBlankOrNull(response.uniInfo[0].tileMediaPath) ? response.uniInfo[0].tileMediaPath : !isBlankOrNull(localStorage.getItem('tileMediaPath')) ? localStorage.getItem('tileMediaPath') : UniRandomImage());
					//var background = !isBlankOrNull(localStorage.getItem('tileMediaPath')) ? localStorage.getItem('tileMediaPath') : UniRandomImage();
					var className = !isBlankOrNull(response.uniInfo[0].tileMediaPath) ? 'usr_sec pr_grdnt' : 'usr_sec';
					$scope.university = {
						"id": localStorage.getItem('universityId'),
						"name": response.uniInfo[0].collegeName,
						"collegeLogo": response.uniInfo[0].collegeLogo,
						"logoImage": background,
						"backgroundImageClassName": className,
						"logoName": response.uniInfo[0].logoName,
						"backgroundImage": background,
						"reviewCount": response.uniInfo[0].reviewCount,
						"overallRating": response.uniInfo[0].overallRating,
						"reviewRatingDisplay": response.uniInfo[0].reviewRatingDisplay,
						"pullQuotes": response.uniInfo[0].pullQuotes,
						"matchingPercentage": response.uniInfo[0].matchingPercentage,
						"latitude": response.uniInfo[0].latitude,
						"longitude": response.uniInfo[0].longitude,
						"addressLineOne": response.uniInfo[0].addressLineOne,
						"addressLineTwo": response.uniInfo[0].addressLineTwo,
						"town": response.uniInfo[0].town,
						"countryState": response.uniInfo[0].countryState,
						"postcode": response.uniInfo[0].postcode,
						"nearestTrainStation": response.uniInfo[0].nearestTrainStation,
						"distanceFromTrainStn": response.uniInfo[0].distanceFromTrainStn,
						"nearestTubeStation": response.uniInfo[0].nearestTubeStation,
						"distanceFromTubeStn": response.uniInfo[0].distanceFromTubeStn,
						"isInLondon": response.uniInfo[0].isInLondon,
						"courseExistFlag": response.uniInfo[0].courseExistFlag,
						"shortListFlag": response.uniInfo[0].shortListFlag
					};
					customDimensionUniversityName = $scope.university.name;
					if (response.keyStatsInfo.length > 0) {
						$scope.keyStatsInfo = {
							"noOfStudents": response.keyStatsInfo[0].noOfStudents,
							"wuscaRanking": response.keyStatsInfo[0].wuscaRanking,
							"totalWuscaRanking": response.keyStatsInfo[0].totalWuscaRanking,
							"timesRanking": response.keyStatsInfo[0].timesRanking,
							"totalTimesRanking": response.keyStatsInfo[0].totalTimesRanking,
							"fullTime": response.keyStatsInfo[0].fullTime,
							"partTime": response.keyStatsInfo[0].partTime,
							"schoolLeavers": response.keyStatsInfo[0].schoolLeavers,
							"mature": response.keyStatsInfo[0].mature,
							"ukStudents": response.keyStatsInfo[0].ukStudents,
							"international": response.keyStatsInfo[0].international,
							"undergraduate": response.keyStatsInfo[0].undergraduate,
							"postgraduate": response.keyStatsInfo[0].postgraduate,
							"employmentRate": response.keyStatsInfo[0].employmentRate,
							"whoAreTheyFlag": "",
							"wuscaRankingFlag": "",
							"jobProspectusFlag": ""
						};
					}
					//
					if (isBlankOrNull($scope.keyStatsInfo.fullTime) && isBlankOrNull($scope.keyStatsInfo.partTime) && isBlankOrNull($scope.keyStatsInfo.schoolLeavers) && isBlankOrNull($scope.keyStatsInfo.mature) && isBlankOrNull($scope.keyStatsInfo.ukStudents) && isBlankOrNull($scope.keyStatsInfo.international) && isBlankOrNull($scope.keyStatsInfo.undergraduate) && isBlankOrNull($scope.keyStatsInfo.postgraduate)) {
						$scope.keyStatsInfo.whoAreTheyFlag = "N";
					}
					//
					if (isBlankOrNull($scope.keyStatsInfo.noOfStudents) && isBlankOrNull($scope.keyStatsInfo.wuscaRanking) && isBlankOrNull($scope.keyStatsInfo.timesRanking)) {
						$scope.keyStatsInfo.wuscaRankingFlag = "N";
					}
					//
					if (isBlankOrNull($scope.keyStatsInfo.employmentRate)) {
						$scope.keyStatsInfo.jobProspectusFlag = "N";
					}
					//
					// if (response.opendayDetails.length > 0) {
					// 	$scope.opendayDetails = response.opendayDetails;
					// 	// alert("not empty")
					// } else {
					// 	//alert("empty")
					// }
					// console.log("open days");
					// console.log($scope.opendayDetails);
					$scope.reviewUrlWebsite = response.universityCourseReviewURL;
					$scope.dateConversion = function (date, time) {
						time = time.replace(/ /g, "");
						var dateFormat = "";
						var dateFormatArray = date.split(" ");
						dateFormatArray[0] = dateFormatArray[0].replace(/[^0-9\.]+/g, "")
						var dateFormat = dateFormatArray.join(" ") + " " + time;

						timestamp = time.replace(/[ap]m$/i, '');
						timestamp = timestamp.replace(".", ':');
						var timeFormatArray = timestamp.split(":");
						if (time.toLowerCase().indexOf("pm") != -1) {
							if (timeFormatArray[0] != "12") {
								timestamp = parseInt(timeFormatArray[0]) + 12;
								timeFormatArray[0] = timestamp;
							}
						}
						if (timeFormatArray[0] == "12" && time.toLowerCase().indexOf("pm") == -1) {
							timeFormatArray[0] = "00";
						}
						var dateFormat = dateFormatArray.join(" ") + " " + timeFormatArray.join(":") + ":" + "00";
						return dateFormat;
					}
					$scope.eventSaveTrack = function (eventId, openDate, time, subOrderItemId, returnFlag, opendayId) {
						if (isGuestUser()) {
							var id = "saveToCalendarLbl-" + opendayId;
							console.log("id - " + id);
							localStorage.setItem('action', "saveopenday");
							localStorage.setItem('action_id', id);
							Constants.mainViewObj.router.loadPage({ url: "html/user/home.html", pushState: false });
						} else {
							//
							//googleTagManagerEventTrack(null, 'Provider Nav', 'Add to WU Calender', $scope.university.name, '0');																
							//					
							localStorage.removeItem('eventId');
							localStorage.removeItem('openDate');
							localStorage.removeItem('time');
							localStorage.removeItem('subOrderItemId');
							localStorage.removeItem('opendayId');
							//					
							localStorage.setItem('eventId', eventId);
							localStorage.setItem('openDate', openDate);
							localStorage.setItem('time', time);
							localStorage.setItem('subOrderItemId', subOrderItemId);
							localStorage.setItem('opendayId', opendayId);
							//		
							openDate = getDate(openDate);

							//
							$('#addToCalendar').removeClass('btn_fl');
							$scope.saveTocal();
						}
					}
					$scope.saveToDB = function (calenderPermission) {
						var eventId = localStorage.getItem('eventId');
						var openDate = localStorage.getItem('openDate');
						var subOrderItemId = localStorage.getItem('subOrderItemId');
						var opendayId = localStorage.getItem('opendayId');
						openDate = getDate(openDate);
						var stringJson = {
							"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
							"institutionId": localStorage.getItem('universityId'),
							"accessToken": "246",
							"affiliateId": "220703",
							"eventId": eventId,
							"openDate": openDate,
						}
						var jsonData = JSON.stringify(stringJson);
						var url = "https://mtest.whatuni.com/wuappws/enquiry/save-open-days/";
						var req = {
							method: 'POST',
							url: url,
							headers: { 'Content-Type': 'application/json' },
							data: jsonData
						}
						$http(req).then(function (httpresponse) {
							var response = httpresponse.data;
							$scope.returnFlag = response.returnFlag.split('##SPLIT##');

							if ($scope.returnFlag[0] == 'ISEXISTS') {
								var calMsg = "Thanks, we've added the open day to your phone calendar and we'll send you an email reminder so you don't forget the date";
								if (calenderPermission == 'NOT_ALLOWED') {
									calMsg = "Ok we haven't added the open day to your calendar. If you want to undo this then you’ll need to allow us permission to access your calendar in your device settings.";
								}
								$$D('calMsg').innerHTML = calMsg;
								$$D('addCalendarPopup').style.display = 'block';
								$$D('closePicker').style.display = 'block';
								console.log("saveToDB closePicker block");
								myApp.pickerModal('.my_page');
							}
							if ($scope.returnFlag[0] == 'SUCCESS') {
								var calMsg = "Thanks, we've added the open day to your phone calendar and we'll send you an email reminder so you don't forget the date";
								if (calenderPermission == 'NOT_ALLOWED') {
									calMsg = "Ok we haven't added the open day to your calendar. If you want to undo this then you’ll need to allow us permission to access your calendar in your device settings.";
								}
								$$D('calMsg').innerHTML = calMsg;
								$$D('addCalendarPopup').style.display = 'block';
								$$D('closePicker').style.display = 'block';
								//
								$("#saveToCalendarLbl-" + opendayId).html("Saved to calendar");
								$("#saveToCalendarLbl-" + opendayId).attr("disabled", "disabled");
								myApp.pickerModal('.my_page');
								var stringJson = {
									"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
									"institutionId": localStorage.getItem('universityId'),
									"courseId": "",
									"userIP": "",
									"userAgent": navigator.userAgent,
									"jsLog": "Y",
									"subOrderItemId": subOrderItemId,
									"profileId": "",
									"typeName": "",
									"activity": "HOTCOURSES: OPEN DAYS: ADD CALENDAR",
									"extraAgent": "",
									"appVersion": Constants.appVersion,
									"accessToken": "246",
									"affiliateId": "220703",
									"latitude": latitude,
									"longitude": longtitude
								}
								var jsonData = JSON.stringify(stringJson);
								var url = "https://mtest.whatuni.com/wuappws/open-days/stats-logging/";
								var req = {
									method: 'POST',
									url: url,
									headers: { 'Content-Type': 'application/json' },
									data: jsonData
								}
								$http(req).success(function (response) {
									var response = httpresponse.data;

								}).error(function (response, status) {

								});
							}
						}, function (response, status) {
						});
					}

					$scope.saveTocal = function () {
						//
						//googleTagManagerEventTrack(null, 'Provider Nav', 'Add Phone Calender', $scope.university.name, '0');
						//						
						var eventId = localStorage.getItem('eventId');
						var date = localStorage.getItem('openDate');
						var time = localStorage.getItem('time');
						//					
						if (!isBlankOrNull(eventId) && !isBlankOrNull(date) && !isBlankOrNull(time)) {
							var startTime = "";
							var endTime = "";
							if (time.indexOf('-') != -1) {
								var timeArray = time.split("-");
								startTime = timeArray[0];
								endTime = timeArray[1];
							} else {
								startTime = time;
								endTime = time;
							}
							var dateFormat_1 = $scope.dateConversion(date, startTime);
							var dateFormat_2 = $scope.dateConversion(date, endTime);
							var universityName = $scope.university.name;

							if (startTime == endTime) {
								var milliseconds = new Date(dateFormat_2).getTime() + (3 * 60 * 60 * 1000);
								var later = new Date(milliseconds);
								dateFormat_2 = milliseconds;
							}
							addToDeviceCalender('event1', universityName + " Open Day", "", "", dateFormat_1, dateFormat_2);
						} else {

							var startTime = "10:00 am";
							var endTime = "10:00 am";

							var dateFormat_1 = $scope.dateConversion(date, startTime);
							var dateFormat_2 = $scope.dateConversion(date, endTime);
							var universityName = $scope.university.name;
							addToDeviceCalender('event1', universityName + " Open Day", "", "", dateFormat_1, dateFormat_2);

						}
					}
					$scope.costConversion = function (costValue) {
						var costConversionValue;
						if (!isBlankOrNull(costValue)) {
							if (costValue.indexOf('-') != -1) {
								var costConversionValueArray = costValue.split("-");
								costConversionValue = "£" + costConversionValueArray[0] + " - " + "£" + costConversionValueArray[1];
							} else {
								costConversionValue = "£" + costValue;
							}
						}
						return costConversionValue
					}
					if (response.accomodationDetails.length > 0) {
						$scope.accommodationCost = {
							"accommodationCost": $scope.costConversion(response.accomodationDetails[0].accommodationCost),
							"livingCost": $scope.costConversion(response.accomodationDetails[0].livingCost),
							"whatuniCostOfPint": $scope.costConversion(response.accomodationDetails[0].whatuniCostOfPint),
							"accomodationFlag": ""
						};
					}
					//
					if (isBlankOrNull($scope.accommodationCost.accommodationCost) && isBlankOrNull($scope.accommodationCost.livingCost) && isBlankOrNull($scope.accommodationCost.whatuniCostOfPint)) {
						$scope.keyStatsInfo.accomodationFlag = "N";
					}
					//
					//if(response.keyStatsInfo.length>0) {
					$scope.reviewStarRating = response.reviewStarRating;
					$scope.ratelimit = $scope.reviewStarRating.length;

					$scope.visitWebsite = null;
					if (response.enquiryDetails.length > 0)
						$scope.visitWebsite = response.enquiryDetails[0].website;
					$scope.hotline = response.enquiryDetails[0].hotline;
					//
					localStorage.setItem('suborderItemId', response.enquiryDetails[0].suborderItemId);
					//
					$scope.enquiryDetails = {
						"emailWebformFlag": response.enquiryDetails[0].emailWebformFlag,
						"emailWebform": response.enquiryDetails[0].emailWebform,
						"email": response.enquiryDetails[0].email,
						"prospectusWebformFlag": response.enquiryDetails[0].prospectusWebformFlag,
						"prospectusWebform": response.enquiryDetails[0].prospectusWebform,
						"prospectus": response.enquiryDetails[0].prospectus,
						"buttonNavClass": "pr_btn ",
						"emailPrice": response.enquiryDetails[0].emailPrice,
						"prospectusPrice": response.enquiryDetails[0].prospectusPrice,
						"openDaysPrice": response.enquiryDetails[0].openDaysPrice
					};
					//
					var btnCount = 0;
					/*if($scope.university.courseExistFlag == 'Y') {
						btnCount = 1;
					}*/
					if ($scope.opendayFlag == 'Y') {
						btnCount = 1;
					}
					if ($scope.enquiryDetails.prospectusWebformFlag != 'Y' && $scope.enquiryDetails.prospectus != null) {
						btnCount = btnCount + 1;
					}
					if ($scope.enquiryDetails.emailWebformFlag != 'Y' && $scope.enquiryDetails.email != null) {
						btnCount = btnCount + 1;
					}
					if (btnCount == 1) {
						$scope.enquiryDetails.buttonNavClass += 'w100p';
					} else if (btnCount == 2) {
						$scope.enquiryDetails.buttonNavClass += 'w50p';
					}
					//
					if (response.studentReview.length > 0) {
						$scope.studentReview = response.studentReview;
					}

					/* $scope.studentReview=[];
					 if(response.studentReview.length>0) {
						 angular.forEach(response.studentReview,function(studentReviewKey,studentReviewValue) {
 
						 });    
					 }*/

					$scope.reviewSection = [];
					if (response.studentReview.length > 0) {
						angular.forEach(response.studentReview, function (studentReviewValue, studentReviewKey) {
							$scope.reviewSection.push(studentReviewValue.sections.replace("Uni ", "").toLocaleLowerCase())
						});
					}
					$scope.universityTabs = response.uniProfileContent;
					var height = $(document).height();
					if ($scope.universityTabs.length > 0) {
						angular.forEach($scope.universityTabs, function (photoValue, photoKey) {
							if (photoValue.imagePath != null) {
								console.log('mediaType > ' + photoValue.mediaType);
								if (photoValue.mediaType == 'VIDEO') {
									$scope.photos.push('<video id="video_' + photoKey + '" src="' + photoValue.videoUrl + '" playsinline ad-outlet="video" width="100%" height="' + height + '" poster="' + photoValue.imagePath + '" ></video> <span id="play_' + photoKey + '" class="provie" onclick="showVideo(\'' + photoKey + '\');"><i class="fa fa-play-circle-o"></i></span>');
								} else {
									$scope.photos.push(photoValue.imagePath);
								}
							}
						})
					}

					$scope.competitorDetails = otherUniNullCheck(response.competitorDetails);


					$scope.keys = Object.keys($scope.universityTabs)
					console.log($scope.university);
					setTimeout(function () {
						$$D('loaderInstitutionView').style.display = 'block';
						//alert("test")
						//profile swiper
						var currentSlideCount = 0;
						var galleryTop = new Swiper('#institutionhomeCtrl .gallery-top', {
							nextButton: '.swiper-button-next',
							prevButton: '.swiper-button-prev',
							spaceBetween: 100,
							autoHeight: 'false',
							onSlideChangeStart: function (swiper) {
								$scope.appInteractionLogging(null, null, $scope.universityTabs[swiper.activeIndex].profileId, $scope.universityTabs[swiper.activeIndex].typeName, swiper.activeIndex);
								//googleTagManagerEventTrack(null, 'Provider Nav', $("#institutionhomeCtrl .gallery-thumbs .thumbs-"+swiper.activeIndex).attr('button-label'), $scope.university.name, '0');
								$("#institutionhomeCtrl .gallery-thumbs .swiper-slide").removeClass('swiper-slide-active-freemode');
								$("#institutionhomeCtrl .gallery-thumbs .thumbs-" + swiper.activeIndex).addClass('swiper-slide-active-freemode');

								$("#institutionhomeCtrl .gallery-top .swiper-slide").removeClass('swiper-slide-active');
								$("#institutionhomeCtrl .gallery-top .gallery_nav-" + swiper.activeIndex).addClass('swiper-slide-active');

								$("#institutionhomeCtrl .gallery-top .swiper-wrapper").css('height', $("#institutionhomeCtrl .gallery_nav-" + swiper.activeIndex).height() + "px");


								//
								$("#institutionhomeCtrl #extraSpace_" + swiper.activeIndex).hide();
								if (($('#institutionhomeCtrl .gallery_nav-' + swiper.activeIndex).height()) < 550) {
									//$("#extraSpace_"+swiper.activeIndex).css('height', "200px");
									$("#extraSpace_" + swiper.activeIndex).show();
								}
								//console.log('true or false > '+!isBlankOrNull(scrolling_array[swiper.activeIndex]));
								//console.log(scrolling_array);
								//if(currentSlideCount < swiper.activeIndex && isBlankOrNull(scrolling_array[swiper.activeIndex]) && ($('.gallery_nav-'+ swiper.activeIndex).height()) > 800)
								console.log($("#institutionhomeCtrl #extraSpace_" + swiper.activeIndex).css('display'));
								if ($("#institutionhomeCtrl #extraSpace_" + swiper.activeIndex).css('display') == "none") {
									$("#institutionhomeCtrl .gallery-top .swiper-wrapper").css('height', $("#institutionhomeCtrl .gallery_nav-" + swiper.activeIndex).height() - 000 + "px");
								} else {
									$("#institutionhomeCtrl .gallery-top .swiper-wrapper").css('height', $("#institutionhomeCtrl .gallery_nav-" + swiper.activeIndex).height() + 200 + "px");
								}
								if (currentSlideCount < swiper.activeIndex) { //&& $('#stk_app').html()!=''
									scrolling_array[currentSlideCount] = $('#institutionhomeCtrl .page-content').scrollTop();
									//console.log("height"+$('.gallery_nav-'+swiper.activeIndex).height())
									if ($('#institutionhomeCtrl .gallery_nav-' + swiper.activeIndex).height() >= 500 && $('#institutionhomeCtrl .page-content').scrollTop() >= 120) {
										$$('#institutionhomeCtrl .page-content').scrollTop(120, 0);
									} else {
										$$('#institutionhomeCtrl .page-content').scrollTop(0, 0);
									}
								} //else {
								if (currentSlideCount > swiper.activeIndex) {
									//$$('.page-content').scrollTop(scrolling_array[swiper.activeIndex], 0);
									setTimeout(function () {
										if (scrolling_array[swiper.activeIndex] != "undefined")
											$('#institutionhomeCtrl .page-content').scrollTop(scrolling_array[swiper.activeIndex], 0);
									}, 160)
								}
								currentSlideCount = swiper.activeIndex;
								$scope.currentSlideIndex = swiper.activeIndex;
								if (swiper.activeIndex == 0) {
									console.log(translateX);
									//$("#institutionhomeCtrl .gallery-thumbs .swiper-wrapper").addClass('ml35');
									$("#institutionhomeCtrl .gallery-thumbs .swiper-wrapper").css("margin-left", translateX + "px");
								} else {
									//$("#institutionhomeCtrl .gallery-thumbs .swiper-wrapper").removeClass('ml35');
									$("#institutionhomeCtrl .gallery-thumbs .swiper-wrapper").css("margin-left", "");
								}
							}
						});
						var currentSlideCount = 0;
						var scrolling_array = [];
						var galleryThumbs = new Swiper('#institutionhomeCtrl .gallery-thumbs', {
							freeMode: true,
							//freeModeMomentum:true,
							spaceBetween: 30,
							centeredSlides: true,
							slidesPerView: 'auto',
							//slidesPerView: 1,
							touchRatio: 0.2,
							slideToClickedSlide: true,
							onSlideChangeStart: function (swiper) {
								//console.log("console.log"+swiper.activeIndex)
							},
							onSlideChangeEnd: function (swiper) {
							},
							onClick: function (swiper, event) {
								galleryTop.slideTo(swiper.activeIndex);
							}
						});
						galleryTop.params.control = galleryThumbs;
						//galleryThumbs.params.control = galleryTop;

						galleryNextSlide = galleryTop;

						//$("#institutionhomeCtrl .gallery-thumbs .swiper-wrapper").addClass('ml35');
						$('#institutionhomeCtrl .gallery-top  .swiper-wrapper').css('transform', 'translate3d(0px, 0px, 0px)');

						//$('#institutionhomeCtrl .gallery-thumbs  .swiper-wrapper').css('transform','translate3d(0px, 0px, 0px)');

						var translateX = parseInt(getTranslateX($("#institutionhomeCtrl .gallery-thumbs .swiper-wrapper"))) - 12;
						if (translateX < 0) {
							translateX = translateX * -1 + 5;
						} else {
							translateX = "-" + translateX;
						}
						console.log(translateX);
						$("#institutionhomeCtrl .gallery-thumbs .swiper-wrapper").css("margin-left", translateX + "px");

						//$("#institutionhomeCtrl .gallery-thumbs .swiper-wrapper").addClass('ml35');

						$("#institutionhomeCtrl .gallery-thumbs .swiper-slide").removeClass('swiper-slide-active-freemode');
						$("#institutionhomeCtrl .gallery-thumbs .thumbs-0").addClass('swiper-slide-active-freemode');
						//$("#institutionhomeCtrl .gallery-thumbs .thumbs-0").addClass('swiper-slide-active');

						//galleryTop.slideTo(0);
						//galleryThumbs.slideTo(0);

						$("#extraSpace_0").hide();
						if (($('#institutionhomeCtrl .gallery_nav-0').height()) < 550) {
							$("#extraSpace_0").show();
						}
						if ($("#extraSpace_0").css('display') == "none") {
							$("#institutionhomeCtrl .gallery-top .swiper-wrapper").css('height', $("#institutionhomeCtrl .gallery_nav-0").height() - 000 + "px");
						} else {
							$("#institutionhomeCtrl .gallery-top .swiper-wrapper").css('height', $("#institutionhomeCtrl .gallery_nav-0").height() + 200 + "px");
						}
						if (arg != '') {
							myApp.pullToRefreshDone();
						}

					}, 0);
					//
					// --- PAGE VIEW STATS
					//
					if ($scope.universityTabs.length > 0) {
						$scope.appInteractionLogging(null, null, $scope.universityTabs[0].profileId, $scope.universityTabs[0].typeName);
						//googleTagManagerEventTrack(null, 'Provider Nav', 'Overview', $scope.university.name, '0');
					} else if ($scope.checkAdvUniversity == 'N') {
						$scope.appInteractionLogging(null, 'Overview', null, 'NON_ADV_PROFILE');
					}
					if (arg == '') {
						//if($scope.universityTabs.length > 0) {
						if ($scope.checkAdvUniversity == 'N') {

							var userID = localStorage.getItem('LOGGED_IN_USER_ID');
							firebaseEventTrack('screenview', { 'page_name': '/institution-profile/non-advertiser/' + localStorage.getItem('universityId') });
						} else {

							var userID = localStorage.getItem('LOGGED_IN_USER_ID');

							firebaseEventTrack('screenview', { 'page_name': '/institution-profile/advertiser/' + localStorage.getItem('universityId') });
						}
					}

					//
					var page = $$('.page[data-page="institution-profile"]')[0].f7PageData;
					if (Object.keys(page.query).length !== 0 && arg == '') {
						setTimeout(function () {
							if (page.query.action == "shortlist") {
								if (!$("#" + page.query.action_id).hasClass('fav_fill'))
									$("#" + page.query.action_id).trigger('click');
							} else if (page.query.action == "saveopenday") {
								$('#institutionhomeCtrl .page-content').scrollTop($("#saveToCalendarLbl-0").offset().top - 300, 100);
								$("#" + page.query.action_id).trigger('click');
							} else {
								$("#" + page.query.action_id).trigger('click');
							}
						}, 1000);
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					// myApp.alert("error");
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			
			//$('#institutionhomeCtrl .page-content.ip_header').on('scroll touchmove', function(e) {
			$('#institutionhomeCtrl #pullContent.ip_header').on('scroll touchmove', function (e) {
				console.log('scroll log');
				var ptrContent = $$('.pull-to-refresh-content');
				scroll = $(this).scrollTop() + 100;
				//console.log($('.gallery-top').height());
				if (scroll > 200 && $('#institutionhomeCtrl #stk_app').html() == '' && $('#institutionhomeCtrl .gallery-top').height() > 550) {
					statusBarFontColor('black');
					//var height=parseInt($('.gallery-top .swiper-wrapper').css('height').replace("px",""))+80;
					//console.log(height);
					//$('.gallery-top .swiper-wrapper').css('height',height+"px");
					$('#institutionhomeCtrl #stk_app').append($("#institutionhomeCtrl .stk_div"));
					$('#institutionhomeCtrl #stk_app').addClass('stk_fix');
					$('#institutionhomeCtrl .stk_div').addClass('mobsec');
					$('#institutionhomeCtrl #stk_app').hide();
					$('#institutionhomeCtrl #stk_app').slideToggle();
					if($scope.articleFlag=="Y") {
						$('#institutionhomeCtrl #covid_upt').addClass("pot260");
					}
					
					
					setTimeout(function () {
						$('#institutionhomeCtrl #stk_app .stk_div').addClass('visible');
					}, 100)
					myApp.destroyPullToRefresh(ptrContent)
				}
				if (scroll < 200 && $('#institutionhomeCtrl #stk_app').html() != '') {
					$('#institutionhomeCtrl #stk_app').slideToggle();
					$('#institutionhomeCtrl #stk_div').prepend($("#institutionhomeCtrl .stk_div"));
					$('#institutionhomeCtrl #stk_app').removeClass('stk_fix');
					//$('#stk_div').hide();
					$('#institutionhomeCtrl #stk_div').fadeIn(40);
					//$('.page-content').scrollTop(20,0)
					$('#institutionhomeCtrl .stk_div').removeClass('mobsec');
					$('#institutionhomeCtrl .stk_div').removeClass('visible');
					if($scope.articleFlag=="Y") {
						$('#institutionhomeCtrl #covid_upt').removeClass("pot260");
					}
					setTimeout(function () {
						myApp.initPullToRefresh(ptrContent)
					}, 1000);
					//statusBarFontColor('black');
					console.log('checkAdvUniversity > ' + $scope.checkAdvUniversity);
					if ($scope.checkAdvUniversity == 'N') {
						statusBarFontColor('white');
					}
				}
			});
			//
			$(document).on('click', '#institutionhomeCtrl .open-popup1', function () {
				statusBarFontColor('black');
				//console.log('About Popup opened');

				if ($(this).attr('data-popup') == ".second_page") {
					console.log($scope.yearList);
					$('#enqYoeSubmitBtnIP').attr("disabled", "disabled");
					//$(".test").removeClass('active');
					//$scope.yearList=$scope.yearList;
					angular.forEach($scope.yearList, function (value, key) {
						if (value.selectedYear == 'Y') {
							$("#enquiryYearOfEntry-" + key + 'IP').addClass('active');
							$('#enqYoeSubmitBtnIP').removeAttr("disabled");
						} else {
							$("#enquiryYearOfEntry-" + key + 'IP').removeClass('active');
						}
					});
					var yearData = $$D('emailYearIP').innerHTML;
					$compile(yearData)($scope);
				}
				if ($(this).attr('data-popup') == ".req_props") {
					$('#prosYoeSubmitBtnIP').attr("disabled", "disabled");
					angular.forEach($scope.yearList, function (value, key) {
						if (value.selectedYear == 'Y') {
							$("#prospectusYearOfEntry-" + key + 'IP').addClass('active');
							$('#prosYoeSubmitBtnIP').removeAttr("disabled");
						} else {
							$("#prospectusYearOfEntry-" + key + 'IP').removeClass('active');
						}
					});
					var yearData = $$D('prospectusYearIP') ? $$D('prospectusYearIP').innerHTML : "";
					$compile(yearData)($scope);
					var addressData = $$D('prospectusAddressIP') ? $$D('prospectusAddressIP').innerHTML : "";
					$compile(addressData)($scope);
				}
				statusBarFontColor('black');
				//console.log('About Popup opened')
			});
			//
			$$(document).on('click', '.close-popup', function () {
				statusBarFontColor('black');
				if ($scope.checkAdvUniversity == 'N') {
					function onDeviceReady() {
						statusBarFontColor('white');
					}
					document.addEventListener("deviceready", onDeviceReady, false);
				}
			});
			//
			$scope.getUniversity('');
			var ptrContent = $$('#institutionhomeCtrl .pull-to-refresh-content');
			ptrContent.on('refresh', function (e) {
				// Emulate 2s loading
				//setTimeout(function() {
				$scope.getUniversity('pull-to-refresh');
				$scope.showEnquiryForm();
				$scope.showProspectusEnquiryForm();
				//myApp.pullToRefreshDone();
				//}, 3000);
				//
			});
			//
			$scope.interactionEventLogging = function (interationType) {
				var uniName = $scope.university.name;
				if (!isBlankOrNull(uniName)) {
					uniName = uniName.toLowerCase();
				} else {
					uniName = 'not set';
				}
				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}

				var coupon_value = 'not set';
				var currency_value = 0;
				if (interationType == "PROSPECTUS") {
					coupon_value = "engagement_order_prospectus";
					if ($scope.enquiryDetails.hasOwnProperty('prospectusPrice')) {
						currency_value = parseFloat($scope.enquiryDetails.prospectusPrice);
					}
				} else if (interationType == "EMAIL") {
					coupon_value = "engagement_request_email";
					console.log("enquiryDetails");
					console.log($scope.enquiryDetails);
					if ($scope.enquiryDetails.hasOwnProperty('emailPrice')) {
						currency_value = parseFloat($scope.enquiryDetails.emailPrice);
					}

				} else if (interationType == "OPEN_DAYS") {
					coupon_value = "engagement_open_days_button";
					if ($scope.enquiryDetails.hasOwnProperty('openDaysPrice')) {
						currency_value = parseFloat($scope.enquiryDetails.openDaysPrice);
					}
				}

				var interactionPagename = ""
				if ($scope.checkAdvUniversity == 'N') {
					interactionPagename = '/institution-profile/non-advertiser/' + localStorage.getItem('universityId');
				} else {
					interactionPagename = '/institution-profile/advertiser/' + localStorage.getItem('universityId');
				}
				firebaseEventTrack('addtocart', { item_category: uniName, item_name: firebaseSubject.toLowerCase(), item_location_id: 'not set', currency: 'GBP', value: currency_value, coupon: coupon_value, item_id: localStorage.getItem('universityId'), quantity: 1, interaction_pagename: interactionPagename });

			}
			//
			$scope.slideNext = function (name) {
				// var mySwiper = $$('.swiper-container')[0].swiper;
				//mySwiper.slideNext();
				// mySwiper.slideTo(2);
				var currentslider = $('#institutionhomeCtrl .gallery-top  .swiper-slide-active').attr('id');
				console.log(currentslider);
				currentslider = currentslider.replace("swiper-slide-mycontent_", "");

				currentslider = parseInt(currentslider) + 1;

				//galleryTop.slideTo(currentslider);
				galleryNextSlide.slideTo(currentslider);
			}
			$scope.providerResults = function (tileMediaPath) {
				var nearYouFlag = 'NEAR YOU';
				/*localStorage.setItem('institutionId', localStorage.getItem('universityId'));			
				localStorage.setItem('categoryId', localStorage.getItem('categoryId'));
				localStorage.setItem('categoryCode', localStorage.getItem('categoryCode'));
				localStorage.setItem('subject', localStorage.getItem('subject'));
				localStorage.setItem('nearYouFlag', nearYouFlag);*/
				if (!isBlankOrNull(tileMediaPath)) {
					localStorage.setItem('tileMediaPath', tileMediaPath);
				}
				//googleTagManagerEventTrack(null, 'Button', 'view courses', $scope.university.name, '0');
				localStorage.setItem('institutionId', localStorage.getItem('universityId'));
				localStorage.setItem('FROM_PAGE', 'institution-profile');
				localStorage.setItem('VIEW_COURSES_FLAG', 'VIEW_COURSES_FLAG');
				//localStorage.setItem('nearYouFlag', nearYouFlag);document.referrer;
				mainView.router.loadPage('html/institution/providerResults.html');
			}
			$scope.pageViewStats = function () {
				if ($scope.universityTabs.length > 0) {
					$scope.appInteractionLogging(null, null, $scope.universityTabs[0].profileId, $scope.universityTabs[0].typeName);
				}
			}
			//
			//Below controller for enquiry page
			$scope.showEnquiryForm = function () {

				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var institutionId = !isBlankOrNull(localStorage.getItem('universityId')) ? localStorage.getItem('universityId') : "";
				//				
				var stringJson = {
					"userId": userId,
					"institutionId": institutionId,
					"courseId": '',
					"enquiryType": "EMAIL",
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/enquiry/enquiry-form/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;

					$scope.clientConsentFlag = response.clientConsentFlag;
					$scope.clientConsentText = response.clientConsentText;
					//console.log($scope.clientConsentText)
					//$scope.competitorDetails=response.competitorDetails;					
					$scope.institutionDetailsList = response.institutionDetailsList;
					$scope.userAddressList = response.userAddressList;
					$scope.yearList = response.yearList;
					if (response.yearList != null && response.yearList.length > 0) {
						for (var lp = 0; lp < response.yearList.length; lp++) {
							if (response.yearList[lp].selectedYear == 'Y') {
								$scope.enquirySelectedYearFlag = 'Y';
								localStorage.setItem('selectedYear', response.yearList[lp].yearOfEntry);
								break;
							} else {
								$scope.enquirySelectedYearFlag = 'N';
							}
						}
					}
					//
					localStorage.setItem('institutionId', response.institutionDetailsList[0].institutionId);
					localStorage.setItem('institutionDisplayName', response.institutionDetailsList[0].institutionDisplayName);
					localStorage.setItem('logo', response.institutionDetailsList[0].logo);
					//
					if (response.versionDetails != null && response.versionDetails.length > 0) {
						if (response.versionDetails[0].versionName > Constants.appVersion && sessionStorage.getItem('VERSION_INFORMED') != 'true') {
							sessionStorage.setItem('VERSION_INFORMED', 'true');
							localStorage.setItem('updateVersionMessage', response.versionDetails[0].message);
							localStorage.setItem('updateVersion', response.versionDetails[0].versionName);
							//window.location.replace('../../html/versioning/updateVersion.html');
							mainView.router.loadPage('html/versioning/updateVersion.html');
						}
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//
			
				$scope.showEnquiryForm();
			
			//
			//Below controller for request prospectus
			$scope.showProspectusEnquiryForm = function () {

				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var institutionId = !isBlankOrNull(localStorage.getItem('universityId')) ? localStorage.getItem('universityId') : "";
				//
				var stringJson = {
					"userId": userId,
					"institutionId": institutionId,
					"courseId": '',
					"enquiryType": "PROSPECTUS",
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/enquiry/enquiry-form/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;

					$scope.clientConsentFlag = response.clientConsentFlag;
					$scope.clientConsentText = response.clientConsentText;
					//$scope.competitorDetails=response.competitorDetails;					
					$scope.institutionDetailsList = response.institutionDetailsList;
					$scope.yearList = response.yearList;
					if (response.yearList != null && response.yearList.length > 0) {
						for (var lp = 0; lp < response.yearList.length; lp++) {
							if (response.yearList[lp].selectedYear == 'Y') {
								$scope.prospectusSelectedYearFlag = 'Y';
								localStorage.setItem('selectedYear', response.yearList[lp].yearOfEntry);
								break;
							} else {
								$scope.prospectusSelectedYearFlag = 'N';
							}
						}
					}
					localStorage.removeItem('addressFlag');
					localStorage.removeItem('addressLineOne');
					localStorage.removeItem('addressLineTwo');
					localStorage.removeItem('city');
					localStorage.removeItem('postCode');
					if (response.userAddressList.length > 0) {
						if (response.userAddressList.length == 1) {
							if (isBlankOrNull(response.userAddressList[0].addressLineOne) && isBlankOrNull(response.userAddressList[0].addressLineTwo) && isBlankOrNull(response.userAddressList[0].city) && isBlankOrNull(response.userAddressList[0].postCode)) {
								$scope.userAddressList = [];
							} else {
								$scope.userAddressList = response.userAddressList;
								localStorage.setItem('addressFlag', 'YES');
								localStorage.setItem('addressLineOne', response.userAddressList[0].addressLineOne);
								localStorage.setItem('addressLineTwo', response.userAddressList[0].addressLineTwo);
								localStorage.setItem('city', response.userAddressList[0].city);
								localStorage.setItem('postCode', response.userAddressList[0].postCode);
							}
						}
					}
					//				
					localStorage.setItem('institutionId', response.institutionDetailsList[0].institutionId);
					localStorage.setItem('institutionDisplayName', response.institutionDetailsList[0].institutionDisplayName);
					localStorage.setItem('logo', response.institutionDetailsList[0].logo);
					//localStorage.setItem('courseId', response.institutionDetailsList[0].courseId);
					//localStorage.setItem('courseTitle', response.institutionDetailsList[0].courseTitle);
					//
					$scope.institutionDetails = [];
					var institutionId = !isBlankOrNull(localStorage.getItem('institutionId')) ? localStorage.getItem('institutionId') : "";
					var institutionDisplayName = !isBlankOrNull(localStorage.getItem('institutionDisplayName')) ? localStorage.getItem('institutionDisplayName') : "";
					var logo = !isBlankOrNull(localStorage.getItem('logo')) ? localStorage.getItem('logo') : "";
					var selectedYear = !isBlankOrNull(localStorage.getItem('selectedYear')) ? localStorage.getItem('selectedYear') : "";
					var addressLineOne = !isBlankOrNull(localStorage.getItem('addressLineOne')) ? localStorage.getItem('addressLineOne') : "";
					var addressLineTwo = !isBlankOrNull(localStorage.getItem('addressLineTwo')) ? localStorage.getItem('addressLineTwo') : "";
					var city = !isBlankOrNull(localStorage.getItem('city')) ? localStorage.getItem('city') : "";
					var postCode = !isBlankOrNull(localStorage.getItem('postCode')) ? localStorage.getItem('postCode') : "";
					var countyState = !isBlankOrNull(localStorage.getItem('countyState')) ? localStorage.getItem('countyState') : "";
					$scope.addressFlag = localStorage.getItem('addressFlag');
					if (isBlankOrNull(addressLineTwo)) {
						addressLineOne = addressLineOne + ",";
					} else {
						addressLineTwo = addressLineTwo + ",";
					}
					$scope.institutionDetails = {
						"institutionId": institutionId,
						"institutionDisplayName": institutionDisplayName,
						"logo": logo,
						"addressLineOne": addressLineOne,
						"addressLineTwo": addressLineTwo,
						"city": city,
						"postCode": postCode,
						"countyState": countyState
					};
					//
					if (response.versionDetails != null && response.versionDetails.length > 0) {
						if (response.versionDetails[0].versionName > Constants.appVersion && sessionStorage.getItem('VERSION_INFORMED') != 'true') {
							sessionStorage.setItem('VERSION_INFORMED', 'true');
							localStorage.setItem('updateVersionMessage', response.versionDetails[0].message);
							localStorage.setItem('updateVersion', response.versionDetails[0].versionName);
							//window.location.replace('../../html/versioning/updateVersion.html');
							mainView.router.loadPage('html/versioning/updateVersion.html');
						}
					}

				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('/html/exception/exception.html');
				});
			}
			//
			$scope.submitEnquiryForm = function () {
				//
				$("#enqSuccessImgIP").hide();
				$("#prosSuccessImgIP").hide();
				//
				var uniName = $scope.university.name;
				if (!isBlankOrNull(uniName)) {
					uniName = uniName.toLowerCase();
				}
				//googleTagManagerEventTrack(null, 'Interaction', 'Send Email', uniName, '0');
				//
				var currency_value = 0;
				if ($scope.enquiryDetails.hasOwnProperty('emailPrice')) {
					currency_value = parseFloat($scope.enquiryDetails.emailPrice);
				}
				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}
				var collegeId = localStorage.getItem('universityId');
				var interactionPagename = ""
				if ($scope.checkAdvUniversity == 'N') {
					interactionPagename = '/institution-profile/non-advertiser/' + localStorage.getItem('universityId');
				} else {
					interactionPagename = '/institution-profile/advertiser/' + localStorage.getItem('universityId');
				}
				firebaseEventTrack('ecommercepurchase', { coupon: 'interaction_request_email', currency: "GBP", value: currency_value, transaction_id: collegeId, location: uniName.toLowerCase(), item_name: firebaseSubject, interaction_pagename: interactionPagename  });
				//
				$$D('enquirySuccessDivIP').style.display = 'none';
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var institutionId = !isBlankOrNull(localStorage.getItem('institutionId')) ? localStorage.getItem('institutionId') : "";
				var selectedYear = !isBlankOrNull(localStorage.getItem('selectedYear')) ? localStorage.getItem('selectedYear') : "";
				var message = !isBlankOrNull($$D("enqMsgIP").value) ? $$D("enqMsgIP").value : "";
				var suborderItemId = !isBlankOrNull(localStorage.getItem('suborderItemId')) ? localStorage.getItem('suborderItemId') : "";
				var searchCategoryCode = "";
				var searchCategoryId = "";
				var keywordSearch = "";
				var qualification = "";
				var orderBy = "";
				var locationType = "";
				var region = "";
				var regionFlag = "";
				var studyMode = "";
				var previousQual = "";
				var previousQualGrade = "";
				var assessmentType = "";
				var reviewCategory = "";
				var reviewCategoryName = "";
				//
				if (localStorage.recentSearchCriteria != undefined) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);

					searchCategoryCode = !isBlankOrNull(recentSearchCriteria.searchCategoryCode) ? recentSearchCriteria.searchCategoryCode : "";
					searchCategoryId = !isBlankOrNull(recentSearchCriteria.searchCategoryId) ? recentSearchCriteria.searchCategoryId : "";
					keywordSearch = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "";
					qualification = !isBlankOrNull(recentSearchCriteria.qualification) ? recentSearchCriteria.qualification : "";
					orderBy = !isBlankOrNull(recentSearchCriteria.orderBy) ? recentSearchCriteria.orderBy : "";
					locationType = !isBlankOrNull(recentSearchCriteria.locationType) ? recentSearchCriteria.locationType : "";
					region = !isBlankOrNull(recentSearchCriteria.region) ? recentSearchCriteria.region : "";
					regionFlag = !isBlankOrNull(recentSearchCriteria.regionFlag) ? recentSearchCriteria.regionFlag : "";
					studyMode = !isBlankOrNull(recentSearchCriteria.studyMode) ? recentSearchCriteria.studyMode : "";
					previousQual = !isBlankOrNull(recentSearchCriteria.previousQual) ? recentSearchCriteria.previousQual : "";
					previousQualGrade = !isBlankOrNull(recentSearchCriteria.previousQualGrade) ? recentSearchCriteria.previousQualGrade : "";
					assessmentType = !isBlankOrNull(recentSearchCriteria.assessmentType) ? recentSearchCriteria.assessmentType : "";
					reviewCategory = !isBlankOrNull(recentSearchCriteria.reviewCategory) ? recentSearchCriteria.reviewCategory : "";
					reviewCategoryName = !isBlankOrNull(recentSearchCriteria.reviewCategoryName) ? recentSearchCriteria.reviewCategoryName : "";
					//
					if (!isBlankOrNull(region) || !isBlankOrNull(locationType) || !isBlankOrNull(studyMode) || !isBlankOrNull(previousQual) || !isBlankOrNull(assessmentType) || !isBlankOrNull(reviewCategory)) {
						$scope.matchPecenatgeFlag = true;
					}
					//
				}
				var reviewSubjectOne = ""; var reviewSubjectTwo = ""; var reviewSubjectThree = "";
				if (!isBlankOrNull(reviewCategory)) {
					reviewCategoryArray = reviewCategory.split(",");
					if (reviewCategoryArray.length == 3) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; reviewSubjectThree = reviewCategoryArray[2]; }
					else if (reviewCategoryArray.length == 2) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; }
					else { reviewSubjectOne = reviewCategoryArray[0]; }
				}
				//Below code for submit email enquiry form
				if ($$D('sendButtonIP') && $$D('sendButtonIP').className == 'right snd_lnk' && !isBlankOrNull(selectedYear) && !isBlankOrNull(message)) {
					$('#sendButtonIP').addClass('gry');
					$('#sendButtonIP').attr("disabled", "disabled");
					$$D('enqSuccessLoaderIP').style.display = 'block';
					var httpClientConsentFlag = "";
					if ($scope.clientConsentFlag === "Y") {
						httpClientConsentFlag = $scope.emailClientConsentFlagIP;
					}
					var stringJson = {
						"userId": userId,
						"institutionId": institutionId,
						"courseId": '',
						"yearOfEntry": selectedYear,
						"userIP": "",
						"userAgent": navigator.userAgent,
						"enquiryType": "EMAIL",
						"message": message,
						"addressLineOne": "",
						"addressLineTwo": "",
						"city": "",
						"countyState": "",
						"postCode": "",
						"suborderItemId": suborderItemId,
						"categoryCode": searchCategoryCode,
						"categoryId": searchCategoryId,
						"qualification": qualification,
						"searchKeyword": keywordSearch,
						"locationType": locationType,
						"region": region,
						"regionFlag": regionFlag,
						"studyMode": studyMode,
						"previousQualification": previousQual,
						"previousQualificationGrade": previousQualGrade,
						"assessmentType": assessmentType,
						"reviewSubjectOne": reviewSubjectOne,
						"reviewSubjectTwo": reviewSubjectTwo,
						"reviewSubjectThree": reviewSubjectThree,
						"appVersion": Constants.appVersion,
						"affiliateId": "220703",
						"accessToken": "246",
						"clientConsentFlag": httpClientConsentFlag,
						"latitude": latitude,
						"longitude": longtitude
					};
					var jsonData = JSON.stringify(stringJson);
					var url = "https://mtest.whatuni.com/wuappws/enquiry/submit-enquiry-form/";
					var req = {
						method: 'POST',
						url: url,
						headers: { 'Content-Type': 'application/json' },
						data: jsonData
					}
					$http(req).then(function (httpresponse) {
						var response = httpresponse.data;

					
						
						xtremePushTag("Year_Of_Entry", selectedYear);
						if ($$D('enqSuccessLoaderIP')) {
							setTimeout(function () {
								$$D('enqSuccessLoaderIP').style.display = 'none';
							}, 1000);
						}
						setTimeout(function () {
							$$D('enquirySuccessDivIP').style.display = 'block';
						}, 1000);
						$scope.matchingPercentageFlag;
						if (!isBlankOrNull(locationType) || !isBlankOrNull(region) || !isBlankOrNull(regionFlag) || !isBlankOrNull(studyMode) || !isBlankOrNull(previousQual) || !isBlankOrNull(previousQualGrade) || !isBlankOrNull(assessmentType) || !isBlankOrNull(reviewCategory)) {
							$scope.matchingPercentageFlag = true;
						}
						$scope.otherInstitutionsList = response.otherInstitutionsList;
						$scope.otherCoursesList = response.otherCoursesList;
						if (response.institutionDetailsList.length > 0) {
							$scope.institutionDetailsList = response.institutionDetailsList;
						}
						if (response.otherInstitutionsList.length == 1) {
							if (isBlankOrNull(response.otherInstitutionsList[0].institutionId) && isBlankOrNull(response.otherInstitutionsList[0].institutionDisplayName)) {
								$scope.otherInstitutionsList = [];
							}
						}
						$scope.otherCoursesList = response.otherCoursesList;
						if (response.otherCoursesList.length == 1) {
							if (isBlankOrNull(response.otherCoursesList[0].institutionId) && isBlankOrNull(response.otherCoursesList[0].institutionDisplayName)) {
								$scope.otherCoursesList = [];
							}
						}
						$scope.emailSuccess = 'Y';
						$scope.prospectusSuccess = 'N';
						$scope.closeMark = 'institution-profile';
						var data = $$D('success-page-IP').innerHTML;
						$compile(data)($scope);
						setTimeout(function () { $("#enqSuccessImgIP").show(); }, 200);
						setTimeout(function () { reviewAndRating(true); }, 500);
					}, function (httpresponse, status) {
						var response = httpresponse.data;
						if ($$D('enqSuccessLoaderIP')) {
							$$D('enqSuccessLoaderIP').style.display = 'none';
						}
						localStorage.setItem('exception', response.exception);
						localStorage.setItem('status', response.status);
						localStorage.setItem('message', response.message);
						mainView.router.loadPage('html/exception/exception.html');
					});
				}
			}
			//
			//Below code for validate the email enquiry form
			$scope.validateEmailEnquiryform = function (institutionId, selectedYear) {
				if (!isBlankOrNull(institutionId) && !isBlankOrNull(selectedYear) && ($$D('enqMsg').value.length > 0 && $$D('enqMsg').value.length < 3800) && $$D('sendButton').className == 'right') {
					mainView.router.loadPage('../../html/enquiry/emailEnqSuccess.html');
				}
			}

			//Below code for highlight the send button
			$scope.showHideSendbutton = function () {
				if ($('#enqMsgIP') && !isBlankOrNull($('#enqMsgIP').val()) && ($('#enqMsgIP').val()).length > 0) {
					$('#sendButtonIP').removeClass('gry');
					$('#sendButtonIP').removeAttr('disabled', 'disabled');
				} else {
					$('#sendButtonIP').addClass('gry');
					$('#sendButtonIP').attr("disabled", "disabled");
				}
			}
			//
			$scope.showProspectusEnquiryForm();
			//
			$scope.saveSelectedYear = function (index, id, elementName) {
				/*if(id == "PROSPECTUS") {
					$('.req_props .tabs').removeClass("trnsfrm");
					$('#back-popup').hide();
					$('#back-tab').show();
				} else {
					$('.second_page .tabs').removeClass("trnsfrm");
					$('#back-popup-email').hide();
					$('#back-tab-email').show();
					if($$D('enqMsg')){
						$$D('enqMsg').focus();
					}
					//$('.snd_lnk').show();
				}*/
				/*if($$D('enqMsg')){
					setTimeout(function(){
						$$D('enqMsg').focus();
					},5);				
				}*/
				localStorage.removeItem('selectedYear');
				var selectedYear = $$D(elementName + "-" + index + 'IP').innerHTML.split("-")[0];
				var yearArray = document.getElementsByName(elementName + 'IP');
				for (var i = 0; i < yearArray.length; i++) {
					yearArray[i].className = 'btn btn_wht tab-link';
				}
				$("#" + elementName + "-" + index + 'IP').addClass('active');
				localStorage.setItem('selectedYear', selectedYear);
				//if($('.done_btn')) { $('.done_btn').hide(); } 
				$('#enqMsgIP').val('');
				//
				if (id == "PROSPECTUS") {
					$('#prosYoeSubmitBtnIP').removeAttr("disabled");
				} else {
					$('#enqYoeSubmitBtnIP').removeAttr("disabled");
				}
				/*
				if(id == 'EMAIL'){
					mainView.router.loadPage('../../html/enquiry/enquiryMessage.html');
				}else if(id == 'PROSPECTUS'){
					if(localStorage.getItem('addressFlag') == 'YES'){
						$scope.addressFlag = localStorage.getItem('addressFlag');				
					}else{
						localStorage.removeItem('addressFlag');
					}
					mainView.router.loadPage('../../html/enquiry/orderProspectus.html');				
				}
				*/
			}
			//
			//
			$scope.emailClientConsentFlagIPChange = function () {
				if ($("#emailClientConsentFlagIP").is(":checked")) {
					$scope.emailClientConsentFlagIP = "Y";
				} else {
					$scope.emailClientConsentFlagIP = "N";
				}
			}
			$scope.prospectusClientConsentFlagIPChange = function () {
				if ($("#prospectusClientConsentFlagIP").is(":checked")) {
					$scope.prospectusClientConsentFlagIP = "Y";
				} else {
					$scope.prospectusClientConsentFlagIP = "N";
				}
			}
			//
			$scope.goToNextPage = function (id, elementName) {
				if (id == "PROSPECTUS") {
					$('#institutionhomeCtrl .req_props .tabs').removeClass("trnsfrm");
					$('#back-popupIP').hide();
					$('#back-tabIP').show();
					if ($('#institutionhomeCtrl .prospectus')) { $('#institutionhomeCtrl .prospectus').hide(); }
				} else {
					$('#institutionhomeCtrl .second_page .tabs').removeClass("trnsfrm");
					$('#back-popup-emailIP').hide();
					$('#back-tab-emailIP').show();
					$('.snd_lnk').show();
					if ($$D('enqMsgIP')) {
						setTimeout(function () {
							$$D('enqMsgIP').focus();
						}, 500);
					}
					if ($('#institutionhomeCtrl .enq')) { $('#institutionhomeCtrl .enq').hide(); }
				}
				//
				var yearArray = document.getElementsByName(elementName + "IP");
				for (var i = 0; i < yearArray.length; i++) {
					if (yearArray[i].className.indexOf('active') > -1) {
						localStorage.setItem('selectedYear', yearArray[i].innerHTML);
					}
				}
				var selectedYear = localStorage.getItem('selectedYear');
				//googleTagManagerEventTrack(null, 'Start Date', 'Year of Entry', selectedYear, '0');
				firebaseEventTrack('year_of_entry', { start_date: selectedYear });
				/*
				if(id == 'EMAIL'){
					mainView.router.loadPage('../../html/enquiry/enquiryMessage.html');
				}else if(id == 'PROSPECTUS'){
					if(localStorage.getItem('addressFlag') == 'YES'){
						$scope.addressFlag = localStorage.getItem('addressFlag');				
					}else{
						localStorage.removeItem('addressFlag');
					}
					mainView.router.loadPage('../../html/enquiry/orderProspectus.html');				
				}
				*/
			}
			$scope.submitEnquiryProspectus = function () {
				//
				$('#prospectusSubBtnIP').attr("disabled", "disabled");
				$("#enqSuccessImgIP").hide();
				$("#prosSuccessImgIP").hide();
				//
				var uniName = $scope.university.name;
				if (!isBlankOrNull(uniName)) {
					uniName = uniName.toLowerCase();
				}
				//googleTagManagerEventTrack(null, 'Interaction', 'Order Prospectus', uniName, '0');
				//
				var currency_value = 0;
				if ($scope.enquiryDetails.hasOwnProperty('prospectusPrice')) {
					currency_value = parseFloat($scope.enquiryDetails.prospectusPrice);
				}
				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}
				var collegeId = localStorage.getItem('universityId');
				var interactionPagename = ""
				if ($scope.checkAdvUniversity == 'N') {
					interactionPagename = '/institution-profile/non-advertiser/' + localStorage.getItem('universityId');
				} else {
					interactionPagename = '/institution-profile/advertiser/' + localStorage.getItem('universityId');
				}
				firebaseEventTrack('ecommercepurchase', { coupon: 'interaction_order_prospectus', currency: "GBP", value: currency_value, transaction_id: collegeId, location: uniName.toLowerCase(), item_name: firebaseSubject, interaction_pagename: interactionPagename  });
				//

				$$D('enquirySuccessDivIP').style.display = 'none';
				$$D('enqSuccessLoaderIP').style.display = 'block';
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var institutionId = !isBlankOrNull(localStorage.getItem('institutionId')) ? localStorage.getItem('institutionId') : "";
				var selectedYear = !isBlankOrNull(localStorage.getItem('selectedYear')) ? localStorage.getItem('selectedYear') : "";
				var addressLineOne = !isBlankOrNull(localStorage.getItem('addressLineOne')) ? localStorage.getItem('addressLineOne') : "";
				var addressLineTwo = !isBlankOrNull(localStorage.getItem('addressLineTwo')) ? localStorage.getItem('addressLineTwo') : "";
				var city = !isBlankOrNull(localStorage.getItem('city')) ? localStorage.getItem('city') : "";
				var postCode = !isBlankOrNull(localStorage.getItem('postCode')) ? localStorage.getItem('postCode') : "";
				var countyState = !isBlankOrNull(localStorage.getItem('countyState')) ? localStorage.getItem('countyState') : "";
				var suborderItemId = !isBlankOrNull(localStorage.getItem('suborderItemId')) ? localStorage.getItem('suborderItemId') : "";
				//
				var searchCategoryCode = "";
				var searchCategoryId = "";
				var keywordSearch = "";
				var qualification = "";
				var orderBy = "";
				var locationType = "";
				var region = "";
				var regionFlag = "";
				var studyMode = "";
				var previousQual = "";
				var previousQualGrade = "";
				var assessmentType = "";
				var reviewCategory = "";
				var reviewCategoryName = "";
				//
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);

					searchCategoryCode = !isBlankOrNull(recentSearchCriteria.searchCategoryCode) ? recentSearchCriteria.searchCategoryCode : "";
					searchCategoryId = !isBlankOrNull(recentSearchCriteria.searchCategoryId) ? recentSearchCriteria.searchCategoryId : "";
					keywordSearch = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "";
					qualification = !isBlankOrNull(recentSearchCriteria.qualification) ? recentSearchCriteria.qualification : "";
					orderBy = !isBlankOrNull(recentSearchCriteria.orderBy) ? recentSearchCriteria.orderBy : "";
					locationType = !isBlankOrNull(recentSearchCriteria.locationType) ? recentSearchCriteria.locationType : "";
					region = !isBlankOrNull(recentSearchCriteria.region) ? recentSearchCriteria.region : "";
					regionFlag = !isBlankOrNull(recentSearchCriteria.regionFlag) ? recentSearchCriteria.regionFlag : "";
					studyMode = !isBlankOrNull(recentSearchCriteria.studyMode) ? recentSearchCriteria.studyMode : "";
					previousQual = !isBlankOrNull(recentSearchCriteria.previousQual) ? recentSearchCriteria.previousQual : "";
					previousQualGrade = !isBlankOrNull(recentSearchCriteria.previousQualGrade) ? recentSearchCriteria.previousQualGrade : "";
					assessmentType = !isBlankOrNull(recentSearchCriteria.assessmentType) ? recentSearchCriteria.assessmentType : "";
					reviewCategory = !isBlankOrNull(recentSearchCriteria.reviewCategory) ? recentSearchCriteria.reviewCategory : "";
					reviewCategoryName = !isBlankOrNull(recentSearchCriteria.reviewCategoryName) ? recentSearchCriteria.reviewCategoryName : "";
				}
				var reviewSubjectOne = ""; var reviewSubjectTwo = ""; var reviewSubjectThree = "";
				if (!isBlankOrNull(reviewCategory)) {
					reviewCategoryArray = reviewCategory.split(",");
					if (reviewCategoryArray.length == 3) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; reviewSubjectThree = reviewCategoryArray[2]; }
					else if (reviewCategoryArray.length == 2) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; }
					else { reviewSubjectOne = reviewCategoryArray[0]; }
				}
				var httpClientConsentFlag = "";
				if ($scope.clientConsentFlag === "Y") {
					httpClientConsentFlag = $scope.prospectusClientConsentFlagIP;
				}
				//
				var stringJson = {
					"userId": userId,
					"institutionId": institutionId,
					"courseId": '',
					"yearOfEntry": selectedYear,
					"userIP": "",
					"userAgent": navigator.userAgent,
					"enquiryType": "PROSPECTUS",
					"message": '',
					"addressLineOne": addressLineOne,
					"addressLineTwo": addressLineTwo,
					"city": city,
					"countyState": countyState,
					"postCode": postCode,
					"suborderItemId": suborderItemId,
					"categoryCode": "",
					"categoryId": "",
					"qualification": qualification,
					"searchKeyword": keywordSearch,
					"locationType": locationType,
					"region": region,
					"regionFlag": regionFlag,
					"studyMode": studyMode,
					"previousQualification": previousQual,
					"previousQualificationGrade": previousQualGrade,
					"assessmentType": assessmentType,
					"reviewSubjectOne": reviewSubjectOne,
					"reviewSubjectTwo": reviewSubjectTwo,
					"reviewSubjectThree": reviewSubjectThree,
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246",
					"clientConsentFlag": httpClientConsentFlag,
					"latitude": latitude,
					"longitude": longtitude
				};
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/enquiry/submit-enquiry-form/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					if ($$D('enqSuccessLoaderIP')) {
						setTimeout(function () {
							$$D('enqSuccessLoaderIP').style.display = 'none';
						}, 1000)

					}

					xtremePushTag("User_Downloads_A_Prospectus", localStorage.getItem('universityId'));
					xtremePushTag("Year_Of_Entry", selectedYear);


					setTimeout(function () {
						$$D('enquirySuccessDivIP').style.display = 'block';
					}, 1000)
					$scope.institutionDetailsList = response.institutionDetailsList;
					$scope.otherInstitutionsList = response.otherInstitutionsList;
					$scope.otherCoursesList = response.otherCoursesList;
					//
					$scope.matchingPercentageFlag;
					if (!isBlankOrNull(locationType) || !isBlankOrNull(region) || !isBlankOrNull(regionFlag) || !isBlankOrNull(studyMode) || !isBlankOrNull(previousQual) || !isBlankOrNull(previousQualGrade) || !isBlankOrNull(assessmentType) || !isBlankOrNull(reviewCategory)) {
						$scope.matchingPercentageFlag = true;
					}
					//
					if (response.otherInstitutionsList != null && response.otherInstitutionsList.length == 1) {
						if (isBlankOrNull(response.otherInstitutionsList[0].institutionId) && isBlankOrNull(response.otherInstitutionsList[0].institutionDisplayName)) {
							$scope.otherInstitutionsList = [];
						}
					}
					$scope.otherCoursesList = response.otherCoursesList;
					if (response.otherInstitutionsList != null && response.otherCoursesList.length == 1) {
						if (isBlankOrNull(response.otherCoursesList[0].institutionId) && isBlankOrNull(response.otherCoursesList[0].institutionDisplayName)) {
							$scope.otherCoursesList = [];
						}
					}
					$scope.closeMark = "course-details";
					$scope.emailSuccess = 'N'
					$scope.prospectusSuccess = 'Y';
					var data = $$D('success-page-IP').innerHTML;
					$compile(data)($scope);
					setTimeout(function () { $("#prosSuccessImgIP").show(); }, 200);
					setTimeout(function () { reviewAndRating(true); }, 500);
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					if ($$D('enqSuccessLoaderIP')) {
						$$D('enqSuccessLoaderIP').style.display = 'none';
					}
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//
			$scope.goCourseDetailsPage = function (institutionId, institutionName, courseId, courseTitle, tileMediaPath) {
				if (!isBlankOrNull(institutionId) && !isBlankOrNull(courseId) && !isBlankOrNull(courseTitle)) {
					localStorage.setItem('courseId', courseId);
					localStorage.setItem('courseName', courseTitle);
					localStorage.setItem('collegeId', institutionId);
				}
				if (!isBlankOrNull(tileMediaPath)) {
					localStorage.setItem('tileMediaPath', tileMediaPath);
				}
				//window.location.href = '../../html/coursedetails/courseDetails.html';
				mainView.router.loadPage('html/coursedetails/courseDetails.html');
			}
			//
			$scope.goToInstitutionProfilePage = function (institutionId, tileMediaPath, obj) {//window.location.replace('ins_profile.html');		
				localStorage.setItem('universityId', institutionId);
				//myApp.closeModal('#institutionhomeCtrl .suc_page');
				myApp.closeModal('#institutionhomeCtrl .popup');

				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}

				firebaseEventTrack('viewitem', { origin: 'recommended', item_category: obj.institutionDisplayName.toLowerCase(), item_name: firebaseSubject.toLowerCase(), item_id: obj.institutionId, search_term: "na" });

				if (!isBlankOrNull(tileMediaPath)) {
					localStorage.setItem('tileMediaPath', tileMediaPath);
				}
				//window.location.href = '../../html/institution/ins_profile.html';
				mainView.router.reloadPage('html/institution/ins_profile.html');
			}

			$scope.validatePostcode = function () {
				if ($('#addressAjaxIP')) {
					if (($('#addressAjaxIP').val()).length > 0) {
						$('#xmarkIP').addClass('srclear');
						$('#ajaxListIP').css('display', 'none');
						var postcode = $('#addressAjaxIP').val();
						postcode = checkPostCode(postcode);
						if (checkPostCode(postcode)) {
							var stringJson = {
								"postCode": postcode.toUpperCase(),
								"appVersion": Constants.appVersion,
								"affiliateId": "220703",
								"accessToken": "246"
							};
							var jsonData = JSON.stringify(stringJson);
							var url = "https://mtest.whatuni.com/wuappws/get-ajax-address/";
							var req = {
								method: 'POST',
								url: url,
								headers: { 'Content-Type': 'application/json' },
								data: jsonData
							}
							$http(req).then(function (httpresponse) {
								var response = httpresponse.data;
								$scope.ajaxAddressList = response.ajaxAddressList;
								if (response.ajaxAddressList != null && response.ajaxAddressList.length > 0) {
									$('#ajaxListIP').css('display', 'block');
								}
							}, function (httpresponse, status) {
								var response = httpresponse.data;
								localStorage.setItem('exception', response.exception);
								localStorage.setItem('status', response.status);
								localStorage.setItem('message', response.message);
								mainView.router.loadPage('html/exception/exception.html');
							});

						}
					} else {
						$('#xmarkIP').removeClass("srclear")
						$('#ajaxListIP').css('display', 'none');
					}
				}
			}
			//
			$scope.gotoPreviousPopup = function () {
				myApp.closeModal('.search_address');
				$scope.clearSearch();
				$('.req_props .tabs #tab8').addClass("active");
				$('.req_props .tabs #tab7').removeClass("active");
			}
			//
			$scope.addMyAddressURL = function () {
				$('#tab9cont').hide();
				$('#tab10cont').show();
				$('#back-tab-searchpopup').show();
				$('.search_address .tabs').removeClass("trnsfrm")
				$('.search_address').removeClass("trnsfrm_fade");
				$('.search_address').addClass("trns_lft");
				$('#buildingNameIP').val('');
				$('#addressLineIP').val('');
				$('#cityIP').val('');
				$('#postCodeIP').val('');
				$('#buildingName_clsIP').removeClass("hd_cls");
				$('#addressLine_clsIP').removeClass("hd_cls");
				$('#city_clsIP').removeClass("hd_cls");
				$('#postCode_clsIP').removeClass("hd_cls");
				$('#saveButtonIP').removeClass("btn_blu");
				$(".search_address .pr_dtl").scrollTop(0, 0);
				if ($$D('buildingNameIP')) {
					returnKey("next");
					setTimeout(function () {
						$$D('buildingNameIP').focus();
					}, 450);
				}

				//mainView.router.loadPage('../../html/enquiry/addMyAddress.html');
			}
			//
			$scope.orderProspectusURL = function () {
				if (localStorage.getItem('addressFlag') == 'YES') {
					localStorage.setItem('addressFlag', 'YES');
				} else {
					localStorage.removeItem('addressFlag');
				}
				mainView.router.loadPage('../../html/enquiry/orderProspectus.html');
			}
			//
			$scope.clearSearch = function () {
				if ($('#addressAjaxIP')) {
					$('#addressAjaxIP').val('');
					$('#xmarkIP').removeClass('srclear').addClass('');
					$('#ajaxListIP').css('display', 'none');
				}
			}
			//
			$scope.orderProspectusPage = function (index) {
				if ($$D('address-' + index + 'IP')) {
					var address = $$D('address-' + index + 'IP').value.split("###");
					var length = address.length;
					var postcode = address[length - 1];
					//var countyState = address[length - 2] ;
					var city = address[length - 2];
					var addressLineTwo = address[length - 3];
					var addressLineOne = address[length - 4];
					localStorage.setItem('addressFlag', 'YES');
					localStorage.setItem('addressLineOne', addressLineOne);
					localStorage.setItem('addressLineTwo', addressLineTwo);
					localStorage.setItem('city', city);
					localStorage.setItem('postCode', postcode);
					localStorage.setItem('countyState', countyState);
					//mainView.router.loadPage('../../html/enquiry/orderProspectus.html');
					$scope.institutionDetails = [];
					var institutionId = !isBlankOrNull(localStorage.getItem('institutionId')) ? localStorage.getItem('institutionId') : "";
					var institutionDisplayName = !isBlankOrNull(localStorage.getItem('institutionDisplayName')) ? localStorage.getItem('institutionDisplayName') : "";
					var logo = !isBlankOrNull(localStorage.getItem('logo')) ? localStorage.getItem('logo') : "";
					var selectedYear = !isBlankOrNull(localStorage.getItem('selectedYear')) ? localStorage.getItem('selectedYear') : "";
					var addressLineOne = !isBlankOrNull(localStorage.getItem('addressLineOne')) ? localStorage.getItem('addressLineOne') : "";
					var addressLineTwo = !isBlankOrNull(localStorage.getItem('addressLineTwo')) ? localStorage.getItem('addressLineTwo') : "";
					var city = !isBlankOrNull(localStorage.getItem('city')) ? localStorage.getItem('city') : "";
					var postCode = !isBlankOrNull(localStorage.getItem('postCode')) ? localStorage.getItem('postCode') : "";
					var countyState = !isBlankOrNull(localStorage.getItem('countyState')) ? localStorage.getItem('countyState') : "";
					$scope.addressFlag = localStorage.getItem('addressFlag');
					if (isBlankOrNull(addressLineTwo)) {
						addressLineOne = addressLineOne + ",";
					} else {
						addressLineTwo = addressLineTwo + ",";
					}
					$scope.institutionDetails = {
						"institutionId": institutionId,
						"institutionDisplayName": institutionDisplayName,
						"logo": logo,
						"addressLineOne": addressLineOne,
						"addressLineTwo": addressLineTwo,
						"city": city,
						"postCode": postCode,
						"countyState": countyState
					};
					var data = $$D('tab8').innerHTML;
					$compile(data)($scope);
					//$('.search_address .tabs').removeClass("trnsfrm");
					myApp.closeModal('.search_address');
					$('.req_props .tabs #tab8').addClass("active");
					$('.req_props .tabs #tab7').removeClass("active");
					$('#submitProspectusIP').show();
				}
			}
			/*
			if($$D('addressAjax')){
				$$D('addressAjax').focus();
			}
			*/
			$scope.addXmark = function (id, xmarkId) {
				if ($$D(id) && $$D(xmarkId)) {
					if ($$D(id).value) {
						$$D(xmarkId).className = 'hd_cls';
						var count = $('.hd_cls')
						if (count.length == 4) {
							$('#saveButtonIP').addClass('btn_blu');
						} else {
							if (count.length == 3) {
								if ($$D('addressLine_clsIP') && isBlankOrNull($$D('addressLine_clsIP').className)) {
									$('#saveButtonIP').addClass('btn_blu');
								}
							} else {
								$('#saveButtonIP').removeClass('btn_blu');
							}
						}
					} else {
						$$D(xmarkId).className = '';
						$('#saveButtonIP').removeClass('btn_blu');
					}
				}
			}
			//
			$scope.validateUserAddress = function () {
				if ($$D('buildingName_clsIP') && $$D('addressLine_clsIP') && $$D('city_clsIP') && $$D('postCode_clsIP')) {
					if ($('#buildingName_clsIP').hasClass('hd_cls') && $('#city_clsIP').hasClass('hd_cls') && $('#postCode_clsIP').hasClass('hd_cls')) {
						$('#saveButtonIP').addClass('btn_blu');
					} else {
						$('#saveButtonIP').removeClass('btn_blu');
					}
				}
			}
			//
			$scope.clearField = function (id, xmarkId) {
				if ($$D(id)) {
					$$D(id).value = '';
					$$D(xmarkId).className = '';
					$('#saveButtonIP').removeClass('btn_blu');
				}

			}
			//
			$scope.saveUserAddress = function (buildingName, addressLine, city, postcode) {
				if ($$D(buildingName) && $$D(addressLine) && $$D(city) && $$D(postcode)) {
					if ($('#saveButtonIP').hasClass('btn_blu')) {
						var buildingName = $$D(buildingName).value.length > 0 ? $$D(buildingName).value : '';
						var addressLine = $$D(addressLine).value.length > 0 ? $$D(addressLine).value : '';
						var city = $$D(city).value.length > 0 ? $$D(city).value : '';
						var postcode = $$D(postcode).value.length > 0 ? $$D(postcode).value : '';
						localStorage.setItem('addressLineOne', buildingName);
						localStorage.setItem('addressLineTwo', addressLine);
						localStorage.setItem('city', city);
						localStorage.setItem('postCode', postcode);
						localStorage.setItem('addressFlag', 'YES');
						//mainView.router.loadPage('../../html/enquiry/orderProspectus.html');
						$scope.institutionDetails = [];
						var institutionId = !isBlankOrNull(localStorage.getItem('institutionId')) ? localStorage.getItem('institutionId') : "";
						var institutionDisplayName = !isBlankOrNull(localStorage.getItem('institutionDisplayName')) ? localStorage.getItem('institutionDisplayName') : "";
						var logo = !isBlankOrNull(localStorage.getItem('logo')) ? localStorage.getItem('logo') : "";
						var selectedYear = !isBlankOrNull(localStorage.getItem('selectedYear')) ? localStorage.getItem('selectedYear') : "";
						var addressLineOne = !isBlankOrNull(localStorage.getItem('addressLineOne')) ? localStorage.getItem('addressLineOne') : "";
						var addressLineTwo = !isBlankOrNull(localStorage.getItem('addressLineTwo')) ? localStorage.getItem('addressLineTwo') : "";
						var city = !isBlankOrNull(localStorage.getItem('city')) ? localStorage.getItem('city') : "";
						var postCode = !isBlankOrNull(localStorage.getItem('postCode')) ? localStorage.getItem('postCode') : "";
						var countyState = !isBlankOrNull(localStorage.getItem('countyState')) ? localStorage.getItem('countyState') : "";
						$scope.addressFlag = localStorage.getItem('addressFlag');
						if (isBlankOrNull(addressLineTwo)) {
							addressLineOne = addressLineOne + ",";
						} else {
							addressLineTwo = addressLineTwo + ",";
						}
						$scope.institutionDetails = {
							"institutionId": institutionId,
							"institutionDisplayName": institutionDisplayName,
							"logo": logo,
							"addressLineOne": addressLineOne,
							"addressLineTwo": addressLineTwo,
							"city": city,
							"postCode": postCode,
							"countyState": countyState
						};
						var data = $$D('tab8').innerHTML;
						$compile(data)($scope);
						//myApp.popup('.req_props');
						//$('.search_address .tabs').removeClass("trnsfrm");
						myApp.closeModal('.search_address');
						$('.req_props .tabs #tab8').addClass("active");
						$('.req_props .tabs #tab7').removeClass("active");
						$('#submitProspectusIP').show();
					}
				}
			}
			//
			$scope.openSearchAdders = function () {
				//myApp.popup('.req_props');
				$('#tab9cont').show();
				$('#tab10cont').hide();
				//$('.search_address .tabs #tab3').addClass("active");
				//$('.search_address .tabs #tab4').removeClass("active");
				var myappLocal = Constants.myapp;
				myappLocal.popup('.req_props');
				if ($("#tab10").hasClass("active")) {
					$('.search_address .tabs').addClass("trnsfrm");
				}
				if ($("#tab9").hasClass("active")) {
					$('.search_address .tabs').addClass("trnsfrm");
				}
				$('.search_address').removeClass("trns_lft");
				$('.search_address').removeClass("trnsfrm_fade");
				$('.search_address').addClass("trnsfrm_fade");
				//if($('.done_btn')) { $('.done_btn').show(); } 

				if ($$D('addressAjaxIP')) {
					setTimeout(function () {
						$$D('addressAjaxIP').focus();
					}, 5);
				}

				clearSearch();
			}
			//
			$scope.getShortListFlag = function (institutionId, removeFrom, addTo, rootCtrl, pageFlag) {
				getShortListRootCtrl(institutionId, removeFrom, addTo, $scope, $http, rootCtrl, pageFlag);
			}
		})
		.controller("dataCtrl", function ($scope, $http) {
			$scope.kisLoadYear = Constants.kisLoadYear;
			$scope.hesaLoadYear = Constants.hesaLoadYear;
			$scope.hcLoadYear = Constants.hcLoadYear;
			$scope.assessedLoadYear = Constants.assessedLoadYear;
		})
		.controller("CourseDetailsRootCtrl", function ($scope, $http, $sce, $timeout, $compile) {
			$scope.emailClientConsentFlagCourse = "N";
			$scope.prospectusClientConsentFlagCourse = "N";
			//
			var customDimensionUniversityName;
			var customDimensionCourseName;
			function onDeviceReady() {
				statusBarFontColor('black');
			}
			document.addEventListener("deviceready", onDeviceReady, false);
			$scope.getSafeHtml = function (x) {
				return $sce.trustAsHtml(x);
			};
			$scope.goBack = function () {
				window.history.back();
			}
			$scope.urlFormat = function (name) {
				name = name.replace(/ /g, "_").toLowerCase();
				return name;
			}
			$scope.isEmpty = function (obj) {
				for (var prop in obj) {
					if (obj.hasOwnProperty(prop))
						return false;
				}
				return true;
			}

			//var galleryTop=null;
			//var galleryThumbs=null;
			var slideCount = 0;
			
			$scope.appInteractionLogging = function (activity, extraTextURL, sectionProfileId, sectionTypeName, extraParam) {
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var courseId = !isBlankOrNull(localStorage.getItem('courseId')) ? localStorage.getItem('courseId') : null;
				var courseName = !isBlankOrNull(localStorage.getItem('courseName')) ? localStorage.getItem('courseName') : null;
				var collegeId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : null;
				var userAgent = navigator.userAgent;
				var userIp = null;
				var suborderItemId = null;
				var profileId = null;
				if ($scope.enquiryInfo[0].suborderItemId) {
					suborderItemId = $scope.enquiryInfo[0].suborderItemId;
				}
				if (!isBlankOrNull(sectionProfileId)) {
					profileId = sectionProfileId;
				}
				//to null course id for contact instituion and hotline no db stats
				if (!isBlankOrNull(extraParam) && extraParam == 'NULL_COURSE_ID') {
					courseId = null;
				}
				//var typeName = "COURSE_DETAIL";	
				var typeName = "";
				if (!isBlankOrNull(sectionTypeName)) {
					typeName = sectionTypeName;
				}
				var activity = activity;
				var extraText = null;
				if (!isBlankOrNull(extraTextURL)) {
					extraText = extraTextURL;
				}
				var stringJson = {
					"userId": userId,
					"collegeId": collegeId,
					"courseId": courseId,
					"userIp": userIp,
					"userAgent": userAgent,
					"jsLog": 'Y',
					"suborderItemId": suborderItemId,
					"profileId": profileId,
					"typeName": typeName,
					"activity": activity,
					"extraText": extraText,
					"latitude": latitude,
					"longitude": longtitude,
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/interaction/db-stats/logging/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					var logStatusId = response.logStatusId;
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					// do nothing				
				});
			}
			//
			//googleTagManagerPageView('/course-details/'+localStorage.getItem('courseId'));
			//
			$scope.uniName = "";
			//
			var galleryNextSlide = null;
			$scope.getCourses = function (arg, opportunityId, opportunityName) {
				preMoreInfoHgt = 0;
				preAddInfoHgt = 0;
				$scope.institutionBrowseLogo = Constants.institutionBrowseLogo;
				$scope.matchPecenatgeFlag;
				var studyMode = "";
				var previousQual = "";
				var previousQualGrade = "";
				var courseId = "";
				var courseName = "";
				var collegeId = "";
				var qualification = "";
				var orderBy = "";
				var locationType = "";
				var region = "";
				var regionFlag = "";
				var assessmentType = "";
				var keywordSearch = "";
				var reviewCategory = "";
				var reviewCategoryName = "";
				var searchCategoryCode = "";
				//
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					//
					searchCategoryCode = !isBlankOrNull(recentSearchCriteria.searchCategoryCode) ? recentSearchCriteria.searchCategoryCode : "";
					searchCategoryId = !isBlankOrNull(recentSearchCriteria.searchCategoryId) ? recentSearchCriteria.searchCategoryId : "";
					keywordSearch = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "";
					qualification = !isBlankOrNull(recentSearchCriteria.qualification) ? recentSearchCriteria.qualification : "";
					orderBy = !isBlankOrNull(recentSearchCriteria.orderBy) ? recentSearchCriteria.orderBy : "";
					locationType = !isBlankOrNull(recentSearchCriteria.locationType) ? recentSearchCriteria.locationType : "";
					region = !isBlankOrNull(recentSearchCriteria.region) ? recentSearchCriteria.region : "";
					regionFlag = !isBlankOrNull(recentSearchCriteria.regionFlag) ? recentSearchCriteria.regionFlag : "";
					studyMode = !isBlankOrNull(recentSearchCriteria.studyMode) ? recentSearchCriteria.studyMode : "";
					previousQual = !isBlankOrNull(recentSearchCriteria.previousQual) ? recentSearchCriteria.previousQual : "";
					previousQualGrade = !isBlankOrNull(recentSearchCriteria.previousQualGrade) ? recentSearchCriteria.previousQualGrade : "";
					assessmentType = !isBlankOrNull(recentSearchCriteria.assessmentType) ? recentSearchCriteria.assessmentType : "";
					reviewCategory = !isBlankOrNull(recentSearchCriteria.reviewCategory) ? recentSearchCriteria.reviewCategory : "";
					reviewCategoryName = !isBlankOrNull(recentSearchCriteria.reviewCategoryName) ? recentSearchCriteria.reviewCategoryName : "";
					//
					if (!isBlankOrNull(studyMode) || !isBlankOrNull(previousQual) || !isBlankOrNull(previousQualGrade) || !isBlankOrNull(assessmentType) || !isBlankOrNull(locationType) || !isBlankOrNull(region) || !isBlankOrNull(regionFlag) || !isBlankOrNull(reviewCategory)) {
						$scope.matchPecenatgeFlag = true;
					}
				}
				courseId = !isBlankOrNull(localStorage.getItem('courseId')) ? localStorage.getItem('courseId') : "";
				courseName = !isBlankOrNull(localStorage.getItem('courseName')) ? localStorage.getItem('courseName') : "";
				collegeId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";
				//
				var reviewSubjectOne = ""; var reviewSubjectTwo = ""; var reviewSubjectThree = "";
				if (!isBlankOrNull(reviewCategory)) {
					reviewCategoryArray = reviewCategory.split(",");
					if (reviewCategoryArray.length == 3) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; reviewSubjectThree = reviewCategoryArray[2]; }
					else if (reviewCategoryArray.length == 2) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; }
					else { reviewSubjectOne = reviewCategoryArray[0]; }
				}
				//
				if (!isBlankOrNull(opportunityId)) {
					$$D('loaderCourseCD').style.display = 'block';
				} else { opportunityId = ""; }
				//
				if (!isBlankOrNull(searchCategoryCode) && !isBlankOrNull(collegeId)) { xtremePushTag("Category_Code", searchCategoryCode + "&IP - " + collegeId); }
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"userAgent": navigator.userAgent,
					"collegeId": collegeId,
					"courseId": courseId,
					"opportunityId": opportunityId,
					"trackSessionId": "",
					"qualification": qualification,
					"region": region,
					"regionFlag": regionFlag,
					"studyMode": studyMode,
					"previousQual": previousQual,
					"previousQualGrade": previousQualGrade,
					"locationType": locationType,
					"searchKeyword": keywordSearch,
					"assessmentType": assessmentType,
					"reviewSubjectOne": reviewSubjectOne,
					"reviewSubjectTwo": reviewSubjectTwo,
					"reviewSubjectThree": reviewSubjectThree,
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion,
					"userLatitude": latitude,
					"userLongitude": longtitude,
				};
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/course-details";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					
					//				
					$$D('loaderCourseCD').style.display = 'none';
					$$D('coursesDetailsView').style.display = 'block';
					//

					angular.forEach(response.loadYearList, function (value, key) {
						if (value.loadYearName == "KIS_LOAD_YEAR") {
							Constants.kisLoadYear = value.loadYearData;
						}
						if (value.loadYearName == "HESA_LOAD_YEAR") {
							Constants.hesaLoadYear = value.loadYearData;
						}
						if (value.loadYearName == "HC_LOAD_YEAR") {
							Constants.hcLoadYear = value.loadYearData;
						}
						if (value.loadYearName == "ASSESSED_LOAD_YEAR") {
							Constants.assessedLoadYear = value.loadYearData;
						}
					});
					$scope.tooltipindex = 0;
					$scope.showFeePodFlag = "NO";
					//$scope.feesRegion = [];
					$scope.feesDetailsList = response.feesDetailsList;
					$scope.oppurtunityEntryReqList = response.oppurtunityEntryReqList;
					console.log($scope.feesRegion);
					$scope.advertiserFlag = response.advertiserFlag;
					advertiserFlag = response.advertiserFlag;
					$scope.courseInfoList = response.courseInfo;
					$scope.openDayFlag = response.openDayFlag;
					//for tuition fees pod to find the initial available value and data available					
					var feeRegionCnt = 1;
					for (var i = 0; i < $scope.feesDetailsList.length; i++) {
						if ($scope.feesDetailsList[i].fee || $scope.feesDetailsList[i].state || $scope.feesDetailsList[i].feeDesc || $scope.feesDetailsList[i].url) {
							if ($scope.feesDetailsList[i].feeType && feeRegionCnt == 1) {
								$scope.showFeePodFlag = "YES";
								$scope.initialFeeSelect = "div_" + $scope.feesDetailsList[i].feeType;
								$scope.initialFeeType = $scope.feesDetailsList[i].feeType;
								$scope.tooltipindex = i;
								//console.log($scope.initialFeeSelect);	
							}
							if (feeRegionCnt == 2) {
								$scope.showFeeSelectBox = "YES";
								//console.log("dd"+$scope.showFeeSelectBox)
								//break;
							}
							feeRegionCnt++;

							if ($scope.feesDetailsList[i].matchedRegion == "Y") {
								$scope.initialFeeSelect = "div_" + $scope.feesDetailsList[i].feeType;
								$scope.initialFeeType = $scope.feesDetailsList[i].feeType;
								$scope.tooltipindex = i;
							}
						}

					}
					//console.log("pod status >>"+$scope.showFeePodFlag + "showfeeSctbox" + feeRegionCnt + "shwsts" +$scope.showFeeSelectBox); 					
					//for clearing entry requirement to find the initial available value
					if ($scope.oppurtunityEntryReqList.length > 0) {
						$scope.selectedEntryDesc = $scope.oppurtunityEntryReqList[0].entryDesc;
						$scope.initialEntryDesc = $scope.oppurtunityEntryReqList[0].entryDesc;
						//console.log($scope.selectedEntryDesc);						
					}
					//
					if ($scope.openDayFlag == "Y") {
						$scope.reservePlace(null, null);
					}
					var userID = localStorage.getItem('LOGGED_IN_USER_ID');
					customDimensionUniversityName = response.courseInfo[0].collegeNameDisplay;
					customDimensionCourseName = response.courseInfo[0].courseTitle;

					firebaseEventTrack('screenview', { 'page_name': '/course-details/' + localStorage.getItem('courseId') });



					if ($scope.advertiserFlag == "N") {
						function onDeviceReady() {
							statusBarFontColor('white');
						}
						document.addEventListener("deviceready", onDeviceReady, false);
					}
					//                             
					$scope.enquiryInfo = response.enquiryInfo;
					$scope.enquiryInfo[0].buttonNavClass = "pr_btn ";
					if ($scope.enquiryInfo[0].suborderItemId) {
						localStorage.setItem('cdSuborderItemId', $scope.enquiryInfo[0].suborderItemId)
					}
					var btnCount = 0;
					if ($scope.openDayFlag == 'Y') {
						btnCount = 1;
					}
					if ($scope.enquiryInfo[0].prospectusWebformFlag != 'Y' && $scope.enquiryInfo[0].prospectus != null) {
						btnCount = btnCount + 1;
					}
					if ($scope.enquiryInfo[0].emailWebformFlag != 'Y' && $scope.enquiryInfo[0].email != null) {
						btnCount = btnCount + 1;
					}
					if (btnCount == 1) {
						$scope.enquiryInfo[0].buttonNavClass += 'w100p';
					} else if (btnCount == 2) {
						$scope.enquiryInfo[0].buttonNavClass += 'w50p';
					}

					$scope.applicantsData = response.applicantsData;
					//				
					$scope.moduleInfo = response.moduleInfo;
					$scope.moduleDetails = response.moduleDetails;
					$scope.uniRankingDetails = response.uniRankingDetails;
					$scope.opportunityDetails = response.opportunityDetails;
					$scope.previousSubject = response.previousSubject;
					//
					$scope.entryRequirementList = response.entryRequirementList;
					$scope.assessedCourseDet = response.assessedCourse;
					$scope.keyStatsInfo = response.keyStatsInfo;
					$scope.reviewList = response.reviewList;
					$scope.otherCoursesPod = response.otherCoursesPod;
					//    
					$scope.subjects = Constants.subjects;
					$scope.cdAdvertImg = Constants.cdAdvertImg;
					$scope.cdNonAdvertImg = !isBlankOrNull(localStorage.getItem('tileMediaPath')) ? localStorage.getItem('tileMediaPath') : UniRandomImage();
					//
					$scope.universityCourseReviewURL = response.universityCourseReviewURL;
					//
					$scope.cdSection = [];
					if (($scope.courseInfoList.length > 0) || ($scope.opportunityDetails[0].domPerYearCost || $scope.opportunityDetails[0].restUkCourseCost || $scope.opportunityDetails[0].euCostPerYear || $scope.opportunityDetails[0].intlPerYearCost)) {
						$scope.overviewSectionFlag = true;
						$scope.uniName = $scope.courseInfoList[0].collegeNameDisplay;
						$scope.cdSection.push({ "sectionFlag": $scope.overviewSectionFlag, "sectionName": "Overview" });
						//
						if ($scope.opportunityDetails.length > 1) {
							angular.forEach($scope.opportunityDetails, function (value, index) {
								if ($scope.opportunityDetails[index].selectedOpportunity == "Y") {
									$("#multipleOppDetail").show();
									$scope.mobselected = $scope.opportunityDetails[index].opportunityId;
									$("#opportunityName").html("<span>Course options</span>" + $scope.opportunityDetails[index].opportunity);
								}
							});
						}
					}
					if (($scope.oppurtunityEntryReqList.length > 0) || (($scope.applicantsData.length > 0) && ($scope.applicantsData[0].totalApplicants || $scope.applicantsData[0].successfulApplicants || $scope.applicantsData[0].applicantSuccessRate)) || ($scope.previousSubject.length > 0)) {
						$scope.entryReqSectionFlag = true;
						$scope.cdSection.push({ "sectionFlag": $scope.entryReqSectionFlag, "sectionName": "Entry reqs" });
					}
					if (($scope.moduleDetails.length > 0 && !$scope.moduleInfo) || (($scope.assessedCourseDet.length > 0) && (!isCDPageBlankOrNull($scope.assessedCourseDet[0].exam) || !isCDPageBlankOrNull($scope.assessedCourseDet[0].exam) || !isCDPageBlankOrNull($scope.assessedCourseDet[0].exam)))) {
						$scope.moduleDetailsSectionFlag = true;
						$scope.cdSection.push({ "sectionFlag": $scope.moduleDetailsSectionFlag, "sectionName": "Modules" });
					}
					if ((($scope.keyStatsInfo.length > 0) && ($scope.keyStatsInfo[0].dropOutRate || $scope.keyStatsInfo[0].ratio)) || ($scope.reviewList.length > 0) || (($scope.keyStatsInfo.length > 0) && ($scope.keyStatsInfo[0].salary6MonthsAtUniPer || $scope.keyStatsInfo[0].salary6Months || $scope.keyStatsInfo[0].salary6MonthsAtUni))) {
						$scope.keyStatsSectionFlag = true;
						$scope.cdSection.push({ "sectionFlag": $scope.keyStatsSectionFlag, "sectionName": "Key stats" });
					}
					//
					// --- PAGE VIEW STATS
					//												
					if ($scope.courseInfoList.length > 0) {
						//googleTagManagerEventTrack(null, 'Course Nav', 'Overview', $scope.uniName, '0');
						if ('Y' == $scope.clearingResultFlag) {
							$scope.appInteractionLogging(null, null, null, "CLEARING_COURSE_DETAIL");
						} else {
							$scope.appInteractionLogging(null, null, null, "COURSE_DETAIL");
						}
					}
					setTimeout(function () {
						var currentSlideCount = 0;
						var galleryTop = new Swiper('#courseDetailsRootCtrl .gallery-top', {
							nextButton: '.swiper-button-next',
							prevButton: '.swiper-button-prev',
							spaceBetween: 100,
							autoHeight: 'true',
							onSlideChangeStart: function (swiper) {
								//googleTagManagerEventTrack(null, 'Course Nav', $("#courseDetailsRootCtrl .gallery-thumbs .thumbs-"+swiper.activeIndex).html(), $scope.uniName, '0');
								$("#courseDetailsRootCtrl .gallery-thumbs .swiper-slide").removeClass('swiper-slide-active');
								$("#courseDetailsRootCtrl .gallery-thumbs .thumbs-" + swiper.activeIndex).addClass('swiper-slide-active');

								$("#courseDetailsRootCtrl .gallery-top .swiper-slide").removeClass('swiper-slide-active');
								$("#courseDetailsRootCtrl .gallery-top .gallery_nav-" + swiper.activeIndex).addClass('swiper-slide-active');

								//$("#courseDetailsRootCtrl .gallery-top .swiper-wrapper").css('height',$("#courseDetailsRootCtrl .gallery_nav-"+swiper.activeIndex).height()+"px");

								$("#courseDetailsRootCtrl .gallery-thumbs .swiper-slide").removeClass('swiper-slide-active-freemode');
								$("#courseDetailsRootCtrl .gallery-thumbs .thumbs-" + swiper.activeIndex).addClass('swiper-slide-active-freemode');
								//
								if (swiper.activeIndex == 0) {
									$(".gallery-thumbs .swiper-wrapper").addClass('ml35');
								} else {
									$(".gallery-thumbs .swiper-wrapper").removeClass('ml35');
								}
								//
								$("#courseDetailsRootCtrl #extraSpace_" + swiper.activeIndex).hide();
								if (($('#courseDetailsRootCtrl .gallery_nav-' + swiper.activeIndex).height()) < 550) {
									$("#courseDetailsRootCtrl #extraSpace_" + swiper.activeIndex).show();
								}
								if ($("#courseDetailsRootCtrl #extraSpace_" + swiper.activeIndex).css('display') == "none") {
									$("#mainSection .swiper-wrapper").height($("#courseDetailsRootCtrl .gallery_nav-" + swiper.activeIndex).height() + 25);
								} else {
									$("#mainSection .swiper-wrapper").height($("#courseDetailsRootCtrl .gallery_nav-" + swiper.activeIndex).height() + 200);
								}
								if (currentSlideCount < swiper.activeIndex) {
									scrolling_array[currentSlideCount] = $('#courseDetailsRootCtrl .page-content').scrollTop();
									console.log('scrolling_array');
									console.log(scrolling_array);
									console.log('scroll top > ' + $('#courseDetailsRootCtrl .page-content').scrollTop());
									if ($('#courseDetailsRootCtrl .gallery_nav-' + swiper.activeIndex).height() >= 500 && $('#courseDetailsRootCtrl .page-content').scrollTop() >= 120) {
										$$('#courseDetailsRootCtrl .page-content').scrollTop(120, 0);
									} else {
										$$('#courseDetailsRootCtrl .page-content').scrollTop(0, 0);
									}
								}
								if (currentSlideCount > swiper.activeIndex) {
									setTimeout(function () {
										if (scrolling_array[swiper.activeIndex] != "undefined")
											$('#courseDetailsRootCtrl .page-content').scrollTop(scrolling_array[swiper.activeIndex], 0);
									}, 160)
								}
								currentSlideCount = swiper.activeIndex;


							}
						});
						var currentSlideCount = 0;
						var scrolling_array = [];
						var galleryThumbs = new Swiper('#courseDetailsRootCtrl .gallery-thumbs', {
							freeMode: true,
							spaceBetween: 30,
							centeredSlides: true,
							slidesPerView: 'auto',
							touchRatio: 0.2,
							slideToClickedSlide: true,
							onSlideChangeStart: function (swiper) {
								//alert("gallery-thumbs");
								/*
								$("#courseDetailsRootCtrl .gallery-top .swiper-slide").removeClass('swiper-slide-active');
								$("#courseDetailsRootCtrl .gallery-top .gallery_nav-"+swiper.activeIndex).addClass('swiper-slide-active');
								*/
								//
								$("#courseDetailsRootCtrl .gallery-thumbs .swiper-slide").removeClass('swiper-slide-active-freemode');
								$("#courseDetailsRootCtrl .gallery-thumbs .thumbs-" + swiper.activeIndex).addClass('swiper-slide-active-freemode');

								//
								//
								if (swiper.activeIndex == 0) {
									$(".gallery-thumbs .swiper-wrapper").addClass('ml35');
								} else {
									$(".gallery-thumbs .swiper-wrapper").removeClass('ml35');
								}
								//
								//alert(swiper.activeIndex);
								//galleryTop.slideTo(swiper.activeIndex);
								galleryThumbs.update();
								galleryTop.update();
							},
							onSlideChangeEnd: function (swiper) {
								if (currentSlideCount < swiper.activeIndex) { }
								currentSlideCount = swiper.activeIndex;
								slideCount = swiper.activeIndex;
							},
							onClick: function (swiper, event) {
								//alert("test"+swiper.activeIndex);
								event.preventDefault();
								galleryTop.slideTo(swiper.activeIndex);
							}
						});
						galleryThumbs.update();
						galleryTop.update();
						galleryTop.params.control = galleryThumbs;
						//galleryThumbs.params.control = galleryTop;   
						galleryNextSlide = galleryTop;
						$("#courseDetailsRootCtrl .gallery-thumbs .swiper-slide").removeClass('swiper-slide-active-freemode');
						$("#courseDetailsRootCtrl .gallery-thumbs .thumbs-0").addClass('swiper-slide-active-freemode');
						$("#courseDetailsRootCtrl .gallery-thumbs .swiper-wrapper").addClass('ml35');
						$('#courseDetailsRootCtrl .gallery-top  .swiper-wrapper').css('transform', 'translate3d(0px, 0px, 0px)');
						if (arg != '') {
							myApp.pullToRefreshDone();
							slideCount = 0;
						}
						//
						$("#doughnutChart_application_rate").html("");
						$("#doughnutChart_application_rate1").html("");
						$("#doughnutChart_application_rate2").html("");
						if (!isCDPageBlankOrNull($('#exam').val()) || !isCDPageBlankOrNull($('#courseWork').val()) || !isCDPageBlankOrNull($('#praticalWork').val())) {
							drawCdAssesseddoughnutchart($$D('exam').value, $$D('courseWork').value, $$D('praticalWork').value, 'doughnutChart_application_rate1');
						}
						if (!isCDPageBlankOrNull($('#employedAfter6').val())) {
							drawCddoughnutchart('employedAfter6', 'doughnutChart_application_rate2', '#f8716c');
						}
						if (!isCDPageBlankOrNull($('#successRate').val())) {
							drawCddoughnutchart('successRate', 'doughnutChart_application_rate', '#f8716c');
						}
						$("#courseDetailsRootCtrl #extraSpace_0").hide();
						if (($('#courseDetailsRootCtrl .gallery_nav-0').height()) < 550) {
							$("#courseDetailsRootCtrl #extraSpace_0").show();
						}
						if ($("#courseDetailsRootCtrl #extraSpace_0").css('display') == "none") {
							$("#mainSection  .swiper-wrapper").height($("#courseDetailsRootCtrl .gallery_nav-0").height() + 30);
						} else {
							$("#mainSection  .swiper-wrapper").height($("#courseDetailsRootCtrl .gallery_nav-0").height() + 200);
						}
					}, 300);
					//
					var page = $$('.page[data-page="course-details"]')[0].f7PageData;
					if (Object.keys(page.query).length !== 0 && arg == '') {
						setTimeout(function () {
							console.log("trigger called");
							if (page.query.action == "shortlist") {
								if (!$("#" + page.query.action_id).hasClass('fav_fill'))
									$("#" + page.query.action_id).trigger('click');
							} else {
								//$("#"+page.query.action_id).addClass('open-popup');
								$("#" + page.query.action_id).trigger('click');
								//$("#"+page.query.action_id).click();
							}
						}, 1000);
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			$scope.emailCheckGuestUser = function () {
				if (isGuestUser()) {
					localStorage.setItem('action', "cdEmailuni");
					localStorage.setItem('action_id', "emailUni");
					Constants.mainViewObj.router.loadPage({ url: "html/user/home.html", pushState: false });
				} else {
					$scope.emailClientConsentFlagCourse = "N";
					$("#emailClientConsentFlagCourse").prop('checked', false);
					myApp.popup('#emailUni_popup');
					//$("#emailUni").addClass('open-popup');
					$scope.interactionEventLogging('EMAIL');
					$scope.emailuni(5);

				}
			}


			$scope.prospectusCheckGuestUser = function () {
				if (isGuestUser()) {
					localStorage.setItem('action', "cdProspectus");
					localStorage.setItem('action_id', "cd_req_props");
					Constants.mainViewObj.router.loadPage({ url: "html/user/home.html", pushState: false });
				} else {
					$scope.prospectusClientConsentFlagCourse = "N";
					$("#prospectusClientConsentFlagCourse").prop('checked', false);
					//myApp.popup('#cd_req_props_popup');
					$("#cd_req_props").addClass('open-popup');
					$scope.interactionEventLogging('PROSPECTUS');
					$scope.prospectus(1);

				}
			}
			//
			$scope.getFeeType = function (optionValue) {
				var changedFeeType = optionValue.replace("div_", "");
				$scope.initialFeeType = changedFeeType;
				console.log("changedFeeType >>" + changedFeeType);
				$scope.tooltipindex = $("#" + optionValue).attr("tooltipindex");
				console.log("scopechangedFeeType >>" + $scope.initialFeeType);
			}
			//please contact link logging in fees pod
			$scope.contactUniversityLog = function (contactUrl) {
				if ($scope.advertiserFlag == "Y")
					$scope.appInteractionLogging('HOTCOURSES: WEBSITE CLICK', encodeURI(contactUrl));
				var uniName = $scope.uniName;
				if (!isBlankOrNull(uniName)) {
					uniName = uniName.toLowerCase();
				}
				//googleTagManagerEventTrack(null, 'Interaction', 'Webclick', uniName, encodeURI(contactUrl));
				window.open(encodeURI(contactUrl), '_system');
			}
			
			//contact institution
			$scope.contactInstituion = function (num) {
				var collegeId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";
				var uniName = $scope.uniName;
				if (!isBlankOrNull(uniName)) {
					uniName = uniName.toLowerCase();
				}
				/*
				if(!isBlankOrNull($scope.enquiryInfo[0].emailWebform)) {					
					googleTagManagerEventTrack(null, 'Interaction', 'Webclick', uniName, encodeURI($scope.enquiryInfo[0].emailWebform));
					$scope.appInteractionLogging('HOTCOURSES: EMAIL SENT: WEBSITE CLICK', encodeURI($scope.enquiryInfo[0].emailWebform), null, null, 'NULL_COURSE_ID');
					window.open(encodeURI($scope.enquiryInfo[0].website), '_system');
				} else {*/
				//googleTagManagerEventTrack(null, 'Engagement', 'Request Email', uniName, '0');

				var userID = localStorage.getItem('LOGGED_IN_USER_ID');
				firebaseEventTrack('screenview', { 'page_name': '/email/' + collegeId });
				emailUniPopup(num);
				//window.location.href='../../html/institution/emailuni.html';                
				//}
			}
			
			//End of clearing button action in CD page
			$$('#courseDetailsRootCtrl .course_header').scroll(function () {
				scroll = $('#courseDetailsRootCtrl .page-content').scrollTop() + 100;
				if (scroll > 200 && $('#courseDetailsRootCtrl #stk_app').html() == '' && $('#courseDetailsRootCtrl .gallery-top').height() > 550) {
					var height = parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height').replace("px", ""));
					$('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height', height + "px");
					$('#courseDetailsRootCtrl #stk_app').append($("#courseDetailsRootCtrl .stk_div"));
					$('#courseDetailsRootCtrl #stk_app').addClass('stk_fix');
					$('#courseDetailsRootCtrl .stk_div').addClass('mobsec');
					statusBarFontColor('black');
				}
				if (scroll < 200 && $('#courseDetailsRootCtrl #stk_app').html() != '') {
					$('#courseDetailsRootCtrl #stk_div').prepend($("#courseDetailsRootCtrl .stk_div"));
					$('#courseDetailsRootCtrl #stk_app').removeClass('stk_fix');
					$('#courseDetailsRootCtrl #stk_div').fadeIn(40);
					$('#courseDetailsRootCtrl .stk_div').removeClass('mobsec');
					console.log('$scope.advertiserFlag > ' + $scope.advertiserFlag);
					if ($scope.advertiserFlag == 'N') {
						statusBarFontColor('white');
					}
				}
			});
			//
			$scope.interactionEventLogging = function (interationType) {
				var uniName = $scope.uniName;
				if (!isBlankOrNull(uniName)) {
					uniName = uniName.toLowerCase();
				} else {
					uniName = 'not set';
				}

				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}

				var coupon_value = 'not set';
				var currency_value = 0;
				if (interationType == "PROSPECTUS") {
					coupon_value = "engagement_order_prospectus";
					if ($scope.enquiryInfo[0].hasOwnProperty('prospectusPrice')) {
						currency_value = parseFloat($scope.enquiryInfo[0].prospectusPrice);
					}
				} else if (interationType == "EMAIL") {
					coupon_value = "engagement_request_email";
					console.log("enquiryDetails");
					console.log($scope.enquiryDetails);
					if ($scope.enquiryInfo[0].hasOwnProperty('emailPrice')) {
						currency_value = parseFloat($scope.enquiryInfo[0].emailPrice);
					}

				} else if (interationType == "OPEN_DAYS") {
					coupon_value = "engagement_open_days_button";
					if ($scope.enquiryInfo[0].hasOwnProperty('openDaysPrice')) {
						currency_value = parseFloat($scope.enquiryInfo[0].openDaysPrice);
					}
				}



				
				var interactionPagename = '/course-details/' + localStorage.getItem('courseId');
				
				firebaseEventTrack('addtocart', { item_category: uniName, item_name: firebaseSubject.toLowerCase(), item_location_id: localStorage.getItem('courseId'), currency: 'GBP', value: currency_value, coupon: coupon_value, item_id: localStorage.getItem('collegeId'), quantity: 1, interaction_pagename: interactionPagename  });

			}
			//			
			$(document).on('click', '#courseDetailsRootCtrl .open-popup1', function () {
				//$$('#courseDetailsRootCtrl .open-popup').click(function () {
				//alert($(this).attr('data-popup'));
				//if($(this).attr('data-popup') == 'req_props') { myApp.closeModal('.search_address'); }
				var courseId = !isBlankOrNull(localStorage.getItem('courseId')) ? localStorage.getItem('courseId') : "";
				var collegeId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";
				console.log($scope.yearList);
				if ($(this).attr('data-popup') == ".second_page") {
					$('#enqYoeSubmitBtn').attr("disabled", "disabled");
					//googleTagManagerPageView('/email/'+localStorage.getItem('universityId'));

					//$(".test").removeClass('active');
					//$scope.yearList=$scope.yearList;
					angular.forEach($scope.yearList, function (value, key) {
						if (value.selectedYear == 'Y') {
							$("#enquiryYearOfEntry-" + key).addClass('active');
							$('#enqYoeSubmitBtn').removeAttr("disabled");
						} else {
							$("#enquiryYearOfEntry-" + key).removeClass('active');
						}
					});
					var yearData = $$D('emailYear').innerHTML;
					$compile(yearData)($scope);
				}
				if ($(this).attr('data-popup') == ".req_props") {
					$('#prosYoeSubmitBtn').attr("disabled", "disabled");
					angular.forEach($scope.yearList, function (value, key) {
						if (value.selectedYear == 'Y') {
							$("#prospectusYearOfEntry-" + key).addClass('active');
							$('#prosYoeSubmitBtn').removeAttr("disabled");
						} else {
							$("#prospectusYearOfEntry-" + key).removeClass('active');
						}
					});
					var yearData = $$D('prospectusYear').innerHTML;
					$compile(yearData)($scope);
					var addressData = $$D('prospectusAddress').innerHTML;
					$compile(addressData)($scope);
				}
				//statusBarFontColor('black');
			});
			$$(document).on('click', '.close-popup', function () {
				console.log('About Popup close')
				statusBarFontColor('black');
				if ($scope.checkAdvUniversity == 'N') {
					function onDeviceReady() {
						statusBarFontColor('white');
					}
					document.addEventListener("deviceready", onDeviceReady, false);
				}
			});
			$scope.getCourses('');
			//Below code for navigate to institution profile page
			$scope.goToInstitutionProfilePage = function (institutionId, tileMediaPath) {
				if (!isBlankOrNull(tileMediaPath)) {
					localStorage.setItem('tileMediaPath', tileMediaPath);
				}
				localStorage.setItem('universityId', institutionId);
				mainView.router.loadPage('html/institution/ins_profile.html');
			}
			var ptrContent = $$('#courseDetailsRootCtrl .pull-to-refresh-content');
			ptrContent.on('refresh', function (e) {
				$scope.getCourses('pull-to-refresh', $("#multiOppId").val(), $("#multiOppName").val());
				
					$scope.showEnquiryForm();
					$scope.showProspectusEnquiryForm();
			});

			$scope.slideNext = function (slide) {
				var currentslider = $('#courseDetailsRootCtrl .gallery-top  .swiper-slide-active').attr('currentslider');
				currentslider = currentslider.replace("swiper-slide-mycontent_", "");
				currentslider = parseInt(currentslider) + 1;
				if (slide == 'Overview') { currentslider = 0; }
				//galleryTop.slideTo(currentslider);
				galleryNextSlide.slideTo(currentslider);
			}
			//		
			$scope.courseDetailsURL = function (courseId, collegeId, tileMediaPath, obj) {
				localStorage.setItem('courseId', courseId);
				localStorage.setItem('collegeId', collegeId);
				if (!isBlankOrNull(tileMediaPath)) {
					localStorage.setItem('tileMediaPath', tileMediaPath);
				}

				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}

				firebaseEventTrack('viewitem', { origin: 'recommended', item_category: obj.collegeNameDisplay.toLowerCase(), item_name: firebaseSubject.toLowerCase(), item_id: collegeId, search_term: "na" });


				mainView.router.reloadPage('html/coursedetails/courseDetails.html')
			}
			$scope.emailuni = function (num) {
				var collegeId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";
				if (!isBlankOrNull($scope.enquiryInfo[0].emailWebform)) {
					$scope.appInteractionLogging('HOTCOURSES: EMAIL SENT: WEBSITE CLICK', encodeURI($scope.enquiryInfo[0].emailWebform));
					window.open(encodeURI($scope.enquiryInfo[0].website), '_system');
				} else {

					var userID = localStorage.getItem('LOGGED_IN_USER_ID');
					firebaseEventTrack('screenview', { 'page_name': '/email/' + collegeId });
					emailUniPopup(num);
					//window.location.href='../../html/institution/emailuni.html';                
				}
			}
			$scope.prospectus = function (num) {
				var collegeId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";
				if ($scope.enquiryInfo[0].prospectusWebform != null) {
					$scope.appInteractionLogging('HOTCOURSES: EMAIL SENT: REQUEST PROSPECTUS: WEBSITE CLICK', encodeURI($scope.enquiryInfo[0].prospectusWebform));
					window.open(encodeURI($scope.enquiryInfo[0].prospectus), '_system');
				} else {

					var userID = localStorage.getItem('LOGGED_IN_USER_ID');
					firebaseEventTrack('screenview', { 'page_name': '/prospectus/' + collegeId });
					if ($$D('prospectusSubBtn')) {
						if ($('#prospectusSubBtn').is(':disabled')) {
							$("#prospectusSubBtn").removeAttr('disabled');
						}
					}
					prospectusPopup(num);
					//window.location.href='../../html/institution/prospectus.html';
				}
			}
			//
			$scope.opendayCountCheck = function() {
				if($scope.moreOpendayList.length===1) {
					var url = $scope.opendayList[0].bookingUrl;
					console.log("openday url",url);
					$scope.reservePlaceLogging();
					//$scope.reservePlaceActionLog();
					inAppBrowser(url);
				} else {
					localStorage.setItem('openday_college_id', localStorage.getItem('universityId'));
					localStorage.setItem('openday_event_id', "");
					mainView.router.loadPage('html/openday/openDays.html');
				}
			}
			//
			$scope.reservePlace = function (opendayEventId, opendate) {
				collegeId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";

				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"accessToken": "246",
					"affiliateId": "220703",
					"eventId": opendayEventId,
					"collegeId": collegeId,
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/enquiry/reserve-open-days/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;

					//googleTagManagerEventTrack(null, 'interaction', 'Webclick', response.opendayList[0].collegeName, '100');

					//googleTagManagerEventTrack(null, 'open-days', 'reserve-place', response.opendayList[0].collegeName, '1');				

					$scope.opendayList = response.opendayList;
					$scope.moreOpendayList = response.moreOpendayList;
					if ($scope.moreOpendayList.length > 1) {
						angular.forEach($scope.moreOpendayList, function (value, index) {
							if ($scope.moreOpendayList[index].selectedEvent == "Y") {
								//$("#multipleOppDetail").show();
								$scope.obselected = $scope.moreOpendayList[index].eventId;
								//$("#moreOpendays").html($scope.moreOpendayList[index].opportunity); 
							}
						});
					}
					//reservePlacePopup();
				});
			}
			$scope.goToBookingUrl = function () {
				window.open($scope.opendayList[0].bookingUrl, '_system');
			}
			//
			$scope.reservePlaceLogging = function (interationType) {
				var uniName = $scope.uniName;

				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}
				var collegeId = localStorage.getItem('universityId');
				var interactionPagename = '/course-details/' + localStorage.getItem('courseId');
				firebaseEventTrack('ecommercepurchase', { coupon: 'interaction_reserve_a_place', currency: "GBP", value: 0, transaction_id: collegeId, location: uniName.toLowerCase(), item_name: firebaseSubject, interaction_pagename: interactionPagename  });
				//window.open($scope.opendayList[0].bookingUrl, '_system');
				//googleTagManagerEventTrack(null, 'Interaction', 'Reserve a Place', uniName, $scope.opendayList[0].bookingUrl);
				$scope.reservePlaceStats();
			}
			$scope.openMoreDates = function () {
				if (Constants.devicePlatform == 'IOS')
					Keyboard.hideFormAccessoryBar(false);
				setTimeout(function () {
					if (Constants.devicePlatform == 'IOS') {
						$("#coursemoreOpendays").focus();
					}
				}, 100);

			}
			//
			$scope.reservePlaceStats = function () {
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var collegeId = localStorage.getItem('universityId');
				var userAgent = navigator.userAgent;
				var suborderItemId = null;
				suborderItemId = $scope.opendayList[0].subOrderItemId;
				var extraText = null;
				extraText = encodeURI($scope.opendayList[0].bookingUrl);
				var stringJson = {
					"userId": userId,
					"collegeId": collegeId,
					"courseId": "0",
					"userIp": "",
					"userAgent": navigator.userAgent,
					"jsLog": 'Y',
					"suborderItemId": suborderItemId,
					"profileId": "",
					"typeName": "",
					"activity": "HOTCOURSES: OPEN DAYS: WEBSITE CLICK",
					"extraText": extraText,
					"latitude": latitude,
					"longitude": longtitude,
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/interaction/db-stats/logging/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					//$$D('loaderCourse').style.display = 'none';
					var logStatusId = response.logStatusId;
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					// do nothing				
				});



			}
			$scope.reservePlaceActionLog = function () {
				console.log("opendate-->" + $scope.opendayList[0].odMonthYear);
				var date = getCurrentDate();
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"actionType": "OD",
					"actionDate": "",
					"actionStatus": "C",
					"actionUrl": "",
					"actionKeyId": localStorage.getItem('universityId'),
					"actionDetails": $scope.opendayList[0].odMonthYear,
					"trackSessionId": "",
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/save-user-actions/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}

			$scope.reserveopenday = function () {
				//var collegeId  = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";
				var collegeId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";
				firebaseEventTrack('screenview', { 'page_name': '/open-day/' + collegeId });
				$("#mutlipleOpportunities").css('display', 'none');
				courseDetailsReserveopendayPopup();

			}
			//
			$scope.providerResults = function (tileMediaPath) {
				var collegeId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : ""
				var nearYouFlag = 'NEAR YOU';
				localStorage.setItem('institutionId', collegeId);
				localStorage.setItem('FROM_PAGE', 'course-details');
				if (!isBlankOrNull(tileMediaPath)) {
					localStorage.setItem('tileMediaPath', tileMediaPath);
				}
				//googleTagManagerEventTrack(null, 'Button', 'view courses', $scope.uniName, '0');
				localStorage.setItem('VIEW_COURSES_FLAG', 'VIEW_COURSES_FLAG');
				//window.location.href='../../html/institution/providerResults.html';
				mainView.router.loadPage('html/institution/providerResults.html');
			}
			//Below controller for enquiry page
			$scope.showEnquiryForm = function () {
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var courseId = !isBlankOrNull(localStorage.getItem('courseId')) ? localStorage.getItem('courseId') : "";
				var courseName = !isBlankOrNull(localStorage.getItem('courseName')) ? localStorage.getItem('courseName') : "";
				var institutionId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";
				//			
				var stringJson = {
					"userId": userId,
					"institutionId": institutionId,
					"courseId": courseId,
					"enquiryType": "EMAIL",
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/enquiry/enquiry-form/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;

					$scope.clientConsentFlag = response.clientConsentFlag;
					$scope.clientConsentText = response.clientConsentText;
					//$scope.competitorDetails=response.competitorDetails;					
					$scope.institutionDetailsList = response.institutionDetailsList;
					$scope.userAddressList = response.userAddressList;
					$scope.yearList = response.yearList;
					if (response.yearList != null && response.yearList.length > 0) {
						for (var lp = 0; lp < response.yearList.length; lp++) {
							if (response.yearList[lp].selectedYear == 'Y') {
								$scope.enquirySelectedYearFlag = 'Y';
								localStorage.setItem('selectedYear', response.yearList[lp].yearOfEntry);
								break;
							} else {
								$scope.enquirySelectedYearFlag = 'N';
							}
						}
					}
					//
					localStorage.setItem('institutionId', response.institutionDetailsList[0].institutionId);
					localStorage.setItem('institutionDisplayName', response.institutionDetailsList[0].institutionDisplayName);
					localStorage.setItem('logo', response.institutionDetailsList[0].logo);
					localStorage.setItem('courseId', response.institutionDetailsList[0].courseId);
					localStorage.setItem('courseTitle', response.institutionDetailsList[0].courseTitle);
					//
					if (response.versionDetails != null && response.versionDetails.length > 0) {
						if (response.versionDetails[0].versionName > Constants.appVersion && sessionStorage.getItem('VERSION_INFORMED') != 'true') {
							sessionStorage.setItem('VERSION_INFORMED', 'true');
							localStorage.setItem('updateVersionMessage', response.versionDetails[0].message);
							localStorage.setItem('updateVersion', response.versionDetails[0].versionName);
							//window.location.replace('../../html/versioning/updateVersion.html');
							mainView.router.loadPage('html/versioning/updateVersion.html');
						}
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//
			
				$scope.showEnquiryForm();
			
			//
			//Below controller for request prospectus
			$scope.showProspectusEnquiryForm = function () {

				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var courseId = !isBlankOrNull(localStorage.getItem('courseId')) ? localStorage.getItem('courseId') : "";
				var courseName = !isBlankOrNull(localStorage.getItem('courseName')) ? localStorage.getItem('courseName') : "";
				var institutionId = !isBlankOrNull(localStorage.getItem('collegeId')) ? localStorage.getItem('collegeId') : "";
				//
				var stringJson = {
					"userId": userId,
					"institutionId": institutionId,
					"courseId": courseId,
					"enquiryType": "PROSPECTUS",
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/enquiry/enquiry-form/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;

					$scope.clientConsentFlag = response.clientConsentFlag;
					$scope.clientConsentText = response.clientConsentText;
					//$scope.competitorDetails=response.competitorDetails;					
					$scope.institutionDetailsList = response.institutionDetailsList;
					$scope.yearList = response.yearList;
					if (response.yearList != null && response.yearList.length > 0) {
						for (var lp = 0; lp < response.yearList.length; lp++) {
							if (response.yearList[lp].selectedYear == 'Y') {
								$scope.prospectusSelectedYearFlag = 'Y';
								localStorage.setItem('selectedYear', response.yearList[lp].yearOfEntry);
								break;
							} else {
								$scope.prospectusSelectedYearFlag = 'N';
							}
						}
					}
					localStorage.removeItem('addressFlag');
					localStorage.removeItem('addressLineOne');
					localStorage.removeItem('addressLineTwo');
					localStorage.removeItem('city');
					localStorage.removeItem('postCode');
					if (response.userAddressList.length > 0) {
						if (response.userAddressList.length == 1) {
							if (isBlankOrNull(response.userAddressList[0].addressLineOne) && isBlankOrNull(response.userAddressList[0].addressLineTwo) && isBlankOrNull(response.userAddressList[0].city) && isBlankOrNull(response.userAddressList[0].postCode)) {
								$scope.userAddressList = [];

							} else {
								//
								$scope.userAddressList = response.userAddressList;
								//
								localStorage.setItem('addressFlag', 'YES');
								localStorage.setItem('addressLineOne', response.userAddressList[0].addressLineOne);
								localStorage.setItem('addressLineTwo', response.userAddressList[0].addressLineTwo);
								localStorage.setItem('city', response.userAddressList[0].city);
								localStorage.setItem('postCode', response.userAddressList[0].postCode);
							}
						}
					}
					//				
					localStorage.setItem('institutionId', response.institutionDetailsList[0].institutionId);
					localStorage.setItem('institutionDisplayName', response.institutionDetailsList[0].institutionDisplayName);
					localStorage.setItem('logo', response.institutionDetailsList[0].logo);
					localStorage.setItem('courseId', response.institutionDetailsList[0].courseId);
					localStorage.setItem('courseTitle', response.institutionDetailsList[0].courseTitle);
					//
					$scope.institutionDetails = [];
					var institutionId = !isBlankOrNull(localStorage.getItem('institutionId')) ? localStorage.getItem('institutionId') : "";
					var institutionDisplayName = !isBlankOrNull(localStorage.getItem('institutionDisplayName')) ? localStorage.getItem('institutionDisplayName') : "";
					var logo = !isBlankOrNull(localStorage.getItem('logo')) ? localStorage.getItem('logo') : "";
					var selectedYear = !isBlankOrNull(localStorage.getItem('selectedYear')) ? localStorage.getItem('selectedYear') : "";
					var addressLineOne = !isBlankOrNull(localStorage.getItem('addressLineOne')) ? localStorage.getItem('addressLineOne') : "";
					var addressLineTwo = !isBlankOrNull(localStorage.getItem('addressLineTwo')) ? localStorage.getItem('addressLineTwo') : "";
					var city = !isBlankOrNull(localStorage.getItem('city')) ? localStorage.getItem('city') : "";
					var postCode = !isBlankOrNull(localStorage.getItem('postCode')) ? localStorage.getItem('postCode') : "";
					var countyState = !isBlankOrNull(localStorage.getItem('countyState')) ? localStorage.getItem('countyState') : "";
					$scope.addressFlag = localStorage.getItem('addressFlag');
					if (isBlankOrNull(addressLineTwo)) {
						addressLineOne = addressLineOne + ",";
					} else {
						addressLineTwo = addressLineTwo + ",";
					}
					$scope.institutionDetails = {
						"institutionId": institutionId,
						"institutionDisplayName": institutionDisplayName,
						"logo": logo,
						"addressLineOne": addressLineOne,
						"addressLineTwo": addressLineTwo,
						"city": city,
						"postCode": postCode,
						"countyState": countyState
					};
					//
					if (response.versionDetails != null && response.versionDetails.length > 0) {
						if (response.versionDetails[0].versionName > Constants.appVersion && sessionStorage.getItem('VERSION_INFORMED') != 'true') {
							sessionStorage.setItem('VERSION_INFORMED', 'true');
							localStorage.setItem('updateVersionMessage', response.versionDetails[0].message);
							localStorage.setItem('updateVersion', response.versionDetails[0].versionName);
							//window.location.replace('../../html/versioning/updateVersion.html');
							mainView.router.loadPage('html/versioning/updateVersion.html');
						}
					}

				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//
			$scope.submitEnquiryForm = function () {
				//
				$("#enqSuccessImg").hide();
				$("#prosSuccessImg").hide();
				//
				var uniName = $scope.uniName;
				if (!isBlankOrNull(uniName)) {
					uniName = uniName.toLowerCase();
				}
				//googleTagManagerEventTrack(null, 'Interaction', 'Send Email', uniName, '0');
				//
				var currency_value = 0;
				if ($scope.enquiryInfo[0].hasOwnProperty('emailPrice')) {
					currency_value = parseFloat($scope.enquiryInfo[0].emailPrice);
				}
				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}
				var collegeId = localStorage.getItem('universityId');
				var interactionPagename = '/course-details/' + localStorage.getItem('courseId');
				firebaseEventTrack('ecommercepurchase', { coupon: 'interaction_request_email', currency: "GBP", value: currency_value, transaction_id: collegeId, location: uniName.toLowerCase(), item_name: firebaseSubject, interaction_pagename: interactionPagename  });
				//
				$$D('enquirySuccessDiv').style.display = 'none';
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var institutionId = !isBlankOrNull(localStorage.getItem('institutionId')) ? localStorage.getItem('institutionId') : "";
				var courseId = !isBlankOrNull(localStorage.getItem('courseId')) ? localStorage.getItem('courseId') : "";
				var selectedYear = !isBlankOrNull(localStorage.getItem('selectedYear')) ? localStorage.getItem('selectedYear') : "";
				var suborderItemId = !isBlankOrNull(localStorage.getItem('cdSuborderItemId')) ? localStorage.getItem('cdSuborderItemId') : "0";
				var message = !isBlankOrNull($$D("enqMsg")) ? $$D("enqMsg").value : "";
				var searchCategoryCode = "";
				var searchCategoryId = "";
				var keywordSearch = "";
				var qualification = "";
				var orderBy = "";
				var locationType = "";
				var region = "";
				var regionFlag = "";
				var studyMode = "";
				var previousQual = "";
				var previousQualGrade = "";
				var assessmentType = "";
				var reviewCategory = "";
				var reviewCategoryName = "";
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					//
					searchCategoryCode = !isBlankOrNull(recentSearchCriteria.searchCategoryCode) ? recentSearchCriteria.searchCategoryCode : "";
					searchCategoryId = !isBlankOrNull(recentSearchCriteria.searchCategoryId) ? recentSearchCriteria.searchCategoryId : "";
					keywordSearch = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "";
					qualification = !isBlankOrNull(recentSearchCriteria.qualification) ? recentSearchCriteria.qualification : "";
					orderBy = !isBlankOrNull(recentSearchCriteria.orderBy) ? recentSearchCriteria.orderBy : "";
					locationType = !isBlankOrNull(recentSearchCriteria.locationType) ? recentSearchCriteria.locationType : "";
					region = !isBlankOrNull(recentSearchCriteria.region) ? recentSearchCriteria.region : "";
					regionFlag = !isBlankOrNull(recentSearchCriteria.regionFlag) ? recentSearchCriteria.regionFlag : "";
					studyMode = !isBlankOrNull(recentSearchCriteria.studyMode) ? recentSearchCriteria.studyMode : "";
					previousQual = !isBlankOrNull(recentSearchCriteria.previousQual) ? recentSearchCriteria.previousQual : "";
					previousQualGrade = !isBlankOrNull(recentSearchCriteria.previousQualGrade) ? recentSearchCriteria.previousQualGrade : "";
					assessmentType = !isBlankOrNull(recentSearchCriteria.assessmentType) ? recentSearchCriteria.assessmentType : "";
					reviewCategory = !isBlankOrNull(recentSearchCriteria.reviewCategory) ? recentSearchCriteria.reviewCategory : "";
					reviewCategoryName = !isBlankOrNull(recentSearchCriteria.reviewCategoryName) ? recentSearchCriteria.reviewCategoryName : "";
				}
				//
				var reviewSubjectOne = ""; var reviewSubjectTwo = ""; var reviewSubjectThree = "";
				if (!isBlankOrNull(reviewCategory)) {
					reviewCategoryArray = reviewCategory.split(",");
					if (reviewCategoryArray.length == 3) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; reviewSubjectThree = reviewCategoryArray[2]; }
					else if (reviewCategoryArray.length == 2) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; }
					else { reviewSubjectOne = reviewCategoryArray[0]; }
				}
				//Below code for submit email enquiry form
				if ($$D('sendButton') && $$D('sendButton').className == 'right snd_lnk' && !isBlankOrNull(selectedYear) && $$D("enqMsg") && ($$D('enqMsg').value.length > 0) && selectedYear.length > 0) {
					$('#sendButton').addClass('gry');
					$('#sendButton').attr("disabled", "disabled");
					$$D('enqSuccessLoader').style.display = 'block';
					var httpClientConsentFlag = "";
					if ($scope.clientConsentFlag === "Y") {
						httpClientConsentFlag = $scope.emailClientConsentFlagCourse;
					}
					var stringJson = {
						"userId": userId,
						"institutionId": institutionId,
						"courseId": courseId,
						"yearOfEntry": selectedYear,
						"userIP": "",
						"userAgent": navigator.userAgent,
						"enquiryType": "EMAIL",
						"message": message,
						"addressLineOne": "",
						"addressLineTwo": "",
						"city": "",
						"countyState": "",
						"postCode": "",
						"suborderItemId": suborderItemId,
						"categoryCode": searchCategoryCode,
						"categoryId": searchCategoryId,
						"qualification": qualification,
						"searchKeyword": keywordSearch,
						"locationType": locationType,
						"region": region,
						"regionFlag": regionFlag,
						"studyMode": studyMode,
						"previousQualification": previousQual,
						"previousQualificationGrade": previousQualGrade,
						"assessmentType": assessmentType,
						"reviewSubjectOne": reviewSubjectOne,
						"reviewSubjectTwo": reviewSubjectTwo,
						"reviewSubjectThree": reviewSubjectThree,
						"appVersion": Constants.appVersion,
						"affiliateId": "220703",
						"accessToken": "246",
						"clientConsentFlag": httpClientConsentFlag,
						"latitude": latitude,
						"longitude": longtitude
					};
					var jsonData = JSON.stringify(stringJson);
					var url = "https://mtest.whatuni.com/wuappws/enquiry/submit-enquiry-form/";
					var req = {
						method: 'POST',
						url: url,
						headers: { 'Content-Type': 'application/json' },
						data: jsonData
					}
					$http(req).then(function (httpresponse) {
						var response = httpresponse.data;

						

						xtremePushTag("Year_Of_Entry", selectedYear);
						if ($$D('enqSuccessLoader')) {
							setTimeout(function () {
								$$D('enqSuccessLoader').style.display = 'none';
							}, 1000);

						}
						setTimeout(function () {
							$$D('enquirySuccessDiv').style.display = 'block';
						}, 1000);
						$scope.matchingPercentageFlag;
						if (!isBlankOrNull(locationType) || !isBlankOrNull(region) || !isBlankOrNull(regionFlag) || !isBlankOrNull(studyMode) || !isBlankOrNull(previousQual) || !isBlankOrNull(previousQualGrade) || !isBlankOrNull(assessmentType) || !isBlankOrNull(reviewCategory)) {
							$scope.matchingPercentageFlag = true;
						}
						$scope.otherInstitutionsList = response.otherInstitutionsList;
						$scope.otherCoursesList = response.otherCoursesList;
						if (response.institutionDetailsList.length > 0) {
							$scope.institutionDetailsList = response.institutionDetailsList;
						}
						if (response.otherInstitutionsList.length == 1) {
							if (isBlankOrNull(response.otherInstitutionsList[0].institutionId) && isBlankOrNull(response.otherInstitutionsList[0].institutionDisplayName)) {
								$scope.otherInstitutionsList = [];
							}
						}
						$scope.otherCoursesList = response.otherCoursesList;
						if (response.otherCoursesList.length == 1) {
							if (isBlankOrNull(response.otherCoursesList[0].institutionId) && isBlankOrNull(response.otherCoursesList[0].institutionDisplayName)) {
								$scope.otherCoursesList = [];
							}
						}
						$scope.emailSuccess = 'Y';
						$scope.prospectusSuccess = 'N';
						$scope.closeMark = 'institution-profile';
						var data = $$D('success-page').innerHTML;
						$compile(data)($scope);
						setTimeout(function () { $("#enqSuccessImg").show(); }, 200);
						setTimeout(function () { reviewAndRating(true); }, 500);
					}, function (httpresponse, status) {
						var response = httpresponse.data;
						if ($$D('enqSuccessLoader')) {
							$$D('enqSuccessLoader').style.display = 'none';
						}
						localStorage.setItem('exception', response.exception);
						localStorage.setItem('status', response.status);
						localStorage.setItem('message', response.message);
						mainView.router.loadPage('html/exception/exception.html');
					});
				} else {
					if ($$D('enqSuccessLoader')) {
						$$D('enqSuccessLoader').style.display = 'none';
					}
				}
			}
			//
			//Below code for validate the email enquiry form
			$scope.validateEmailEnquiryform = function(institutionId, selectedYear){
				if(!isBlankOrNull(institutionId) && !isBlankOrNull(selectedYear) && ($$D('enqMsg').value.length > 0 && $$D('enqMsg').value.length < 3800) && $$D('sendButton').className == 'right'){
					mainView.router.loadPage('../../html/enquiry/emailEnqSuccess.html');
				}
			}

			//Below code for highlight the send button
			$scope.showHideSendbutton = function () {
				if ($$D('enqMsg') && !isBlankOrNull($$D('enqMsg').value) && $$D('enqMsg').value.length > 0) {
					$('#sendButton').removeClass('gry');
					$('#sendButton').removeAttr('disabled', 'disabled');
				} else {
					$('#sendButton').addClass('gry');
					$('#sendButton').attr("disabled", "disabled");
				}
			}
			//
			
				$scope.showProspectusEnquiryForm();
			
			//
			$scope.saveSelectedYear = function (index, id, elementName) {
				/*if(id == "PROSPECTUS") {
					$('.req_props .tabs').removeClass("trnsfrm");
					$('#back-popup').hide();
					$('#back-tab').show();
				} else {
					$('.second_page .tabs').removeClass("trnsfrm");
					$('#back-popup-email').hide();
					$('#back-tab-email').show();
					if($$D('enqMsg')){
						$$D('enqMsg').focus();
					}
					//$('.snd_lnk').show();
				}*/
				/*if($$D('enqMsg')){
					setTimeout(function(){
						$$D('enqMsg').focus();
					},5);				
				}*/
				localStorage.removeItem('selectedYear');
				var selectedYear = $$D(elementName + "-" + index).innerHTML.split("-")[0];
				var yearArray = document.getElementsByName(elementName);
				for (var i = 0; i < yearArray.length; i++) {
					yearArray[i].className = 'btn btn_wht tab-link';
				}
				$("#" + elementName + "-" + index).addClass('active');
				localStorage.setItem('selectedYear', selectedYear);
				//if($('.done_btn')) { $('.done_btn').hide(); } 
				$('#enqMsg').val('');
				//
				if (id == "PROSPECTUS") {
					$('#prosYoeSubmitBtn').removeAttr("disabled");
				} else {
					$('#enqYoeSubmitBtn').removeAttr("disabled");
				}
				/*
				if(id == 'EMAIL'){
					mainView.router.loadPage('../../html/enquiry/enquiryMessage.html');
				}else if(id == 'PROSPECTUS'){
					if(localStorage.getItem('addressFlag') == 'YES'){
						$scope.addressFlag = localStorage.getItem('addressFlag');				
					}else{
						localStorage.removeItem('addressFlag');
					}
					mainView.router.loadPage('../../html/enquiry/orderProspectus.html');				
				}
				*/
			}
			//
			//
			$scope.emailClientConsentFlagCourseChange = function () {
				if ($("#emailClientConsentFlagCourse").is(":checked")) {
					$scope.emailClientConsentFlagCourse = "Y";
				} else {
					$scope.emailClientConsentFlagCourse = "N";
				}
			}
			$scope.prospectusClientConsentFlagCourseChange = function () {
				if ($("#prospectusClientConsentFlagCourse").is(":checked")) {
					$scope.prospectusClientConsentFlagCourse = "Y";
				} else {
					$scope.prospectusClientConsentFlagCourse = "N";
				}
			}
			//
			$scope.goToNextPage = function (id, elementName) {
				if (id == "PROSPECTUS") {
					$('.req_props .tabs').removeClass("trnsfrm");
					$('#back-popup').hide();
					$('#back-tab').show();
					if ($('.prospectus')) { $('.prospectus').hide(); }
				} else {
					$('.second_page .tabs').removeClass("trnsfrm");
					$('#back-popup-email').hide();
					$('#back-tab-email').show();
					//$('.snd_lnk').show();
					if ($$D('enqMsg')) {
						setTimeout(function () {
							$$D('enqMsg').focus();
						}, 500);
					}
					//					
					if ($('.enq')) { $('.enq').hide(); }
				}
				//
				var yearArray = document.getElementsByName(elementName);
				for (var i = 0; i < yearArray.length; i++) {
					if (yearArray[i].className.indexOf('active') > -1) {
						localStorage.setItem('selectedYear', yearArray[i].innerHTML);
					}
				}
				var selectedYear = localStorage.getItem('selectedYear');
				//googleTagManagerEventTrack(null, 'Start Date', 'Year of Entry', selectedYear, '0');	
				firebaseEventTrack('year_of_entry', { start_date: selectedYear });
				/*
				if(id == 'EMAIL'){
					mainView.router.loadPage('../../html/enquiry/enquiryMessage.html');
				}else if(id == 'PROSPECTUS'){
					if(localStorage.getItem('addressFlag') == 'YES'){
						$scope.addressFlag = localStorage.getItem('addressFlag');				
					}else{
						localStorage.removeItem('addressFlag');
					}
					mainView.router.loadPage('../../html/enquiry/orderProspectus.html');				
				}
				*/
			}
			$scope.submitEnquiryProspectus = function () {
				//
				$('#prospectusSubBtn').attr("disabled", "disabled");
				$("#enqSuccessImg").hide();
				$("#prosSuccessImg").hide();
				//
				var uniName = $scope.uniName;
				if (!isBlankOrNull(uniName)) {
					uniName = uniName.toLowerCase();
				}
				//googleTagManagerEventTrack(null, 'Interaction', 'Order Prospectus', uniName, '0');
				var currency_value = 0;
				if ($scope.enquiryInfo[0].hasOwnProperty('prospectusPrice')) {
					currency_value = parseFloat($scope.enquiryInfo[0].prospectusPrice);
				}
				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}
				var collegeId = localStorage.getItem('universityId');
				var interactionPagename = '/course-details/' + localStorage.getItem('courseId');
				firebaseEventTrack('ecommercepurchase', { coupon: 'interaction_order_prospectus', currency: "GBP", value: currency_value, transaction_id: collegeId, location: uniName.toLowerCase(), item_name: firebaseSubject, interaction_pagename: interactionPagename  });
				//
				$$D('enqSuccessLoader').style.display = 'block';
				$$D('enquirySuccessDiv').style.display = 'none';
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var institutionId = !isBlankOrNull(localStorage.getItem('institutionId')) ? localStorage.getItem('institutionId') : "";
				var courseId = !isBlankOrNull(localStorage.getItem('courseId')) ? localStorage.getItem('courseId') : "";
				var selectedYear = !isBlankOrNull(localStorage.getItem('selectedYear')) ? localStorage.getItem('selectedYear') : "";
				var addressLineOne = !isBlankOrNull(localStorage.getItem('addressLineOne')) ? localStorage.getItem('addressLineOne') : "";
				var addressLineTwo = !isBlankOrNull(localStorage.getItem('addressLineTwo')) ? localStorage.getItem('addressLineTwo') : "";
				var city = !isBlankOrNull(localStorage.getItem('city')) ? localStorage.getItem('city') : "";
				var postCode = !isBlankOrNull(localStorage.getItem('postCode')) ? localStorage.getItem('postCode') : "";
				var countyState = !isBlankOrNull(localStorage.getItem('countyState')) ? localStorage.getItem('countyState') : "";
				var suborderItemId = !isBlankOrNull(localStorage.getItem('cdSuborderItemId')) ? localStorage.getItem('cdSuborderItemId') : "0";
				//
				var searchCategoryCode = "";
				var searchCategoryId = "";
				var keywordSearch = "";
				var qualification = "";
				var orderBy = "";
				var locationType = "";
				var region = "";
				var regionFlag = "";
				var studyMode = "";
				var previousQual = "";
				var previousQualGrade = "";
				var assessmentType = "";
				var reviewCategory = "";
				var reviewCategoryName = "";
				//
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					//          
					searchCategoryCode = !isBlankOrNull(recentSearchCriteria.searchCategoryCode) ? recentSearchCriteria.searchCategoryCode : "";
					searchCategoryId = !isBlankOrNull(recentSearchCriteria.searchCategoryId) ? recentSearchCriteria.searchCategoryId : "";
					keywordSearch = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "";
					qualification = !isBlankOrNull(recentSearchCriteria.qualification) ? recentSearchCriteria.qualification : "";
					orderBy = !isBlankOrNull(recentSearchCriteria.orderBy) ? recentSearchCriteria.orderBy : "";
					locationType = !isBlankOrNull(recentSearchCriteria.locationType) ? recentSearchCriteria.locationType : "";
					region = !isBlankOrNull(recentSearchCriteria.region) ? recentSearchCriteria.region : "";
					regionFlag = !isBlankOrNull(recentSearchCriteria.regionFlag) ? recentSearchCriteria.regionFlag : "";
					studyMode = !isBlankOrNull(recentSearchCriteria.studyMode) ? recentSearchCriteria.studyMode : "";
					previousQual = !isBlankOrNull(recentSearchCriteria.previousQual) ? recentSearchCriteria.previousQual : "";
					previousQualGrade = !isBlankOrNull(recentSearchCriteria.previousQualGrade) ? recentSearchCriteria.previousQualGrade : "";
					assessmentType = !isBlankOrNull(recentSearchCriteria.assessmentType) ? recentSearchCriteria.assessmentType : "";
					reviewCategory = !isBlankOrNull(recentSearchCriteria.reviewCategory) ? recentSearchCriteria.reviewCategory : "";
					reviewCategoryName = !isBlankOrNull(recentSearchCriteria.reviewCategoryName) ? recentSearchCriteria.reviewCategoryName : "";
				}
				var reviewSubjectOne = ""; var reviewSubjectTwo = ""; var reviewSubjectThree = "";
				if (!isBlankOrNull(reviewCategory)) {
					reviewCategoryArray = reviewCategory.split(",");
					if (reviewCategoryArray.length == 3) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; reviewSubjectThree = reviewCategoryArray[2]; }
					else if (reviewCategoryArray.length == 2) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; }
					else { reviewSubjectOne = reviewCategoryArray[0]; }
				}
				//Below code for submit email enquiry form
				var httpClientConsentFlag = "";
				if ($scope.clientConsentFlag === "Y") {
					httpClientConsentFlag = $scope.prospectusClientConsentFlagCourse;
				}
				var stringJson = {
					"userId": userId,
					"institutionId": institutionId,
					"courseId": courseId,
					"yearOfEntry": selectedYear,
					"userIP": "",
					"userAgent": navigator.userAgent,
					"enquiryType": "PROSPECTUS",
					"message": '',
					"addressLineOne": addressLineOne,
					"addressLineTwo": addressLineTwo,
					"city": city,
					//"countyState":countyState,
					"postCode": postCode,
					"suborderItemId": suborderItemId,
					"categoryCode": searchCategoryCode,
					"categoryId": searchCategoryId,
					"qualification": qualification,
					"searchKeyword": keywordSearch,
					"locationType": locationType,
					"region": region,
					"regionFlag": regionFlag,
					"studyMode": studyMode,
					"previousQualification": previousQual,
					"previousQualificationGrade": previousQualGrade,
					"assessmentType": assessmentType,
					"reviewSubjectOne": reviewSubjectOne,
					"reviewSubjectTwo": reviewSubjectTwo,
					"reviewSubjectThree": reviewSubjectThree,
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246",
					"clientConsentFlag": httpClientConsentFlag,
					"latitude": latitude,
					"longitude": longtitude
				};
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/enquiry/submit-enquiry-form/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					if ($$D('enqSuccessLoader')) {
						setTimeout(function () {
							$$D('enqSuccessLoader').style.display = 'none';
						}, 1000);

					}

					
					xtremePushTag("User_Downloads_A_Prospectus", localStorage.getItem('universityId'));
					xtremePushTag("Year_Of_Entry", selectedYear);
					setTimeout(function () {
						$$D('enquirySuccessDiv').style.display = 'block';
					}, 1000);
					$scope.institutionDetailsList = response.institutionDetailsList;
					$scope.otherInstitutionsList = response.otherInstitutionsList;
					$scope.otherCoursesList = response.otherCoursesList;
					//
					$scope.matchingPercentageFlag;
					if (!isBlankOrNull(locationType) || !isBlankOrNull(region) || !isBlankOrNull(regionFlag) || !isBlankOrNull(studyMode) || !isBlankOrNull(previousQual) || !isBlankOrNull(previousQualGrade) || !isBlankOrNull(assessmentType) || !isBlankOrNull(reviewCategory)) {
						$scope.matchingPercentageFlag = true;
					}
					//
					if (response.otherInstitutionsList != null && response.otherInstitutionsList.length == 1) {
						if (isBlankOrNull(response.otherInstitutionsList[0].institutionId) && isBlankOrNull(response.otherInstitutionsList[0].institutionDisplayName)) {
							$scope.otherInstitutionsList = [];
						}
					}
					$scope.otherCoursesList = response.otherCoursesList;
					if (response.otherInstitutionsList != null && response.otherCoursesList.length == 1) {
						if (isBlankOrNull(response.otherCoursesList[0].institutionId) && isBlankOrNull(response.otherCoursesList[0].institutionDisplayName)) {
							$scope.otherCoursesList = [];
						}
					}
					$scope.closeMark = "course-details";
					$scope.emailSuccess = 'N';
					$scope.prospectusSuccess = 'Y';
					var data = $$D('success-page').innerHTML;
					$compile(data)($scope);
					setTimeout(function () { $("#prosSuccessImg").show(); }, 200);
					setTimeout(function () { reviewAndRating(true); }, 500);
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					if ($$D('enqSuccessLoader')) {
						$$D('enqSuccessLoader').style.display = 'none';
					}
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//
			$scope.goCourseDetailsPage = function (institutionId, institutionName, courseId, courseTitle, tileMediaPath) {
				if (!isBlankOrNull(institutionId) && !isBlankOrNull(courseId) && !isBlankOrNull(courseTitle)) {
					localStorage.setItem('courseId', courseId);
					localStorage.setItem('courseName', courseTitle);
					localStorage.setItem('collegeId', institutionId);
				}

				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}

				firebaseEventTrack('viewitem', { origin: 'recommended', item_category: institutionName.toLowerCase(), item_name: firebaseSubject.toLowerCase(), item_id: institutionId, search_term: "na" });

				if (!isBlankOrNull(tileMediaPath)) {
					localStorage.setItem('tileMediaPath', tileMediaPath);
				}
				mainView.router.reloadPage('html/coursedetails/courseDetails.html');
			}
			//						
			$scope.validatePostcode = function () {
				if ($$D('addressAjax')) {
					if ($$D('addressAjax').value.length > 0) {
						$$D('xmark').className = 'srclear';
						$('#ajaxList').css('display', 'none');
						var postcode = $$D('addressAjax').value;
						postcode = checkPostCode(postcode);
						//if(postcode.toUpperCase() == checkPostCode(postcode)){						
						if (checkPostCode(postcode)) {
							var stringJson = {
								"postCode": postcode.toUpperCase(),
								"appVersion": Constants.appVersion,
								"affiliateId": "220703",
								"accessToken": "246"
							};
							var jsonData = JSON.stringify(stringJson);
							var url = "https://mtest.whatuni.com/wuappws/get-ajax-address/";
							var req = {
								method: 'POST',
								url: url,
								headers: { 'Content-Type': 'application/json' },
								data: jsonData
							}
							$http(req).then(function (httpresponse) {
								var response = httpresponse.data;
								$scope.ajaxAddressList = response.ajaxAddressList;
								if (response.ajaxAddressList != null && response.ajaxAddressList.length > 0) {
									$('#ajaxList').css('display', 'block');
								}
							}, function (httpresponse, status) {
								var response = httpresponse.data;
								localStorage.setItem('exception', response.exception);
								localStorage.setItem('status', response.status);
								localStorage.setItem('message', response.message);
								mainView.router.loadPage('html/exception/exception.html');
							});

						}
					} else {
						$$D('xmark').className = '';
						$('#ajaxList').css('display', 'none');
					}
				}
			}
			//
			$scope.gotoPreviousPopup = function () {
				myApp.closeModal('.search_address');
				$scope.clearSearch();
				$('.req_props .tabs #tab2').addClass("active");
				$('.req_props .tabs #tab1').removeClass("active");
			}
			//
			$scope.addMyAddressURL = function () {
				$('#tab3cont').hide();
				$('#tab4cont').show();
				$('#back-tab-searchpopup').show();
				$('.search_address .tabs').removeClass("trnsfrm");
				$('.search_address').removeClass("trnsfrm_fade");
				$('.search_address').addClass("trns_lft");
				$('#buildingName').val('');
				$('#addressLine').val('');
				$('#city').val('');
				$('#postCode').val('');
				$('#buildingName_cls').removeClass("hd_cls");
				$('#addressLine_cls').removeClass("hd_cls");
				$('#city_cls').removeClass("hd_cls");
				$('#postCode_cls').removeClass("hd_cls");
				$('#saveButton').removeClass("btn_blu");
				$(".search_address .pr_dtl").scrollTop(0, 0);
				if ($$D('buildingName')) {
					returnKey("next");
					setTimeout(function () {
						$$D('buildingName').focus();
					}, 450);
				}
				//mainView.router.loadPage('../../html/enquiry/addMyAddress.html');
			}
			//
			$scope.orderProspectusURL = function () {
				if (localStorage.getItem('addressFlag') == 'YES') {
					localStorage.setItem('addressFlag', 'YES');
				} else {
					localStorage.removeItem('addressFlag');
				}
				//mainView.router.loadPage('../../html/enquiry/orderProspectus.html');
				mainView.router.loadPage('html/enquiry/orderProspectus.html');
			}
			//
			$scope.clearSearch = function () {
				if ($$D('addressAjax')) {
					$$D('addressAjax').value = '';
					$$D('xmark').className = '';
					$('#ajaxList').css('display', 'none');
				}
			}
			//
			$scope.orderProspectusPage = function (index) {
				if ($$D('address-' + index)) {
					var address = $$D('address-' + index).value.split("###");
					var length = address.length;
					var postcode = address[length - 1];
					//var countyState = address[length - 2] ;
					var city = address[length - 2];
					var addressLineTwo = address[length - 3];
					var addressLineOne = address[length - 4];
					localStorage.setItem('addressFlag', 'YES');
					localStorage.setItem('addressLineOne', addressLineOne);
					localStorage.setItem('addressLineTwo', addressLineTwo);
					localStorage.setItem('city', city);
					localStorage.setItem('postCode', postcode);
					localStorage.setItem('countyState', countyState);
					//mainView.router.loadPage('../../html/enquiry/orderProspectus.html');
					$scope.institutionDetails = [];
					var institutionId = !isBlankOrNull(localStorage.getItem('institutionId')) ? localStorage.getItem('institutionId') : "";
					var institutionDisplayName = !isBlankOrNull(localStorage.getItem('institutionDisplayName')) ? localStorage.getItem('institutionDisplayName') : "";
					var logo = !isBlankOrNull(localStorage.getItem('logo')) ? localStorage.getItem('logo') : "";
					var selectedYear = !isBlankOrNull(localStorage.getItem('selectedYear')) ? localStorage.getItem('selectedYear') : "";
					var addressLineOne = !isBlankOrNull(localStorage.getItem('addressLineOne')) ? localStorage.getItem('addressLineOne') : "";
					var addressLineTwo = !isBlankOrNull(localStorage.getItem('addressLineTwo')) ? localStorage.getItem('addressLineTwo') : "";
					var city = !isBlankOrNull(localStorage.getItem('city')) ? localStorage.getItem('city') : "";
					var postCode = !isBlankOrNull(localStorage.getItem('postCode')) ? localStorage.getItem('postCode') : "";
					var countyState = !isBlankOrNull(localStorage.getItem('countyState')) ? localStorage.getItem('countyState') : "";
					$scope.addressFlag = localStorage.getItem('addressFlag');
					if (isBlankOrNull(addressLineTwo)) {
						addressLineOne = addressLineOne + ",";
					} else {
						addressLineTwo = addressLineTwo + ",";
					}
					$scope.institutionDetails = {
						"institutionId": institutionId,
						"institutionDisplayName": institutionDisplayName,
						"logo": logo,
						"addressLineOne": addressLineOne,
						"addressLineTwo": addressLineTwo,
						"city": city,
						"postCode": postCode,
						"countyState": countyState
					};
					var data = $$D('tab2').innerHTML;
					$compile(data)($scope);
					//$('.search_address .tabs').removeClass("trnsfrm");
					myApp.closeModal('.search_address');
					$('.req_props .tabs #tab2').addClass("active");
					$('.req_props .tabs #tab1').removeClass("active");
					$('#submitProspectus').show();
				}
			}
			/*
			if($$D('addressAjax')){
				$$D('addressAjax').focus();
			}
			*/
			$scope.addXmark = function (id, xmarkId) {
				if ($$D(id) && $$D(xmarkId)) {
					if ($$D(id).value) {
						$$D(xmarkId).className = 'hd_cls';
						var count = $('.hd_cls')
						if (count.length == 4) {
							$('#saveButton').addClass('btn_blu');
						} else {
							if (count.length == 3) {
								if ($$D('addressLine_cls') && isBlankOrNull($$D('addressLine_cls').className)) {
									$('#saveButton').addClass('btn_blu');
								}
							} else {
								$('#saveButton').removeClass('btn_blu');
							}
						}
					} else {
						$$D(xmarkId).className = '';
						$('#saveButton').removeClass('btn_blu');
					}
				}
			}
			//
			$scope.validateUserAddress = function () {
				if ($$D('buildingName_cls') && $$D('addressLine_cls') && $$D('city_cls') && $$D('postCode_cls')) {
					if ($('#buildingName_cls').hasClass('hd_cls') && $('#city_cls').hasClass('hd_cls') && $('#postCode_cls').hasClass('hd_cls')) {
						$('#saveButton').addClass('btn_blu');
					} else {
						$('#saveButton').removeClass('btn_blu');
					}
				}
			}
			//
			$scope.clearField = function (id, xmarkId) {
				if ($$D(id)) {
					$$D(id).value = '';
					$$D(xmarkId).className = '';
					$('#saveButton').removeClass('btn_blu');
				}

			}
			//
			$scope.saveUserAddress = function (buildingName, addressLine, city, postcode) {
				if ($$D(buildingName) && $$D(addressLine) && $$D(city) && $$D(postcode)) {
					if ($('#saveButton').hasClass('btn_blu')) {
						var buildingName = $$D(buildingName).value.length > 0 ? $$D(buildingName).value : '';
						var addressLine = $$D(addressLine).value.length > 0 ? $$D(addressLine).value : '';
						var city = $$D(city).value.length > 0 ? $$D(city).value : '';
						var postcode = $$D(postcode).value.length > 0 ? $$D(postcode).value : '';
						localStorage.setItem('addressLineOne', buildingName);
						localStorage.setItem('addressLineTwo', addressLine);
						localStorage.setItem('city', city);
						localStorage.setItem('postCode', postcode);
						localStorage.setItem('addressFlag', 'YES');
						//mainView.router.loadPage('../../html/enquiry/orderProspectus.html');
						$scope.institutionDetails = [];
						var institutionId = !isBlankOrNull(localStorage.getItem('institutionId')) ? localStorage.getItem('institutionId') : "";
						var institutionDisplayName = !isBlankOrNull(localStorage.getItem('institutionDisplayName')) ? localStorage.getItem('institutionDisplayName') : "";
						var logo = !isBlankOrNull(localStorage.getItem('logo')) ? localStorage.getItem('logo') : "";
						var selectedYear = !isBlankOrNull(localStorage.getItem('selectedYear')) ? localStorage.getItem('selectedYear') : "";
						var addressLineOne = !isBlankOrNull(localStorage.getItem('addressLineOne')) ? localStorage.getItem('addressLineOne') : "";
						var addressLineTwo = !isBlankOrNull(localStorage.getItem('addressLineTwo')) ? localStorage.getItem('addressLineTwo') : "";
						var city = !isBlankOrNull(localStorage.getItem('city')) ? localStorage.getItem('city') : "";
						var postCode = !isBlankOrNull(localStorage.getItem('postCode')) ? localStorage.getItem('postCode') : "";
						var countyState = !isBlankOrNull(localStorage.getItem('countyState')) ? localStorage.getItem('countyState') : "";
						$scope.addressFlag = localStorage.getItem('addressFlag');
						if (isBlankOrNull(addressLineTwo)) {
							addressLineOne = addressLineOne + ",";
						} else {
							addressLineTwo = addressLineTwo + ",";
						}
						$scope.institutionDetails = {
							"institutionId": institutionId,
							"institutionDisplayName": institutionDisplayName,
							"logo": logo,
							"addressLineOne": addressLineOne,
							"addressLineTwo": addressLineTwo,
							"city": city,
							"postCode": postCode,
							"countyState": countyState
						};
						var data = $$D('tab2').innerHTML;
						$compile(data)($scope);
						//myApp.popup('.req_props');
						//$('.search_address .tabs').removeClass("trnsfrm");
						myApp.closeModal('.search_address');
						$('.req_props .tabs #tab2').addClass("active");
						$('.req_props .tabs #tab1').removeClass("active");
						$('#submitProspectus').show();
					}
				}
			}
			//

			/*
			if($$D('buildingName')){
				setTimeout(function(){
					$$D('buildingName').focus();
				},10);
			}
			*/
			$scope.openSearchAdders = function () {
				//myApp.popup('.req_props');
				$('#tab3cont').show();
				$('#tab4cont').hide();
				//$('.search_address .tabs #tab3').addClass("active");
				//$('.search_address .tabs #tab4').removeClass("active");
				var myappLocal = Constants.myapp;
				myappLocal.popup('.req_props');
				if ($("#tab4").hasClass("active")) {
					$('.search_address .tabs').addClass("trnsfrm");
				}
				if ($("#tab3").hasClass("active")) {
					$('.search_address .tabs').addClass("trnsfrm");
				}
				$('.search_address').removeClass("trns_lft");
				$('.search_address').removeClass("trnsfrm_fade");
				$('.search_address').addClass("trnsfrm_fade");
				//if($('.done_btn')) { $('.done_btn').show(); } 
				if ($$D('addressAjax')) {
					setTimeout(function () {
						$$D('addressAjax').focus();
					}, 300);
				}
				clearSearch();
			}
			$scope.getShortListFlag = function (institutionId, removeFrom, addTo, rootCtrl, pageFlag) {
				getShortListRootCtrl(institutionId, removeFrom, addTo, $scope, $http, rootCtrl, pageFlag);
			}
			//returnKey("next");	
		})
		//
		.controller("preferencesCtrl", function ($scope, $http, $element) {
			//
			$scope.getUserPreferences = function () {
				$('#loaderCourseSignupPreferences').show();
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/profile/get-user-preference/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$('#loaderCourseSignupPreferences').hide();
					$scope.userFlagList = response.userFlagList;

				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			$scope.getUserPreferences();
			//
			$scope.saveMailingPreferences = function (id) {
				$('#loaderCourseSignupPreferences').show();
				setTimeout(function () {
					var receiveNoEmailFlag = $("#receiveNoEmailFlag").is(':checked') ? "N" : "Y";
					//var permitEmail = $("#permitEmail").is(':checked') ? "Y" : "N";
					var solusEmail = $("#solusEmail").is(':checked') ? "Y" : "N";
					var marketingEmail = $("#marketingEmail").is(':checked') ? "Y" : "N";
					var surveyEmail = $("#surveyFlag").is(':checked') ? "Y" : "N";
					//var privacyFlag = $("#privacyFlag").is(':checked') ? "Y" : "N";
					var caFlag = $("#caFlag").is(':checked') ? "Y" : "N";
					var eventLabelArray = [];
					if (marketingEmail == "Y") {
						eventLabelArray.push("Newsletters");
					}
					if (solusEmail == "Y") {
						eventLabelArray.push("Partner content");
					}
					if (receiveNoEmailFlag == "N") {
						eventLabelArray.push("Reminders");
					}
					if (surveyEmail == "Y") {
						eventLabelArray.push("Surveys");
					}
					var eventLabel = eventLabelArray.join(" | ");
					//
					if (eventLabelArray.length == 0) {
						eventLabel = 'None';
					}
					//
					//googleTagManagerEventTrack(null, 'Email Settings', 'Preferences Set', eventLabel, '0');


					var stringJson = {
						"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
						"receiveNoEmailFlag": receiveNoEmailFlag,
						//"permitEmail" : permitEmail,
						"solusEmail": solusEmail,
						"marketingEmail": marketingEmail,
						"surveyEmail": surveyEmail,
						//"privacyFlag" : privacyFlag,
						"caTrackFalg": caFlag,
						"accessToken": "246",
						"affiliateId": "220703",
						"appVersion": Constants.appVersion
					};
					var jsonData = JSON.stringify(stringJson);
					var req = {
						method: 'POST',
						url: 'https://mtest.whatuni.com/wuappws/profile/save-user-preference/',
						headers: { 'Content-Type': 'application/json' },
						data: jsonData
					}
					$http(req).then(function (httpresponse) {
						var response = httpresponse.data;
						var status = response.successMsg;
						$('#loaderCourseSignupPreferences').hide();
						if (status == "SUCCESS") {
							localStorage.setItem('QUIZ_COMPLETE', "No");
							// if (localStorage.getItem("USER_CLEARING_FLAG") == "ON") {
							// 	if ($("#main-index")) { $("#main-index").removeClass('lgn_bdy') }
							// 	if ($("#main-index")) { $("#main-index").removeClass('quiz_page') }
							// 	mainView.router.loadPage('html/home/homeLanding.html');
							// } else {
								mainView.router.loadPage('html/quiz/quiz.html');
							//}
						}
					}, function (httpresponse, status) {
						var response = httpresponse.data;
						localStorage.setItem('exception', response.exception);
						localStorage.setItem('status', response.status);
						localStorage.setItem('message', response.message);
						mainView.router.loadPage('html/exception/exception.html');
					});
				}, 500);
			}
			//
		})
		//
		.controller("userProfileCtrl", function ($scope, $http, $element) {

			var userID = localStorage.getItem('LOGGED_IN_USER_ID');
			firebaseEventTrack('screenview', { 'page_name': '/user-profile' });
			if (!isBlankOrNull(localStorage.getItem('LOGGED_IN_USER_FIRST_NAME'))) {
				var name = localStorage.getItem('LOGGED_IN_USER_FIRST_NAME');
				$scope.firstName = name.charAt(0).toUpperCase() + name.substring(1, name.length);
			}
			if (!isBlankOrNull(localStorage.getItem('LOGGED_IN_USER_LAST_NAME'))) {
				var name = localStorage.getItem('LOGGED_IN_USER_LAST_NAME');
				$scope.lastName = name.charAt(0).toUpperCase() + name.substring(1, name.length);
			}
			//	
			$scope.goToAppStore = function () {
				if (Constants.devicePlatform == 'IOS') {
					window.open(encodeURI("https://itunes.apple.com/gb/app/whatuni-university-degrees-uk/id1267341390?mt=8"), '_system');
				}
				if (Constants.devicePlatform == 'ANDROID') {
					window.open(encodeURI("https://play.google.com/store/apps/details?id=com.hotcourses.group.wuapp"), '_system');
				}
			}
			$scope.quizPageURL = function () {
				localStorage.setItem('TAKE_QUIZ', 'QUIZ_RETURN');
				clearFilter();
				mainView.router.loadPage('html/quiz/quiz.html');
			}
			$scope.goToForgotPswd = function () {
				localStorage.setItem("FROM_PROFILE_PAGE", "HOME");
				//userLogoutFnc();
				Constants.mainViewObj.router.loadPage('html/user/forgotPassword.html');
			}
			$scope.goToProdiverPage = function (url) {
				goToProdiverPage(url);
			}

			//Get user preference
			$scope.getUserPreferences = function () {
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/profile/get-user-preference/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$('#loaderCourseUP').hide();
					$scope.guestUserFlag = response.guestUserFlag;
					$scope.editUserInfoList = response.editUserInfoList;
					$scope.userClientConsentList = response.userClientConsentList;
					angular.forEach($scope.editUserInfoList, function (value, index) {
						if (!isBlankOrNull($scope.editUserInfoList[index].firstName)) {
							var name = $scope.editUserInfoList[index].firstName;
							$scope.firstName = name.charAt(0).toUpperCase() + name.substring(1, name.length);
							localStorage.setItem('LOGGED_IN_USER_FIRST_NAME', $scope.firstName);
						}
						if (!isBlankOrNull($scope.editUserInfoList[index].lastName)) {
							var name = $scope.editUserInfoList[index].lastName;
							$scope.lastName = name.charAt(0).toUpperCase() + name.substring(1, name.length);
							localStorage.setItem('LOGGED_IN_USER_LAST_NAME', $scope.lastName);
						}
						var userImg = $scope.editUserInfoList[index].userImage;
						if (!isBlankOrNull(userImg)) {
							$scope.userImage = userImg.replace("http://", "https://");
						}
						localStorage.setItem('LOGGED_IN_USER_IMAGE', $scope.userImage);
					});
					$scope.privacyCaFlagList = response.privacyCaFlagList;
					$scope.userFlagList = response.userFlagList;

				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			$scope.getUserPreferences();
			//$scope.isGuestUser = true;
			//$('#loaderCourseUP').hide();
			/*if(isGuestUser()) {
				$('#loaderCourseUP').hide();
				$scope.isGuestUser = true;
			} else {
				$scope.getUserPreferences();	
				$scope.isGuestUser = false;
			}*/
			$scope.goRegisteration = function () {
				localStorage.setItem('action', "");
				localStorage.setItem('action_id', "");
				mainView.router.loadPage('html/user/home.html');
			}
			var page = $$('.page[data-page="user-profile"]')[0].f7PageData;
			console.log("page ", page);
			if (Object.keys(page.query).length !== 0) {
				setTimeout(function () {
					$("#" + page.query.action_id).trigger('click');
				}, 1000);
			}
		
			$scope.saveMailingPreferences = function (id) {
				setTimeout(function () {
					var receiveNoEmailFlag = $("#receiveNoEmailFlag").is(':checked') ? "N" : "Y";
					//var permitEmail = $("#permitEmail").is(':checked') ? "Y" : "N";
					var solusEmail = $("#solusEmail").is(':checked') ? "Y" : "N";
					var marketingEmail = $("#marketingEmail").is(':checked') ? "Y" : "N";
					var surveyEmail = $("#surveyFlag").is(':checked') ? "Y" : "N";
					//var privacyFlag = $("#privacyFlag").is(':checked') ? "Y" : "N";
					var caFlag = $("#caFlag").is(':checked') ? "Y" : "N";
					var stringJson = {
						"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
						"receiveNoEmailFlag": receiveNoEmailFlag,
						//"permitEmail" : permitEmail,
						"solusEmail": solusEmail,
						"marketingEmail": marketingEmail,
						"surveyEmail": surveyEmail,
						//"privacyFlag" : privacyFlag,
						"caTrackFalg": caFlag,
						"accessToken": "246",
						"affiliateId": "220703",
						"appVersion": Constants.appVersion
					};
					var jsonData = JSON.stringify(stringJson);
					var req = {
						method: 'POST',
						url: 'https://mtest.whatuni.com/wuappws/profile/save-user-preference/',
						headers: { 'Content-Type': 'application/json' },
						data: jsonData
					}
					$http(req).then(function (httpresponse) {
						var response = httpresponse.data;
						var status = response.successMsg;
						if (status == "SUCCESS") { }
					}, function (httpresponse, status) {
						var response = httpresponse.data;
						localStorage.setItem('exception', response.exception);
						localStorage.setItem('status', response.status);
						localStorage.setItem('message', response.message);
						mainView.router.loadPage('html/exception/exception.html');
					});
				}, 500);
			}
			//
			$scope.sendEmail = function () {
				var firstname = !isBlankOrNull($scope.contactUsFirstName) ? $scope.contactUsFirstName : "";
				var lastname = !isBlankOrNull($scope.contactUsLastName) ? $scope.contactUsLastName : "";
				var email = !isBlankOrNull($scope.contactUsEmail) ? $scope.contactUsEmail : "";
				var message = !isBlankOrNull($scope.contactUsText) ? $scope.contactUsText : "";
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"firstName": firstname,
					"lastName": lastname,
					"email": email,
					"message": message,
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/profile/send-email/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					myApp.closeModal('.contactus');
					$("#contactUsSendBtn").attr("disabled", "disabled"); $("#contactUsSendBtn").addClass("gry");
					myApp.popup('#contactUsSuccessDiv');
					setTimeout(function () {
						$("#contactUsSuccessLoaderDiv").css("display", "none");
						$("#contactUsSuccessContentDiv").css("display", "block");
					}, 1000)
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			$scope.enableContactUs = function () {
				var contactUsValid = false;
				if (isGuestUser()) {
					if (isEmpty($scope.contactUsFirstName) && $scope.contactUsFirstName.length < 1) {
						contactUsValid = true;
					} else if (!validateUserName($scope.contactUsFirstName)) {
						contactUsValid = true;
					}
					if (isEmpty($scope.contactUsLastName) && $scope.contactUsLastName.length < 1) {
						contactUsValid = true;
					} else if (!validateUserName($scope.contactUsLastName)) {
						contactUsValid = true;
					}
					if (isEmpty($scope.contactUsEmail) && $scope.contactUsEmail.length < 1) {
						contactUsValid = true;
					} else if (!checkValidEmail($scope.contactUsEmail)) {
						contactUsValid = true;
					}
				}

				if (isEmpty($scope.contactUsText) && $scope.contactUsText.length < 1) {
					contactUsValid = true;
				}

				if (!contactUsValid) {
					$("#contactUsSendBtn").removeClass("gry");
					$("#contactUsSendBtn").removeAttr("disabled", "disabled");
				} else {
					$("#contactUsSendBtn").addClass("gry");
					$("#contactUsSendBtn").attr("disabled", "disabled");
				}
			}
			$scope.focusFunc = function (id) {
				$("#messageToWhatuni").val("");
				$scope.contactUsText = "";
				$("#contactUsSendBtn").attr("disabled", "disabled");
				$("#contactUsSendBtn").addClass("gry");
				$("#firstNameToWhatuni").removeAttr("readonly", "readonly");
				$("#lastNameToWhatuni").removeAttr("readonly", "readonly");
				$("#emailToWhatuni").removeAttr("readonly", "readonly");

				$("#firstNameToWhatuni").removeClass("dis_inp");
				$("#lastNameToWhatuni").removeClass("dis_inp");
				$("#emailToWhatuni").removeClass("dis_inp");


				if (isGuestUser()) {
					$("#firstNameToWhatuni").val("");
					$("#lastNameToWhatuni").val("");
					$("#emailToWhatuni").val("");
					$scope.contactUsFirstName = "";
					$scope.contactUsLastName = "";
					$scope.contactUsEmail = "";
				} else {
					$scope.contactUsFirstName = localStorage.getItem("LOGGED_IN_USER_FIRST_NAME");
					$scope.contactUsLastName = localStorage.getItem("LOGGED_IN_USER_LAST_NAME");
					$scope.contactUsEmail = localStorage.getItem("LOGGED_IN_USER_EMAIL");
					$("#firstNameToWhatuni").attr("readonly", "readonly");
					$("#lastNameToWhatuni").attr("readonly", "readonly");
					$("#emailToWhatuni").attr("readonly", "readonly");

					$("#firstNameToWhatuni").addClass("dis_inp");
					$("#lastNameToWhatuni").addClass("dis_inp");
					$("#emailToWhatuni").addClass("dis_inp");
					//$("#firstNameToWhatuni").val(localStorage.getItem("LOGGED_IN_USER_FIRST_NAME"));
					//$("#lastNameToWhatuni").val(localStorage.getItem("LOGGED_IN_USER_LAST_NAME"));
					//$("#emailToWhatuni").val(localStorage.getItem("LOGGED_IN_USER_EMAIL"));
				}
				setTimeout(function () {
					if (id == "messageToWhatuni") {
						if (Constants.devicePlatform == 'IOS')
							Keyboard.hideFormAccessoryBar(true);
						if (isGuestUser()) {
							$("#firstNameToWhatuni").focus();
						} else {
							$("#" + id).focus();
						}
					} else {
						if (isGuestUser()) {
							$("#firstNameToWhatuni").focus();
						} else {
							$("#" + id).focus();
						}
					}
				}, 600);
			}
			$scope.goToProfilePage = function () {
				myApp.closeModal('#contactUsSuccessDiv');
				$("#contactUsSuccessLoaderDiv").css("display", "block");
				$("#contactUsSuccessContentDiv").css("display", "none");

				//myApp.closeModal('.pop_lst');			
			}
		})

		.controller("editProfileController", function ($scope, $http, $element) {
			var d = new Date();
			var mindate = parseInt(d.getFullYear()) - 17 + "-" + ("0" + (parseInt(d.getMonth()) + 1)).slice(-2) + "-" + ("0" + (parseInt(d.getDate()))).slice(-2);
			//console.log(mindate);


			var minDateAndroid = parseInt(d.getFullYear()) - 100 + "-" + ("0" + (parseInt(d.getMonth()) + 1)).slice(-2) + "-" + ("0" + (parseInt(d.getDate()))).slice(-2);
			var maxDateAndroid = parseInt(d.getFullYear()) - 14 + "-" + ("0" + (parseInt(d.getMonth()) + 1)).slice(-2) + "-" + ("0" + (parseInt(d.getDate()))).slice(-2);



			$scope.goBackToSearchAddr = function (id) {
				myApp.popup('#searchAddress');
				myApp.closeModal('#searchAddressManual');
			}



			$scope.openDatePicker = function () {
				if ($$("#dateOfBirth").val() == "") {
					//$$("#dateOfBirth").val('2014-04-30');
					if (!isBlankOrNull($scope.userInfoList[0].dateOfBirth)) {
						$("#dateOfBirth").val($scope.userInfoList[0].dateOfBirth);
					} else {
						//$("#dateOfBirth").val("2002-01-01"); 
						$$("#dateOfBirth").val(mindate)

						var date = new Date(mindate);

						var day = ("0" + date.getDate()).slice(-2);
						var monthIndex = date.getMonth();
						var year = date.getFullYear();
						var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
						var inputDate = day + ' ' + monthNames[monthIndex] + ' ' + year;
						//$$("#date_txt").html(inputDate +"   "+test);
						var hiddenDate = parseInt(year) + "-" + ("0" + (parseInt(monthIndex) + 1)).slice(-2) + "-" + ("0" + (parseInt(day))).slice(-2);
						$$("#date_txt_1").val(hiddenDate);


						$$("#date_txt").html(inputDate);
					}
				}
				if (Constants.devicePlatform == 'IOS') {
					$$("#dateOfBirth").focus();
				} else {

					setTimeout(function () {
						cordova.plugins.Focus.focus($("#dateOfBirth"));
					}, 200);
				}
			}


			var userID = localStorage.getItem('LOGGED_IN_USER_ID');
			firebaseEventTrack('screenview', { 'page_name': '/edit-profile' });
			$scope.pastedText = '';
			//				   
			$$D('editProfileLoader').style.display = 'block';
			$scope.editProfile = function () {
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/profile/user-profile/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('editProfileLoader').style.display = 'none';
					$scope.password = localStorage.getItem('LOGGED_IN_USER_PASSWORD');
					$scope.userInfoList = response.userInfoList;


					setTimeout(function () {
						if (!isBlankOrNull($scope.userInfoList[0].dateOfBirth)) {
							var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
							$("#dateOfBirth").val($scope.userInfoList[0].dateOfBirth);
							var datePickerValue = $scope.userInfoList[0].dateOfBirth;
							var datePickerArray = datePickerValue.split("-");
							var datePickerYear = datePickerArray[0];

							//var date = new Date(this.value);
							var date = new Date(datePickerValue);

							var day = ("0" + date.getDate()).slice(-2);
							var monthIndex = date.getMonth();
							var year = date.getFullYear();
							var inputDate = day + ' ' + monthNames[monthIndex] + ' ' + year;
							//$$("#date_txt").html(inputDate +"   "+test);
							$$("#date_txt").html(inputDate);
							//$$("#date_txt_1").val(inputDate);
							var hiddenDate = parseInt(year) + "-" + ("0" + (parseInt(monthIndex) + 1)).slice(-2) + "-" + ("0" + (parseInt(day))).slice(-2);
							$$("#date_txt_1").val(hiddenDate);
						}
						else {
							//$("#dateOfBirth").val(mindate); 
							//$$("#date_txt").html(mindate);
							//$$("#date_txt_1").val(mindate);
						}

						$("#dateOfBirth").attr("min", minDateAndroid);
						$("#dateOfBirth").attr("max", maxDateAndroid);

						if (!isBlankOrNull($scope.userInfoList[0].schoolId)) { $("#nonUKSchoolNameFlag").val("N"); }
						if (!isBlankOrNull($scope.userInfoList[0].email)) {
							localStorage.setItem('LOGGED_IN_USER_EMAIL', $scope.userInfoList[0].email);
						}

						var input = document.getElementById('dateOfBirth');
						var output = document.getElementById('date_txt');
						var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
						input.addEventListener('change', function () {
							//output.innerHTML = 'change ' + this.value
							removeDisabledProfileSaveButton();
							if (this.value == "") {
								//$$("#searchFieldText_1").val('2014-04-30');
								$$("#date_txt").html("Enter date");
							} else {
								//$$("#date_txt").html(this.value);
								var datePickerValue = this.value;
								var datePickerArray = datePickerValue.split(" ");
								var datePickerYear = datePickerArray[0];

								//var date = new Date(this.value);
								var date = new Date(datePickerValue);

								var day = ("0" + date.getDate()).slice(-2);
								var monthIndex = date.getMonth();
								var year = date.getFullYear();
								var inputDate = day + ' ' + monthNames[monthIndex] + ' ' + year;
								//$$("#date_txt").html(inputDate +"   "+test);
								$$("#date_txt").html(inputDate);
								$$("#date_txt_1").val(datePickerValue);
							}
						});

						input.addEventListener('input', function () {
							//output.innerHTML = 'input ' + this.value;
							removeDisabledProfileSaveButton();
							if (this.value == "") {
								//$$("#date_txt").val('2014-04-30');
								$$("#date_txt").html("Enter date");
							} else {
								//$$("#date_txt").html(this.value);
								var datePickerValue = this.value;
								var datePickerArray = datePickerValue.split(" ");
								var datePickerYear = datePickerArray[0];

								//var date = new Date(this.value);
								var date = new Date(datePickerValue);

								var day = ("0" + date.getDate()).slice(-2);
								var monthIndex = date.getMonth();
								var year = date.getFullYear();
								var inputDate = day + ' ' + monthNames[monthIndex] + ' ' + year;
								//$$("#date_txt").html(inputDate +"   "+test);
								$$("#date_txt").html(inputDate);
								$$("#date_txt_1").val(datePickerValue);
							}

						});
						input.addEventListener('blur', function () {
							//output.innerHTML = 'input ' + this.value;
							removeDisabledProfileSaveButton();
							if (this.value == "") {
								//$$("#date_txt").val('2014-04-30');
								$$("#date_txt").html("Enter date");
							} else {
								//$$("#date_txt").html(this.value);
								var datePickerValue = this.value;
								var datePickerArray = datePickerValue.split(" ");
								var datePickerYear = datePickerArray[0];

								//var date = new Date(this.value);
								var date = new Date(datePickerValue);

								var day = ("0" + date.getDate()).slice(-2);
								var monthIndex = date.getMonth();
								var year = date.getFullYear();
								var inputDate = day + ' ' + monthNames[monthIndex] + ' ' + year;
								//$$("#date_txt").html(inputDate +"   "+test);
								$$("#date_txt").html(inputDate);
								$$("#date_txt_1").val(datePickerValue);
							}

						});

						$('#first-name').focus();
						$('#first-name').caret(-1);

					}, 200);

					$scope.countriesList = response.countriesList;
					$scope.qualificationList = response.qualificationList;
					/*Start date*/
					$scope.yearOfEntryList = response.yearOfEntryList;
					angular.forEach($scope.yearOfEntryList, function (value, index) {
						if ($scope.yearOfEntryList[index].selectedYear == 'Y') {
							$scope.startDate = $scope.yearOfEntryList[index].yearOfEntry;
							$("#saveYoe").removeClass("gry");
						}
					});
					/*Previous qualification filter*/
					$scope.previousQualificationfilters = response.previousQualList;
					var pqSelectedVal = "";
					$scope.qualselected = '1|' + $scope.previousQualificationfilters[0].gradeLevel + '|' + $scope.previousQualificationfilters[0].textKey;
					angular.forEach($scope.previousQualificationfilters, function (value, index) {
						if (!isBlankOrNull($scope.previousQualificationfilters[index].previousQualSelected)) {
							$scope.qualselected = index + 1 + '|' + $scope.previousQualificationfilters[index].gradeLevel + '|' + $scope.previousQualificationfilters[index].textKey;
							pqSelectedVal = $scope.previousQualificationfilters[index].previousQualSelected;
							if ($scope.previousQualificationfilters[index].textKey == "TARIFF_POINTS") {
								$scope.qualselected = index + 1 + '||' + $scope.previousQualificationfilters[index].textKey;
								setTimeout(function () { $$D("pqTariffSavedValue").value = $scope.previousQualificationfilters[index].previousQualSelected; }, 500);
							}
						}
					});
					setTimeout(function () {
						$("#previousQualGradesSelected").val(pqSelectedVal);


						onChageQualGrades($scope.qualselected, 'preselect', '', '', ''); $("#pqSavedValue").val($scope.qualselected);
						$("#selectGrades").val($scope.qualselected);
					}, 500);
					//
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			$scope.editProfile();
			//
			$scope.validateName = function (name, id) {
				if (isEmpty($("#" + name).val()) || !validateUserName($("#" + name).val())) {
					$$D(id).innerHTML = ErrorMessage.error_signup_name; $("#" + id).show();
				} else {
					$$D(id).innerHTML = ""; $("#" + id).hide();
				}
			};
			$scope.validateEmail = function (email, id) {
				if (isEmpty($$D(email).value) || !checkValidEmail($$D(email).value)) {
					$$D(id).innerHTML = ErrorMessage.error_signup_email; $("#" + id).show();
				} else {
					$$D(id).innerHTML = ""; $("#" + id).hide();
				}
			}
			$scope.validateDate = function (date, id) {
				if (isEmpty($$D(date).value) || !isValidDate($$D(date).value)) {
					$("#" + id).show();
				} else {
					$("#" + id).hide();
				}
			}
			$scope.saveUserDetails = function () {
				if (saveUserDetailsValidation()) {
					var firstName = !isBlankOrNull($$D("first-name").value) ? $$D("first-name").value : "";
					var lastName = !isBlankOrNull($$D("last-name").value) ? $$D("last-name").value : "";
					var email = !isBlankOrNull($$D("email").value) ? $$D("email").value : "";


					//if(Constants.devicePlatform == 'IOS') { 
					var dateOfBirth = !isBlankOrNull($$D("dateOfBirth").value) ? $$D("dateOfBirth").value : "";
					//} 
					//if(Constants.devicePlatform == 'ANDROID') { 
					//var dateOfBirth = !isBlankOrNull($$D("date_txt_android_1").value) ? $$D("date_txt_android_1").value : "";
					//} 
					var nationality = !isBlankOrNull($$D("nationality").value) ? $$D("nationality").value : "";
					var studyLevel = !isBlankOrNull($$D("studyLevel").value) ? $$D("studyLevel").value : "";
					var startDate = !isBlankOrNull($$D("startDate").innerHTML) ? $$D("startDate").innerHTML : "";
					var userSchoolName = !isBlankOrNull($$D("userSchoolName").innerHTML) ? $$D("userSchoolName").innerHTML : "";
					var userSchoolId = !isBlankOrNull($$D("userSchoolId").value) ? $$D("userSchoolId").value : "";

					localStorage.setItem('SCHOOL_NAME', userSchoolName);
					localStorage.setItem('SCHOOL_URN', userSchoolId);
					//var gradeText = !isBlankOrNull($$D("gradeText").value) ? $$D("gradeText").value : "";																														
					var gradeText = "";
					var addressLineOne = !isBlankOrNull($("#userAddressOne").val()) ? $("#userAddressOne").val() : "";
					var addressLineTwo = !isBlankOrNull($("#userAddressTwo").val()) ? $("#userAddressTwo").val() : "";
					var city = !isBlankOrNull($('#userCity').html()) ? $('#userCity').html() : "";
					var postCode = !isBlankOrNull($('#userPostcode').html()) ? $('#userPostcode').html() : "";
					var countyState = !isBlankOrNull($('#userCountyState').html()) ? $('#userCountyState').html() : "";
					var password = localStorage.getItem('LOGGED_IN_USER_PASSWORD');
					//var preQualType = !isBlankOrNull($('#previousQualSelected').val()) ? $('#previousQualSelected').val() : "";
					var preQualType = "";
					var nonUkSchool = !isBlankOrNull($('#nonUKSchoolNameFlag').val()) ? $('#nonUKSchoolNameFlag').val() : "N";
					if (nonUkSchool == "N") { userSchoolName = userSchoolId; }
					var stringJson = {
						"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
						"firstName": firstName,
						"lastName": lastName,
						"email": email,
						"password": password,
						"dateOfBirth": dateOfBirth,
						"nationality": nationality,
						"addressLineOne": addressLineOne,
						"addressLineTwo": addressLineTwo,
						"city": city,
						"countyState": countyState,
						"postCode": postCode,
						"yearOfEntry": startDate,
						"studyLevel": studyLevel,
						"school": userSchoolName,
						"nonUkSchool": nonUkSchool,
						"preQualType": preQualType,
						"preQualGrade": gradeText,
						"accessToken": "246",
						"affiliateId": "220703",
						"appVersion": Constants.appVersion
					};
					var jsonData = JSON.stringify(stringJson);
					var req = {
						method: 'POST',
						url: 'https://mtest.whatuni.com/wuappws/profile/save-user-profile/',
						headers: { 'Content-Type': 'application/json' },
						data: jsonData
					}
					$http(req).then(function (httpresponse) {
						var response = httpresponse.data;
						
						var status = response.successMsg;
						if (status == "SUCCESS") {
							xtremePushTag("Year_Of_Entry", startDate);
							$$D("emailErrMsg").innerHTML = "";
							if (!isBlankOrNull($$D("email").value)) { localStorage.setItem('LOGGED_IN_USER_EMAIL', $$D("email").value); }
							var firstName = !isBlankOrNull($$D("first-name").value) ? $$D("first-name").value : "";
							var fName = firstName.charAt(0).toUpperCase() + firstName.substring(1, firstName.length);
							var lastName = !isBlankOrNull($$D("last-name").value) ? $$D("last-name").value : "";
							var lName = lastName.charAt(0).toUpperCase() + lastName.substring(1, lastName.length);
							setTimeout(function () { $("#firstLastName").html(fName + " " + lName); }, 300);
							localStorage.setItem('LOGGED_IN_USER_FIRST_NAME', firstName);
							localStorage.setItem('LOGGED_IN_USER_LAST_NAME', lastName);
							mainView.router.loadPage('html/user/userProfile.html');
						}
						else if (status == "EXISTS") {
							$$D("emailErrMsg").innerHTML = ErrorMessage.error_email_already_exists;
							$("#emailErrMsg").show();
						}
					}, function (httpresponse, status) {
						var response = httpresponse.data;
						localStorage.setItem('exception', response.exception);
						localStorage.setItem('status', response.status);
						localStorage.setItem('message', response.message);
						mainView.router.loadPage('html/exception/exception.html');
					});
				}
			}
			$scope.focus = function (id) {
				$("#" + id).focus();
			}
			//Previous qualification		
			$scope.saveUCASTariff = function () {
				validateSelectedGrades('', 'gradeText', '', '');
				if (validateSelectedGrades("closePopUp", 'gradeText', '', '')) { myApp.closeModal('.pred_grades'); }
			}
			$scope.closePreviousQual = function () {
				myApp.closeModal('.pred_grades');
				if (!isBlankOrNull($$D("pqSavedValue").value)) { $("#selectGrades").val($$D("pqSavedValue").value); }
				else { $("#selectGrades").val($$D("pqResetValue").value); }
				onChageQualGrades($$D("selectGrades").value, 'preselect', '', '');
				$("#previousQualErrMsg").hide();
			}
			$scope.backToEditPage = function () {
				myApp.closeModal('.add_address');
				if ($$D('addressAjax')) {
					$$D('addressAjax').value = '';
					$$D('xmark').className = '';
					$('#ajaxList').css('display', 'none');
				}
			}
			$scope.clearSearch = function () {
				if ($$D('addressAjax')) {
					$$D('addressAjax').value = '';
					$$D('xmark').className = '';
					$('#ajaxList').css('display', 'none');
				}
			}
			//
			$(".add_address").on("open", function () {
				if (Constants.devicePlatform == 'IOS')
					Keyboard.hideFormAccessoryBar(true);
			});
			//
			$scope.validatePostcode = function () {
				if ($$D('addressAjax')) {
					if ($$D('addressAjax').value.length > 0) {
						$$D('xmark').className = 'searchbar-clear';
						$('#ajaxList').css('display', 'none');
						var postcode = $$D('addressAjax').value;
						postcode = checkPostCode(postcode);
						if (checkPostCode(postcode)) {
							$$D('editAddrLoader').style.display = 'block';
							var stringJson = {
								"postCode": postcode.toUpperCase(),
								"appVersion": Constants.appVersion,
								"affiliateId": "220703",
								"accessToken": "246"
							};
							var jsonData = JSON.stringify(stringJson);
							var url = "https://mtest.whatuni.com/wuappws/get-ajax-address/";
							var req = {
								method: 'POST',
								url: url,
								headers: { 'Content-Type': 'application/json' },
								data: jsonData
							}
							$http(req).then(function (httpresponse) {
								var response = httpresponse.data;
								$scope.ajaxAddressList = response.ajaxAddressList;
								if (response.ajaxAddressList != null && response.ajaxAddressList.length > 0) {
									setTimeout(function () {
										$('#ajaxList').css('display', 'block');
										$$D('editAddrLoader').style.display = 'none';
									}, 300);
								}
							}, function (httpresponse, status) {
								var response = httpresponse.data;
								localStorage.setItem('exception', response.exception);
								localStorage.setItem('status', response.status);
								localStorage.setItem('message', response.message);
								mainView.router.loadPage('html/exception/exception.html');
							});
						}
					} else {
						$$D('xmark').className = '';
						$('#ajaxList').css('display', 'none');
					}
				}
			}
			//
			$scope.addXmark = function (id, xmarkId, pastedTxt) {
				if ($$D(id) && $$D(xmarkId)) {
					if ($$D(id).value) {
						$$D(xmarkId).className = 'hd_cls';
						var count = $('.hd_cls')
						if (count.length == 4) {
							$('#saveButton').addClass('btn_blu');
						} else {
							if (count.length == 3) {
								if ($$D('addressLine_cls') && isBlankOrNull($$D('addressLine_cls').className)) {
									$('#saveButton').addClass('btn_blu');
								}
							} else {
								$('#saveButton').removeClass('btn_blu');
							}
						}
					} else {
						$$D(xmarkId).className = '';
						$('#saveButton').removeClass('btn_blu');
					}
				}
			}
			//
			$scope.validateUserAddress = function () {
				if ($$D('buildingName_cls') && $$D('addressLine_cls') && $$D('city_cls') && $$D('postCode_cls')) {
					if ($('#buildingName_cls').hasClass('hd_cls') && $('#addressLine_cls').hasClass('hd_cls') && $('#city_cls').hasClass('hd_cls') && $('#postCode_cls').hasClass('hd_cls')) {
						$('#saveButton').addClass('btn_blu');
					} else {
						$('#saveButton').removeClass('btn_blu');
					}
				}
			}
			//
			$scope.clearField = function (id, xmarkId) {
				if ($$D(id)) {
					$$D(id).value = '';
					$$D(xmarkId).className = '';
					$('#saveButton').removeClass('btn_blu');
				}
			}
			//
			$scope.saveUserAddress = function (buildingName, addressLine, city, postcode) {
				if ($$D(buildingName) && $$D(addressLine) && $$D(city) && $$D(postcode)) {
					if ($('#saveButton').hasClass('btn_blu')) {
						var buildingName = $$D(buildingName).value.length > 0 ? $$D(buildingName).value : '';
						var addressLine = $$D(addressLine).value.length > 0 ? $$D(addressLine).value : '';
						var city = $$D(city).value.length > 0 ? $$D(city).value : '';
						var postcode = $$D(postcode).value.length > 0 ? $$D(postcode).value : '';
						$("#userAddress").html(buildingName + " " + addressLine);
						$("#userAddressOne").val(buildingName);
						$("#userAddressTwo").val(addressLine);
						$("#userCity").html(city);
						$("#userPostcode").html(postcode);
						myApp.closeModal('.search_land');
						myApp.closeModal('.add_address');
					}
				}
			}
			//
			$scope.getAddressList = function (index) {
				if ($$D('address-' + index)) {
					var address = $$D('address-' + index).value.split("###");
					var length = address.length;
					var postcode = address[length - 1];
					//var countyState = address[length - 2] ;
					var city = address[length - 2];
					var addressLineTwo = address[length - 3];
					var addressLineOne = address[length - 4];
					$("#userAddress").html(addressLineOne + " " + addressLineTwo);
					$("#userCity").html(city);
					$("#userPostcode").html(postcode);
					$("#userAddressOne").val(addressLineOne);
					$("#userAddressTwo").val(addressLineTwo);
					myApp.closeModal('.add_address');
				}
			}
			//
			$scope.getSchool = function () {
				var nonUKCheck = $('#nonUKCheck').prop('checked');
				if ($$D('schoolAjax') && !nonUKCheck) {
					if ($$D('schoolAjax').value.length > 0) {
						$$D('xmark').className = 'searchbar-clear';
						var school = $$D('schoolAjax').value;
						if (school.length > 2) {
							$$D('editSchoolLoader').style.display = 'block';
							var stringJson = {
								"school": school,
								"appVersion": Constants.appVersion,
								"affiliateId": "220703",
								"accessToken": "246"
							};
							var jsonData = JSON.stringify(stringJson);
							var url = "https://mtest.whatuni.com/wuappws/profile/school-list-ajax/";
							var req = {
								method: 'POST',
								url: url,
								headers: { 'Content-Type': 'application/json' },
								data: jsonData
							}
							$http(req).then(function (httpresponse) {
								var response = httpresponse.data;
								$$D('editSchoolLoader').style.display = 'none';
								if (!($('#nonUKCheck').prop('checked'))) { $scope.schoolList = response.schoolList; }
								if (response.schoolList != null && response.schoolList.length > 0 && $$D('schoolAjax').value.length > 2) {
									$('#schoolAjax').css('display', 'block');
								} else {
									$("#saveUserSchoolName").addClass("gry"); $("#saveUserSchoolName").attr("disabled", "disabled");
									$scope.schoolList = null;
								}
							}, function (httpresponse, status) {
								var response = httpresponse.data;
								localStorage.setItem('exception', response.exception);
								localStorage.setItem('status', response.status);
								localStorage.setItem('message', response.message);
								mainView.router.loadPage('html/exception/exception.html');
							});
						} else {
							$("#saveUserSchoolName").addClass("gry"); $("#saveUserSchoolName").attr("disabled", "disabled");
							$scope.schoolList = null;
						}
					} else {
						$$D('xmark').className = '';
					}
				} else {
					var school = $$D('schoolAjax').value;
					if (school.length >= 1) {
						$("#userSchoolName").html(school);
						$("#saveUserSchoolName").removeClass("gry"); $("#saveUserSchoolName").removeAttr("disabled", "disabled");
					}
					else {
						$("#userSchoolName").html($("#oldUserSchoolName").val());
						$("#saveUserSchoolName").addClass("gry"); $("#saveUserSchoolName").attr("disabled", "disabled");
					}
				}
			}
			//		
			$scope.saveSchool = function (index) {
				if ($$D('school-' + index)) {
					var school = $$D('school-' + index).value.split("|");
					var schoolId = school[0];
					var schoolName = school[1];
					$("#userSchoolName").html(schoolName);
					$("#userSchoolId").val(schoolId);
					$("#schoolAjax").val(schoolName);
					$("#saveUserSchoolName").removeClass("gry");
					$("#saveUserSchoolName").removeAttr("disabled", "disabled");
					$scope.schoolList = null;
				}
			}
			$scope.saveUserSchoolAjax = function () {
				$("#oldUserSchoolName").val($("#userSchoolName").html());
				$("#nonUKSchoolNameFlag").val($('#nonUKCheck').prop('checked') ? "Y" : "N");
				myApp.closeModal('.schl_name'); $scope.closeAndClearSchool();
			}
			$scope.closeAndClearSchool = function () {
				if (!isBlankOrNull($("#oldUserSchoolName").val())) { $("#userSchoolName").html($("#oldUserSchoolName").val()); }
				else { $("#userSchoolName").html("Not selected"); }
				$("#schoolAjax").val("");
				$("#schoolAjax").attr("placeholder", "Enter school name");
				$("input[id='nonUKCheck']").prop('checked', false);
				$scope.schoolList = null;
				$("#saveUserSchoolName").addClass("gry"); $("#saveUserSchoolName").attr("disabled", "disabled");
				myApp.closeModal('.schl_name');
			}
			//
			$scope.saveYearOfEntry = function (yoe) {
				if (yoe == 'save') {
					$("#oldStartDate").val($("#startDate").html()); myApp.closeModal('.start_date');
					//googleTagManagerEventTrack(null, 'Start Date', 'Year of Entry', $("#startDate").html(), '0'); 
					firebaseEventTrack('year_of_entry', { start_date: $("#startDate").html() });
				} else {
					$("#startDate").html(yoe); $("#saveYoe").removeClass("gry"); $("#saveYoe").removeAttr("disabled", "disabled");
					$(".selectedYear").removeClass("cl_active");
					$("#year_" + yoe).addClass("cl_active");
				}
			}
			$scope.closeYoe = function () {
				$(".selectedYear").removeClass("cl_active");
				$("#startDate").html($("#oldStartDate").val());
				if (!isBlankOrNull($("#oldStartDate").val())) { $("#startDate").html($("#oldStartDate").val()); $("#year_" + $("#startDate").html()).addClass("cl_active"); }
				else {
					$("#startDate").html("Not selected");
					$("#saveYoe").addClass("gry"); $("#saveYoe").attr("disabled", "disabled");
				}
				myApp.closeModal('.start_date');
			}
			$scope.checkNonUkFlag = function () {
				$('#nonUKCheck').attr('checked', true);
				$("#schoolAjax").val("");
				$("#schoolAjax").attr("placeholder", "Enter school name");
				$("#saveUserSchoolName").addClass("gry"); $("#saveUserSchoolName").attr("disabled", "disabled");
				$scope.schoolList = null;
			}
			$scope.focusFunc = function (id) {
				if (id == "buildingName") {
					$("#buildingName").val("");
					$("#addressLine").val("");
					$("#city").val("");
					$("#postCode").val("");
					$("#buildingName_cls").removeClass("hd_cls");
					$("#addressLine_cls").removeClass("hd_cls");
					$("#city_cls").removeClass("hd_cls");
					$("#postCode_cls").removeClass("hd_cls");
					$("#saveButton").removeClass('btn_blu');
				}
				setTimeout(function () { $("#" + id).focus(); }, 300);
			}
			returnKey("next");
			//Keyboard.disableScrollingInShrinkView(true);
			//Keyboard.shrinkView(true);
			//Keyboard.automaticScrollToTopOnHiding = true;
		})
		//
		.controller("quizCtrl", function ($scope, $http, $sce, $element, $timeout) {
			var userID = localStorage.getItem('LOGGED_IN_USER_ID');
			firebaseEventTrack('screenview', { 'page_name': '/quiz' });
			if (isBlankOrNull(localStorage.getItem('QUIZ_COMPLETE'))) {
				localStorage.setItem('QUIZ_COMPLETE', "No");
			}
			localStorage.quizTracking = "";
			var quizbutton = null;
			var quizbox = null;
			var quizquestion = null;
			var quizDbTimeout = null;
			var quizproceedTime = null;
			var quizdbresponse = null;

			if (!isBlankOrNull(localStorage.getItem('TAKE_QUIZ'))) {
				Constants.gtmQuizCategory = 'Quiz Return';
				localStorage.removeItem('TAKE_QUIZ');
			} else {
				Constants.gtmQuizCategory = 'Quiz';
			}
			//clearFilter();
			$scope.getSafeHtml = function (x) {
				if (!isBlankOrNull($("#jobOrIndustryName").val()) && !isBlankOrNull(x)) { x = replaceAll(x, '***JOB***', $("#jobOrIndustryName").val()); }
				return $sce.trustAsHtml(x);
			};

			$scope.clearAllTimeout = function () {
				// clear all timeout Start
				if (quizquestion != null) {
					clearTimeout(quizquestion);
				}
				if (quizbox != null) {
					clearTimeout(quizbox);
				}
				// clear all timeout End
			}
			//
			$scope.quiz = function () {
				if ($scope.tempQuestionDetailsList[0].answerType != 'AJAX')
					$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
				//			
				var timeout = 2000;
				if (Constants.devicePlatform == 'IOS')
					Keyboard.hideFormAccessoryBar(true);
				var filterNameScope = $scope.footerDetailsList[0].filterName ? $scope.footerDetailsList[0].filterName.toLowerCase() : "";
				if ($scope.tempQuestionList.length == 0 && (filterNameScope == "grade1" || filterNameScope == "grade2" || filterNameScope == "grade3" || filterNameScope == "grade4" || filterNameScope == "grade5" || filterNameScope == "grade6")) {
					if (Constants.devicePlatform == 'IOS')
						Keyboard.hideFormAccessoryBar(false);
					setTimeout(function () {
						//$("#gradeSubject").focus(); 
						if (Constants.devicePlatform == 'IOS') {
							$("#gradeSubject").focus();
						} else {
							var pluginGradeSubject = [];
							$("#gradeSubject option").each(function (index) {
								pluginGradeSubject.push({ text: $(this).text(), value: $(this).val() })
								//A - Levels|C
							});
							var preSelectedValue = pluginGradeSubject[0].value;
							var config = {
								title: "",
								items: pluginGradeSubject,
								selectedValue: preSelectedValue,
								doneButtonLabel: "Done",
								cancelButtonLabel: "Cancel"
							};

							// Show the picker
							window.plugins.listpicker.showPicker(config,
								function (item) {
									$("#gradeSubject").val(item);
									$("#gradeSubject").change();
								},
								function () {
									$("#gradeSubject").val(preSelectedValue);
									$("#gradeSubject").change();
								}
							);
						}

					}, 100);
				}
				angular.forEach($scope.tempQuestionList, function (value, index) {
					if (index == 0) {
						$("#image_" + $scope.tempQuestionDetailsList[0].questionId + index).hide();
						$("#questionTxt_" + $scope.tempQuestionDetailsList[0].questionId + index).show();
						$("#question_" + $scope.tempQuestionDetailsList[0].questionId + (index + 1)).show();
						if (index == $scope.tempQuestionList.length - 1 && $scope.tempQuestionDetailsList[0].answerType == 'AJAX') {
							$("#searchsubject_" + $scope.tempQuestionDetailsList[0].questionId + index).show();
							setTimeout(function () {
								$("#searchsubject_" + $scope.tempQuestionDetailsList[0].questionId + index).focus()
							}, 300);
						} if (index == $scope.tempQuestionList.length - 1) { timeout = 0; }
					} else {
						quizquestion = setTimeout(function () {
							$("#image_" + $scope.tempQuestionDetailsList[0].questionId + index).hide();
							if ((index == $scope.tempQuestionList.length - 1) && ($scope.tempQuestionDetailsList[0].questionId == 2)) {
								$("#chtMsg_" + $scope.tempQuestionDetailsList[0].questionId + index).addClass('quz_hm');
							}
							$("#questionTxt_" + $scope.tempQuestionDetailsList[0].questionId + index).show();
							//
							$("#question_" + $scope.tempQuestionDetailsList[0].questionId + (index + 1)).show();
							if (index == $scope.tempQuestionList.length - 1 && $scope.tempQuestionDetailsList[0].answerType == 'AJAX') {
								$("#searchsubject_" + $scope.tempQuestionDetailsList[0].questionId + index).show();
								setTimeout(function () {
									$("#searchsubject_" + $scope.tempQuestionDetailsList[0].questionId + index).focus();
								}, 300)
								$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
							}
						}, timeout);

						if (index != $scope.tempQuestionList.length - 1) { timeout = timeout * 2; }
						if ($scope.footerDetailsList[0].answerDisplayStyle == 'GRADE_BOX') {
							onChageQualGradesQuiz($scope.previousQualList[0].qualification, $scope.previousQualList[0].gradeStr, $scope.previousQualList[0].gradeLevel);
						}
					}
					if (index == $scope.tempQuestionList.length - 1) {
						//
						quizbox = setTimeout(function () {
							if ($scope.footerDetailsList[0].answerDisplayStyle == 'GRADE_BOX') {
								$("#question_" + $scope.tempQuestionDetailsList[0].questionId + "_q").show();
								setTimeout(function () {
									$("#skipQuesDiv").hide();
								}, 100);
								$("#gradeSaveDiv").show();
							} else {
								$("#question_" + $scope.tempQuestionDetailsList[0].questionId + "_q").show();
							}
							var filterNameTemp = $scope.footerDetailsList[0].filterName ? $scope.footerDetailsList[0].filterName.toLowerCase() : "";
							if (filterNameTemp == "grade1" || filterNameTemp == "grade2" || filterNameTemp == "grade3" || filterNameTemp == "grade4" || filterNameTemp == "grade5" || filterNameTemp == "grade6") {
								if (Constants.devicePlatform == 'IOS')
									Keyboard.hideFormAccessoryBar(false);
								setTimeout(function () {
									//$("#gradeSubject").focus(); 
									if (Constants.devicePlatform == 'IOS') {
										$("#gradeSubject").focus();
									} else {
										var pluginGradeSubject = [];
										$("#gradeSubject option").each(function (index) {
											pluginGradeSubject.push({ text: $(this).text(), value: $(this).val() })
											//A - Levels|C
										});
										var preSelectedValue = pluginGradeSubject[0].value;
										var config = {
											title: "",
											items: pluginGradeSubject,
											selectedValue: preSelectedValue,
											doneButtonLabel: "Done",
											cancelButtonLabel: "Cancel"
										};

										// Show the picker
										window.plugins.listpicker.showPicker(config,
											function (item) {
												$("#gradeSubject").val(item);
												$("#gradeSubject").change();
											},
											function () {
												$("#gradeSubject").val(preSelectedValue);
												$("#gradeSubject").change();
											}
										);
									}
								}, 100);
							}
							if ($scope.tempQuestionDetailsList[0].answerType != 'AJAX') {
								console.log("test now");
								$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
							}
							if ($scope.footerDetailsList[0].answerDisplayStyle == 'BUTTONS' && $scope.footerDetailsList[0].answerType != 'GRADE_DROPDOWN') {
								$("#yesNoDiv").show();
								if ($scope.tempQuestionDetailsList[0].questionId != '1') {
									var height = ($("#quiz-cont").outerHeight() / 4) + $("#mainSection").outerHeight() + adjustHeight; adjustHeight = 0;
									$("#mainSection").css({ "height": height });
								}
							} else if ($scope.footerDetailsList[0].answerDisplayStyle == 'FULL_BUTTON') {
								$("#fullButton").show();
								if ($scope.tempQuestionDetailsList[0].questionId != '1') {
									var height = ($("#quiz-cont").outerHeight() / 4) + $("#mainSection").outerHeight();
									$("#mainSection").css({ "height": height });
								}
							} else if ($scope.footerDetailsList[0].ctaButtonText == 'Done') {
								$("#selectAns").attr("disabled", "disabled");
								if ($scope.footerDetailsList[0].filterName == 'Location') { $("#selectAns").removeAttr("disabled"); }
								if ($scope.footerDetailsList[0].answerType != 'GRADE_DROPDOWN') { $("#skipQuesDiv").show(); }
								var height = $("#mainSection").outerHeight() + $("#skipQuesDiv").outerHeight() + adjustableScrollHeight; adjustableScrollHeight = 0;
								if ($scope.footerDetailsList[0].filterName == 'Subject qual' && height > 460) { height = 458; }
								$("#mainSection").css({ "height": height });
								$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
							}
						}, timeout + 1000);
					} else {
						$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
					}
				});
			}

			$scope.closeSkipDiv = function (id) {
				$("#" + id).hide(); $("#skipQuesDiv").show();
				var height = $("#mainSection").height();
				$("#mainSection").css({ "height": height - 80 });
				$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
			}
			$scope.getQuizDetails = function (questionId, optionValue, optionText, currentQuesId, skipExitFlag, ansTxt) {
				localStorage.quizTracking += "<br><br><b>QuizDetails :</b><br>";
				localStorage.quizTracking += "questionId : " + questionId + "<br> optionValue : " + optionValue + "<br> optionText : " + optionText + "<br> currentQuesId : " + currentQuesId + "<br> skipExitFlag : " + skipExitFlag + "<br>";
				//
				var qualificationCode = "";
				var searchCategoryCode = "";
				var previousQualGrade = "";
				var previousQualSel = "";
				var jacsCode = "";
				//
				var proceed = true; var proceedTime = 0;
				if (ansTxt == 'yesNo') {
					$("#mainSection").append('<div class="cht_usr" id="answer_' + currentQuesId + '">' + optionText + '</div>');
				}
				if (proceed) { $("#yesNoDiv").hide(); $("#skipQuesDiv").hide(); $("#skipAndViewResults").hide(); }
				$("#subSection").html(Constants.quizContent);
				//	
				if (questionId == 'done' && proceed && $("#filterFlag").val() == 'location') {
					var location = $("input[name='location']:checked").val();
					var locDetails = location.split("|");
					$("#region").val(locDetails[1]); $("#valueToBeShown").val(locDetails[1]);
					$("#regionFlag").val("N");
					$("#nextQuestionId").val(locDetails[2]);
					$("#question_" + currentQuesId + "_q").hide();
					$("#filterFlag").val("");
					if (locDetails[1].toLowerCase() != "england") { adjustableScrollHeight = 150; }
					else { adjustHeight = -200; }
				} else if (questionId == 'done' && proceed && $("#filterFlag").val() == 'locType') {
					var locationType = ""; var locType = "";
					$("input[name='locationType']:checked").each(function () {
						locationType += this.value.split('|')[0] + ", ";
						locType += this.value.split('|')[1] + ",";
					});
					locationType = locationType.substr(0, locationType.length - 2);
					locType = locType.substr(0, locType.length - 1);
					//
					if ($("#filterName").val().toLowerCase() == 'your preference') {
						$("#reviewCategory").val(locType);
						$("#reviewCategoryName").val(locationType);
						$("#filterFlag").val("");
					} else {
						$("#locationType").val(locType);
						$("#locationTypeName").val(locationType); adjustableScrollHeight = 150;
					}
					$("#question_" + currentQuesId + "_q").hide(); $("#valueToBeShown").val(locationType);
				} else if (questionId == 'done' && proceed && $("#filterFlag").val() == 'single_select') {
					var val = $("input[name='radioOpt']:checked").val();
					var splitVal = val.split("|");
					if ($("#filterName").val().toLowerCase() == "assesment type") {
						$("#assessmentType").val(splitVal[0]);
						$("#assessmentTypeId").val(splitVal[1]); adjustableScrollHeight = 250;
					} else if ($("#filterName").val().toLowerCase() == "previous qual") {
						$("#previousQual").val(splitVal[1]); optionValue = splitVal[1]; $("#filterFlag").val("");
						$("#previousQualSelected").val(splitVal[0]); adjustableScrollHeight = 200;
					} else if ($("#filterName").val().toLowerCase() == "study mode") {
						$("#studyMode").val(splitVal[1]); $("#filterFlag").val("");
						$("#studyModeTextName").val(splitVal[0]);
						adjustableScrollHeight = 200;
					} else if ($("#filterName").val().toLowerCase() == "subject") {
						$("#searchCategoryCode").val(splitVal[2]);
						$("#searchCategoryId").val(splitVal[1]);
						$("#keywordSearch").val(splitVal[0]);
					} else if ($("#filterName").val().toLowerCase() == "jacs subject") {
						$("#jacsCode").val(splitVal[1]);
						$("#keywordSearch").val(splitVal[0]);
					} else if ($("#filterName").val().toLowerCase() == "subject qual") {
						if (splitVal[1] == "0") { $("#nextQuestionId").val(splitVal[2]); }
						$("#subjectQualName").val(splitVal[0]);
						$("#previousQualSelected").val(splitVal[0]);
						$("#subjectQualKeyVal").val(splitVal[1]);
						$("#previousQual").val(splitVal[1]);
						optionValue = splitVal[1];
					} else {
						$("#qualification").val(splitVal[1]);
						$("#qualificationName").val(splitVal[0]);
					}
					$("#valueToBeShown").val(splitVal[0]);
					$("#question_" + currentQuesId + "_q").hide();
				}
				var filterNameTemp = $("#filterName").val().toLowerCase();
				if (filterNameTemp == "subject1" || filterNameTemp == "subject2" || filterNameTemp == "subject3" || filterNameTemp == "subject4" || filterNameTemp == "subject5" || filterNameTemp == "subject6") {
					optionValue = $("#subjectQualKeyVal").val();
				}
				if (questionId == 'done' && proceed && !isBlankOrNull($("#gradeCount")) && $("#filterName").val() == 'Grade') {
					proceed = validateSelectedGrades("quiz", '', '', '');
				}
				if (questionId == 'done' && proceed) {
					$("#gradeSaveDiv").hide();
					if ($("#filterFlag").val() == 'single_select' && $("#filterName").val().toLowerCase() == "jacs subject") {
						setTimeout(function () { $("#mainSection").append('<div class="cht_usr" id="answer_' + $("#questionId").val() + '">' + $("#valueToBeShown").val() + '</div>'); }, 500);
						subSaveSubject($("#questionId").val(), $("#valueToBeShown").val(), 'jacsSubject'); proceed = false;
					}
					else { $("#mainSection").append('<div class="cht_usr" id="answer_' + currentQuesId + '">' + $("#valueToBeShown").val() + '</div>'); }
				}
				if (questionId == 'done' && proceed && $("#filterFlag").val() == 'locType') {
					$("#mainSection").append('<div class="cht_bot"><div class="cht_ico"></div><div id="searchLoad' + currentQuesId + '" class="cht_msg cht_min"><img src="img/cht_loader.gif" alt="" width="40"></div></div>');
					proceedTime = 2000;
					setTimeout(function () {
						var comment = ""; var length = 0;
						$("input[name='locationType']:checked").each(function () {
							comment += this.value.split('|')[2]; length++;
						});
						if (length == 1) { $("#searchLoad" + currentQuesId).html(comment); } else {
							$("#searchLoad" + currentQuesId).html($("#commentForLocType").val());
						}
					}, 2000); $("#filterFlag").val("");
				}
				if (questionId == 'done' && proceed && !isBlankOrNull($("#gradeCount")) && $("#filterName").val() == 'Grade' && parseInt($("#gradeCount").val()) > 2) {
					proceedTime = 2000;
					$("#mainSection").append('<div class="cht_bot"><div class="cht_ico"></div><div id="searchLoad' + currentQuesId + '" class="cht_msg cht_min"><img src="img/cht_loader.gif" alt="" width="40"></div></div>');
					setTimeout(function () { $("#searchLoad" + currentQuesId).html($("#answerOpt_0").html()); }, 2000);
				}
				//
				// --- GTM event tracking --- 
				//
				var label = $("#answer_" + currentQuesId).html();
				var currentQuestionName = $("#questionName").val();
				//if(!isBlankOrNull(currentQuestionName) && currentQuestionName.toLowerCase() == 'grades') {
				if (currentQuesId == '11') {
					//
					var gradeAnswer = $("#answer_" + currentQuesId).html();
					console.log('gradeAnswer > ' + gradeAnswer);
					var gradeAnswerArr = (gradeAnswer.trim()).split(' ');
					console.log('gradeAnswerArr > ' + gradeAnswerArr.length);
					var gradeResult = "";
					//
					for (var i = 0; i < gradeAnswerArr.length; i++) {
						console.log('gradeAnswerArr[i] > ' + gradeAnswerArr[i].length);
						var gradeLengthGTM = gradeAnswerArr[i].length;
						var gradeValueGTM = gradeAnswerArr[i].charAt(0);
						if (gradeAnswerArr[i].indexOf('A*') > -1) {
							gradeLengthGTM = gradeAnswerArr[i].length / 2;
							gradeValueGTM = 'A*';
						}
						gradeResult += gradeLengthGTM + ',' + gradeValueGTM;
						if (gradeAnswerArr.length > 1 && i != gradeAnswerArr.length - 1) {
							gradeResult += '-';
						}
					}
					//
					console.log('gradeResult > ' + gradeResult);
					label = $("#answer_" + (currentQuesId - 1)).html() + ' | ' + gradeResult;
				}
				if (optionValue == 'skpExt' || $("#nextQuestionId").val() == "12" || $("#filterName").val() == 'Grade' || ($("#nextQuestionId").val() == "10" && !isBlankOrNull($("#previousQual").val()))) {
					qualificationCode = !isBlankOrNull($("#qualification").val()) ? $("#qualification").val() : "";
					searchCategoryCode = !isBlankOrNull($("#searchCategoryCode").val()) ? $("#searchCategoryCode").val() : "";
					previousQualGrade = !isBlankOrNull($("#pqSelectedGrades").val()) ? $("#pqSelectedGrades").val() : "";
					previousQualSel = !isBlankOrNull($("#previousQual").val()) ? $("#previousQual").val() : "";
					jacsCode = !isBlankOrNull($("#jacsCode").val()) ? $("#jacsCode").val() : "";
					if (!isBlankOrNull(jacsCode)) { searchCategoryCode = ""; }
				}
				if (optionValue == 'skpNxt' || optionValue == 'skpExt') {
					label = optionValue == 'skpNxt' ? 'Skip' : 'Skip to end';
					optionValue = "";
				}
				if (!isBlankOrNull(currentQuesId)) {
					if (currentQuestionName == 'Quiz Type') {
						label = label.indexOf('Yes I know') > -1 ? 'Standard' : 'discovery';
						if (label == 'Standard') { localStorage.setItem('QUIZ_JOURNEY', label); }
					} else if (currentQuestionName == 'Discovery Type') {
						label = label.indexOf('Yes I know') > -1 ? 'I want to be' : 'What should i do';
						localStorage.setItem('QUIZ_JOURNEY', label);
					} else if (currentQuestionName == 'Priorities') {
						for (i = 0; i < 3; i++) {
							if (label.indexOf(',') > -1) {
								label = label.replace(',', ' |');
								continue;
							} else {
								break;
							}
						}
					}
					if (currentQuestionName == "Priorities") {
						console.log($("#reviewCategory").val())
						var reviewCategory = $("#reviewCategory").val();
						var reviewCategoryArray = reviewCategory.split(",")
						for (var i = 0; i < reviewCategoryArray.length; i++) {
							if (reviewCategoryArray[i].indexOf("_") > 0) {
								reviewCategoryArray[i] = reviewCategoryArray[i].replace("_", " ");
							}
							reviewCategoryArray[i] = reviewCategoryArray[i].charAt(0).toUpperCase() + reviewCategoryArray[i].substr(1).toLowerCase();
						}
						label = reviewCategoryArray.join(" | ");
					}

					if (((currentQuestionName == 'Suggested Subject' || currentQuestionName == 'Jacs subject') && proceed == false) || (currentQuestionName == 'Grades' && currentQuesId != '11')) {
						// do nothing
					} else {
						//googleTagManagerEventTrack(null, Constants.gtmQuizCategory, currentQuestionName, decodeUsingTextArea(label), '0');
					}
				}
				//
				localStorage.quizTracking += "<br> proceed :" + proceed + "<br>";
				if (quizproceedTime != null) {
					clearTimeout(quizproceedTime);
				}
				quizproceedTime = setTimeout(function () {
					var subject1_id = "";
					var subject2_id = "";
					var subject3_id = "";
					var subject4_id = "";
					var subject5_id = "";
					var subject6_id = "";
					var subj1_tariff_points = "";
					var subj2_tariff_points = "";
					var subj3_tariff_points = "";
					var subj4_tariff_points = "";
					var subj5_tariff_points = "";
					var subj6_tariff_points = "";
					var qualTypeId = "";
					if (proceed) {
						var filterGradeTemp = $("#filterName").val().toLowerCase();
						if ((filterGradeTemp == "grade1" || filterGradeTemp == "grade2" || filterGradeTemp == "grade3" || filterGradeTemp == "grade4" || filterGradeTemp == "grade5" || filterGradeTemp == "grade6") && optionText.toLowerCase() == "done") {
							subject1_id = !isBlankOrNull($("#subject1").val()) ? $("#subject1").val() : "";
							subject2_id = !isBlankOrNull($("#subject2").val()) ? $("#subject2").val() : "";
							subject3_id = !isBlankOrNull($("#subject3").val()) ? $("#subject3").val() : "";
							subject4_id = !isBlankOrNull($("#subject4").val()) ? $("#subject4").val() : "";
							subject5_id = !isBlankOrNull($("#subject5").val()) ? $("#subject5").val() : "";
							subject6_id = !isBlankOrNull($("#subject6").val()) ? $("#subject6").val() : "";
							subj1_tariff_points = !isBlankOrNull($("#grade1").val()) ? $("#grade1").val() : "";
							subj2_tariff_points = !isBlankOrNull($("#grade2").val()) ? $("#grade2").val() : "";
							subj3_tariff_points = !isBlankOrNull($("#grade3").val()) ? $("#grade3").val() : "";
							subj4_tariff_points = !isBlankOrNull($("#grade4").val()) ? $("#grade4").val() : "";
							subj5_tariff_points = !isBlankOrNull($("#grade5").val()) ? $("#grade5").val() : "";
							subj6_tariff_points = !isBlankOrNull($("#grade6").val()) ? $("#grade6").val() : "";
							$("#pqSelectedGrades").val($("#tempQualGrades").val());
						}
						qualTypeId = $("#subjectQualKeyVal").val();
						$("#question_" + currentQuesId + "_q").remove();
						if (questionId == 'done') { questionId = $("#nextQuestionId").val(); }
						questionId = !isBlankOrNull(questionId) ? questionId : "1";
						optionValue = !isBlankOrNull(optionValue) ? optionValue : "";
						var stringJson = {
							"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
							"questionId": questionId,
							"answer": optionValue,
							"qualificationCode": qualificationCode,
							"categoryCode": searchCategoryCode,
							"previousQualGrades": previousQualGrade,
							"previousQual": previousQualSel,
							"qualTypeId": qualTypeId,
							"subject1_id": subject1_id,
							"subject2_id": subject2_id,
							"subject3_id": subject3_id,
							"subject4_id": subject4_id,
							"subject5_id": subject5_id,
							"subject6_id": subject6_id,
							"subj1_tariff_points": subj1_tariff_points,
							"subj2_tariff_points": subj2_tariff_points,
							"subj3_tariff_points": subj3_tariff_points,
							"subj4_tariff_points": subj4_tariff_points,
							"subj5_tariff_points": subj5_tariff_points,
							"subj6_tariff_points": subj6_tariff_points,
							"jacsCode": jacsCode,
							"accessToken": "246",
							"affiliateId": "220703",
							"appVersion": Constants.appVersion
						};
						var jsonData = JSON.stringify(stringJson);

						localStorage.quizTracking += "<br><b>Request :</b><br>";
						localStorage.quizTracking += "jsonData : " + jsonData + "<br>";
						start_time = new Date().getTime();
						localStorage.quizTracking += "start_time : " + start_time + "<br>";

						var req = {
							method: 'POST',
							url: 'https://mtest.whatuni.com/wuappws/quiz/quiz-details/',
							headers: { 'Content-Type': 'application/json' },
							data: jsonData,
							timeout: 100000
						}
						$http(req).then(function (httpresponse) {
							var response = httpresponse.data;
							localStorage.quizTracking += "<br><b>Response :</b><br>"
							localStorage.quizTracking += "response : " + JSON.stringify(response) + "<br>";
							localStorage.quizTracking += "<b>end_time :</b> " + (new Date().getTime() - start_time) + "<br>";
							$scope.tempQuestionDetailsList = response.questionDetailsList;
							$scope.tempQuestionList = $scope.tempQuestionDetailsList[0].questionList;
							$scope.answerOptionsList = $scope.tempQuestionDetailsList[0].answerOptionsList;
							$scope.footerDetailsList = $scope.tempQuestionDetailsList;
							$scope.previousQualList = response.previousQualList;
							var tempQualGrades = "";
							if ($scope.tempQuestionDetailsList[0].filterName == "Grade1") {
								angular.forEach($scope.previousQualList, function (value, index) {
									tempQualGrades += "0" + $scope.previousQualList[index].gradeStr + "-";
								});
								tempQualGrades = tempQualGrades.substring(0, tempQualGrades.length - 1);
								$("#tempQualGrades").val(tempQualGrades);
							}
							if (!isBlankOrNull(response.courseCount) && response.courseCount == "0") {
								$("#qualificationName").val("Undergraduate");
								$("#qualification").val("M");
								$("#previousQual").val("");
								$("#pqSelectedGrades").val("");
								$("#previousQualSelected").val(""); localStorage.setItem('quizQualMessage', "0_COURSE");
							}
							//
							Constants.quizContent = $("#subSection").html();
							//
							if (questionId == '1') {
								$scope.questionList = $scope.tempQuestionList;
								$scope.questionDetailsList = $scope.tempQuestionDetailsList;
								$("#questionName").val($scope.questionDetailsList[0].questionName);
							} else {
								$scope.subQuestionList = $scope.tempQuestionList;
								$scope.subQuestionDetailsList = $scope.tempQuestionDetailsList;
								//
								if ($scope.subQuestionDetailsList[0].filterName == "Location") {
									$scope.subAnswerOptionsList = [];
									angular.forEach($scope.answerOptionsList, function (value, index) {
										if (!isBlankOrNull($scope.answerOptionsList[index].optionValue)) {
											var imageName = $scope.answerOptionsList[index].optionValue.toLowerCase().replace(/ /g, "_");
											$scope.subAnswerOptionsList.push({ "optionText": $scope.answerOptionsList[index].optionText, "optionValue": $scope.answerOptionsList[index].optionValue, "nextQuestionId": $scope.answerOptionsList[index].nextQuestionId, "commentAgainstAnswer": $scope.answerOptionsList[index].commentAgainstAnswer, "imageName": imageName });
										}
									}); $("#filterFlag").val('location');
								} else {
									$scope.subAnswerOptionsList = $scope.answerOptionsList;
								}
								if (quizdbresponse != null) {
									clearTimeout(quizdbresponse);
								}
								quizdbresponse = setTimeout(function () {
									$("#mainSection").append($("#subSection").html());
									$("#question_" + $scope.subQuestionDetailsList[0].questionId + "0").show();
									$("#questionId").val($scope.subQuestionDetailsList[0].questionId);
									$("#nextQuestionId").val($scope.subAnswerOptionsList[0].nextQuestionId);
									if ($scope.subAnswerOptionsList.length > 1) { $("#multipleNextQuestionId").val($scope.subAnswerOptionsList[1].nextQuestionId); }
									$("#questionName").val($scope.subQuestionDetailsList[0].questionName);
									$("#filterName").val($scope.subQuestionDetailsList[0].filterName);
									if ($scope.subQuestionDetailsList[0].filterName == "Region") {
										if (Constants.devicePlatform == 'IOS')
											Keyboard.hideFormAccessoryBar(false);
										setTimeout(function () {
											//$("#locationEngland").focus(); 
											if (Constants.devicePlatform == 'IOS') {
												$("#locationEngland").focus();
											} else {
												var pluginGradeSubject = [];
												$("#locationEngland option").each(function (index) {
													pluginGradeSubject.push({ text: $(this).text(), value: $(this).val() })
													//A - Levels|C
												});
												var preSelectedValue = pluginGradeSubject[0].value;
												var config = {
													title: "",
													items: pluginGradeSubject,
													selectedValue: preSelectedValue,
													doneButtonLabel: "Done",
													cancelButtonLabel: "Cancel"
												};

												// Show the picker
												window.plugins.listpicker.showPicker(config,
													function (item) {
														$("#locationEngland").val(item);
														$("#locationEngland").change();
													},
													function () {
														$("#locationEngland").val(preSelectedValue);
														$("#locationEngland").change();
													}
												);
											}
										}, 100);
									}
									var percent = $scope.subQuestionDetailsList[0].progressPercent;
									$("#percentageMatch").attr("style", "width:" + percent + "%");
									$scope.subQuestionList = null;
									$scope.subQuestionDetailsList = null; $("#subSection").html(""); tempId = "";

								}, 500);
							}

							$scope.clearAllTimeout();
							if (quizDbTimeout != null) {
								clearTimeout(quizDbTimeout);
							}
							quizDbTimeout = setTimeout(function () {
								$scope.quiz();
							}, $scope.tempQuestionDetailsList[0].timeDeplay);
						}, function (httpresponse, status) {
							var response = httpresponse.data;
							localStorage.quizTracking += "<br><b>Error :</b><br>"
							localStorage.quizTracking += "Error response : " + JSON.stringify(response) + "<br>";
							localStorage.quizTracking += "Error status : " + status + "<br>";
							localStorage.quizTracking += "<b>end_time : </b>" + (new Date().getTime() - start_time) + "<br>";
							localStorage.setItem('exception', response.exception);
							localStorage.setItem('status', response.status);
							localStorage.setItem('message', response.message);
							mainView.router.loadPage('html/exception/exception.html');

						});
					}
				}, proceedTime);
			}
			$scope.getQuizDetails();
			//
			$scope.getCourseList = function (searchId) {
				if (quizbox != null) {
					clearTimeout(quizbox);
				}
				var id = searchId.split("_")[1];
				if ($$D(searchId)) {
					var subject = $$D(searchId).value;
					if (subject.length > 2) {
						var ajaxUrl = "";
						var stringJson = {};
						var filterNameTemp = $("#filterName").val().toLowerCase();
						if (filterNameTemp == "subject1" || filterNameTemp == "subject2" || filterNameTemp == "subject3" || filterNameTemp == "subject4" || filterNameTemp == "subject5" || filterNameTemp == "subject6") {
							ajaxUrl = 'https://mtest.whatuni.com/wuappws/quiz/qual-subject-ajax/';
							//
							stringJson = {
								"subjectText": subject,
								"qualTypeId": $("#subjectQualKeyVal").val(),
								"accessToken": "246",
								"affiliateId": "220703",
								"appVersion": Constants.appVersion
							};
						} else {
							ajaxUrl = 'https://mtest.whatuni.com/wuappws/quiz/course-list-ajax/';
							stringJson = {
								"keywordText": subject,
								"accessToken": "246",
								"affiliateId": "220703",
								"appVersion": Constants.appVersion
							};
						}
						var jsonData = JSON.stringify(stringJson);
						var req = {
							method: 'POST',
							url: ajaxUrl,
							headers: { 'Content-Type': 'application/json' },
							data: jsonData
						}
						$http(req).then(function (httpresponse) {
							var response = httpresponse.data;
							var filterNameTemp = $("#filterName").val().toLowerCase();
							if (filterNameTemp == "subject1" || filterNameTemp == "subject2" || filterNameTemp == "subject3" || filterNameTemp == "subject4" || filterNameTemp == "subject5" || filterNameTemp == "subject6") {
								$scope.courseList = response.qualSubjectList;
							} else { $scope.courseList = response.courseList; }
							setTimeout(function () {
								if ($scope.courseList != null && $scope.courseList.length > 0 && $$D(searchId).value.length > 2) {
									$("#searchResultList").html($("#search_ul").html());
									$('#searchResultList').show();
									//
									if (quizCursorCount == 0) {
										//$$("#"+searchId)[0].setSelectionRange(0,0);
										quizCursorCount = 1;
										var height = $('#quiz-cont')[0].scrollHeight;
										setTimeout(function () {
											height = parseInt(height) - parseInt(500);
											$("#quiz-cont").animate({ scrollTop: height }, 200, function () {

											});
											setTimeout(function () {
												$("#" + searchId).siblings('span').html($("#" + searchId).siblings('span').html());
												$("#" + searchId).caret(-1);
											}, 500);
										}, 200);
									}
									setTimeout(function () {
										var fldLength = $$("#" + searchId).val().length;
										$("#" + searchId).caret(-1);
									}, 500);

									//
									if ($$D(searchId).style.display == "none") { $('#searchResultList').hide(); }
								} else {
									quizCursorCount = 0;
									$scope.courseList = null; $("#searchResultList").hide();
								}
							}, 500);
						}, function (httpresponse, status) {
							var response = httpresponse.data;
							localStorage.setItem('exception', response.exception);
							localStorage.setItem('status', response.status);
							localStorage.setItem('message', response.message);
							mainView.router.loadPage('html/exception/exception.html');
						});
					} else {
						quizCursorCount = 0;
						$scope.courseList = null; $("#searchResultList").hide();
					}
				}
			}
			//
			$scope.getJobOrIndustryList = function (searchId) {
				if (quizbox != null) {
					clearTimeout(quizbox);
				}
				var id = searchId.split("_")[1];
				if ($$D(searchId)) {
					var jobIndustry = $$D(searchId).value;
					if (jobIndustry.length > 2) {
						var stringJson = {
							"jobOrIndustry": jobIndustry,
							"accessToken": "246",
							"affiliateId": "220703",
							"appVersion": Constants.appVersion
						};
						var jsonData = JSON.stringify(stringJson);
						var req = {
							method: 'POST',
							url: 'https://mtest.whatuni.com/wuappws/quiz/job-industry-list-ajax/',
							headers: { 'Content-Type': 'application/json' },
							data: jsonData
						}
						$http(req).then(function (httpresponse) {
							var response = httpresponse.data;
							$scope.jobIndustryList = response.jobIndustryList;
							$scope.jobIndustryList.push({ "jobOrIndustryName": "Career not listed", "jobOrIndustryId": "0", "jobOrIndustryFlag": "0" });
							setTimeout(function () {
								if ($scope.jobIndustryList != null && $scope.jobIndustryList.length > 0 && $$D(searchId).value.length > 2) {
									$("#jobOrIndustryResultList").html($("#search_ul").html());
									$('#jobOrIndustryResultList').show();
									//
									if (quizCursorCount == 0) {
										//$$("#"+searchId)[0].setSelectionRange(0,0);
										quizCursorCount = 1;
										var height = $('#quiz-cont')[0].scrollHeight;
										setTimeout(function () {
											height = parseInt(height) - parseInt(500);
											$("#quiz-cont").animate({ scrollTop: height }, 200, function () {
											});
											setTimeout(function () {
												$("#" + searchId).siblings('span').html($("#" + searchId).siblings('span').html());
												$("#" + searchId).caret(-1);
											}, 500);
										}, 200);
									}
									setTimeout(function () {
										$("#" + searchId).caret(-1);
									}, 500);

									//
									if ($$D(searchId).style.display == "none") { $('#jobOrIndustryResultList').hide(); }
								} else {
									quizCursorCount = 0;
									$scope.jobIndustryList = null; $("#jobOrIndustryResultList").hide();
								}
							}, 500);
						}, function (httpresponse, status) {
							var response = httpresponse.data;
							localStorage.setItem('exception', response.exception);
							localStorage.setItem('status', response.status);
							localStorage.setItem('message', response.message);
							mainView.router.loadPage('html/exception/exception.html');
						});
					} else {
						quizCursorCount = 0;
						$scope.jobIndustryList = null; $("#jobOrIndustryResultList").hide();
					}
				}
			}
			//
			$scope.goToHomePage = function () {
				var userID = localStorage.getItem('LOGGED_IN_USER_ID');
				mainView.router.loadPage('html/home/homeLanding.html');
			}
			//
			$scope.checkYourResults = function (nextQuestionId) {
				if (nextQuestionId == '0') {
					mainView.router.loadPage('html/home/homeLanding.html');
				} else {
					var quizCompleteFlag = "Quiz Complete";
					var userID = localStorage.getItem('LOGGED_IN_USER_ID');
					if (Constants.gtmQuizCategory == 'Quiz Return') {
						quizCompleteFlag = "Quiz Return Complete";
					}

					if (localStorage.getItem('QUIZ_COMPLETE') == "No") {
						localStorage.setItem('QUIZ_COMPLETE', quizCompleteFlag);
					}


					$scope.userTrackActionLog();
					var searchCategoryCode = !isBlankOrNull($("#searchCategoryCode").val()) ? $("#searchCategoryCode").val() : "";
					var searchCategoryId = !isBlankOrNull($("#searchCategoryId").val()) ? $("#searchCategoryId").val() : "";
					var subjectName = !isBlankOrNull($("#keywordSearch").val()) ? $("#keywordSearch").val() : "";
					var qualificationName = !isBlankOrNull($("#qualificationName").val()) ? $("#qualificationName").val() : "";
					var qualificationCode = !isBlankOrNull($("#qualification").val()) ? $("#qualification").val() : "";
					var locationType = !isBlankOrNull($("#locationType").val()) ? $("#locationType").val() : "";
					var locationTypeName = !isBlankOrNull($("#locationTypeName").val()) ? $("#locationTypeName").val() : "";
					var region = !isBlankOrNull($("#region").val()) ? $("#region").val() : "";
					var regionFlag = !isBlankOrNull($("#regionFlag").val()) ? $("#regionFlag").val() : "";
					var previousQual = !isBlankOrNull($("#previousQual").val()) ? $("#previousQual").val() : "";
					var previousQualGrade = !isBlankOrNull($("#pqSelectedGrades").val()) ? $("#pqSelectedGrades").val().toLowerCase() : "";
					var previousQualSel = !isBlankOrNull($("#previousQualSelected").val()) ? $("#previousQualSelected").val() : "";
					previousQualSel += (!isBlankOrNull($("#previousQualSelected").val()) && !isBlankOrNull($("#pqSelToDisplay").val())) ? (" - " + $("#pqSelToDisplay").val()) : "";
					var assessmentType = !isBlankOrNull($("#assessmentType").val()) ? $("#assessmentType").val() : "";
					var assessmentTypeId = !isBlankOrNull($("#assessmentTypeId").val()) ? $("#assessmentTypeId").val() : "";
					var studyMode = !isBlankOrNull($("#studyMode").val()) ? $("#studyMode").val() : "";
					var studyModeTextName = !isBlankOrNull($("#studyModeTextName").val()) ? $("#studyModeTextName").val() : "";
					var reviewCategory = !isBlankOrNull($("#reviewCategory").val()) ? $("#reviewCategory").val() : "";
					var reviewCategoryName = !isBlankOrNull($("#reviewCategoryName").val()) ? $("#reviewCategoryName").val() : "";
					var jacsCode = !isBlankOrNull($("#jacsCode").val()) ? $("#jacsCode").val() : "";
					if (!isBlankOrNull(jacsCode)) { searchCategoryCode = ""; searchCategoryId = ""; }
					//								
					var searchCriteria = {
						"searchCategoryCode": searchCategoryCode,
						"searchCategoryId": searchCategoryId,
						"keywordSearch": subjectName,
						"qualification": qualificationCode,
						"orderBy": "",
						"selectedOrderByVal": "",
						"qualificationName": qualificationName,
						"locationType": locationType,
						"locationTypeName": locationTypeName,
						"region": region,
						"regionFlag": regionFlag,
						"studyMode": studyMode,
						"studyModeTextName": studyModeTextName,
						"previousQual": previousQual,
						"previousQualGrade": previousQualGrade,
						"previousQualGradeName": previousQualSel,
						"assessmentTypeName": assessmentType,
						"assessmentType": assessmentTypeId,
						"reviewCategory": reviewCategory,
						"reviewCategoryName": reviewCategoryName,
						"jacsCode": jacsCode
					}
					//
					localStorage.setItem('recentSearchCriteria', JSON.stringify(searchCriteria));
					localStorage.setItem('quizPageFlag', "QUIZ");
					mainView.router.loadPage('html/search/searchResults.html');
				}
			}
			$scope.userTrackActionLog = function () {
				var date = getCurrentDate();
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"actionType": "QP",
					"actionDate": "",
					"actionStatus": "I",
					"actionUrl": "",
					"actionKeyId": "",
					"actionDetails": "",
					"trackSessionId": "",
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/save-user-actions/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;

				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
		})
		.controller("favouritesCtrl", function ($scope, $http, $sce, $element, $timeout) {

			var userID = localStorage.getItem('LOGGED_IN_USER_ID');
			firebaseEventTrack('screenview', { 'page_name': '/favourite' });


			if (isBlankOrNull(localStorage.final5count)) {
				localStorage.final5count = 0;
			}
			$scope.removeFlag = 'N';
			$scope.institutionBrowseLogo = Constants.institutionBrowseLogo;
			//Below code for get the suggestions and shorlists
			$scope.getSuggestionList = function () {

				$("#finalFiveParentDiv").removeClass("sb-hide");
				if ($$D('addToFinal5Tab')) {
					$('#addToFinal5Tab').show();
				}


				$$D('favouritesLoader').style.display = 'block';
				var searchCategoryCode = "";
				var searchCategoryId = "";
				var keywordSearch = "";
				var qualification = "";
				var orderBy = "";
				var locationType = "";
				var region = "";
				var regionFlag = "";
				var studyMode = "";
				var previousQual = "";
				var previousQualGrade = "";
				var assessmentType = "";
				var reviewCategory = "";
				var reviewCategoryName = "";
				var jacsCode = "";
				//
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					//          
					searchCategoryCode = !isBlankOrNull(recentSearchCriteria.searchCategoryCode) ? recentSearchCriteria.searchCategoryCode : "";
					searchCategoryId = !isBlankOrNull(recentSearchCriteria.searchCategoryId) ? recentSearchCriteria.searchCategoryId : "";
					keywordSearch = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "";
					qualification = !isBlankOrNull(recentSearchCriteria.qualification) ? recentSearchCriteria.qualification : "";
					orderBy = !isBlankOrNull(recentSearchCriteria.orderBy) ? recentSearchCriteria.orderBy : "";
					locationType = !isBlankOrNull(recentSearchCriteria.locationType) ? recentSearchCriteria.locationType : "";
					region = !isBlankOrNull(recentSearchCriteria.region) ? recentSearchCriteria.region : "";
					regionFlag = !isBlankOrNull(recentSearchCriteria.regionFlag) ? recentSearchCriteria.regionFlag : "";
					studyMode = !isBlankOrNull(recentSearchCriteria.studyMode) ? recentSearchCriteria.studyMode : "";
					previousQual = !isBlankOrNull(recentSearchCriteria.previousQual) ? recentSearchCriteria.previousQual : "";
					previousQualGrade = !isBlankOrNull(recentSearchCriteria.previousQualGrade) ? recentSearchCriteria.previousQualGrade : "";
					assessmentType = !isBlankOrNull(recentSearchCriteria.assessmentType) ? recentSearchCriteria.assessmentType : "";
					reviewCategory = !isBlankOrNull(recentSearchCriteria.reviewCategory) ? recentSearchCriteria.reviewCategory : "";
					reviewCategoryName = !isBlankOrNull(recentSearchCriteria.reviewCategoryName) ? recentSearchCriteria.reviewCategoryName : "";
					jacsCode = !isBlankOrNull(recentSearchCriteria.jacsCode) ? recentSearchCriteria.jacsCode : "";
				}
				var reviewSubjectOne = ""; var reviewSubjectTwo = ""; var reviewSubjectThree = "";
				if (!isBlankOrNull(reviewCategory)) {
					reviewCategoryArray = reviewCategory.split(",");
					if (reviewCategoryArray.length == 3) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; reviewSubjectThree = reviewCategoryArray[2]; }
					else if (reviewCategoryArray.length == 2) { reviewSubjectOne = reviewCategoryArray[0]; reviewSubjectTwo = reviewCategoryArray[1]; }
					else { reviewSubjectOne = reviewCategoryArray[0]; }
				}
				var subject = (!isBlankOrNull(searchCategoryCode) ? searchCategoryCode : keywordSearch);

				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"latitude": latitude,
					"longitude": longtitude,
					"subject": subject,
					"qualification": qualification,
					"locationType": locationType,
					"region": region,
					"regionFlag": regionFlag,
					"studyMode": studyMode,
					"previousQualification": previousQual,
					"previousQualificationGrade": previousQualGrade,
					"reviewSubjectOne": reviewSubjectOne,
					"reviewSubjectTwo": reviewSubjectTwo,
					"reviewSubjectThree": reviewSubjectThree,
					"jacsCode": jacsCode,
					"assessmentType": assessmentType,
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/shortlist-details',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					if ((window.screen.height * window.devicePixelRatio) == 2436 && Framework7.prototype.device.ios) {
						$('#favFooterDiv').css('bottom', '0');
					}
					$("li.swipeout").each(function (i, e) {
						myApp.swipeoutClose($$('li.swipeout').eq(i), function () { })
					});
					$scope.shortlistExistFlag = response.shortlistExistFlag;
					$scope.tempShortlistDetails = response.shortlistDetails;
					$scope.suggestionDetails = response.suggestionDetails;
					if ($scope.suggestionDetails != null && $scope.suggestionDetails.length == 3) {
						if ($("#finalFiveParentDiv")) { $("#finalFiveParentDiv").addClass("sb-hide"); }
					}
					$scope.final5ShortlistDetails = [];
					$scope.shortlistDetails = [];
					angular.forEach($scope.tempShortlistDetails, function (value, index) {
						if (!isBlankOrNull($scope.tempShortlistDetails[index].position) && parseInt($scope.tempShortlistDetails[index].position) > 0) {
							$scope.final5ShortlistDetails.push({ "finalFiveClassName": "swipeout slct_f5", "logo": $scope.tempShortlistDetails[index].logo, "logoName": $scope.tempShortlistDetails[index].logoName, "courseTitle": $scope.tempShortlistDetails[index].courseTitle, "institutionName": $scope.tempShortlistDetails[index].institutionName, "matchingPercentage": $scope.tempShortlistDetails[index].matchingPercentage, "position": $scope.tempShortlistDetails[index].position, "courseId": $scope.tempShortlistDetails[index].courseId, "courseId": $scope.tempShortlistDetails[index].courseId, "institutionId": $scope.tempShortlistDetails[index].institutionId, "finalChoiceId": $scope.tempShortlistDetails[index].finalChoiceId, "basketContentId": $scope.tempShortlistDetails[index].basketContentId, "positionClassName": "f5_n" + $scope.tempShortlistDetails[index].position + " lt_bld" });
						} else if (isBlankOrNull($scope.tempShortlistDetails[index].position)) {
							$scope.shortlistDetails.push({ "finalFiveClassName": "swipeout", "logo": $scope.tempShortlistDetails[index].logo, "logoName": $scope.tempShortlistDetails[index].logoName, "courseTitle": $scope.tempShortlistDetails[index].courseTitle, "institutionName": $scope.tempShortlistDetails[index].institutionName, "matchingPercentage": $scope.tempShortlistDetails[index].matchingPercentage, "position": $scope.tempShortlistDetails[index].position, "courseId": $scope.tempShortlistDetails[index].courseId, "courseId": $scope.tempShortlistDetails[index].courseId, "institutionId": $scope.tempShortlistDetails[index].institutionId, "finalChoiceId": $scope.tempShortlistDetails[index].finalChoiceId, "basketContentId": $scope.tempShortlistDetails[index].basketContentId });
						}
					})
					if ($scope.shortlistDetails != null && $scope.shortlistDetails.length >= 1) {
						if ($scope.shortlistDetails[0].basketContentId != null) {
							if ($("#finalFiveParentDiv")) { $("#finalFiveParentDiv").removeClass("sb-hide"); }
						}
					}
					//  
					$scope.final5ListLength = '';
					setTimeout(function () {
						if ($scope.final5ShortlistDetails != null && $scope.final5ShortlistDetails.length > 0) {
							if ($scope.final5ShortlistDetails.length > 0 && $scope.final5ShortlistDetails.length <= 1) {
								$('#tabToAddFinal5').html('Tap an option to add to Final 5');
							} else {
								$('#tabToAddFinal5').html('Drag to reorder');
							}
							$scope.final5ListLength = $scope.final5ShortlistDetails.length;
						} else {
							$('#tabToAddFinal5').html('Tap an option to add to Final 5');
						}
						$$D('favouritesLoader').style.display = 'none';
					}, 200);
					//
					$('#addToFavourites').hide();
					$('#addToFinal5').hide();
					//

					if ($scope.final5ShortlistDetails.length > 0) {
						localStorage.final5count = $scope.final5ShortlistDetails.length;
					} else {
						localStorage.final5count = 0;
						//xtremePushTag(tagKey,"NA");
					}
					setTimeout(function () {
						$("#sortable-1").sortable({
							scroll: true
							, tolerance: "pointer"
							//scrollSensitivity: 10,
							//scrollSpeed: 40,
							, items: '> .slct_f5'
							, axis: "y"
							, delay: 0
							//, distance: 5
							, start: function (e, ui) {
								console.log("sortable start");
								ui.helper.addClass('highlight');
								$('.slct_f5').removeClass('swipeout');
								$('.swipeout-actions-right').css('display', 'none')
								$(ui.helper).css('width', "100%");
								var divHeight = (100 / localStorage.final5count);
								$(ui.helper).css('height', divHeight + "%");
							}
							, beforeStop: function (e, ui) {
								ui.helper.removeClass('highlight');
								$('.slct_f5').addClass('swipeout');
								$('.swipeout-actions-right').css('display', 'flex');
								$("#sortable-1").sortable('disable');
								clearTimeout(downTimer);
								touch = false;
							}
							, stop: function (e, ui) {
								var finalChoiceIds = '';
								$("#sortable-1 li").each(function () {
									finalChoiceIds += $(this).attr("finalChoiceId") + ",";
								});
								$scope.reorderFinalFive(finalChoiceIds);
							}
							,
						}).disableSelection();
						$("#sortable-1").sortable('disable');
					}, 100)
					//
					$scope.matchingPercentageFlag;
					if (!isBlankOrNull(locationType) || !isBlankOrNull(region) || !isBlankOrNull(regionFlag) || !isBlankOrNull(studyMode) || !isBlankOrNull(previousQual) || !isBlankOrNull(previousQualGrade) || !isBlankOrNull(assessmentType) || !isBlankOrNull(reviewCategory)) {
						$scope.matchingPercentageFlag = true;
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			$scope.getSuggestionList();
			//Below code for Add to Favourites
			var favSelectedCourseIds = '';
			var favSelectedInstitutionIds = '';

			var favSelectedCourseNames = Array();
			var favSelectedInstitutionNames = Array();

			var favSelectedCourseIdsArray = Array();
			var favSelectedInstitutionIdsArray = Array();

			var firebaseCourseIds = Array();
			var firebaseInstitutionIds = Array();


			$scope.selectSuggestions = function (index) {

				var university_name = $("#suggestions-" + index).attr("university_name");
				var course_name = $("#suggestions-" + index).attr('course_name');



				$('#favIcon-' + index).toggleClass('animated flash fav_fill');
				if ($('#favIcon-' + index).hasClass('animated')) {
					$('.add_fav').addClass('rev');
					$('#addToFavourites').show();
					if ((window.screen.height * window.devicePixelRatio) == 2436 && Framework7.prototype.device.ios) {
						$('#favFooterDiv').css('bottom', 'unset');
					}


					favSelectedInstitutionNames.push(university_name);
					favSelectedCourseNames.push(course_name);
					//
					firebaseInstitutionIds.push($$D('favInstitutionId-' + index).value);
					firebaseCourseIds.push($$D('favCourseId-' + index).value);
				}
				else {
					if (!$('.fav_ico').hasClass('animated')) {
						$('.add_fav').removeClass('rev');
						$('#addToFavourites').hide();
						if ((window.screen.height * window.devicePixelRatio) == 2436 && Framework7.prototype.device.ios) {
							$('#favFooterDiv').css('bottom', '0');
						}
					}

					var favSelectedInstitutionNamesindex = favSelectedInstitutionNames.indexOf(university_name);
					favSelectedInstitutionNames.splice(favSelectedInstitutionNamesindex, 1);
					favSelectedCourseNames.splice(favSelectedInstitutionNamesindex, 1);
					//
					firebaseInstitutionIds.splice(favSelectedInstitutionNamesindex, 1);
					firebaseCourseIds.splice(favSelectedInstitutionNamesindex, 1);

				}
				var courseId = $$D('favCourseId-' + index).value;
				var institutionId = $$D('favInstitutionId-' + index).value;
				if (!isBlankOrNull(courseId)) {
					if (!isBlankOrNull(favSelectedCourseIds) && favSelectedCourseIds.indexOf(courseId) > -1) {
						var courseIdsArray = favSelectedCourseIds.split(",");
						favSelectedCourseIds = "";
						for (var lp = 0; lp < courseIdsArray.length; lp++) {
							if (!isBlankOrNull(courseIdsArray[lp]) && courseIdsArray[lp] != courseId) {
								favSelectedCourseIds += courseIdsArray[lp] + ","
							}
						}
					} else {
						favSelectedCourseIds += courseId + ",";
					}
				} else if (!isBlankOrNull(institutionId)) {
					if (!isBlankOrNull(favSelectedInstitutionIds) && favSelectedInstitutionIds.indexOf(institutionId) > -1) {
						var institutionIdsArray = favSelectedInstitutionIds.split(",");
						favSelectedInstitutionIds = "";
						for (var lp = 0; lp < institutionIdsArray.length; lp++) {
							if (!isBlankOrNull(institutionIdsArray[lp]) && institutionIdsArray[lp] != institutionId) {
								favSelectedInstitutionIds += institutionIdsArray[lp] + ","
							}
						}
					} else {
						favSelectedInstitutionIds += institutionId + ",";
					}
				}
			}
			$scope.addToFavourites = function () {
				$$D('favouritesLoader').style.display = 'block';

				console.log("firebaseInstitutionIds", firebaseInstitutionIds);
				console.log("firebaseCourseIds", firebaseCourseIds);
				console.log("favSelectedCourseNames", favSelectedCourseNames);
				console.log("favSelectedInstitutionNames", favSelectedInstitutionNames);

				var firebaseCourse = 'not set';


				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}

				angular.forEach(favSelectedInstitutionNames, function (value, key) {
					console.log("Key - " + key + " value - " + value);
					if (favSelectedCourseNames[key] != "") {
						firebaseEventTrack('addtowishlist', { item_category: favSelectedInstitutionNames[key].toLowerCase(), item_name: firebaseSubject.toLowerCase(), item_location_id: firebaseCourseIds[key], item_id: firebaseInstitutionIds[key], quantity: 1, interaction_pagename: "/favourite" });
					} else {
						firebaseEventTrack('addtowishlist', { item_category: favSelectedInstitutionNames[key].toLowerCase(), item_name: firebaseSubject.toLowerCase(), item_location_id: 'not set', item_id: firebaseInstitutionIds[key], quantity: 1, interaction_pagename: "/favourite" });
					}
				});


				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"institutionIds": favSelectedInstitutionIds,
					"courseIds": favSelectedCourseIds,
					"basketContentIds": '',
					"removeFrom": '',
					"addTo": 'SHORTLIST',
					"userIP": '',
					"userAgent": '',
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion,
					"latitude": latitude,
					"longitude": longtitude
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/add-remove-shortlist-or-final5',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('favouritesLoader').style.display = 'none';
					$scope.returnFlag = response.returnFlag;
					if (response.returnFlag == 'SUCCESS') {


						var searchCategoryCode = "";
						if (!isBlankOrNull(localStorage.getItem("recentSearchCriteria"))) {
							var json = JSON.parse(localStorage.getItem("recentSearchCriteria"));
							searchCategoryCode = json.searchCategoryCode;
						}
						if (!isBlankOrNull(searchCategoryCode) && !isBlankOrNull(favSelectedCourseIds)) {
							favSelectedCourseIdsArray = favSelectedCourseIds.split(",");
							angular.forEach(favSelectedCourseIdsArray, function (value, key) {
								console.log("Key - " + key + " value - " + value);
								if (!isBlankOrNull(value))
									xtremePushTag("Category_Code", searchCategoryCode + "&SL - " + value);
							});
						}
						if (!isBlankOrNull(searchCategoryCode) && !isBlankOrNull(favSelectedInstitutionIds)) {
							favSelectedInstitutionIdsArray = favSelectedInstitutionIds.split(",");
							angular.forEach(favSelectedInstitutionIdsArray, function (value, key) {
								console.log("Key - " + key + " value - " + value);
								if (!isBlankOrNull(value))
									xtremePushTag("Category_Code", searchCategoryCode + "&SL - " + value);
							});
						}

						angular.forEach(favSelectedInstitutionNames, function (value, key) {

							var university_name = value;
							var actionLabel = value;
							var course_name = favSelectedCourseNames[key];
							if (typeof course_name !== typeof undefined && course_name !== "") {
								actionLabel += " | " + course_name;
							}
							//googleTagManagerEventTrack(null, 'Icon', 'Add Favourite', actionLabel, '0');

							var xtremePushUniversityName = university_name.replace(/\w+/g, function (txt) {
								return txt.charAt(0).toUpperCase() + txt.substr(1);
							}).replace(/\s/g, '_');
							xtremePushTag("Shortlist_Button_Pressed", xtremePushUniversityName);
						});
						$scope.getSuggestionList();
						favSelectedInstitutionNames = [];
						favSelectedCourseNames = [];

						firebaseCourseIds = [];
						firebaseInstitutionIds = [];

						favSelectedCourseIdsArray = [];
						favSelectedInstitutionIdsArray = [];
					} else if (response.returnFlag == "SHORTLIST_LIMIT_EXCEED") {
						$("#tabToAddFinal5").html("shortlist is too full");
						$('#addToFinal5Tab').show();
						if ((window.screen.height * window.devicePixelRatio) == 2436 && Framework7.prototype.device.ios) {
							$('#favFooterDiv').css('bottom', '0');
						}
					} else if (response.returnFlag == "FINAL5_LIMIT_EXCEED") {
						$("#tabToAddFinal5").html("You can only pick 5 choices");
						$('#addToFinal5Tab').show();
						if ((window.screen.height * window.devicePixelRatio) == 2436 && Framework7.prototype.device.ios) {
							$('#favFooterDiv').css('bottom', '0');
						}
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//Below code for Add to Final 5
			var finalSelectedCourseIds = '';
			var finalSelectedInstitutionIds = '';
			var finalBasketContentIds = Array();
			var finalSelectedUniversityName = Array();
			$scope.selectshortLists = function (index, final5ListLength) {
				if ($('#shortlists-' + index).hasClass('slct')) {
					$('#shortlists-' + index).removeClass('slct');
					if (!$('.swipeout').hasClass('slct')) {
						$('.add_fav').removeClass('rev');
						$('#addToFinal5').hide();
						if ((window.screen.height * window.devicePixelRatio) == 2436 && Framework7.prototype.device.ios) {
							$('#favFooterDiv').css('bottom', '0');
						}
					}
					if (finalSelectedUniversityName.length > 0) {
						var university_name = $("#shortlists-" + index).attr("university_name");
						var finalSelectedUniversityNameindex = finalSelectedUniversityName.indexOf(university_name);
						finalSelectedUniversityName.splice(finalSelectedUniversityNameindex, 1);
						//finalSelectedUniversityName.push(university_name);
					}
					if (finalBasketContentIds.length > 0) {
						var basketContentId = $$D('basketContentId-' + index) ? $$D('basketContentId-' + index).value : '';
						var finalBasketContentIdsindex = finalBasketContentIds.indexOf(basketContentId);
						finalBasketContentIds.splice(finalBasketContentIdsindex, 1);
					}
				} else {
					$('#shortlists-' + index).addClass('slct');
					$('.add_fav').addClass('rev');
					$('#addToFinal5').show();
					if ((window.screen.height * window.devicePixelRatio) == 2436 && Framework7.prototype.device.ios) {
						$('#favFooterDiv').css('bottom', 'unset');
					}
					var university_name = $("#shortlists-" + index).attr("university_name");
					finalSelectedUniversityName.push(university_name);
					var basketContentId = $$D('basketContentId-' + index).value;
					finalBasketContentIds.push(basketContentId);
				}
				//
				if (!isBlankOrNull(final5ListLength) && parseInt(final5ListLength) >= 5) {
					$('.add_fav').removeClass('rev');
					$('#addToFinal5').hide();
					$("#tabToAddFinal5").html("You can only pick 5 choices");
					$('#addToFinal5Tab').show();
					if ((window.screen.height * window.devicePixelRatio) == 2436 && Framework7.prototype.device.ios) {
						$('#favFooterDiv').css('bottom', '0');
					}
				}

				if (!isBlankOrNull(finalBasketContentIds)) {
					var length = (!isBlankOrNull(final5ListLength) ? parseInt(final5ListLength) : 0) + parseInt(finalBasketContentIds.length);
					if (length > 5) {
						$("#tabToAddFinal5").html("You can only pick 5 choices");
						$('#addToFinal5Tab').show();

						$('#shortlists-' + index).removeClass('slct');
						var basketContentId = $$D('basketContentId-' + index) ? $$D('basketContentId-' + index).value : '';
						var finalBasketContentIdsindex = finalBasketContentIds.indexOf(basketContentId);
						finalBasketContentIds.splice(finalBasketContentIdsindex, 1);
						finalSelectedUniversityName.splice(finalBasketContentIdsindex, 1);
					} else {
						if (!isBlankOrNull(final5ListLength) && (parseInt(final5ListLength) > 0 && parseInt(final5ListLength) <= 1)) {
							$("#tabToAddFinal5").html("Tap an option to add to Final 5");
							$('#addToFinal5').show();
						} else if (!isBlankOrNull(final5ListLength) && parseInt(final5ListLength) > 1) {
							$("#tabToAddFinal5").html("Drag to reorder");
							$('#addToFinal5').show();
						} else if (!isBlankOrNull(finalBasketContentIds) && parseInt(finalBasketContentIds) > 0) {
							$("#tabToAddFinal5").html("Select your Final 5 choices");
						}
					}
				} else if (isBlankOrNull(finalBasketContentIds)) {
					if (isBlankOrNull(final5ListLength)) {
						$("#tabToAddFinal5").html("Tap an option to add to Final 5");
					}
				}
				//			
			}
			//Below code for Close the Tab to add final 5 alert box
			$scope.closefinal5Tab = function () {
				$("#finalFiveParentDiv").addClass("sb-hide");
				if ($$D('addToFinal5Tab')) {
					$('#addToFinal5Tab').hide();
				}
			}
			//Below code for Add courses/universities to Final 5
			$scope.addToFinal5 = function () {
				var finalBasketContentIdsString = finalBasketContentIds.join(",");
				console.log("----> " + finalBasketContentIdsString);
				$$D('favouritesLoader').style.display = 'block';
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"institutionIds": '',
					"courseIds": '',
					"basketContentIds": finalBasketContentIdsString,
					"removeFrom": '',
					"addTo": 'FINAL5',
					"userIP": '',
					"userAgent": '',
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion,
					"latitude": latitude,
					"longitude": longtitude
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/add-remove-shortlist-or-final5',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('favouritesLoader').style.display = 'none';
					finalBasketContentIds = [];
					$('.swipeout').removeClass('slct')
					$scope.returnFlag = response.returnFlag;
					if (response.returnFlag == 'SUCCESS') {
						$scope.removeFlag = 'N';
						$scope.getSuggestionList();
						var actionLabel = finalSelectedUniversityName.join(" | ");
						//googleTagManagerEventTrack(null, 'Final 5', 'Confirm Final 5', actionLabel, '0');
						//
						var count = parseInt(localStorage.final5count);
						angular.forEach(finalSelectedUniversityName, function (value, key) {
							var university_name = value;
							var xtremePushUniversityName = university_name.replace(/\w+/g, function (txt) {
								return txt.charAt(0).toUpperCase() + txt.substr(1);
							}).replace(/\s/g, '_');
							var tagKey = "Final_5_Button" + (count + 1) + "_Pressed";
							xtremePushTag(tagKey, xtremePushUniversityName);
							count = count + 1;
						});
						localStorage.final5count = parseInt(localStorage.final5count) + parseInt(finalSelectedUniversityName.length);
						if (localStorage.final5count == 5) {
							var tagKey = "Complete_Final_5_Pressed";
							var tagValue = xtremePushCurrentDate();
							xtremePushTag(tagKey, tagValue);
						}
						//
						finalSelectedUniversityName = Array();
					} else if (response.returnFlag == "FINAL5_LIMIT_EXCEED") {
						$("#tabToAddFinal5").html("You can only pick 5 choices");
						$('#addToFinal5Tab').show();
						$("#finalFiveParentDiv").removeClass("sb-hide");
						if ((window.screen.height * window.devicePixelRatio) == 2436 && Framework7.prototype.device.ios) {
							$('#favFooterDiv').css('bottom', '0');
						}
					} else if (response.returnFlag == "SHORTLIST_LIMIT_EXCEED") {
						$("#tabToAddFinal5").html("shortlist is too full");
						$('#addToFinal5Tab').show();
						$("#finalFiveParentDiv").removeClass("sb-hide");
						if ((window.screen.height * window.devicePixelRatio) == 2436 && Framework7.prototype.device.ios) {
							$('#favFooterDiv').css('bottom', '0');
						}
					}
					$('#addToFavourites').hide();
					$('#addToFinal5').hide();
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//Below code for remove courses/universities from shortlists
			$scope.removeFromFavourites = function (index, position, flag) {
				$$D('favouritesLoader').style.display = 'block';
				var courseId = '';
				var institutionId = '';
				var removeFrom = 'SHORTLIST';
				if (!isBlankOrNull(flag) && flag == "FAVOURITES") {
					if ($$D("finalCourseId-" + index) && !isBlankOrNull($$D("finalCourseId-" + index).value)) {
						courseId = $$D("finalCourseId-" + index).value;
					} else {
						if ($$D("finalInstitutionId-" + index) && !isBlankOrNull($$D("finalInstitutionId-" + index).value)) {
							institutionId = $$D("finalInstitutionId-" + index).value;
						}
					}
					$('#addToFinal5').hide();
				} else if (!isBlankOrNull(flag) && flag == "FINAL5") {
					if ($$D("reorderCourseId-" + index) && !isBlankOrNull($$D("reorderCourseId-" + index).value)) {
						courseId = $$D("reorderCourseId-" + index).value;
					} else {
						if ($$D("reorderInstitutionId-" + index) && !isBlankOrNull($$D("reorderInstitutionId-" + index).value)) {
							institutionId = $$D("reorderInstitutionId-" + index).value;
						}
					}
				}
				if (!isBlankOrNull(position) && parseInt(position) > 0) {
					removeFrom = 'BOTH';
				}
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"institutionIds": institutionId,
					"courseIds": courseId,
					"basketContentIds": '',
					"removeFrom": removeFrom,
					"addTo": '',
					"userIP": '',
					"userAgent": '',
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion,
					"latitude": latitude,
					"longitude": longtitude
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/add-remove-shortlist-or-final5',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('favouritesLoader').style.display = 'none';
					finalBasketContentIds = [];
					$scope.returnFlag = response.returnFlag;
					if (response.returnFlag == 'SUCCESS') {
						$scope.removeFlag = 'Y';
						//
						/*var count = 0;
						$("#sortable-1 li").each(function() {
							var university_name = $(this).attr("university_name");
							var xtremePushUniversityName = university_name.replace(/\w+/g, function(txt) {
							  return txt.charAt(0).toUpperCase() + txt.substr(1);
							}).replace(/\s/g, '_');
							var tagKey = "Final_5_Button"+(count+1)+"_Pressed";
							xtremePushTag(tagKey,xtremePushUniversityName);
							count = count + 1;
						});	
						for(i=count+1;i<=parseInt(localStorage.final5count);i++) {
							var tagKey = "Final_5_Button"+(i)+"_Pressed";
							xtremePushTag(tagKey,"Empty");
						}
						localStorage.final5count = count;*/
						//
						$scope.getSuggestionList();
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			$scope.xtremePushTracking = function () {
				var count = 0;
				$("ul#sortable-1 li").each(function () {
					var university_name = $(this).attr("university_name");
					var xtremePushUniversityName = university_name.replace(/\w+/g, function (txt) {
						return txt.charAt(0).toUpperCase() + txt.substr(1);
					}).replace(/\s/g, '_');
					var tagKey = "Final_5_Button" + (count + 1) + "_Pressed";
					xtremePushTag(tagKey, xtremePushUniversityName);
					count = count + 1;
				});
				for (var i = count; i <= parseInt(localStorage.final5count); i++) {
					var tagKey = "Final_5_Button" + (i + 1) + "_Pressed";
					xtremePushTag(tagKey, "NA");
				}
				localStorage.final5count = count;
				$scope.removeFlag = 'N';
			}
			//Below code for remove courses/Universities from Final 5
			$scope.removeFromFinalFive = function (index) {
				$$D('favouritesLoader').style.display = 'block';
				var courseId = '';
				var institutionId = '';
				if ($$D("reorderCourseId-" + index) && !isBlankOrNull($$D("reorderCourseId-" + index).value)) {
					courseId = $$D("reorderCourseId-" + index).value;
				} else {
					if ($$D("reorderInstitutionId-" + index) && !isBlankOrNull($$D("reorderInstitutionId-" + index).value)) {
						institutionId = $$D("reorderInstitutionId-" + index).value;
					}
				}
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"institutionIds": institutionId,
					"courseIds": courseId,
					"basketContentIds": '',
					"removeFrom": 'FINAL5',
					"addTo": '',
					"userIP": '',
					"userAgent": '',
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion,
					"latitude": latitude,
					"longitude": longtitude
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/add-remove-shortlist-or-final5',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('favouritesLoader').style.display = 'none';
					finalBasketContentIds = [];
					$scope.returnFlag = response.returnFlag;
					if (response.returnFlag == 'SUCCESS') {
						//
						$scope.removeFlag = 'Y';
						//
						$scope.getSuggestionList();
					} else if (response.returnFlag == "SHORTLIST_LIMIT_EXCEED") {
						$("#dragToReorder").html("shortlist is too full");
						$('#dragToReorderTab').show();
					} else if (response.returnFlag == "FINAL5_LIMIT_EXCEED") {
						$("#dragToReorder").html("You can only pick 5 choices");
						$('#dragToReorderTab').show();
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			//Below code for reorder final 5 choices
			$scope.reorderFinalFive = function (finalChoiceIds) {
				$$D('favouritesLoader').style.display = 'block';
				var stringJson = {
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"finalChoiceIds": finalChoiceIds,
					"accessToken": "246",
					"affiliateId": "220703"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/reorder-choices',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('favouritesLoader').style.display = 'none';
					finalBasketContentIds = [];
					$scope.returnFlag = response.reorderReturnFlag;
					if (response.reorderReturnFlag == 'SUCCESS') {
						//
						var count = 0;
						$("ul#sortable-1 li").each(function () {
							var university_name = $(this).attr("university_name");
							var xtremePushUniversityName = university_name.replace(/\w+/g, function (txt) {
								return txt.charAt(0).toUpperCase() + txt.substr(1);
							}).replace(/\s/g, '_');
							var tagKey = "Final_5_Button" + (count + 1) + "_Pressed";
							xtremePushTag(tagKey, xtremePushUniversityName);
							count = count + 1;
						});
						//
						$scope.getSuggestionList();
					} else if (response.reorderReturnFlag == "FINAL5_LIMIT_EXCEED") {
						$("#tabToAddFinal5").html("You can only pick 5 choices");
						$('#tabToAddFinal5').show();
					} else if (response.reorderReturnFlag == "SHORTLIST_LIMIT_EXCEED") {
						$("#tabToAddFinal5").html("shortlist is too full");
						$('#tabToAddFinal5').show();
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
		})

		.controller("opendayproviderlandingCtrl", function ($scope, $http, $sce, $element, $timeout) {
			//	
			$scope.swiperSlider = [];
			$scope.university = {};
			$scope.currentEventDetails = {};
			$scope.gallerySection = [];
			$scope.additionalResource = [];
			$scope.currentSlideIndex = 0;
			$scope.institutionBrowseLogo = Constants.institutionBrowseLogo;
			$scope.currentEventItemId = "";
			

			
			$scope.qualificationDetails = [];
			var pulltorefreh = "";

			$scope.getSafeHtml = function (x) {
				return $sce.trustAsHtml(x);
			};
			$scope.getOpenDayDetails = function (eventId, arg) {

				//
				var stringJson = {
					"accessToken": "246",
					"affiliateId": "220703",
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"appVersion": Constants.appVersion,
					"collegeId": localStorage.getItem("openday_college_id"),
					"eventId": eventId //"58643"
				}

				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/open-day/provider-landing/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				var scrolling_array = [0, 0];
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					
					firebaseEventTrack('screenview', { 'page_name': '/openday-provider-landing/' + localStorage.getItem("openday_college_id") });
					//

					if (response.opendayList.length > 0) {
						$scope.currentEventDetails = response.opendayList[0];
						$scope.currentEventItemId = $scope.currentEventDetails.eventItemId;

						$scope.bookingFormFlag = $scope.currentEventDetails.bookingFormFlag;
						$scope.bookingUrl = "";
						if ($scope.bookingFormFlag === "N") {
							$scope.bookingUrl = $scope.currentEventDetails.bookingUrl;
						}
						//localStorage.setItem('openday_event_id', $scope.currentEventItemId)
					}
					$scope.swiperSlider = ["OPEN DAY INFO"];

					console.log("loader hide and show");

					$$D('loaderOpendayProviderLanding').style.display = 'none';
					$$D('loaderOpendayProviderView').style.display = 'block';

					if (response.uniDetails.length > 0) {
						$scope.university = response.uniDetails[0];
					}

					//localStorage.setItem('openday_event_id', $scope.currentEventItemId);
					
					
					if (response.uniAddress.length > 0) {
						$scope.uniAddress = response.uniAddress[0];
					}

					$scope.gallerySection = response.gallerySection;
					$scope.testimonialSection = response.testimonialSection;
					$scope.contentSection = response.contentSection;
					$scope.additionalResource = response.additionalResource;

					$scope.opendayList = [];
					if ("moreOpendayFlag" in $scope.currentEventDetails) {
						if ($scope.currentEventDetails.moreOpendayFlag === "Y") {
							$scope.getMoreOpenDays($scope.currentEventDetails.totalOpendayCount);
						}
					}
					$scope.mapBoxKey = response.mapBoxKey;
					$scope.photos = [];
					var height = $(document).height();
					if ($scope.gallerySection.length > 0) {
						angular.forEach($scope.gallerySection, function (photoValue, photoKey) {
							if (photoValue.mediaPath != null) {
								console.log('mediaType > ' + photoValue.mediaType);
								//var url = "https://mtest.whatuni.com/commimg/myhotcourses/appinst/myhc_233912_type1.jpg"
								if (photoValue.mediaType == 'IMAGE') {
									$scope.photos.push('<img id="img_' + photoKey + '" src="' + photoValue.mediaPath + '" data-rjs="3" />'); //photoValue.mediaPath
								} else {
									$scope.photos.push('<video id="opendayvideo_' + photoKey + '" src="' + photoValue.mediaPath + '" playsinline ad-outlet="video" width="100%" height="' + height + '" poster="' + photoValue.thumbnailPath + '" ></video> <span id="opendayplay_' + photoKey + '" class="provie" onclick="showOpendayVideo(\'' + photoKey + '\');"><i class="fa fa-play-circle-o"></i></span>');
								}
							}
						})
					}

					if (arg != '') {
						myApp.pullToRefreshDone();
					}
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}

			var localEventId = localStorage.getItem('openday_event_id')
			$scope.getOpenDayDetails(localEventId,"");

			//Below code for pull to refresh
			var ptrContent = $$('#opendayproviderlandingCtrl .pull-to-refresh-content');
			ptrContent.on('refresh', function (e) {
				console.log("openday pull to refresh");
				pulltorefreh = 'pulltorefreh';
				$scope.getOpenDayDetails($scope.currentEventItemId, 'pull-to-refresh');
			});


			$scope.universityGallery = function () {
				var myPhotoBrowserStandalone = myApp.photoBrowser({
					photos: $scope.photos,
					theme: 'dark',
					swipeToClose: false
				});
				if ($scope.photos.length > 0) {
					statusBarFontColor('white');
					myPhotoBrowserStandalone.open();
					retinajs();
				} else {
					myApp.alert("No Photos Available")
				}
				//
				applyIconOnPauseOpenday();
				//
			}

			$scope.openPdf = function (url) {
				inAppBrowserPdf(url);
			}

			$scope.googleMap = function () {

				var addressLongLat = $scope.uniAddress.latitude + ',' + $scope.uniAddress.longitude;
			
				var mapOption = '';
				if (Constants.devicePlatform == 'IOS') {
					mapOption = 'clearcache=yes,location=no,hardwareback=no,closebuttoncaption=close,hidenavigationbuttons=yes,toolbarposition=top';
				} else {
					mapOption = 'clearcache=yes,location=yes,hardwareback=no,closebuttoncaption=close,footer=no,hidenavigationbuttons=yes,hideurlbar=yes,toolbarcolor=#333333';
				}
				window.open("http://maps.google.com/?q=" + addressLongLat, '_blank', mapOption);
			}
			$scope.reserveAPlace = function () {
				inAppBrowser($scope.bookingUrl);
				$scope.reservePlaceStats()
			}

			$scope.getMoreOpenDays = function (count) {
				//	
				var stringJson = {
					"accessToken": "246",
					"affiliateId": "220703",
					"collegeId": localStorage.getItem("openday_college_id"),
					"totalOpendayCount": count,
					"bookingFormFlag": ""
				}

				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/more-open-days/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}


				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					console.log("response more open day", response);
					$scope.moreOpenDayList = response.opendayList;


				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}

			$scope.changeOpenDay = function (openday) {
				myApp.closeModal('.popup-openday');
				if ($scope.currentEventItemId !== openday.eventItemId) {
					$$D('loaderOpendayProviderLanding').style.display = 'block';

					//console.log("eventItemId", openday.eventItemId);
					$scope.getOpenDayDetails(openday.eventItemId, "change");

				}

			}
			$('#opendayproviderlandingCtrl #pullContent.ip_header').on('scroll touchmove', function (e) {
				var ptrContent = $$('.pull-to-refresh-content');
				scroll = $(this).scrollTop() + 100;
				if (scroll > 200 && $('#opendayproviderlandingCtrl #stk_app').html() == '' && $('#opendayproviderlandingCtrl .slider-content').height() > 550) {
					statusBarFontColor('black');
					$('#opendayproviderlandingCtrl #stk_app').append($("#opendayproviderlandingCtrl .stk_div"));
					$('#opendayproviderlandingCtrl #stk_app').addClass('stk_fix');
					$('#opendayproviderlandingCtrl .stk_div').addClass('mobsec');
					$('#opendayproviderlandingCtrl #stk_app').hide();
					$('#opendayproviderlandingCtrl #stk_app').slideToggle();
					setTimeout(function () {
						$('#opendayproviderlandingCtrl #stk_app .stk_div').addClass('visible');
					}, 100)
					myApp.destroyPullToRefresh(ptrContent)
					statusBarFontColor('black');
				}
				if (scroll < 200 && $('#opendayproviderlandingCtrl #stk_app').html() != '') {
					$('#opendayproviderlandingCtrl #stk_app').slideToggle();
					$('#opendayproviderlandingCtrl #stk_div').prepend($("#opendayproviderlandingCtrl .stk_div"));
					$('#opendayproviderlandingCtrl #stk_app').removeClass('stk_fix');
					$('#opendayproviderlandingCtrl #stk_div').fadeIn(40);
					$('#opendayproviderlandingCtrl .stk_div').removeClass('mobsec');
					$('#opendayproviderlandingCtrl .stk_div').removeClass('visible');
					setTimeout(function () {
						myApp.initPullToRefresh(ptrContent)
					}, 1000);
					statusBarFontColor('white');
					// console.log('checkAdvUniversity > ' + $scope.checkAdvUniversity);
					// if ($scope.checkAdvUniversity == 'N') {
					// 	statusBarFontColor('white');
					// }
				}
			});

			$scope.reservePlaceStats = function () {
				var userId = localStorage.getItem('LOGGED_IN_USER_ID');
				var collegeId = localStorage.getItem('openday_college_id');
				var userAgent = navigator.userAgent;
				var suborderItemId = null;
				//suborderItemId = $scope.opendayList[0].subOrderItemId;
				var extraText = null;
				extraText = encodeURI($scope.bookingUrl);
				var stringJson = {
					"userId": userId,
					"collegeId": collegeId,
					"courseId": "0",
					"userIp": "",
					"userAgent": navigator.userAgent,
					"jsLog": 'Y',
					"suborderItemId": suborderItemId,
					"profileId": "",
					"typeName": "",
					"activity": "HOTCOURSES: OPEN DAYS: WEBSITE CLICK",
					"extraText": extraText,
					"latitude": latitude,
					"longitude": longtitude,
					"appVersion": Constants.appVersion,
					"affiliateId": "220703",
					"accessToken": "246"
				};
				var jsonData = JSON.stringify(stringJson);
				var req = {
					method: 'POST',
					url: 'https://mtest.whatuni.com/wuappws/interaction/db-stats/logging/',
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					//$$D('loaderCourse').style.display = 'none';
					var logStatusId = response.logStatusId;
				}, function (httpresponse, status) {
					var response = httpresponse.data;
					// do nothing				
				});
			}
		})
		.controller("openDaySearchLandingCtrl", function ($scope, $http) {

			$scope.institutionBrowseLogo = Constants.institutionBrowseLogo;
			$scope.upcomingOpendaysList = [];
			
			$scope.opendayProviderLandingPage = function (openday) {
				localStorage.setItem('openday_college_id', openday.collegeId);
				localStorage.setItem('openday_event_id', openday.eventId);
				
				mainView.router.loadPage('html/openday/openDays.html');
			}
			$scope.opendayAjaxProviderLandingPage = function (openday) {
				if (openday.opendayMsg === null) {
					firebaseEventTrack('open_day_search', { 'search_term':openday.collegeDisplayName, 'page_name': '/open-days-landing' });
					localStorage.setItem('openday_event_id', '');
					localStorage.setItem('openday_college_id', openday.collegeId);
					mainView.router.loadPage('html/openday/openDays.html');
				}
				
			}
			$scope.sliderInit = function (swiperType) {
				console.log("sliderInit");
				if (swiperType === "swiperRegion") {
					setTimeout(function () {
						$scope.swiperSliderRegion = new Swiper('#swiperRegion.swiper-container', {
							centeredSlides: true,
							slidesPerView: 'auto',
							spaceBetween: 10,
							loop: false,
							direction: 'horizontal',
							onSlideChangeStart: function (swiper) {
								console.log("onSlideChangeStart")
							},
							onSlideChangeEnd: function (swiper) {

							}
						});
					}, 10)
				}

				if (swiperType === "swiperEventType") {
					setTimeout(function () {
						$scope.swiperSliderEvent = new Swiper('#swiperEventType.swiper-container', {
							centeredSlides: true,
							slidesPerView: 'auto',
							spaceBetween: 10,
							loop: false,
							direction: 'horizontal',
							onSlideChangeStart: function (swiper) {
								console.log("onSlideChangeStart")
							},
							onSlideChangeEnd: function (swiper) {

							}
						});
					}, 0)
				}

			}

			var locationMode = "";
			var regionName = "";
			var eventTypeName = "";
			var eventCategoryId = "";

			$scope.getOpenDays = function (flag) {
				console.log("getOpenDays ", flag)
				//	
				if(latitude!="") {
					locationMode = "ON";
				} else {
					locationMode = "OFF";
				}
				if (flag == "") {
					
					$scope.regionHeading = 'Open days by location';
					$scope.regionLabel = 'Select region';
				}
				if (flag === "region") {
					
					regionName = $("#opendayByLocation").val();
				}
				if (flag === "event") {
					eventTypeName = $("#opendayByEventType>option:selected").html();
					eventCategoryId = $("#opendayByEventType").val();
				}

				var stringJson = {
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion,
					"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
					"locationMode": locationMode,
					"regionName": regionName,
					"eventTypeName": eventTypeName,
					"eventCategoryId": eventCategoryId,
					"sessionId": sessionStorage.getItem("randomNumber"),
					"userIp": ""
				}


				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/open-day/search-landing/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}

				$$D('loaderOpendaySearchLanding').style.display = 'block';
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$$D('loaderOpendaySearchLanding').style.display = 'none';

					if (flag == "") {
						$$D('loaderOpendaySearchLandingView').style.display = 'block';
						$scope.virtualOpendaysList = response.virtualOpendaysList;
						$scope.upcomingOpendaysList = response.upcomingOpendaysList;

						
						var checkvirtual = obj => obj.eventCategoryId === '6';
						var isvirtual = $scope.virtualOpendaysList.some(checkvirtual);
					
						var checkonline= obj => obj.eventCategoryId === '7';
						var isonline = $scope.virtualOpendaysList.some(checkonline);
				
						var checkphysical = obj => obj.eventCategoryId === '8';
						var isphysical = $scope.virtualOpendaysList.some(checkphysical);
					

						var openDayTypeArray = [];
						if(isvirtual) {
							openDayTypeArray.push("virtual");
						}
						if(isonline) {
							openDayTypeArray.push("online");
						}
						if(isphysical) {
							openDayTypeArray.push("physical");
						}
						var openDayType = openDayTypeArray.join("|")

						firebaseEventTrack('screenview', { 'page_name': '/open-days-landing','open_day_type':openDayType });

					}

					if (flag === "" || flag === "region") {
						$scope.regionOpendaysList = response.regionOpendaysList;
						// var random = Math.floor(Math.random() * Math.floor(2))
						// console.log("random", random);
						// $scope.regionOpendaysList = response.upcomingOpendaysList.filter(function (x, index) {
						// 	return index > random;
						// });
						$scope.regionList = response.regionList;


						if (response.regionName == null) {
							$scope.regionHeading = 'Events by location';
							$scope.regionLabel = 'Select region';
						} else {
							$scope.regionHeading = 'Events in ' + response.regionName;
							$scope.regionLabel = 'Change region';
						}

						var regionSelectedFlagDetails = response.regionList.filter(event => {
							return event.selectedFlag == 'Y'
						});
						$scope.opendayByLocation = (regionSelectedFlagDetails.length > 0) ? regionSelectedFlagDetails[0].locationValue : "";
					}

					if (flag === "" || flag === "event") {

						$scope.eventTypeOpendaysList = response.eventTypeOpendaysList;
						$scope.eventTypeList = response.eventTypeList;

						$scope.eventHeading = response.eventTypeName;
						$scope.eventLabel = 'Change event type';

						var eventSelectedFlagDetails = response.eventTypeList.filter(event => {
							return event.selectedFlag == 'Y'
						});
						$scope.opendayByEventType = (eventSelectedFlagDetails.length > 0) ? eventSelectedFlagDetails[0].eventCategoryId : "";

					}

					setTimeout(function () {
						if (flag == "") {
							$scope.swiperSliderUpcoming = new Swiper('#swiperUpcoming.swiper-container', {
								centeredSlides: true,
								slidesPerView: 'auto',
								spaceBetween: 10
							});
							if ($scope.virtualOpendaysList.length > 0) {
								$scope.swiperSliderVirtual = new Swiper('#swiperVirtual.swiper-container', {
									centeredSlides: true,
									slidesPerView: 'auto',
									spaceBetween: 10
								});
							}
						}
					}, 100)

				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
			$scope.getOpenDays('');

			$scope.opendayChange = function (opendayType) {
				console.log("opendayChange ", opendayType);
				if (opendayType === "region") {
					if($("#opendayByLocation").val()!="") {
						$scope.getOpenDays(opendayType)
					}
				} else {
					$scope.getOpenDays(opendayType)
				}
				
				// if (opendayType === "region") {
				// 	var locationVaule = $("#opendayByLocation").val();
				// 	console.log("change", locationVaule);
				// 	$scope.getOpenDays(locationVaule, opendayType)
				// }
				// if (opendayType === "event") {
				// 	var eventVaule = $("#opendayByEventType").val();
				// 	console.log("change", locationVaule);
				// 	$scope.getOpenDays(eventVaule, opendayType)
				// }

			}
			$scope.clearSearch = function () {
				if ($$D('searchAjaxOpendayLanding')) {
					$$D('searchAjaxOpendayLanding').value = '';
					$$D('opendaySearchLandingXmark').className = '';
					$('#openday-dynamic-unis').css('display', 'none');
				}
			}
			$scope.viewMore = function (opendayType) {
				sessionStorage.setItem('opendayType', opendayType);
				sessionStorage.setItem('opendayValue', '');
				sessionStorage.setItem('opendayName', '');
				if (opendayType === 'region') {
					var region = $('#opendayByLocation').val();
					var name = $("#opendayByLocation>option:selected").html();
					sessionStorage.setItem('opendayValue', region);
					sessionStorage.setItem('opendayName', name);
				}
				if (opendayType === 'event') {
					var region = $('#opendayByEventType').val();
					var name = $("#opendayByEventType>option:selected").html();
					sessionStorage.setItem('opendayValue', region);
					sessionStorage.setItem('opendayName', name);
				}
				mainView.router.loadPage('html/openday/opendaySearchResult.html');

			}
			//
			$scope.goBack = function () {
				//window.history.back();
				mainView.router.back({ 'pushState': false });
			}
			$scope.searchAjaxAutoComplete = function (keyCode) {

				if ($$D('searchAjaxOpendayLanding')) {
					if (($$D('searchAjaxOpendayLanding').value).trim().length > 0) {
						$$D('opendaySearchLandingXmark').className = 'srclear';
						if (keyCode == 13) {
						}
					} else {
						$$D('opendaySearchLandingXmark').className = '';
					}

					if (!isOpendaySearchAjaxNotNull()) {
						$$D('openday-dynamic-unis').style.display = 'none';
					} else if (isOpendaySearchAjaxNotNull()) {
						var searchvalue = $$D('searchAjaxOpendayLanding').value
						$scope.getOpenDaysByAjax(searchvalue);
					}
				}
			}
			$scope.getOpenDaysByAjax = function (keywordText) {
				//	
				var stringJson = {
					"accessToken": "246",
					"affiliateId": "220703",
					"keywordText": keywordText
				}
				$$D('loaderOpendaySearchLanding').style.display = 'block';
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/open-day/search-ajax/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}

				$http(req).then(function (httpresponse) {
					$$D('loaderOpendaySearchLanding').style.display = 'none';
					var response = httpresponse.data;
					console.log("response open day ajax", response);
					//$scope.moreOpenDayList = response.opendayList;
					$$D('openday-dynamic-unis').style.display = 'block';

					$scope.opendayAjaxSearchResult = response.opendaySearchAjaxList;

					if($scope.opendayAjaxSearchResult.length==0) {
						setTimeout(function(){
							$$D('openday-dynamic-unis').style.display = 'none';
						},100);
					}


				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}
		})
		.directive('repeatSliderDone', [function () {
			return {
				restrict: 'A',
				scope: {
					sliderScope: '=repeatSliderDone'
				},
				link: function (scope, element, iAttrs) {
					if (scope.$parent.$last) {
						setTimeout(function () {
							console.log("swiper slider update");

							if (typeof scope.sliderScope !== "undefined") {
								scope.sliderScope.update();
							}

						}, 200)
					}
				}
			};
		}])
		.controller("opendaySearchResultCtrl", function ($scope, $http) {

			$scope.institutionBrowseLogo = Constants.institutionBrowseLogo;
			$scope.monthView = 'N';
			$scope.regionView = 'N';
			$scope.eventView = 'N';
			$scope.virtualView = 'N';
			var opendayType = sessionStorage.getItem('opendayType');
			var opendayValue = sessionStorage.getItem('opendayValue');
			var opendayName = sessionStorage.getItem('opendayName');
			var fromPage = 1;
			var toPage = 6;
			$scope.opendaySearchResult =[];

			var locationMode = "";
			var regionName = "";
			var monthValue = "";
			var monthName = "";
			var eventTypeName = "";
			var eventCategoryId = "";
			var pageName = "";

			if (opendayType === 'virtual') {
				pageName = "VIRTUAL_SEARCH";
			}
			if (opendayType === 'month') {
				pageName = "MONTH_YEAR_SEARCH";
			}
			if (opendayType === 'region') {
				pageName = "LOCATION_SEARCH";
			}
			if (opendayType === 'event') {
				pageName = "EVENT_TYPE_SEARCH";
			}

			$scope.opendayProviderLandingPage = function (openday) {
				localStorage.setItem('openday_college_id', openday.collegeId);
				localStorage.setItem('openday_event_id', openday.eventId);
				mainView.router.loadPage('html/openday/openDays.html');
			}
			$scope.opendayAjaxProviderLandingPage = function (openday) {
				if (openday.opendayMsg === null) {
					if (opendayType === "region") {
						firebaseEventTrack('open_day_search', { 'search_term':openday.collegeDisplayName, 'page_name': '/open-day-regional-landing' });
					}
					if (opendayType === "month" || opendayType === "virtual") {
						firebaseEventTrack('open_day_search', { 'search_term':openday.collegeDisplayName, 'page_name': '/open-day-all-dates' });
					}

					localStorage.setItem('openday_college_id', openday.collegeId);
					localStorage.setItem('openday_event_id', '');
					mainView.router.loadPage('html/openday/openDays.html');
				}
			}

			$scope.getOpenDayResult = function (flag,value, name,fromPage,toPage) {
				//	
				if (opendayType === "region") {
					locationMode = value;
					if(flag=="")
						firebaseEventTrack('screenview', { 'page_name': '/open-day-regional-landing' });

				}
				if (opendayType === "month" || opendayType === "virtual") {
					monthValue = value;
					monthName = name;
					if(flag=="" && opendayType === "month")
						firebaseEventTrack('screenview', { 'page_name': '/open-day-all-dates' });
				}
				if (opendayType === "event") {
					eventTypeName = name;
					eventCategoryId = value;
					//firebaseEventTrack('screenview', { 'page_name': '/open_day_all_dates' });
				}

				var stringJson = {
					"accessToken": "246",
					"affiliateId": "220703",
					"appVersion": Constants.appVersion,
					"pageName": pageName,
					"location": locationMode,
					"monthValue": monthValue,
					"monthName": monthName,
					"eventTypeName": eventTypeName,
					"eventCategoryId": eventCategoryId,
					"sessionId": sessionStorage.getItem("randomNumber"),
					"fromPageNo":fromPage,
					"toPageNo":toPage
				}


				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/open-day/search-results/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}

				$('#loaderOpendaySearchResult').css('display', 'block');
				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					$('#loaderOpendaySearchResult').css('display', 'none');
					$('#loaderOpendaySearchResultView').css('display', 'block');


					if (opendayType === 'month') {
						$scope.monthView = 'Y';
					}
					if (opendayType === 'virtual') {
						$scope.virtualView = 'Y';
					}
					if (opendayType === 'region') {
						$scope.regionView = 'Y';
					}
					if (opendayType === 'event') {
						$scope.eventView = 'Y';
					}

					console.log("response open day result", response);
					if(fromPage==1) {
						$scope.opendaySearchResult = [];
					}
					$scope.tempOpendaySearchResult = response.viewAllOpendaysList;

					angular.forEach($scope.tempOpendaySearchResult, function (value, index) {
						$scope.opendaySearchResult.push(value);
					});


					//$scope.opendaySearchResult = response.viewAllOpendaysList;
					

					if(flag=="") {
						$scope.eventList = (response.eventList != null) ? response.eventList : [];
					}
					$scope.eventListSelect = (response.eventList != null) ? response.eventList : [];
					if ($scope.eventList.length > 0) {
						var eventListDetails = $scope.eventListSelect.filter(event => {
							return event.selectedFlag == "Y";
						})
						$scope.opendayResultByEvent = (eventListDetails.length > 0) ? eventListDetails[0].monthValue : "";

						$scope.virtualHeading = $scope.opendayResultByEvent;
						if ($scope.opendayResultByEvent !== "" && eventListDetails[0].monthName!=null) {
							$scope.virtualHeading = "Events in "+eventListDetails[0].monthName;
						}
					}

					$scope.opendayMonth = response.monthName;
					if(flag=="") {
						$scope.opendayMonthList = response.opendayMonthList;
					}
					$scope.opendayMonthListSelect = response.opendayMonthList;
					if ($scope.opendayMonthListSelect.length > 0) {
						var monthListDetails = $scope.opendayMonthListSelect.filter(month => {
							return month.selectedFlag == "Y";
						})
						$scope.opendayResultByMonth = (monthListDetails.length > 0) ? monthListDetails[0].monthValue : "";
					}

					$scope.opendayRegion = response.location;
					$scope.opendayRegionList = response.opendayRegionList;
					if ($scope.opendayRegionList.length > 0) {
						var eventRegionListDetails = $scope.opendayRegionList.filter(event => {
							return event.selectedFlag == "Y";
						})
						$scope.opendayResultByRegion = (eventRegionListDetails.length > 0) ? eventRegionListDetails[0].locationValue : "";
					}

					$scope.eventTypeName = response.eventTypeName;
					$scope.eventTypeList = response.eventTypeList;

					if ($scope.eventTypeList.length > 0) {
						var eventTypeListDetails = $scope.eventTypeList.filter(event => {
							return event.selectedFlag == "Y";
						})
						$scope.opendayResultByEventType = (eventTypeListDetails.length > 0) ? eventTypeListDetails[0].eventCategoryId : "";
					}


				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}

			$scope.getOpenDayResult("",opendayValue, opendayName,fromPage,toPage);
			//
			//
			$scope.opendayViewMoreResult = function () {
				console.log("opendayViewMoreResult");
				var type = opendayType;
				fromPage = toPage + 1;
				toPage = toPage + 10;
				var nextFromPage = fromPage;
				var nextToPage = toPage;
				if (type === 'virtual') {
					var name = $("#opendayResultByEvent>option:selected").html();
					var value = $("#opendayResultByEvent").val();
					$scope.getOpenDayResult("ajax",value, name,nextFromPage,nextToPage);
				}
				if (type === 'month') {
					var name = $("#opendayResultByMonth>option:selected").html();
					var value = $("#opendayResultByMonth").val();
					$scope.getOpenDayResult("ajax",value, name,nextFromPage,nextToPage);
				}
				if (type === 'region') {
					var name = $("#opendayResultByRegion>option:selected").html();
					var value = $("#opendayResultByRegion").val();
					$scope.getOpenDayResult("ajax",value, name,nextFromPage,nextToPage);
				}
				if (type === 'event') {
					var name = $("#opendayResultByEventType>option:selected").html();
					var value = $("#opendayResultByEventType").val();
					$scope.getOpenDayResult("ajax",value, name,nextFromPage,nextToPage);
				}
				
			}
			//
			$scope.opendayResultMonthChange = function (type) {
				var nextFromPage = 1;
				var nextToPage = 6;
				if (type === 'event') {
					var name = $("#opendayResultByEvent>option:selected").html();
					var value = $("#opendayResultByEvent").val();
					$scope.getOpenDayResult("ajax",value, name,nextFromPage,nextToPage);
				}
				if (type === 'month') {
					var name = $("#opendayResultByMonth>option:selected").html();
					var value = $("#opendayResultByMonth").val();
					$scope.getOpenDayResult("ajax",value, name,nextFromPage,nextToPage);
				}
				if (type === 'region') {
					var name = $("#opendayResultByRegion>option:selected").html();
					var value = $("#opendayResultByRegion").val();
					$scope.getOpenDayResult("ajax",value, name,nextFromPage,nextToPage);
				}
				if (type === 'eventType') {
					var name = $("#opendayResultByEventType>option:selected").html();
					var value = $("#opendayResultByEventType").val();
					$scope.getOpenDayResult("ajax",value, name,nextFromPage,nextToPage);
				}
			}

			//
			$scope.goBack = function () {
				//window.history.back();
				mainView.router.back({ 'pushState': false });
			}

			$scope.clearSearch = function () {
				if ($$D('searchAjaxOpendayResult')) {
					$$D('searchAjaxOpendayResult').value = '';
					$$D('opendaySearchResultXmark').className = '';
					$('#opendayresult-dynamic-unis').css('display', 'none');
				}
			}
			$scope.searchAjaxAutoComplete = function (keyCode) {

				if ($$D('searchAjaxOpendayResult')) {
					if (($$D('searchAjaxOpendayResult').value).trim().length > 0) {
						$$D('opendaySearchResultXmark').className = 'srclear';
						if (keyCode == 13) {
						}
					} else {
						$$D('opendaySearchResultXmark').className = '';
					}

					if (!isOpendaySearchResultAjaxNotNull()) {
						$$D('opendayresult-dynamic-unis').style.display = 'none';
					} else if (isOpendaySearchResultAjaxNotNull()) {
						var searchvalue = $$D('searchAjaxOpendayResult').value
						$scope.getOpenDaysByAjax(searchvalue);
					}
				}
			}
			$scope.getOpenDaysByAjax = function (keywordText) {
				//	
				var stringJson = {
					"accessToken": "246",
					"affiliateId": "220703",
					"keywordText": keywordText
				}

			
				$$D('loaderOpendaySearchResult').style.display = 'block';
				var jsonData = JSON.stringify(stringJson);
				var url = "https://mtest.whatuni.com/wuappws/open-day/search-ajax/";
				var req = {
					method: 'POST',
					url: url,
					headers: { 'Content-Type': 'application/json' },
					data: jsonData
				}

				$http(req).then(function (httpresponse) {
					var response = httpresponse.data;
					console.log("response open day ajax", response);
					//$scope.moreOpenDayList = response.opendayList;
					$$D('loaderOpendaySearchResult').style.display = 'none';
					$$D('opendayresult-dynamic-unis').style.display = 'block';

					$scope.opendayAjaxSearchResult = response.opendaySearchAjaxList;
					if($scope.opendayAjaxSearchResult.length==0) {
						setTimeout(function(){
							$$D('opendayresult-dynamic-unis').style.display = 'none';
						},100)
						
					}


				}, function (httpresponse, status) {
					var response = httpresponse.data;
					localStorage.setItem('exception', response.exception);
					localStorage.setItem('status', response.status);
					localStorage.setItem('message', response.message);
					mainView.router.loadPage('html/exception/exception.html');
				});
			}

		})
}

//
function checkVersionDetails(versionName, versionStatus, versionMessage, exceptionTemplateURL){
	if(!isBlankOrNull(versionName)) {
		if(parseFloat(versionName) > parseFloat(Constants.appVersion) && sessionStorage.getItem('VERSION_INFORMED') != 'true') {
			sessionStorage.setItem('VERSION_INFORMED', 'true');
			localStorage.setItem('updateVersion', versionName);
			localStorage.setItem('updateVersionMessage', versionMessage);
			//window.location.replace(exceptionTemplateURL);
			//window.location.href=exceptionTemplateURL;
			mainView.router.loadPage(exceptionTemplateURL);
		}
	}
}
//
function getShortListRootCtrl(institutionId, removeFrom, addTo, $scope, $http, rootCtrl, pageFlag) {
	var uniId = institutionId.split("-")[1];
	var courseIds = "";
	if (rootCtrl == 'courseDetailsRootCtrl') {
		courseIds = !isBlankOrNull(localStorage.getItem('courseId')) ? localStorage.getItem('courseId') : "";
	}
	var searchCategoryCode = "";
	if (!isBlankOrNull(localStorage.getItem("recentSearchCriteria"))) {
		var json = JSON.parse(localStorage.getItem("recentSearchCriteria"));
		searchCategoryCode = json.searchCategoryCode;
	}
	if (!isBlankOrNull(searchCategoryCode) && !isBlankOrNull(uniId)) {
		xtremePushTag("Category_Code", searchCategoryCode + "&SL - " + uniId);
	}
	if (!isBlankOrNull(searchCategoryCode) && !isBlankOrNull(courseIds)) {
		xtremePushTag("Category_Code", searchCategoryCode + "&SL - " + courseIds);
	}
	var stringJson = {
		"institutionIds": uniId,
		"courseIds": courseIds,
		"basketContentIds": "",
		"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
		"removeFrom": removeFrom,
		"addTo": addTo,
		"userIP": "",
		"userAgent": navigator.userAgent,
		"accessToken": "246",
		"affiliateId": "220703",
		"appVersion": Constants.appVersion,
		"latitude": latitude,
		"longitude": longtitude
	};
	var jsonData = JSON.stringify(stringJson);
	var req = {
		method: 'POST',
		url: 'https://mtest.whatuni.com/wuappws/add-remove-shortlist-or-final5',
		headers: { 'Content-Type': 'application/json' },
		data: jsonData
	}
	$http(req).then(function (httpresponse) {
		var response = httpresponse.data;

		var returnFlag = response.returnFlag; $("#listExc_" + rootCtrl).hide();
		if (returnFlag == "SUCCESS") {
			$("#" + institutionId).toggleClass('fav_fill');
			if (removeFrom == "" && addTo == "SHORTLIST") {
				var university_name = $("#" + institutionId).attr("university_name");
				var actionLabel = $("#" + institutionId).attr("university_name");
				var course_name = $("#" + institutionId).attr('course_name');
				console.log(course_name)
				//if($("#"+institutionId).is("course_name")) {
				if (typeof course_name !== typeof undefined && course_name !== false) {
					//actionLabel +=" | "+$("#"+institutionId).attr("course_name");
				}
				//googleTagManagerEventTrack(null, 'Icon', 'Add Favourite', actionLabel, '0');
				var firebaseCourse = 'not set';
				if (courseIds !== "") {
					firebaseCourse = courseIds;
					uniId = localStorage.getItem('collegeId');
				}

				var firebaseSubject = 'not set';
				if (!isBlankOrNull(localStorage.recentSearchCriteria)) {
					var recentSearchCriteria = JSON.parse(localStorage.recentSearchCriteria);
					firebaseSubject = !isBlankOrNull(recentSearchCriteria.keywordSearch) ? recentSearchCriteria.keywordSearch : "not set";
				}

				var interactionPagename = ""
				if(rootCtrl=="browseHomeCntrl") {
					interactionPagename = "/home";
				} else if(rootCtrl=="mySearchRootCtrl") {
					interactionPagename = "/search-results";
				} else if(rootCtrl=="myProviderRootCtrl") {
					interactionPagename = "/provider-results/"+localStorage.getItem('institutionId');
				} else if(rootCtrl=="courseDetailsRootCtrl") {
					interactionPagename = "/course-details/"+ localStorage.getItem('courseId');
				} else if(rootCtrl=="institutionhomeCtrl") {
					if (angular.element($$D('institutionhomeCtrl')).scope().checkAdvUniversity == 'N') {
						interactionPagename = '/institution-profile/non-advertiser/' + localStorage.getItem('universityId');
					} else {
						interactionPagename = '/institution-profile/advertiser/' + localStorage.getItem('universityId');
					}
					
				}
				firebaseEventTrack('addtowishlist', { item_category: actionLabel.toLowerCase(), item_name: firebaseSubject.toLowerCase(), item_location_id: firebaseCourse, item_id: uniId, quantity: 1, interaction_pagename: interactionPagename });
				//
				var xtremePushUniversityName = university_name.replace(/\w+/g, function (txt) {
					return txt.charAt(0).toUpperCase() + txt.substr(1);
				}).replace(/\s/g, '_');
				xtremePushTag("Shortlist_Button_Pressed", xtremePushUniversityName);
			}
			if (pageFlag == 'enquiry') {
				if ($("#" + institutionId).hasClass('fav_fill')) { $("#" + institutionId).html("Added to shortlist"); }
				else { $("#" + institutionId).html("Add to shortlist"); }
			}
		} else if (returnFlag == "SHORTLIST_LIMIT_EXCEED") {
			//$("#listExcMsg_"+rootCtrl).html("Shortlist limit exceeded");  $("#listExc_"+rootCtrl).show();
			Constants.myapp.alert("shortlist is too full");
		} else if (returnFlag == "FINAL5_LIMIT_EXCEED") {
			//$("#listExcMsg_"+rootCtrl).html("Final5 limit exceeded");  $("#listExc_"+rootCtrl).show();
			Constants.myapp.alert("Final5 limit exceeded");
		}
	}, function (httpresponse, status) {
		var response = httpresponse.data;
		localStorage.setItem('exception', response.exception);
		localStorage.setItem('status', response.status);
		localStorage.setItem('message', response.message);
		mainView.router.loadPage('html/exception/exception.html');
	});
}			