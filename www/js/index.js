//
var app = {
	// Application Constructor
	initialize: function () {
		this.bindEvents();
	},
	// Bind Event Listeners
	//
	// Bind any events that are required on startup. Common events are:
	// 'load', 'deviceready', 'offline', and 'online'.
	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},
	// deviceready Event Handler
	//
	// The scope of 'this' is the event. In order to call the 'receivedEvent'
	// function, we must explicitly call 'app.receivedEvent(...);'
	onDeviceReady: function () {
		//app.receivedEvent('deviceready');
	}
};
// 
function isGuestUser() {
	//if(localStorage.getItem('LOGGED_IN_USER_ID') === "4798824") {4813234
	if (localStorage.getItem('GUEST_USER') === "YES") {
		return true;
	} else {
		return false;
	}
}
//
function randomNumber() {
	console.log("randomNumber");
	return Math.floor(1000000000 + Math.random() * 9000000000);
}
//
function $$D(id) { return document.getElementById(id) }
//
function toggleTooltipGeneral() {
	$(".feerw_lt .tool_tip.general_msg").toggleClass("open_hide");
	if ($(".feerw_lt .tool_tip.general_msg").hasClass("open_hide")) {
		var height = parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height').replace("px", "")) + 160;
		$('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height', height + "px");
	} else {
		var height = parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height').replace("px", "")) - 160;
		$('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height', height + "px");
	}
}
function toggleTooltip() {
	$(".feerw_rt .tool_tip.tooltip_msg").toggleClass("open_hide");
	//$$(".swiper-wrapper").toggleClass("pb10")
	//console.log("open_hide >"+ $(this).attr('title'));
	//var offtp = $(".tuition_fees").offset().top - 560;
	//$(window).scrollTop(-160);
	if ($(".feerw_rt .tool_tip.tooltip_msg").hasClass("open_hide")) {
		var height = parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height').replace("px", "")) + 160;
		$('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height', height + "px");
	} else {
		var height = parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height').replace("px", "")) - 160;
		$('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height', height + "px");
	}
}
//
function showFeeDetail() {
	if ($(".feerw_rt .tool_tip.tooltip_msg").hasClass("open_hide")) {
		$(".feerw_rt .tool_tip.tooltip_msg").toggleClass("open_hide");
	}
	if ($(".feerw_lt .tool_tip.general_msg").hasClass("open_hide")) {
		$(".feerw_lt .tool_tip.general_msg").toggleClass("open_hide");
	}
	$('#feeDtlsID').find("option:selected").each(function () {
		$('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height', parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height')) - preMoreInfoHgt + "px");
		var optionValue = $(this).attr("value");
		var optionValueID = $(this).attr("id");
		console.log(optionValue);
		if (optionValue) {
			$(".crsfee").not("#" + optionValue).hide();
			$("#" + optionValue).show();
			angular.element($$D('courseDetailsRootCtrl')).scope().getFeeType(optionValueID);
			var moreInfoHgt = $('#more_info_' + optionValue).height();
			preMoreInfoHgt = moreInfoHgt;
			if (moreInfoHgt > 75) {
				var height = parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height').replace("px", "")) + moreInfoHgt;
				console.log(height);
				$('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height', height + "px");
			}
			console.log(parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height')));
		} else {
			$(".crsfee").hide();
			console.log("else of optionVlue");
		}
	});
}
//
function showEntryReqDetails() {
	$('#entryReqSelectId').find("option:selected").each(function () {
		$('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height', parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height')) - preAddInfoHgt + "px");
		//var optionValue = $(this).attr("value");
		var optionValue = $(this).attr("id");
		console.log(optionValue + $(this).attr("value"));
		if (optionValue) {
			$(".crsreqdiv").not("#div_" + optionValue).hide();
			$("#div_" + optionValue).show();
			var addInfoHgt = $('#add_info_div_' + optionValue).height();
			preAddInfoHgt = addInfoHgt;
			if (addInfoHgt > 15) {
				var height = parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height').replace("px", "")) + addInfoHgt;
				console.log(height);
				$('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height', height + "px");
			}
			console.log(parseInt($('#courseDetailsRootCtrl .gallery-top .swiper-wrapper').css('height')));
			//angular.element($$D('courseDetailsRootCtrl')).scope().getFeeType(optionValueID);
		} else {
			$(".crsreqdiv").hide();
			console.log("else of optionVlue");
		}
	});
}
//
function xtremePushEvent(eventName) {
	console.log('XtremePush log > ' + eventName);
	/*
	XtremePush.register({    
	   appKey: Constants.xtremePushApiKey,
	   debugLogsEnabled: true,
	   pushOpenCallback: "onPushOpened",
	   inappMessagingEnabled: true,
	   inboxEnabled: true,
	   attributionsEnabled: true,
	   ios: {
		   pushPermissionsRequest: true,
		   locationsEnabled: true,
		   locationsPermissionsRequest: true
	   },
	   android: {
		   gcmProjectNumber: Constants.xtremePushGcmProjectNumber,
		   geoEnabled: true,
		   beaconsEnabled: false,
		   locationsPermissionsRequest:false,
		   setIcon: "wuicon"
			   }
	   });
	XtremePush.hitEvent(eventName);
	*/
}
function xtremePushImpression(impressionName) {
	XtremePush.hitImpression(impressionName);
}
function xtremePushTag(tagKey, tagValue) {
	console.log('XtremePush log > ' + tagKey + ' > ' + tagValue);
	/*
	XtremePush.register({    
		appKey: Constants.xtremePushApiKey,
		debugLogsEnabled: true,
		pushOpenCallback: "onPushOpened",
		inappMessagingEnabled: true,
		inboxEnabled: true,
		attributionsEnabled: true,
		ios: {
			pushPermissionsRequest: true,
			locationsEnabled: true,
			locationsPermissionsRequest: true
		},
		android: {
			gcmProjectNumber: Constants.xtremePushGcmProjectNumber,
			geoEnabled: true,
			beaconsEnabled: false,
			locationsPermissionsRequest:false,
			setIcon: "wuicon"
		}
	});
	XtremePush.hitTag(tagKey, tagValue);
	*/
}
//
function xtremePushCurrentDate() {
	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var date = new Date();
	var day = ("0" + date.getDate()).slice(-2);
	var monthIndex = date.getMonth();
	var year = date.getFullYear();
	var dateFormat = day + '-' + monthNames[monthIndex] + '-' + year;
	return dateFormat;
}
//
function addToDeviceCalender(id, title, loc, notes, startDate, endDate) {
	if (id == 'event1') {
		//loc = $$$('loc1').innerHTML;
		//notes = $$$('notes1').innerHTML;
		startDate = new Date(startDate);
		endDate = new Date(endDate);
		options = { calendarName: 'UG event' };
	} else {
		loc = $$$('loc2').innerHTML;
		notes = $$$('notes2').innerHTML;
		startDate = new Date(startDate);
		endDate = new Date(startDate);
		options = { calendarName: 'PG event' };
	}
	//
	//alert(device.platform);
	if (Constants.devicePlatform == 'IOS') {
		// create a calendar (iOS only for now)
		window.plugins.calendar.createCalendar('Openday calender', function () {
		}, function () {
		});
		var calOptions = window.plugins.calendar.getCalendarOptions();
		calOptions.firstReminderMinutes = 1440;
		window.plugins.calendar.createEventWithOptions(title, loc, notes, startDate, endDate, calOptions, function () {
			//$$D('addCalendarPopup').style.display = 'none';
			//$$D('addedCalendarPopup').style.display = 'block';
			//

			XtremePush.register({
				appKey: Constants.xtremePushApiKey,
				debugLogsEnabled: true,
				pushOpenCallback: "onPushOpened",
				inappMessagingEnabled: true,
				inboxEnabled: true,
				attributionsEnabled: true,
				ios: {
					pushPermissionsRequest: true,
					locationsEnabled: true,
					locationsPermissionsRequest: true
				},
				android: {
					gcmProjectNumber: Constants.xtremePushGcmProjectNumber,
					geoEnabled: true,
					beaconsEnabled: false,
					locationsPermissionsRequest: false,
					setIcon: "wuicon"
				}
			});
			var xtremeOpenDayEventName = "OD - " + localStorage.getItem('eventId');
			xtremePushEvent(xtremeOpenDayEventName);
			xtremePushTag("Open_Day_Button_Pressed", localStorage.getItem('universityId'));
			//
			angular.element($$D('institutionhomeCtrl')).scope().saveToDB('OK');
		}, function () {
			angular.element($$D('institutionhomeCtrl')).scope().saveToDB('NOT_ALLOWED');
		});
		//
	} else {
		//
		var calOptions = window.plugins.calendar.getCalendarOptions();
		calOptions.firstReminderMinutes = 1440;

		if (startDate.getHours() == 10 && endDate.getHours() == 10) {
			calOptions.allday = true;
		}

		//window.plugins.calendar.createEventInteractivelyWithOptions(title, loc, notes, startDate, endDate, calOptions, function(){
		window.plugins.calendar.createEventWithOptions(title, loc, notes, startDate, endDate, calOptions, function () {
			XtremePush.register({
				appKey: Constants.xtremePushApiKey,
				debugLogsEnabled: true,
				pushOpenCallback: "onPushOpened",
				inappMessagingEnabled: true,
				inboxEnabled: true,
				attributionsEnabled: true,
				ios: {
					pushPermissionsRequest: true,
					locationsEnabled: true,
					locationsPermissionsRequest: true
				},
				android: {
					gcmProjectNumber: Constants.xtremePushGcmProjectNumber,
					geoEnabled: true,
					beaconsEnabled: false,
					locationsPermissionsRequest: false,
					setIcon: "wuicon"
				}
			});
			var xtremeOpenDayEventName = "OD - " + localStorage.getItem('eventId');
			xtremePushEvent(xtremeOpenDayEventName);
			//xtremePushTag("Open_Day_Button_Pressed",localStorage.getItem('eventId'));
			//
			//alert("success");
			angular.element($$D('institutionhomeCtrl')).scope().saveToDB('OK');
		}, function () {
			//alert("error");
			angular.element($$D('institutionhomeCtrl')).scope().saveToDB('NOT_ALLOWED');
		});
		//
	}
	//
}
function initApp() {
	logout();
	var fbLoginSuccess = function (userData) {
		//alert("alert 1"+JSON.stringify(userData));
		if (userData.status === 'connected') {
			getUserDetails(userData.authResponse.userID);
		}
	}
	//facebookConnectPlugin.browserInit('979472352152802');
	facebookConnectPlugin.login(["public_profile", "email"], fbLoginSuccess, failed);
}
//
function failed(error) {
	//alert("failed"+JSON.stringify(error));
	//alert('Failed loggin');
}
//
function logout() {
	facebookConnectPlugin.logout(function () { /*alert("logged out")*/ });
}
//
function getUserDetails(userId) {
	facebookConnectPlugin.api("/" + userId + "/?fields=id,name,email,first_name,last_name,age_range,gender,locale,picture,cover", ["public_profile", "email"], function (data) {
		localStorage.setItem('userId', data.id);
		localStorage.setItem('email', data.email);
		localStorage.setItem('firstName', data.first_name);
		localStorage.setItem('lastName', data.last_name);
		localStorage.setItem("fbUserName", data.name);
		//localStorage.setItem("fbAgeRange", data.age_range.min);
		//localStorage.setItem("fbUserGender", data.gender);
		//localStorage.setItem("fbUserLocale", data.locale);
		Constants.homeScopeObj.checkUserStatus();
		//Constants.mainViewObj.router.loadPage('html/user/home.html');			
		/*userPicture = data.picture.data.url;
		data.picture.data.is_silhouette;
		
		coverPicture = data.cover.id
					   data.cover.source
					   data.cover.offset_y*/

	}
		, function (error) {
			Constants.myapp.modal({
				//title:'Whatuni',
				text: "An email address is mandatory to use the Whatuni app. This is so we can make sure it's you using the app each time",
				buttons: [
					{
						text: 'OK'
					}
				]
			});
			//alert("User details failed : " + JSON.stringify(error));
		});
}
//
var latitude = "";
var longtitude = "";
var isLocationPermissionDisabled = false;
//
// -- TODO Need to consider dynamic values for these constants


var Constants = {
	appVersion: "3.6",
	sprint: "14",
	environment: "TEST",
	mainViewObj: "",
	homeScopeObj: "",
	myapp: "",
	googleTagManagerTrackId: 'GTM-58FM5GD',
	xtremePushApiKey: "VDMaoyucCWcGPECZs4LJOCDsRphPilU5",
	xtremePushGcmProjectNumber: "655944901056",
	//googleTagManagerTrackId:'GTM-KR3SK5T',
	//
	cdAdvertImg: "img/cd_img.png",
	cdNonAdvertImg: "img/non_ad_tile.png",
	userId: "",
	gtmQuizCategory: "",
	//
	quizContent: "",
	//
	searchTextMaxLength: "3",
	devicePlatform: "",
	deviceVersion: "",
	subjects: ["architecture", "art-and-design", "biology", "business", "chemistry", "criminology", "economics", "education", "engineering", "english", "english-lit", "fashion", "french", "geography", "german", "history", "it", "journalism", "law", "marketing", "maths", "medicine", "midwifery", "music", "photography", "physics", "politics", "psychology", "spanish", "tourism", "veterinary", "zoology", "archaeology", "dentistry", "earth-sciences", "languages", "management", "nursing", "pharmacology", "accounting", "agriculture", "hospitality", "media"],

	institutionBrowseLogo: ['myhc_133416.jpg', 'myhc_133529.jpg', 'myhc_133590.jpg', 'myhc_176534.jpg', 'myhc_176866.jpg', 'myhc_177108.jpg', 'myhc_177447.jpg', 'myhc_177466.jpg', 'myhc_177631.jpg', 'myhc_191897.jpg', 'myhc_210583.jpg', 'myhc_231782.jpg', 'myhc_232977.jpg', 'myhc_233122.jpg', 'myhc_233123.jpg', 'myhc_233125.jpg', 'myhc_233127.jpeg', 'myhc_233133.jpg', 'myhc_233134.jpg', 'myhc_233139.jpg', 'myhc_233141.jpg', 'myhc_233143.jpg', 'myhc_233146.jpg', 'myhc_233170.jpg', 'myhc_233173.jpeg', 'myhc_233179.jpeg', 'myhc_233191.jpg', 'myhc_233202.jpg', 'myhc_233214.jpg', 'myhc_233217.jpeg', 'myhc_233245.jpg', 'myhc_233246.jpg', 'myhc_233249.jpg', 'myhc_233250.jpg', 'myhc_233266.jpg', 'myhc_233267.jpeg', 'myhc_233272.jpg', 'myhc_233307.jpg', 'myhc_233312.jpeg', 'myhc_233317.jpg', 'myhc_233324.jpg', 'myhc_233336.jpg', 'myhc_233337.jpg', 'myhc_233348.jpg', 'myhc_233350.jpg', 'myhc_233354.jpeg', 'myhc_233355.jpg', 'myhc_233357.jpg', 'myhc_233358.jpeg', 'myhc_233359.jpg', 'myhc_233360.jpg', 'myhc_233361.jpg', 'myhc_233362.jpg', 'myhc_233364.jpg', 'myhc_233368.jpg', 'myhc_233369.jpg', 'myhc_233374.jpg', 'myhc_233379.jpg', 'myhc_233380.jpg', 'myhc_233385.jpg', 'myhc_233386.jpg', 'myhc_233387.jpg', 'myhc_233388.jpg', 'myhc_233389.jpg', 'myhc_233390.jpg', 'myhc_233392.jpg', 'myhc_233393.jpg', 'myhc_233394.jpg', 'myhc_233395.jpg', 'myhc_233397.jpg', 'myhc_233398.jpg', 'myhc_233399.jpg', 'myhc_233400.jpg', 'myhc_233402.jpg', 'myhc_233403.JPg', 'myhc_233404.jpeg', 'myhc_233405.jpeg', 'myhc_233408.jpg', 'myhc_233409.jpg', 'myhc_233411.jpg', 'myhc_233412.jpeg', 'myhc_233413.jpg', 'myhc_233414.jpeg', 'myhc_233415.jpg', 'myhc_233416.jpg', 'myhc_233417.jpeg', 'myhc_233418.jpg', 'myhc_233419.jpg', 'myhc_233420.jpg', 'myhc_233421.jpeg', 'myhc_233422.jpg', 'myhc_233423.jpg', 'myhc_233424.jpg', 'myhc_233425.jpg', 'myhc_233426.jpg', 'myhc_233427.jpg', 'myhc_233428.jpg', 'myhc_233429.jpg', 'myhc_233432.jpeg', 'myhc_233434.jpg', 'myhc_233435.jpg', 'myhc_233436.jpg', 'myhc_233437.jpg', 'myhc_233438.jpg', 'myhc_233439.jpg', 'myhc_233441.jpg', 'myhc_233442.jpg', 'myhc_233443.jpg', 'myhc_233444.jpg', 'myhc_233445.jpg', 'myhc_233446.jpg', 'myhc_233447.jpeg', 'myhc_233448.jpg', 'myhc_233449.jpg', 'myhc_233451.jpg', 'myhc_233452.jpg', 'myhc_233453.jpg', 'myhc_233454.jpg', 'myhc_233455.jpg', 'myhc_233456.jpeg', 'myhc_233458.jpg', 'myhc_233459.jpg', 'myhc_233460.jpg', 'myhc_233461.jpg', 'myhc_233462.jpg', 'myhc_233463.jpg', 'myhc_233464.jpg', 'myhc_233466.jpg', 'myhc_233467.jpg', 'myhc_233468.jpg', 'myhc_233469.jpg', 'myhc_233470.jpg', 'myhc_233471.jpeg', 'myhc_233472.jpg', 'myhc_233473.jpg', 'myhc_233475.jpg', 'myhc_233476.jpg', 'myhc_233477.jpg', 'myhc_233479.jpg', 'myhc_233480.jpg', 'myhc_233481.jpg', 'myhc_233482.jpg', 'myhc_233483.JPg', 'myhc_233484.jpg', 'myhc_233485.jpg', 'myhc_233489.jpg', 'myhc_233490.jpeg', 'myhc_233492.jpg', 'myhc_233493.jpg', 'myhc_233494.jpeg', 'myhc_233496.jpg', 'myhc_233497.jpeg', 'myhc_233498.jpg', 'myhc_233499.jpg', 'myhc_233500.jpeg', 'myhc_233501.jpeg', 'myhc_233502.jpeg', 'myhc_233503.jpeg', 'myhc_233504.jpeg', 'myhc_233506.jpg', 'myhc_233508.jpeg', 'myhc_233509.jpg', 'myhc_233510.jpg', 'myhc_233511.jpg', 'myhc_233512.jpg', 'myhc_233514.jpeg', 'myhc_233515.jpg', 'myhc_233516.jpg', 'myhc_233517.jpg', 'myhc_233518.jpg', 'myhc_233519.jpeg', 'myhc_233520.jpg', 'myhc_233521.jpeg', 'myhc_233522.jpg', 'myhc_233523.jpg', 'myhc_233524.jpg', 'myhc_233525.jpg', 'myhc_233526.jpg', 'myhc_233528.jpg', 'myhc_233529.jpeg', 'myhc_233530.jpg', 'myhc_233531.jpg', 'myhc_233532.jpeg', 'myhc_233533.jpg', 'myhc_233534.jpg', 'myhc_233535.jpg', 'myhc_233536.jpg', 'myhc_233537.jpg', 'myhc_233538.jpg', 'myhc_233539.jpg', 'myhc_233540.jpg', 'myhc_233541.jpeg', 'myhc_233542.jpeg', 'myhc_233543.jpg', 'myhc_233544.jpg', 'myhc_233545.jpg', 'myhc_233546.jpg', 'myhc_233547.jpg', 'myhc_233548.jpg', 'myhc_233549.jpg', 'myhc_233550.jpg', 'myhc_233588.jpg', 'myhc_233593.jpg', 'myhc_233733.jpg', 'myhc_233883.jpg', 'myhc_233957.jpg', 'myhc_234177.jpg', 'myhc_235445.jpg', 'myhc_235463.jpg', 'myhc_235533.jpg', 'myhc_235536.jpg', 'myhc_236784.jpg', 'myhc_236991.jpg', 'myhc_237156.jpg', 'myhc_237169.jpg', 'myhc_237754.jpg', 'myhc_237764.jpg', 'myhc_237766.jpg', 'myhc_237768.jpg', 'myhc_237769.jpg', 'myhc_237771.jpg', 'myhc_243657.jpg', 'myhc_243815.jpg', 'myhc_244918.jpg', 'myhc_245061.jpg', 'myhc_245062.jpg', 'myhc_245063.jpg', 'myhc_245064.jpg', 'myhc_245065.jpg', 'myhc_245066.jpg', 'myhc_245067.jpg', 'myhc_245089.jpg', 'myhc_245090.jpg', 'myhc_245097.jpg', 'myhc_245182.jpg', 'myhc_245646.jpg', 'myhc_245649.jpg', 'myhc_245657.jpg', 'myhc_245659.jpg', 'myhc_245663.jpg', 'myhc_247107.jpg', 'myhc_247108.jpg', 'myhc_247109.jpg', 'myhc_247110.jpg', 'myhc_247111.jpg', 'myhc_247112.jpg', 'myhc_247113.jpg', 'myhc_247114.jpg', 'myhc_247115.jpg', 'myhc_247116.jpg', 'myhc_247117.jpg', 'myhc_247118.jpg', 'myhc_247119.jpg', 'myhc_247120.jpg', 'myhc_247121.jpg', 'myhc_247122.jpg', 'myhc_247123.jpg', 'myhc_247124.jpg', 'myhc_247126.jpg', 'myhc_247127.jpg', 'myhc_247128.jpg', 'myhc_247129.jpg', 'myhc_247130.jpg', 'myhc_247131.jpg', 'myhc_247132.jpg', 'myhc_247133.jpg', 'myhc_247134.jpg', 'myhc_247135.jpg', 'myhc_247136.jpg', 'myhc_247137.jpg', 'myhc_247138.jpg', 'myhc_247139.jpg', 'myhc_247140.jpg', 'myhc_247141.jpg', 'myhc_247142.jpg', 'myhc_247143.jpg', 'myhc_247144.jpg', 'myhc_247145.jpg', 'myhc_247146.jpg', 'myhc_247147.jpg', 'myhc_247148.jpg', 'myhc_247149.jpg', 'myhc_247150.jpg', 'myhc_247151.jpg', 'myhc_247152.jpg', 'myhc_247153.jpg', 'myhc_247154.jpg', 'myhc_247155.jpg', 'myhc_247156.jpg', 'myhc_247157.jpg', 'myhc_247158.jpg', 'myhc_247159.jpg', 'myhc_247160.jpg', 'myhc_247161.jpg', 'myhc_247162.jpg', 'myhc_247163.jpg', 'myhc_247164.jpg', 'myhc_247165.jpg', 'myhc_247166.jpg', 'myhc_247167.jpg', 'myhc_247168.jpg', 'myhc_247169.jpg', 'myhc_247170.jpg', 'myhc_247171.jpg', 'myhc_247172.jpg', 'myhc_247173.jpg', 'myhc_247174.jpg', 'myhc_247175.jpg', 'myhc_247176.jpg', 'myhc_247177.jpg', 'myhc_247178.jpg', 'myhc_247179.jpg', 'myhc_247180.jpg', 'myhc_247181.jpg', 'myhc_247182.jpg', 'myhc_247183.jpg', 'myhc_247185.jpg', 'myhc_247186.jpg', 'myhc_247187.jpg', 'myhc_247188.jpg', 'myhc_247189.jpg', 'myhc_247190.jpg', 'myhc_247191.jpg', 'myhc_247192.jpg', 'myhc_247193.jpg', 'myhc_247194.jpg', 'myhc_247195.jpg', 'myhc_247196.jpg', 'myhc_247197.jpg', 'myhc_247198.jpg', 'myhc_247199.jpg', 'myhc_247200.jpg', 'myhc_247201.jpg', 'myhc_247202.jpg', 'myhc_247203.jpg', 'myhc_247204.jpg', 'myhc_247205.jpg', 'myhc_247206.jpg', 'myhc_247207.jpg', 'myhc_247208.jpg', 'myhc_247209.jpg', 'myhc_247210.jpg', 'myhc_247211.jpg', 'myhc_247212.jpg', 'myhc_247213.jpg', 'myhc_247214.jpg', 'myhc_247215.jpg', 'myhc_247216.jpg', 'myhc_247217.jpg', 'myhc_247218.jpg', 'myhc_247219.jpg', 'myhc_247220.jpg', 'myhc_247221.jpg', 'myhc_247222.jpg', 'myhc_247223.jpg', 'myhc_247224.jpg', 'myhc_247225.jpg', 'myhc_247226.jpg', 'myhc_247227.jpg', 'myhc_247228.jpg', 'myhc_247229.jpg', 'myhc_247230.jpg', 'myhc_247231.jpg', 'myhc_247232.jpg', 'myhc_247233.jpg', 'myhc_247234.jpg', 'myhc_247235.jpg', 'myhc_247236.jpg', 'myhc_247237.jpg', 'myhc_247238.jpg', 'myhc_247239.jpg', 'myhc_247240.jpg', 'myhc_247241.jpg', 'myhc_247242.jpg', 'myhc_247243.jpg', 'myhc_247244.jpg', 'myhc_247245.jpg', 'myhc_247246.jpg', 'myhc_247247.jpg', 'myhc_247248.jpg', 'myhc_247249.jpg', 'myhc_247250.jpg', 'myhc_247251.jpg', 'myhc_247252.jpg', 'myhc_247253.jpg', 'myhc_247254.jpg', 'myhc_247255.jpg', 'myhc_247256.jpg', 'myhc_247257.jpg', 'myhc_247258.jpg', 'myhc_247259.jpg', 'myhc_247260.jpg', 'myhc_247261.jpg', 'myhc_247262.jpg', 'myhc_247263.jpg', 'myhc_247264.jpg', 'myhc_247265.jpg', 'myhc_247266.jpg', 'myhc_247267.jpg', 'myhc_247268.jpg', 'myhc_247269.jpg', 'myhc_247270.jpg', 'myhc_247271.jpg', 'myhc_247272.jpg', 'myhc_247273.jpg', 'myhc_247274.jpg', 'myhc_247275.jpg', 'myhc_247276.jpg', 'myhc_247277.jpg', 'myhc_247278.jpg', 'myhc_247279.jpg', 'myhc_247280.jpg', 'myhc_247281.jpg', 'myhc_247282.jpg', 'myhc_247283.jpg', 'myhc_247284.jpg', 'myhc_247285.jpg', 'myhc_247286.jpg', 'myhc_247287.jpg', 'myhc_247288.jpg', 'myhc_247289.jpg', 'myhc_247290.jpg', 'myhc_247291.jpg', 'myhc_247292.jpg', 'myhc_247293.jpg', 'myhc_247294.jpg', 'myhc_247295.jpg', 'myhc_247296.jpg', 'myhc_247297.jpg', 'myhc_247298.jpg', 'myhc_247299.jpg', 'myhc_247300.jpg', 'myhc_247301.jpg', 'myhc_247302.jpg', 'myhc_247303.jpg', 'myhc_247304.jpg', 'myhc_247305.jpg', 'myhc_247306.jpg', 'myhc_247307.jpg', 'myhc_247308.jpg', 'myhc_247309.jpg', 'myhc_247310.jpg', 'myhc_247311.jpg', 'myhc_247312.jpg', 'myhc_247313.jpg', 'myhc_247314.jpg', 'myhc_247315.jpg', 'myhc_247316.jpg', 'myhc_247317.jpg', 'myhc_247318.jpg', 'myhc_247319.jpg', 'myhc_247320.jpg', 'myhc_247321.jpg', 'myhc_247322.jpg', 'myhc_247323.jpg', 'myhc_247324.jpg', 'myhc_247325.jpg', 'myhc_247326.jpg', 'myhc_247327.jpg', 'myhc_247328.jpg', 'myhc_247329.jpg', 'myhc_247330.jpg', 'myhc_247331.jpg', 'myhc_247332.jpg', 'myhc_247333.jpg', 'myhc_247334.jpg', 'myhc_247335.jpg', 'myhc_247336.jpg', 'myhc_247337.jpg', 'myhc_247338.jpg', 'myhc_247339.jpg', 'myhc_247340.jpg', 'myhc_247341.jpg', 'myhc_247342.jpg', 'myhc_247343.jpg', 'myhc_247344.jpg', 'myhc_247345.jpg', 'myhc_247346.jpg', 'myhc_247347.jpg', 'myhc_247348.jpg', 'myhc_247349.jpg', 'myhc_247350.jpg', 'myhc_247351.jpg', 'myhc_247352.jpg', 'myhc_247353.jpg', 'myhc_247354.jpg', 'myhc_247355.jpg', 'myhc_247356.jpg', 'myhc_247357.jpg', 'myhc_247358.jpg', 'myhc_247531.jpg', 'myhc_247711.jpg', 'myhc_247714.jpg', 'myhc_247717.jpg', 'myhc_247719.jpg', 'myhc_247720.jpg', 'myhc_247721.jpg', 'myhc_247722.jpg', 'myhc_247723.jpg', 'myhc_247724.jpg', 'myhc_247725.jpg', 'myhc_247726.jpg', 'myhc_247727.jpg', 'myhc_247728.jpg', 'myhc_247729.jpg', 'myhc_247730.jpg', 'myhc_247732.jpg', 'myhc_247733.jpg', 'myhc_247734.jpg', 'myhc_247735.jpg', 'myhc_247736.jpg', 'myhc_247737.jpg', 'myhc_247738.jpg', 'myhc_247739.jpg', 'myhc_247741.jpg', 'myhc_247742.jpg', 'myhc_247744.jpg', 'myhc_247745.jpg', 'myhc_247746.jpg', 'myhc_247747.jpg', 'myhc_247748.jpg', 'myhc_247749.jpg', 'myhc_247750.jpg', 'myhc_247751.jpg', 'myhc_247752.jpg', 'myhc_247756.jpg', 'myhc_247757.jpg', 'myhc_247758.jpg', 'myhc_247766.jpg', 'myhc_247777.jpg', 'myhc_247779.jpg', 'myhc_247780.jpg', 'myhc_247781.jpg', 'myhc_247782.jpg', 'myhc_247783.jpg', 'myhc_247784.jpg', 'myhc_247785.jpg', 'myhc_247786.jpg', 'myhc_247787.jpg', 'myhc_32615.jpg', 'myhc_55457.jpeg', 'myhc_61343.jpg', 'myhc_67655.jpg', 'myhc_67685.jpg', 'myhc_93622.jpg'],
	kisLoadYear: "",
	hesaLoadYear: "",
	hcLoadYear: "",
	assessedLoadYear: "",
};
//
var is_error;
function getLatLong() {
	if ((device.platform).toUpperCase() == 'IOS')
		var options = { enableHighAccuracy: true };
	else
		var options = { enableHighAccuracy: true, timeout: 30000, maximumAge: 0 };

	navigator.geolocation.getCurrentPosition(
		whenSuccess = function (position) {
			console.log(">>>>> getLatLong success");
			latitude = position.coords.latitude;
			longtitude = position.coords.longitude;
			//isLocationPermissionDisabled = false;

			//alert("latitude : "+latitude+" longtitude : "+longtitude)
		},
		whenError = function (error) {
			console.log(">>>>> getLatLong error");
			latitude = "";
			longtitude = "";
		

			//isLocationPermissionDisabled = true;
			//alert('While try to get your location we get: code: '    + error.code    + '\n' +  'message: ' + error.message + '\n');
		},
		options
	)
}
//
function swipeTabs(swipeType, pageName) {
	//
	//getLatLong();	
	if (pageName != null && pageName == 'browse') {
		if (swipeType == 'swipeleft' && $$D('uniTab').className != 'button tab-link active') {
			$('#tab1').hide();
			$('#tab2').show();
			$$D('courseTab').className = 'button tab-link';
			$$D('uniTab').className = 'button tab-link active';
			window.location.href = '#browse-university';
		} else if (swipeType == 'swiperight' && $$D('courseTab').className != 'button tab-link active') {
			$('#tab2').hide();
			$('#tab1').show();
			$$D('courseTab').className = 'button tab-link active';
			$$D('uniTab').className = 'button tab-link';
			window.location.href = '#browse-course';
		}
	} else if (pageName != null && pageName == 'search') {
		if (swipeType == 'swipeleft' && $$D('searchUniTab').className != 'button tab-link active') {
			$('#tab1').hide();
			$('#tab2').show();
			$$D('searchCourseTab').className = 'button tab-link';
			$$D('searchUniTab').className = 'button tab-link active';
			if (isSearchAjaxNotNull()) {
				window.location.href = '#ajax-search-university';
			} else {
				window.location.href = '#search-university';
			}
		} else if (swipeType == 'swiperight' && $$D('searchCourseTab').className != 'button tab-link active') {
			$('#tab2').hide();
			$('#tab1').show();
			$$D('searchCourseTab').className = 'button tab-link active';
			$$D('searchUniTab').className = 'button tab-link';
			if (isSearchAjaxNotNull()) {
				window.location.href = '#ajax-search-course';
			} else {
				window.location.href = '#search-course';
			}
		}
	}
}
//
function switchTab(tab) {
	//getLatLong();
	if (tab == 'courseList' && ($$D('courseTab').className == 'button tab-link active-state' || $$D('courseTab').className == 'button tab-link')) {
		$$D('courseTab').className = 'button tab-link active';
		$$D('uniTab').className = 'button tab-link';
		window.location.href = '#browse-course';
	} else if (tab == 'uniList' && ($$D('uniTab').className == 'button tab-link active-state' || $$D('uniTab').className == 'button tab-link')) {
		$$D('courseTab').className = 'button tab-link';
		$$D('uniTab').className = 'button tab-link active';
		window.location.href = '#browse-university';
	} else if (tab == 'searchCourseList' && ($$D('searchCourseTab').className == 'button tab-link active-state' || $$D('searchCourseTab').className == 'button tab-link')) {
		$$D('searchCourseTab').className = 'button tab-link active';
		$$D('searchUniTab').className = 'button tab-link';
		if (isSearchAjaxNotNull()) {
			window.location.href = '#ajax-search-course';
		} else {
			window.location.href = '#search-course';
		}
	} else if (tab == 'searchUniList' && ($$D('searchUniTab').className == 'button tab-link active-state' || $$D('searchUniTab').className == 'button tab-link')) {
		$$D('searchCourseTab').className = 'button tab-link';
		$$D('searchUniTab').className = 'button tab-link active';
		if (isSearchAjaxNotNull()) {
			window.location.href = '#ajax-search-university';
		} else {
			window.location.href = '#search-university';
		}
	}
}
//
function switchHighLightTab(tabId, url, fromUserProfile) {
	//console.log(mainView.history[mainView.history.length-1]);
	//console.log(mainView.history.length);
	if ($("#" + tabId).hasClass('active') && mainView.history[mainView.history.length - 1].replace("?request=login", "") == url) {
		console.log("test now came");
		return false;
	}

	if (tabId == 'homePage' || tabId == 'searchForm') {
		clearFilter();
	}
	if (fromUserProfile == 'PR') {
		removeViewAllFlag();
	}
	if (isBlankOrNull(localStorage.getItem('LOGGED_IN_USER_ID'))) {
		window.location.href = "../../index.html";
	} else if (!isBlankOrNull(localStorage.getItem('LOGGED_IN_USER_ID'))) {
		$('.toolbar-inner').find("a").each(function () {
			//$("#"+$(this).attr("id")).removeClass('active');		
		});
		//$("#"+tabId).addClass('active');
		if (typeof url != "undefined") {
			console.log("Came for switchHighLightTab")
			if (url.indexOf('finalFiveChoice') > -1 && isGuestUser()) {
				localStorage.setItem('action', "final5");
				localStorage.setItem('action_id', "favoriteSubjects");
				Constants.mainViewObj.router.loadPage("html/user/home.html");
			} else if (url.indexOf('searchLanding') > -1 || url.indexOf('homeLanding') > -1) {
				localStorage.setItem('latitude', latitude);
				localStorage.setItem('longitude', longtitude);
			}
			if (url.indexOf('searchLanding') > -1 || url.indexOf('homeLanding') > -1) {
				if (!isBlankOrNull(fromUserProfile)) {
					Constants.mainViewObj.router.loadPage(url);
				} else {
					Constants.mainViewObj.router.loadPage(url);
				}
			} else {
				Constants.mainViewObj.router.loadPage(url);
			}
		}
	}
}
//
function updateMessage(type) {
	if (type == 'version') {
		if ($$D("updateMessage") && $$D("updatedVersion")) {
			$$D("updateMessage").innerHTML = localStorage.getItem('updateVersionMessage');
			if (localStorage.getItem('updateVersionStaus') == 'MANDATORY') {
				$("#updateMsgBackArrow").hide();
				$("#updateVersionFooter").hide();
			} else {
				$$D("updateHeader").innerHTML = "Update available";
			}
			$$D("updatedVersion").innerHTML = 'Available new version ' + localStorage.getItem('updateVersion');
		}
	} else if (type == 'exception') {
		var exception = localStorage.getItem('exception');
		var status = localStorage.getItem('status');
		var message = localStorage.getItem('message');
		if (Constants.environment == 'LIVE' || Constants.environment == 'FO') {
			$$D("expDetails").innerHTML = '<strong>Sorry! Exception occured</strong>';
		} else {
			$$D("expDetails").innerHTML = '<strong>Sorry! Exception occured</strong><br /><div><p>' + status + '<br />' + message + '<br />' + exception + '</p><div>';
		}
	}
	$$D("appVersion").innerHTML = 'Installed version ' + Constants.appVersion;
	$$D("sprint").innerHTML = '(Sprint ' + Constants.sprint + " )";
	$$D("environment").innerHTML = Constants.environment;
	if (Constants.environment == 'LIVE' || Constants.environment == 'FO') {
		$$D("sprint").style.display = 'none';
		$$D("environment").style.display = 'none';
	}
	if (type == "final5") {
		$$D('trackingDiv').innerHTML = localStorage.quizTracking;
	}
}
//This method returns JSON 
function getJson(pullFlag) {
	//alert("latitude : "+latitude +" longtitude : "+ longtitude);
	
	var stringJson = {
		"userId": localStorage.getItem('LOGGED_IN_USER_ID'),
		"latitude": latitude,
		"longitude": longtitude,
		"recentSubjectCount": "",
		"otherSubjectCount": "",
		"institutionsCloseToYouCount": "",
		"institutionsWithUpCommingOpendaysCount": "",
		"appVersion": Constants.appVersion,
		"accessToken": "246",
		"affiliateId": "220703",
		"locationMode": ($('#locationMode') && !isBlankOrNull($('#locationMode').val()) && !(pullFlag == 'pullFlag')) ? $('#locationMode').val() : "CLOSE_TO_YOU"
	}
	return stringJson;
}
//
function clearSearchAjax() {
	if ($$D('searchAjax')) {
		$$D('searchAjax').value = "";
		$$D('xmark').className = '';
	}
	if ($$D('searchCourseTab').className == 'button tab-link active' && $$D('searchUniTab').className == 'button tab-link') {
		window.location.href = '#search-course';
	} else if ($$D('searchUniTab').className == 'button tab-link active' && $$D('searchCourseTab').className == 'button tab-link') {
		window.location.href = '#search-university';
	}
}
//
function isSearchAjaxNotNull() {
	if ($$D('searchAjax') != null && $$D('searchAjax').value.length >= Constants.searchTextMaxLength) {
		return true;
	}
	return false;
}
function isOpendaySearchAjaxNotNull() {
	if ($$D('searchAjaxOpendayLanding') != null && $$D('searchAjaxOpendayLanding').value.length >= Constants.searchTextMaxLength) {
		return true;
	}
	return false;
}
function isOpendaySearchResultAjaxNotNull() {
	if ($$D('searchAjaxOpendayResult') != null && $$D('searchAjaxOpendayResult').value.length >= Constants.searchTextMaxLength) {
		return true;
	}
	return false;
}
//
function convertToSeoFrientlyText(text) {
	var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
	text = text.replace(reg, " ");
	if (text == null) {
		return text;
	}
	var tmpText = text;
	//
	tmpText = replaceAll(tmpText, ' ', '-');
	//
	return tmpText;
}
//
function replaceAll(replacetext, findstring, replacestring) {
	var strReplaceAll = replacetext;
	var intIndexOfMatch = strReplaceAll.indexOf(findstring);
	while (intIndexOfMatch != -1) {
		strReplaceAll = strReplaceAll.replace(findstring, replacestring);
		intIndexOfMatch = strReplaceAll.indexOf(findstring);
	}
	return strReplaceAll;
}
//
function signUpPageURL() {
	window.location.href = 'signUp.html';
}
//
function clearErrorMsg(errorDiv, button) {
	if ($$D(errorDiv)) {
		$$D(errorDiv).style.display = 'none';
	}
	if ($$D(button)) {
		$$D(button).style.display = 'block';
	}
	$("#bodyContent").removeClass("frg_err");
}
//
function showPassword() {
	$$D('password').type = "text";
}
//
function loginPageURL() {
	//window.location.replace('../../html/user/login.html');
	window.location.href = '../../html/user/login.html';
}
//
function showHidePassword() {
	var pwd = $$D("password");
	if (pwd.getAttribute("type") == "password" && !isEmpty($$D("password").value)) {
		pwd.setAttribute("type", "text");
		$$D("showHidePwd").innerHTML = "Hide";
	} else {
		pwd.setAttribute("type", "password");
		$$D("showHidePwd").innerHTML = "Show";
	}
}
//
function trimString(str) { str = this != window ? this : str; return str.replace(/^\s+/g, '').replace(/\s+$/g, ''); }
//
function isEmpty(value) { if (trimString(value) == '' || typeof value == 'undefined') { return true; } else { return false; } }
//
function checkValidEmail(email) {
	if (email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9][A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) == -1) { return false; }
	return true;
}
//
var ErrorMessage = {
	error_user_password: 'Please enter your password',
	error_login: 'Please enter your details again',
	error_user_email: 'Please enter your email address',
	error_user_firstname: 'Please enter your first name',
	error_valid_email: 'Please enter a valid email address',
	error_pass_mismatch: 'Your password do not match, please try again',
	error_email_nofound: "We couldn't find your email address in our records.",
	error_email_already_exists: "Your email address is already registered with us",
	error_signup_name: "something doesn't look right, are you sure this is your name? (you can only enter characters here)",
	error_signup_email: "Something doesn't look right, are you sure this is a real email address?",
	error_oracle: "Sorry! Exception occured",
	error_password_length: "Your password must be at least 4 characters long"
}
//
function validateUserLogin() {
	var email = trimString($$D("email").value);
	var password = trimString($$D("password").value);
	var message = true;
	if (isEmpty(email)) {
		message = false;
		$$D("loginErrMsg").innerHTML = ErrorMessage.error_user_email;
	} else if (!checkValidEmail(email)) { message = false; $$D("loginErrMsg").innerHTML = ErrorMessage.error_valid_email; }
	$$D("loginErrMsgPwd").innerHTML = "";
	if (isEmpty(password)) {
		message = false;
		$$D("loginErrMsgPwd").innerHTML = ErrorMessage.error_user_password;
		if (!checkValidEmail(email)) {
			message = false;
			$$D("loginErrMsg").innerHTML = ErrorMessage.error_valid_email;
		}
	}
	/*
	else if(!isEmpty(password) && password.length < 4){ 
	  message = false; 
	  $$D("loginErrMsgPwd").innerHTML = ErrorMessage.error_password_length; 
	  $$D("loginErrMsg").innerHTML = "";
	}
	*/
	if (isEmpty(email) && isEmpty(password)) {
		message = false;
		$$D("loginErrMsg").innerHTML = ErrorMessage.error_login; $$D("loginErrMsgPwd").innerHTML = "";
	}
	if (message) {
		is_error = false;
		return true;
	} else {
		//console.log("test");
		is_error = true;
		if (Constants.devicePlatform == 'IOS') {
			if (Keyboard.isVisible) {
				setTimeout(function () {
					Keyboard.hide();
				}, 100);
			}
		} else {
			setTimeout(function () {
				//cordova.plugins.Keyboard.close();
				Keyboard.close();
			}, 10);

		}
		$("#loginSucErrBlk").addClass("frg_err");
		$(".err_msg").show();
		return false;
	}
}
//
function showLoginPage(pageName) {
	if (pageName == 'forgotPwd') { Constants.mainViewObj.router.loadPage('html/user/login.html'); }
	else { Constants.mainViewObj.router.loadPage('html/user/login.html'); }
}
//
function showForgotPassword() {
	window.location.href = '../../html/user/forgotPassword.html';
}
//
function showSuccess(textDiv, successDiv, errDiv, errBgDiv) {
	if (!isEmpty($$D(textDiv).value) && ((textDiv == "email" && checkValidEmail($$D("email").value)) || (textDiv == "password"))) {
		$('#' + successDiv).addClass("inp_tick");
		$("#" + errBgDiv).removeClass("frg_err");
		$("#" + errDiv).hide();
	} else {
		$('#' + successDiv).removeClass("inp_tick");
	}
	if (textDiv == "password") { $("#showHidePwd").show(); }
}
//
function backTo(page) {
	if (page == 'home') {
		Constants.mainViewObj.router.loadPage('html/user/home.html');
	}
}
//
function showLoginButton(id) {
	var email = trimString($$D("email").value);
	var password = trimString($$D("password").value);
	if (!isEmpty(email) && checkValidEmail(email) && !isEmpty(password)) { $('#logInButton').removeAttr("disabled"); $("#loginSucErrBlk").removeClass("frg_err"); $(".err_msg").hide(); }
	else { $('#logInButton').attr("disabled", "disabled"); }
}
//
function closeErrMsgFunc(id, flag) {
	if ($("#" + id)) { $("#" + id).hide(); }
	if (flag == "quiz") { $("#gradeSaveDiv").show(); }
}
//
function showForgotPwdButton() {
	var email = trimString($$D("emailId").value);
	if (!isEmpty(email) && checkValidEmail(email)) { $('#recoverPwdButton').removeAttr("disabled"); $('#inp_tick_fwd_pwd').addClass("inp_tick"); return false; }
	else { $('#recoverPwdButton').attr("disabled", "disabled"); return true; }
}
//
function validateForgotPwdUserLogin() {
	var email = trimString($$D("emailId").value);
	var message = true;
	if (isEmpty(email)) {
		message = false;
		$$D("recoverErrMsg").innerHTML = ErrorMessage.error_user_email;
	} else if (!checkValidEmail(email)) { message = false; $$D("recoverErrMsg").innerHTML = ErrorMessage.error_valid_email; }
	if (message) {
		is_error = false;
		return true;
	} else {
		is_error = true;
		if (Constants.devicePlatform == 'IOS') {
			if (Keyboard.isVisible) {
				setTimeout(function () {
					Keyboard.hide();
				}, 100);
			}
		} else {
			setTimeout(function () {
				//cordova.plugins.Keyboard.close();
				Keyboard.close();
			}, 10);


		}
		$("#recoverPwdSucErrBlk").addClass("frg_err");
		$(".err_msg").show();
		return false;
	}
}
//Below code for go back functionality
function goBackFn(fromPage) {
	if (fromPage == 'course-details') {
		//window.location.replace('courseDetails.html');
		window.location.href = 'providerResults.html';
	} else {
		window.history.back();
	}
}
function goBackExceptionFn() {
	window.history.go(-2);
}
//
function showNextButton(id) {
	var firstName = trimString($$D("firstName").value);
	var lastName = trimString($$D("lastName").value);
	if (!isEmpty(firstName) && !isEmpty(lastName)) {
		$('#' + id).removeAttr("disabled");
	} else {
		$('#' + id).attr("disabled", "disabled");
	}
}
function showSignUpButton() {
	var email = trimString($$D("email").value);
	var password = trimString($$D("password").value);
	if (!isEmpty(email) && !isEmpty(password)) { $('#' + id).removeAttr("disabled"); }
	else { $('#' + id).attr("disabled", "disabled"); }
}
//
//Below code for validate user name
function validateUserName(userName) {
	if (userName.search(/^[a-zA-Z ']+$/) == -1) { return false; }
	return true;
}
//
function displayErrMg(errMsg) {
	if (errMsg.trim() == "EMAIL_NOT_FOUND") {
		$$D("loginErrMsg").innerHTML = ErrorMessage.error_email_nofound;
	} else if (errMsg.trim() == "EMAIL_LOCKED") {
		$$D("loginErrMsg").innerHTML = "Sorry your account has been locked.";
	} else if (errMsg.trim() == "CREDENTIAL_MISMATCH") {
		$$D("loginErrMsg").innerHTML = "Please enter your details again.";
	} else if (errMsg.trim() == "EMAIL_ALREADY_EXIST") {
		$$D("loginErrMsg").innerHTML = ErrorMessage.error_email_already_exists;
	} else if (errMsg.trim() == "ORACLE_ERROR") {
		$$D("loginErrMsg").innerHTML = ErrorMessage.error_oracle;
	}
}
//
function hideShowSuccess(textDiv, firstSuccessDiv, lastSuccessDiv, buttonName, msgDiv) {
	//$(".err_msg").hide();	
	if ($$D(firstSuccessDiv).className == 'inp_tick' && $$D(lastSuccessDiv).className == 'inp_tick') {
		showNextButton(buttonName);
		showSuccessMessage(msgDiv, 'block');
	}
}
//
function hideSuccess(successDiv, msgDiv, buttonName) {
	showSuccessMessage(msgDiv, 'none');
	$('#' + buttonName).attr("disabled", "disabled");
	$$D(successDiv).className = "";
}
//
function hideShowError(successDiv, buttonName, msgDiv, pageFrom) {
	if (pageFrom == "user_name") {
		$$D('loginErrMsg').innerHTML = ErrorMessage.error_signup_name;
	} else if (pageFrom == "user_mail") {
		$$D('loginErrMsg').innerHTML = ErrorMessage.error_signup_email;
	}
	$(".err_msg").show();
	showSuccessMessage(msgDiv, 'none');
	$('#' + buttonName).attr("disabled", "disabled");
	$$D(successDiv).className = "";
}
//
function showSuccessMessage(successMsgDiv, value) {
	$$D(successMsgDiv).style.display = value;
}
//
/*function validateSignUpEmail(emailId, successDiv){
	if(!isEmpty($$D(emailId).value) && checkValidEmail(trimString($$D(emailId).value))){		
		$('#'+successDiv).addClass("inp_tick");
	}else{
		hideShowError(successDiv, 'signUpButton', 'mailSuceessMsg', 'user_mail');
	}
}*/
//
function showEmailPage(firstName, lastName) {
	if ((!isEmpty(firstName) && validateUserName(firstName)) || (!isEmpty(lastName) && validateUserName(lastName))) {
		$$D('userName').style.display = 'none';
		$$D('userMail').style.display = 'block';
	} else {
		$(".err_msg").show();
		$$D('loginErrMsg').innerHTML = ErrorMessage.error_signup_name;
		showSuccessMessage('nameSuceessMsg', 'none');
		$('#nextButton').attr("disabled", "disabled");
	}
}
//
function checkUserLoggedIn() {
	//
	if (localStorage.getItem('LOGGED_IN_USER_FLAG') == "Y") {
		localStorage.setItem('latitude', latitude);
		localStorage.setItem('longitude', longtitude);
		Constants.mainViewObj.router.loadPage('html/home/homeLanding.html');
	}
	//
}
//
function isBlankOrNull(value) {
	if (value == null || value == 'null' || value == '' || value == 'undefined' || value == 'Not chosen' || value == 'Not selected') { return true; }
	return false;
}
//
function isCDPageBlankOrNull(value) {
	if (value == null || value == 'null' || value == '' || value == 'undefined' || value == 'Not chosen' || value == '0') { return true; }
	return false;
}
//
function hideFooter() {
	if (isBlankOrNull(localStorage.getItem('LOGGED_IN_USER_ID'))) {
		$$D('searchResults').style.display = 'none';
		$$D('favoriteSubjects').style.display = 'none';
		$$D('myProfile').style.display = 'none';
	}
}
//
function userLogoutFnc() {
	//
	if (Constants.devicePlatform === 'IOS' && parseFloat(Constants.deviceVersion) >= 12) {
		Keyboard.shrinkView(true);
	} else if (Constants.devicePlatform === 'IOS') {
		Keyboard.shrinkView(false);
	}
	//
	//Keyboard.shrinkView(false);
	localStorage.setItem('LOGGED_IN_USER_FLAG', "N");
	localStorage.setItem('LOGGED_IN_USER_ID', "");
	//
	localStorage.setItem('SCHOOL_NAME', "");
	localStorage.setItem('SCHOOL_URN', "");
	localStorage.setItem('USER_MODE', "");
	localStorage.setItem('QUIZ_JOURNEY', "");
	localStorage.removeItem('QUIZ_COMPLETE');
	//

	//
	localStorage.setItem("GUEST_USER", "");
	localStorage.setItem("LOGGED_IN_USER_FIRST_NAME", "");
	localStorage.setItem("LOGGED_IN_USER_LAST_NAME", "");
	//
	clearFilter();
	//window.location.href = '../../index.html';
	Constants.mainViewObj.router.loadPage('html/user/home.html');
}
//
function checkAndShowValidEmail(errorDiv, errorMsgDiv) {
	var email = trimString($$D("email").value);
	var message = true;
	$("#loginErrMsgPwd").html("");
	if (isEmpty(email)) {
		message = false;
		$$D("loginErrMsg").innerHTML = ErrorMessage.error_user_email;
	} else if (!checkValidEmail(email)) { message = false; $$D("loginErrMsg").innerHTML = ErrorMessage.error_signup_email; }
	if (message) {
		is_error = false;
		$("#" + errorDiv).removeClass("frg_err");
		$("#" + errorMsgDiv).hide();
	} else {
		is_error = true;
		if (Constants.devicePlatform == 'IOS') {
			if (Keyboard.isVisible) {
				setTimeout(function () {
					Keyboard.hide();
				}, 100);
			}
		} else {
			setTimeout(function () {
				//cordova.plugins.Keyboard.close();
				Keyboard.close();
			}, 10);

		}

		$("#" + errorDiv).addClass("frg_err");
		$("#" + errorMsgDiv).show();
	}
}
function validateAndShowFirstName(errorDiv) {
	var firstName = trimString($$D("firstName").value);
	var message = true;
	if (isEmpty(firstName)) {

		message = false; $$D("firstnameErrMsg").innerHTML = ErrorMessage.error_user_firstname;
	} else if (!validateUserName(firstName)) {
		message = false; $$D("firstnameErrMsg").innerHTML = ErrorMessage.error_signup_name;
	}
	if (message) {
		is_error = false;
		$("#" + errorDiv).removeClass("frg_err");
		$(".err_msg").hide();
	} else {
		is_error = true;
		if (Constants.devicePlatform == 'IOS') {
			if (Keyboard.isVisible) {
				setTimeout(function () {
					Keyboard.hide();
				}, 100);
			}
		} else {
			setTimeout(function () {
				//cordova.plugins.Keyboard.close();
				Keyboard.close();
			}, 10);


		}
		$("#" + errorDiv).addClass("frg_err");
		$(".err_msg").show();
	}
}
//
function changeErrorPage(divId) {
	if ($$D(divId) && $("#" + divId).hasClass("frg_err")) {
		$("#" + divId).removeClass("frg_err");
	}
}
//
function onChageQualGrades(value, preSelect, page, countpage) {
	//
	closeErrMsgFunc('previousQualErrMsg' + page);
	//
	var previousQualSelected = $$D("previousQualGradesSelected" + page).value;
	var selectedQualId = value.split('|');
	var grade = 0; var totGrades = 0;
	var noOfGradesArr = $$D("qual" + selectedQualId[0]) ? $$D("qual" + selectedQualId[0]).value : "";
	var noOfGrades = noOfGradesArr.split(',').length;
	$$D("numberOfGrades" + page).value = noOfGrades;
	var qualGrades = noOfGradesArr.split(',');
	$('#previousQualSavBtn' + page).attr("disabled", "disabled");
	var onEventmethod = "";
	console.log('countpage > ' + countpage);
	if (!isBlankOrNull(countpage)) {
		if (countpage == 'mySearchRootCtrl') {
			onEventmethod = "validateSelectedGrades(\'get-count\', '', '',  \'mySearchRootCtrl\')";
		} else {
			onEventmethod = "validateSelectedGrades(\'get-count\', '', 'PR', \'myProviderRootCtrl\')";
		}
	}
	if ($$D("dynamicQualList" + page)) {
		$$D("dynamicQualList" + page).innerHTML = '';
		if (!isBlankOrNull(previousQualSelected) && preSelect == 'preselect' && previousQualSelected.split("-").length == 1) { noOfGradesArr = ""; }
		for (var z = 0; z < noOfGrades; z++) {
			if (!isBlankOrNull(noOfGradesArr)) {
				if (!isBlankOrNull(previousQualSelected) && preSelect == 'preselect') {
					var arr = previousQualSelected.split("-");
					if (arr != null && arr.length > 0) {
						grade = arr[z].charAt(0);
					}
					totGrades += parseInt(grade);
				}
				var newFieldValue = '<li><h3 id="gradeTitle_' + z + page + '">' + qualGrades[z] + '</h3><span class="toggle"><span class="minus" onclick="increaseDecreaseGrades(' + z + ',\'decrease\', \'' + page + '\', ' + countpage + ');' + onEventmethod + '"> - </span><span class="blu_box" id="gradeValue_' + z + page + '">' + grade + '</span><span class="plus" onclick="increaseDecreaseGrades(' + z + ',\'increase\',  \'' + page + '\', ' + countpage + ');' + onEventmethod + '"> + </span></span>';
				newFieldValue += '<input type="hidden" name="points_' + z + page + '" id="points_' + z + page + '" maxlength="3" value="' + selectedQualId[1] + '"></li>';
				$$D('dynamicQualList' + page).innerHTML += newFieldValue;
				if (totGrades > 0) { $('#previousQualSavBtn' + page).removeAttr("disabled"); }
				else { $('#previousQualSavBtn' + page).attr("disabled", "disabled"); }
			}
		}
		if (isBlankOrNull(noOfGradesArr)) {
			var newFieldValue = '<li class="tps">';
			if (preSelect == 'preselect' && !isBlankOrNull($$D("pqTariffSavedValue" + page).value)) {
				newFieldValue += '<input type="number" placeholder="Enter tariff point" name="points_TP' + page + '" id="points_TP' + page + '" maxlength="3" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" onblur="validateSelectedGradesShow()" onkeyup="' + onEventmethod + '" onfocus="closeErrMsgFunc(\'previousQualErrMsg\');" value="' + $$D("pqTariffSavedValue" + page).value + '"></li>';
			} else {
				newFieldValue += '<input type="number" placeholder="Enter tariff point" name="points_TP' + page + '" id="points_TP' + page + '" value="" maxlength="3" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" onblur="validateSelectedGradesShow()" onkeyup="' + onEventmethod + '" onfocus="closeErrMsgFunc(\'previousQualErrMsg\');"></li>';
			}
			$$D('dynamicQualList' + page).innerHTML = newFieldValue;
			$('#previousQualSavBtn' + page).removeAttr("disabled");
		}
	}
}
//
function increaseDecreaseGrades(id, flag, page, countpage) {
	var numberOfGrades = $$D("numberOfGrades" + page).value;
	var maxVal = 0; var curVal = 0; var totalGrade = 0; var count = 0;
	if ($$D('gradeValue_' + id + page) && $$D('points_' + id + page)) {
		maxVal = $$D('points_' + id + page).value;
		curVal = $$D('gradeValue_' + id + page).innerHTML;
		if (curVal < maxVal && flag == 'increase') { curVal++; }
		if (curVal > 0 && flag == 'decrease') { curVal--; }
		$$D('gradeValue_' + id + page).innerHTML = curVal;
		for (var i = 0; i < numberOfGrades; i++) {
			if ($$D("gradeValue_" + i + page) && $$D("gradeTitle_" + i + page)) {
				totalGrade += parseInt($$D("gradeValue_" + i + page).innerHTML);
				var gradeVal = $('#gradeTitle_' + i + page).html();
				if (gradeVal.toUpperCase() == "A*") { count += parseInt($$D("gradeValue_" + i + page).innerHTML) + 1; }
				else if (gradeVal.toUpperCase() == "A") { count += parseInt($$D("gradeValue_" + i + page).innerHTML); }
			}
		}
		console.log('totalGrade > ' + totalGrade);
		if (parseInt(totalGrade) > 0) {
			if (isBlankOrNull(countpage)) {
				$('#previousQualSavBtn' + page).removeAttr("disabled");
			}
		}
		else { $('#previousQualSavBtn' + page).attr("disabled", "disabled"); }
		$("#gradeCount").val(count); $("#gradeSaveDiv").show();
	}
}
//
function increaseDecreaseGradesInOpenday(id, flag) {
	var maxVal = 0; var curVal = 0;
	if ($$D('opendaygradeValue_' + id) && $$D('opedaypoints_' + id)) {
		maxVal = $$D('opedaypoints_' + id).value;
		curVal = parseInt($$D('opendaygradeValue_' + id).innerHTML);
		if (curVal < maxVal && flag == 'increase') { curVal++; }
		if (curVal > 1 && flag == 'decrease') { curVal--; }
		$$D('opendaygradeValue_' + id).innerHTML = curVal;
	}
}
function validateSelectedGradesShow() {
	if (is_error)
		is_error = false;
}
//
function validateSelectedGrades(flag, id, page, countpage) {
	var numberOfGrades = $$D("numberOfGrades" + page).value;
	var selectedGradeArr = ""; var selectedGrade = "";
	if (flag == "quiz") { selectedGrade = $("#previousQual").val(); }
	else { selectedGradeArr = $$D("selectGrades" + page).value.split('|'); selectedGrade = selectedGradeArr[2]; }
	var selectedGrades = "";
	var gradeTextVal = "";
	var selectedGradesDisplay = "";
	var totalGrade = 0;
	for (var i = 0; i < numberOfGrades; i++) {
		if ($$D("gradeValue_" + i + page) && $$D("gradeTitle_" + i + page)) {
			totalGrade += parseInt($$D("gradeValue_" + i + page).innerHTML);
			selectedGrades += $$D("gradeValue_" + i + page).innerHTML + $$D("gradeTitle_" + i + page).innerHTML;
			if (id == "gradeText") { selectedGradesDisplay += ""; } else { selectedGradesDisplay += " "; }
			for (var j = 0; j < parseInt($$D("gradeValue_" + i + page).innerHTML); j++) {
				selectedGradesDisplay += $$D("gradeTitle_" + i + page).innerHTML;
			}
			if (i + 1 != numberOfGrades) { selectedGrades += "-"; }
			if (id == "gradeText") { gradeTextVal += "<" + $$D("gradeTitle_" + i + page).innerHTML + ">" + $$D("gradeValue_" + i + page).innerHTML + "</" + $$D("gradeTitle_" + i + page).innerHTML + ">"; }
		} else {
			selectedGrades = $$D('points_TP' + page).value;
			selectedGradesDisplay += $$D('points_TP' + page).value;
			if (id == "gradeText") { gradeTextVal += "<free-text>" + $$D('points_TP' + page).value + "</free-text>"; }
		}
	}
	gradeTextVal = "<entry-grades>" + gradeTextVal + "</entry-grades>";
	var errorMsg = ""; var errorFlag = true;
	if (selectedGrade == "A_LEVEL" && totalGrade > 6 && flag != 'get-count') {
		errorFlag = false;
		errorMsg = '<p class="rd">Woah, steady on there!</p><p></p><p>You can only enter a max of 6 grades. Put in your best ones if you have more</p>';
	} else if ((selectedGrade == "SQA_HIGHER" || selectedGrade == "SQA_ADV") && totalGrade > 10 && flag != 'get-count') {
		errorFlag = false;
		errorMsg = '<p class="rd">Woah, steady on there!</p><p></p><p>You can only enter a max of 10 grades. Put in your best ones if you have more</p>';
	} else if (selectedGrade == "BTEC" && totalGrade > 3 && flag != 'get-count') {
		errorFlag = false;
		errorMsg = '<p class="rd">Woah, steady on there!</p><p></p><p>You can only enter a max of 3 grades. Put in your best ones if you have more</p>'
	} else if (selectedGrade == "TARIFF_POINTS") {
		var tariffPoints = $$D("points_TP" + page).value; errorFlag = false;

		if ((tariffPoints.trim() == "" || tariffPoints == null) && $$D("points_TP" + page).validity.valid) {
			is_error = true;
			errorMsg = "Please enter the tariff points.";
		} else if (!$$D("points_TP" + page).validity.valid) {
			is_error = true;
			errorMsg = "Please enter a number for your tariff points.";
		} else if (isNaN(tariffPoints)) {
			is_error = true;
			errorMsg = "Please enter a number for your tariff points.";
		} else if (tariffPoints > 999) {
			is_error = true;
			errorMsg = '<p class="rd">Woah, steady on there!</p><p></p><p>You can only enter a maximum of 999 tariff points.</p>';
		} else { errorFlag = true; is_error = false; }
	}
	if (!errorFlag && flag != "closePopUp") {
		$$D("pqerrmsg" + page).innerHTML = errorMsg;
		if (flag != 'get-count')
			$("#previousQualErrMsg" + page).show();
	}
	if (errorFlag && flag != 'get-count') {
		if (flag == "quiz") {
			$("#pqSelectedGrades").val(selectedGrades.toLowerCase());
			$("#valueToBeShown").val(selectedGradesDisplay);
			$("#pqSelToDisplay").val(selectedGradesDisplay);
		} else {
			$$D("pqSavedValue" + page).value = $$D("selectGrades" + page).value;
			$$D("previousQualGradesSelected" + page).value = selectedGrades.toLowerCase();
			$("#previousQualSel" + page).html($$D("selectGrades" + page).options[$$D("selectGrades" + page).selectedIndex].text + " - " + selectedGradesDisplay);
			$$D("previousQualSelected" + page).value = selectedGrade; $("#previousQualErrMsg" + page).hide();
			if (id == "gradeText") {
				$("#gradeText").val(gradeTextVal);
				$("#previousQualSel").html($$D("selectGrades" + page).options[$$D("selectGrades" + page).selectedIndex].text + " / " + selectedGradesDisplay);
			}
		}
	} else if (flag == 'get-count') {
		$$D("previousQualGradesSelectedforcount" + page).value = selectedGrades.toLowerCase();
		$$D("previousQualSelectedforcount" + page).value = selectedGrade;
	}
	if (flag == 'get-count') {
		if (totalGrade > 0 || (selectedGrade == 'TARIFF_POINTS' && selectedGrades > 0)) {
			//$('#previousQualSavBtn').attr("disabled", "disabled");
			angular.element($$D(countpage)).scope().getSubjectQualificationCourseCount('prevQualCount', 'pre-qualification');
		} else {
			//do nothing
			$(".courseCount").html($('#oldTotalCourseCount').val());
			$(".courseCount").hide();
			$('#totalCourseCount').val($('#oldTotalCourseCount').val());
			var totalCourseCount = $('#totalCourseCount').val();
			if (totalCourseCount > 0) {
				$('#viewSearchFilterResults').removeAttr('disabled');
			}
		}
	}
	if ($("#previousQualSel" + page).html() == 'Not chosen') { $("#previousQualChangeTxt" + page).html("+ Add"); }
	else { $("#previousQualChangeTxt" + page).html("Change"); }
	return errorFlag;
}
//
function resetSearchFilterValues(fromPage, page) {
	if (fromPage == 'mySearchRootCtrl') {
		$("#orderBySelectBox").val("");
		$("#selectedOrderByVal").html("Not chosen");
		$('#orderByKeyValue').val('');
		$("#orderByAddChangeTxt").html("+ Add");
	}
	//Qualification
	$("#qualificationSelected" + page).html("Undergraduate");
	$$D("qualificationCodeSelected" + page).value = "M";
	$("input[value='M|Undergraduate']").prop('checked', true);
	//subject
	$("input[value='" + $$D("oldCategoryCodeSelected" + page).value + "|" + $$D("oldCategoryIdSelected" + page).value + "|" + $$D("oldCategoryText" + page).value + "']").prop('checked', true);
	$$D("categoryIdSelected" + page).value = $("#oldCategoryIdSelected" + page).val();
	$$D("categoryCodeSelected" + page).value = $("#oldCategoryCodeSelected" + page).val();
	$$D('subjectSelected' + page).innerHTML = $$D("oldCategoryText" + page).value;
	//
	if (fromPage == 'mySearchRootCtrl') {
		//statusBarFontColor('white');
		//Location
		$("#selectedLocation").html("Not chosen");
		$$D('selectedCountryValue').value = "";
		$$D("selectedLocationFlag").value = "";
		$("#locationAddChangeTxt").html("+ Add");
		$('input[name=sublocation]').removeAttr('checked');
		$('input[name=location]').removeAttr('checked');
		$(".city_list").hide();
		$("input[value='All UK']").prop('checked', true);
		//Location type name
		$('input[name=locationType]').removeAttr('checked');
		$("#locationTypeNameSelected").html("Not chosen");
		$("#locationTypeAddChangeTxt").html("+ Add");
		$("#locationTypeIdSelected").val("");
	}
	//Review category
	$('input[name=reviewCategory' + page + ']').removeAttr('checked');
	$("#reviewCategoryNameSelected" + page).html("Not chosen");
	$("#reviewCategoryAddChangeTxt" + page).html("+ Add");
	$("#reviewCategorySelected" + page).val("");
	//Assessment type
	$('input[name=assessment-radio' + page + ']').removeAttr('checked');
	$("#assessmentTypeChangeTxt" + page).html("+ Add");
	$("#assessmentTypeSel" + page).html("Not chosen");
	$$D("assessmentTypeSelected" + page).value = "";

	//Study mode
	$('input[name=studyMode-radio' + page + ']').removeAttr('checked');
	$("#studyModeAddChangeTxt" + page).html("+ Add");
	$("#studyModeNameSelected" + page).html("Not chosen");
	$$D("smTextKeySelected" + page).value = "";
	//
	if (parseInt($$D("totalCourseCount" + page).value) == 0) { $('#viewSearchFilterResults' + page).attr("disabled", "disabled"); }
	else { $('#viewSearchFilterResults' + page).prop("disabled", false); }
	//Previous qual
	$("#previousQualSel" + page).html("Not chosen");
	$$D("previousQualGradesSelected" + page).value = "";
	$$D("previousQualSelected" + page).value = "";
	$("#selectGrades" + page).val($$D("pqResetValue" + page).value);
	$("#previousQualChangeTxt" + page).html("+ Add");
	if (fromPage == 'mySearchRootCtrl') {
		onChageQualGrades($$D("pqResetValue" + page).value, 'preselect', page, 'mySearchRootCtrl');
	} else {
		onChageQualGrades($$D("pqResetValue" + page).value, 'preselect', page, 'myProviderRootCtrl');
	}
	$$D("pqSavedValue" + page).value = "";
	//
	$$D("subjectResetValue" + page).value = $$D("categoryCodeSelected" + page).value + "|" + $$D("categoryIdSelected" + page).value + "|" + $("#subjectSelected" + page).html();
	if (fromPage == 'mySearchRootCtrl') {
		angular.element($$D('mySearchRootCtrl')).scope().getSubjectQualificationCourseCount('subjectResetValue', 'subject');
	} else if (fromPage == 'myProviderRootCtrl') {
		/*statusBarFontColor('white');
		if(document.getElementById(stk_app)!=null) {
			if($('#stk_app').html()=='') {
				statusBarFontColor('white');
			} else {
				statusBarFontColor('black');
			}
		}*/
		angular.element($$D('myProviderRootCtrl')).scope().getSubjectQualificationCourseCount('subjectResetValuePR', 'subject');
	}
}

//
function populateToCurrentValuesInFilterHome(fromPage, page) {
	statusBarFontColor('black');
	// -- Flip-out -- all
	if (fromPage == 'mySearchRootCtrl') {
		statusBarFontColor('black');
		$('.flip-in').each(function (index, obj) {
			if (($(obj).attr('id')).indexOf('-') > -1) {
				angular.element($$D('mySearchRootCtrl')).scope().closeFlipFlop('search', ($(obj).attr('id')).split('-')[1]);
			} else {
				closeFlipFlop($(obj).attr('id'));
			}
		});
	} else if (fromPage == 'myProviderRootCtrl') {
		statusBarFontColor('black');
		$('.pr_bg').each(function (index, obj) {
			if (($(obj).attr('id')).indexOf('-') > -1) {
				angular.element($$D('myProviderRootCtrl')).scope().closeFlipFlop('flip', ($(obj).attr('id')).split('-')[1], event);
			} else {
				providerCloseFlipFlop('flip', $(obj).attr('id'), event);
			}
		});
	}
	//	
	if (fromPage == 'mySearchRootCtrl') {
		//orderBy
		if (!isBlankOrNull($$D("oldOrderByKeyValue").value) && !isBlankOrNull($$D("oldOrderByValue").value)) {
			$("#orderBySelectBox").val($("#oldOrderByKeyValue").val());
			$("#selectedOrderByVal").html($("#oldOrderByValue").val());
			$("orderByKeyValue").val($("#oldOrderByKeyValue").val());
			$("#orderByAddChangeTxt").html("Change");
		}
	}
	//Qualification
	if (!isBlankOrNull($$D("oldQualificationValue" + page).value) && !isBlankOrNull($$D("oldQualificationCodeValue" + page).value)) {
		$("#qualificationSelected" + page).html($("#oldQualificationValue" + page).val());
		$$D("qualificationCodeSelected" + page).value = $("#oldQualificationCodeValue" + page).val();
		$("#qualificationAddChangeTxt" + page).html("Change");
		// Populate 2nd level popup
		$("input[value='" + $$D("qualificationCodeSelected" + page).value + "|" + $$D("qualificationSelected" + page).innerHTML + "']").prop('checked', true);
	} else {
		if (!isBlankOrNull($("input[name='qualificaiton-radio" + page + "']:checked").val())) {
			$("#qualificationSelected" + page).html(($("input[name='qualificaiton-radio" + page + "']:checked").val()).split('|')[1]);
			$$D("qualificationCodeSelected" + page).value = ($("input[name='qualificaiton-radio" + page + "']:checked").val()).split('|')[0];
		}
		$("#qualificationAddChangeTxt" + page).html("Change");
	}
	//Subject
	if (!isBlankOrNull($$D("oldCategoryIdSelected" + page).value) && !isBlankOrNull($$D("oldCategoryCodeSelected" + page).value) && !isBlankOrNull($("#oldCategoryText" + page).val())) {
		$("#subjectSelected" + page).html($("#oldCategoryText" + page).val());
		$$D("categoryIdSelected" + page).value = $("#oldCategoryIdSelected" + page).val();
		$$D("categoryCodeSelected" + page).value = $("#oldCategoryCodeSelected" + page).val();
		$("#subjectSelected" + page).html($("#oldCategoryText" + page).val());
		$(".courseCount" + page).html($$D("oldTotalCourseCount" + page).value + " courses");
		if ($("#totalCourseCount" + page).html() != "0") {
			$(".courseCount" + page).hide();
		}
		// Populate 2nd level popup
		$("input[value='" + $$D("categoryCodeSelected" + page).value + "|" + $$D("categoryIdSelected" + page).value + "|" + $$D("subjectSelected" + page).innerHTML + "']").prop('checked', true);
	}
	if (fromPage == 'mySearchRootCtrl') {
		//Country
		if (!isBlankOrNull($$D("oldCountryValue").value) && !isBlankOrNull($$D("oldCountryFlagValue").value)) {
			$("#selectedLocation").html($("#oldCountryValue").val());
			$$D("selectedLocationFlag").value = $("#oldCountryFlagValue").val();
			$("#locationAddChangeTxt").html("Change");
			// Populate 2nd level popup
			$('input[name=sublocation]').removeAttr('checked');
			$('input[name=location]').removeAttr('checked');
			$("input[value='" + $$D("selectedLocation").innerHTML + "']").prop('checked', true);
			var id = $('input:radio').filter(function () { return this.value === $$D("selectedLocation").innerHTML; }).prop('id');
			if (!isBlankOrNull(id) && id.indexOf('sublocation') > -1) {
				$("input[value='All England']").prop('checked', true);
				$(".city_list").show();
			} else { $(".city_list").hide(); }
			//
		} else {
			$("input[value='All UK']").prop('checked', true);
		}
		//Location type
		if (!isBlankOrNull($$D("oldLocationTypeNameValue").value) && !isBlankOrNull($$D("oldLocationTypeIdValue").value)) {
			$("#locationTypeNameSelected").html($("#oldLocationTypeNameValue").val());
			$$D("locationTypeIdSelected").value = $("#oldLocationTypeIdValue").val();
			$("#locationTypeAddChangeTxt").html("Change");
			// Populate 2nd level popup
			$('input:checkbox').removeAttr('checked');
			var arr = !isBlankOrNull($$D("locationTypeNameSelected").innerHTML) ? $$D("locationTypeNameSelected").innerHTML.split(", ") : "";
			var arrId = !isBlankOrNull($$D("locationTypeIdSelected").value) ? $$D("locationTypeIdSelected").value.split(",") : "";
			for (var z = 0; z < arr.length; z++) {
				$("input[value='" + arrId[z] + "|" + arr[z] + "']").prop('checked', true);
			}
		}
	}
	//Review category
	if (!isBlankOrNull($$D("oldReviewCategorySelected" + page).value) && !isBlankOrNull($$D("oldReviewCategoryNameSelected" + page).value)) {
		//$("#reviewCategoryNameSelected"+page).html($("#oldReviewCategoryNameSelected"+page).val()); // need to change
		//
		var selectedArray = !isBlankOrNull($$D("reviewCategoryNameSelected" + page).innerHTML) ? $$D("reviewCategoryNameSelected" + page).innerHTML.split(", ") : "";
		if (fromPage == 'myProviderRootCtrl') {
			var reviewCategoryFilterDetails = angular.element($$D('myProviderRootCtrl')).scope().getReviewCategoryFilterDetails();
			var selectedArray = $$D("oldReviewCategoryNameSelected" + page).value.split(", ");
			var tempReviewCategoryNameArray = [];
			angular.forEach(reviewCategoryFilterDetails, function (value, index) {
				if (selectedArray.indexOf(value.reviewCategoryName.trim()) >= 0) {
					tempReviewCategoryNameArray.push(value.reviewCategoryDisplayName);
				}
			});
			reviewCategoryNameText = tempReviewCategoryNameArray.join(", ");
			$("#reviewCategoryNameSelected" + page).html(reviewCategoryNameText);
		}
		if (fromPage == 'mySearchRootCtrl') {
			var reviewCategoryFilterDetails = angular.element($$D('mySearchRootCtrl')).scope().getReviewCategoryFilterDetails();
			var selectedArray = $$D("oldReviewCategoryNameSelected" + page).value.split(", ");
			var tempReviewCategoryNameArray = [];
			angular.forEach(reviewCategoryFilterDetails, function (value, index) {
				if (selectedArray.indexOf(value.reviewCategoryName.trim()) >= 0) {
					tempReviewCategoryNameArray.push(value.reviewCategoryDisplayName);
				}
			});
			reviewCategoryNameText = tempReviewCategoryNameArray.join(", ");
			$("#reviewCategoryNameSelected" + page).html(reviewCategoryNameText);
		}
		//
		$$D("reviewCategorySelected" + page).value = $("#oldReviewCategorySelected" + page).val();
		$("#reviewCategoryAddChangeTxt" + page).html("Change");
		// Populate 2nd level popup
		$('input[name=reviewCategory' + page + ']').removeAttr('checked');
		//var arr = !isBlankOrNull($$D("reviewCategoryNameSelected"+page).innerHTML) ? $$D("reviewCategoryNameSelected"+page).innerHTML.split(", ") : "";
		var arr = selectedArray;
		var arrId = !isBlankOrNull($$D("reviewCategorySelected" + page).value) ? $$D("reviewCategorySelected" + page).value.split(",") : "";
		for (var z = 0; z < arr.length; z++) {
			$("input[value='" + arrId[z] + "|" + arr[z] + "']").prop('checked', true);
		}
	}
	//Studymode
	if (!isBlankOrNull($$D("oldStudyModeNameValue" + page).value) && !isBlankOrNull($$D("oldsmTextKeyValue" + page).value)) {
		$("#studyModeNameSelected" + page).html($("#oldStudyModeNameValue" + page).val());
		$$D("smTextKeySelected" + page).value = $("#oldsmTextKeyValue" + page).val();
		$("#studyModeAddChangeTxt" + page).html("Change");
		// Populate 2nd level popup
		$('input[name=studyMode-radio' + page + ']').removeAttr('checked');
		if (!isBlankOrNull($$D("studyModeNameSelected" + page).innerHTML)) { $("input[value='" + $$D("smTextKeySelected" + page).value + "|" + $$D("studyModeNameSelected" + page).innerHTML + "']").prop('checked', true); }
	} else {
		$("input[value='all-study-types|All study types']").prop('checked', true);
	}
	//Previous qualification
	if (!isBlankOrNull($$D("oldPreviousQualText" + page).value) && !isBlankOrNull($$D("oldPreviousQualValue" + page).value) && !isBlankOrNull($$D("oldPreviousQualGradesValue" + page).value)) {
		$("#previousQualSel" + page).html($("#oldPreviousQualText" + page).val());
		$$D("previousQualGradesSelected" + page).value = $("#oldPreviousQualGradesValue" + page).val();
		$$D("previousQualSelected" + page).value = $("#oldPreviousQualValue" + page).val();
		$("#previousQualChangeTxt" + page).html("Change");
		// Populate 2nd level popup
		if (!isBlankOrNull($$D("pqSavedValue" + page).value)) { $("#selectGrades" + page).val($$D("pqSavedValue" + page).value); }
		else { $("#selectGrades" + page).val($$D("pqResetValue" + page).value); }
		if (fromPage == 'mySearchRootCtrl') {
			onChageQualGrades($$D("selectGrades" + page).value, 'preselect', page, 'mySearchRootCtrl');
		} else {
			onChageQualGrades($$D("selectGrades"+page).value,'preselect', page,'myProviderRootCtrl');
		}
		$("#previousQualErrMsg" + page).hide();

	}
	//Assessment Type filter
	if (!isBlankOrNull($$D("oldAssessmentTypeSelected" + page).value) && !isBlankOrNull($$D("oldAssessmentTypeNameSelected" + page).value)) {
		$("#assessmentTypeSel" + page).html($("#oldAssessmentTypeNameSelected" + page).val());
		$$D("assessmentTypeSelected" + page).value = $("#oldAssessmentTypeSelected" + page).val();
		$("#assessmentTypeChangeTxt" + page).html("Change");
		// Populate 2nd level popup
		$('input[name=assessment-radio' + page + ']').removeAttr('checked');
		if (!isBlankOrNull($$D("assessmentTypeSel" + page).innerHTML)) { $("input[value='" + $$D("assessmentTypeSelected" + page).value + "|" + $$D("assessmentTypeSel" + page).innerHTML + "']").prop('checked', true); }
	}

}
//
function flipFlop(index) {
	$("#percentageMatchBlock").html($('#back_' + index).html());
}
//		
function closeFlipFlop(index) {
	var arr = index.split("_"); index = arr[1];
	$("#flip-close_" + index).parents('body').find('#fl-card_' + index + ' #back_' + index + ',#fl-card_' + index + ' #front_' + index).removeClass('flip-in');
	$("#flip-close_" + index).parents('body').find('#fl-card_' + index + ' #back_' + index + ',#fl-card_' + index + ' #front_' + index).addClass('flip-out');
	$('#back_' + index).hide();
}
//
function setOrderByValue(text, val) {
	if (!isBlankOrNull(val)) { $("#orderByKeyValue").val(val); } else { $("#orderByKeyValue").val("BEST_MATCH"); }
	if (!isBlankOrNull(val)) { $("#selectedOrderByValueHidden").val(val); } else { $("#selectedOrderByValueHidden").val("BEST_MATCH"); }
	angular.element($$D('mySearchRootCtrl')).scope().searchResultsDetails(1, 6, 'orderby');
}
//Below methods for provider results page
//
function providerResultsPageURL(id, event, elem, flag) {
	if (flag == "percentPopUp") {
		var arr = id.split("_")[1];
		$("#percentageMatchUni").html($("#uni-nme-" + arr).val());
		$("#percentageMatchBlock").html($('#back_' + arr).html());
		$(".reviewCatOne").hide(); $(".reviewCatTwo").hide(); $(".reviewCatThree").hide();
		if (!isBlankOrNull($("#oldReviewCategoryNameSelected").val())) {
			var reviewArr = $("#oldReviewCategoryNameSelected").val().split(", ");


			var reviewCategoryFilterDetails = angular.element($$D('mySearchRootCtrl')).scope().getReviewCategoryFilterDetails();
			var selectedArray = $$D("oldReviewCategoryNameSelected").value.split(", ");
			var tempReviewCategoryArray = [];
			angular.forEach(reviewCategoryFilterDetails, function (value, index) {
				if (selectedArray.indexOf(value.reviewCategoryName.trim()) >= 0) {
					tempReviewCategoryArray.push(value.reviewCategoryDisplayName);
				}
			});
			for (var lp = 0; lp < reviewArr.length; lp++) {
				if (lp == 0 && !isBlankOrNull(reviewArr[0])) { $(".reviewCatOne").html("<span>" + tempReviewCategoryArray[0] + "</span>"); $(".reviewCatOne").show(); }
				else if (lp == 1 && !isBlankOrNull(reviewArr[1])) { $(".reviewCatTwo").html("<span>" + tempReviewCategoryArray[1] + "</span>"); $(".reviewCatTwo").show(); }
				else if (lp == 2 && !isBlankOrNull(reviewArr[2])) { $(".reviewCatThree").html("<span>" + tempReviewCategoryArray[2] + "</span>"); $(".reviewCatThree").show(); }
			}
		}
		angular.element($$D('mySearchRootCtrl')).scope().openPercentagePopUp();
		event.stopPropagation();
		return false;
	} else {
		var arr = id.split("_");
		var name = arr[0];
		var uniId = $("#uni-id-" + arr[1]).val();
		var uniName = $("#uni-nme-" + arr[1]).val();
		var tileMediaPath = elem.getAttribute('tile_media_path');
		if (name == 'fl-span') {
			flipFlop(id);
			event.stopPropagation();
		} else {
			angular.element($$D('mySearchRootCtrl')).scope().providerResultsPageURL(name, '', uniId, uniName, event, tileMediaPath);
		}
	}
}
//
function providerFlipFlop(id, index, event) {
	var arr = index.split("-"); index = arr[1];
	if (id == 'flip') {
		$("#percentageMatchPRBlock").html($('#target-' + index).html());
		$("#percentageMatchPRUni").html($("#course-nme-" + index).val());
		$(".reviewCatOnePR").hide(); $(".reviewCatTwoPR").hide(); $(".reviewCatThreePR").hide();
		if (!isBlankOrNull($("#reviewCategoryNameSelectedPR").html())) {
			var reviewArr = $("#reviewCategoryNameSelectedPR").html().split(", ");
			for (var lp = 0; lp < reviewArr.length; lp++) {
				if (lp == 0 && !isBlankOrNull(reviewArr[0])) { $(".reviewCatOnePR").html("<span>" + reviewArr[0] + "</span>"); $(".reviewCatOnePR").show(); }
				else if (lp == 1 && !isBlankOrNull(reviewArr[1])) { $(".reviewCatTwoPR").html("<span>" + reviewArr[1] + "</span>"); $(".reviewCatTwoPR").show(); }
				else if (lp == 2 && !isBlankOrNull(reviewArr[2])) { $(".reviewCatThreePR").html("<span>" + reviewArr[2] + "</span>"); $(".reviewCatThreePR").show(); }
			}
		}
		angular.element($$D('myProviderRootCtrl')).scope().openPercentagePopUp();
		event.stopPropagation();
		return false;
	} else if (id == 'course-details') {
		//window.location.replace('courseDetails.html');
		window.location.href = 'courseDetails.html';
	}
}
function providerCloseFlipFlop(id, index, event) {
	event.stopPropagation();
	var arr = index.split("-"); index = arr[1];
	if (id == 'flip') {
		$('#target-' + index).slideUp('fast');
		$('#cont-' + index).removeClass("pr_bg");
	} else if (id == 'course-details') {
		//window.location.replace('courseDetails.html');
		window.location.href = 'courseDetails.html';
	}
}
//
function preventDefault(event, thisId, rootCtrl, pageFlag) {
	if (rootCtrl == "mySearchRootCtrl") {
		sessionStorage.setItem("SR_Pagination", $("#nextSetResults").val());
		sessionStorage.setItem("SR_OrderBySelectBox", $("#orderBySelectBox").val());
	}
	if (isGuestUser()) {
		console.log("thisId", thisId)
		localStorage.setItem('action', "shortlist");
		localStorage.setItem('action_id', thisId);
		Constants.mainViewObj.router.loadPage({ url: "html/user/home.html", pushState: false });
	} else {
		var removeFrom = "";
		var addTo = "";
		if ($("#" + thisId).hasClass('fav_fill')) {
			removeFrom = "BOTH"; addTo = "";
		} else {
			removeFrom = ""; addTo = "SHORTLIST";
		}
		angular.element($$D(rootCtrl)).scope().getShortListFlag(thisId, removeFrom, addTo, rootCtrl, pageFlag);
	}
	event.stopPropagation();
	return false;
}

// Institution Page URL
function institutionPageURL(elem, page) {
	var university_id = elem.getAttribute('university_id');
	var university_name = elem.getAttribute('university_name');
	var eventCategory = 'Tile';
	if (page == 'SL') {
		eventCategory = 'Search';
	}
	firebaseEventTrack('viewitem', { origin: 'top nav', item_category: university_name.toLowerCase(), item_name: "not set", item_id: university_id, search_term: "na" });

	var tile_media_path = elem.getAttribute('tile_media_path');
	if (!isBlankOrNull(university_name) && !isBlankOrNull(university_id)) {
		localStorage.setItem('universityId', university_id);
		localStorage.setItem('universityName', university_name);
	}
	if (!isBlankOrNull(tile_media_path)) {
		localStorage.setItem('tileMediaPath', tile_media_path);
	} else {
		localStorage.setItem('tileMediaPath', "");
	}
	Constants.mainViewObj.router.loadPage('html/institution/ins_profile.html');
}
//
function doPageViewStats(id) {
	angular.element($$D(id)).scope().pageViewStats();
}
//
function cpeProfileInnerLinkWebClick(obj, collegeId, suborderItemId, networkId, clearing_flag = "") {
	//
	console.log(clearing_flag);
	if (clearing_flag === "CLEARING") {
		angular.element($$D('institutionhomeCtrl')).scope().appInteractionLogging('HOTCOURSES: CLEARING: WEBSITE CLICK', 'Overview : ' + obj.href, null, null);
	} else {
		angular.element($$D('institutionhomeCtrl')).scope().appInteractionLogging('HOTCOURSES: WEBSITE CLICK', 'Overview : ' + obj.href, null, null);
	}

	window.open(obj.href, '_system');
	//
}
//firebaseInteractionEventTracking
function firebaseInteractionEventTracking(applyNow, mode, action, collegeName, networkId) {
	// do nothing for now
}
//
function openParentModule(id) {
	var module = id.split("_");
	$(".moduleDiv").removeClass("acr_h2");
	if ($("#innerDiv_" + module[1]).hasClass("dispon")) {
		$("#innerDiv_" + module[1]).removeClass("dispon");
		$("#outerDiv_" + module[1]).removeClass("acr_h2");
	} else {
		$(".acr_cnt").removeClass("dispon");
		$("#innerDiv_" + module[1]).addClass("dispon");
		$("#outerDiv_" + module[1]).addClass("acr_h2");
	}
	var moduleId = $("#" + id).parents('.pr_dtl').parent().attr('id');
	if (module[1] > 1) { $('#courseDetailsRootCtrl .page-content').scrollTop(120, 0); }
	else { $('#courseDetailsRootCtrl .page-content').scrollTop(0, 0); }
	var mId = moduleId.split("_");
	$("#courseDetailsRootCtrl #extraSpace_" + mId[1]).hide();
	if (($('#courseDetailsRootCtrl .gallery_nav-' + mId[1]).height()) < 550) {
		$("#courseDetailsRootCtrl #extraSpace_" + mId[1]).show();
	}
	if ($("#courseDetailsRootCtrl #extraSpace_" + mId[1]).css('display') == "none") {
		$("#mainSection .swiper-wrapper").height($("#courseDetailsRootCtrl .gallery_nav-" + mId[1]).height() + 25);
	} else {
		$("#mainSection .swiper-wrapper").height($("#courseDetailsRootCtrl .gallery_nav-" + mId[1]).height() + 200);
	}
}

function drawCdAssesseddoughnutchart(value1, value2, value3, currentElement) {
	var limit1 = parseInt(value1);
	var limit2 = parseInt(value2);
	var limit3 = parseInt(value3);
	var limit4 = 100 - (parseInt(value1) + parseInt(value2) + parseInt(value3));
	if ((parseInt(value1) + parseInt(value2) + parseInt(value3)) < 100) {
		$("#" + currentElement).drawDoughnutChart([
			{
				value: limit1,
				color: "#039BE5",
				prcct: "0"
			},
			{
				value: limit2,
				color: "#FC7169",
				prcct: "0"
			},
			{
				value: limit3,
				color: "#A85265",
				prcct: "0"
			},
			{
				value: limit4,
				color: "#dededf"
			}
		]);
	} else {
		$("#" + currentElement).drawDoughnutChart([
			{
				value: limit1,
				color: "#039BE5",
				prcct: "0"
			},
			{
				value: limit2,
				color: "#FC7169",
				prcct: "0"
			},
			{
				value: limit3,
				color: "#A85265",
				prcct: "0"
			}
		]);
	}
}
function drawCddoughnutchart(index, currentElement, colour) {
	var limit1 = $$D(index).value;
	limit1 = parseInt(limit1);
	limit2 = 100 - limit1;
	limit2 = parseInt(limit2);
	$("#" + currentElement).drawDoughnutChart([
		{
			value: limit1,
			color: colour
		},
		{
			value: limit2,
			color: "#dededf"
		}
	]);
}
//
function courseDetailsPageURL(id, event) {
	var arr = id.split("-");
	var courseId = $("#course-id-" + arr[1]).val();
	var courseName = $("#course-nme-" + arr[1]).val();
	var collegeId = $("#college-id").val();
	event.stopPropagation();
	removeViewAllFlag();
	angular.element($$D('myProviderRootCtrl')).scope().courseDetailsPageURL(courseId, collegeId, courseName, event);
}

function UniRandomImage() {
	var imagePath = "img/unis/images/"
	var myImages = ["non_ad_tile_1.png", "non_ad_tile_2.png", "non_ad_tile_3.png", "non_ad_tile_4.png", "non_ad_tile_5.png"];
	var randomIndexHomeUniTile = sessionStorage.getItem("RANDOM_INDEX_HOME_UNI_TILE");
	if (isBlankOrNull(randomIndexHomeUniTile)) {
		randomIndexHomeUniTile = 0;
	} else {
		if (parseInt(randomIndexHomeUniTile) < 4) {
			randomIndexHomeUniTile = parseInt(randomIndexHomeUniTile) + 1;
		} else {
			randomIndexHomeUniTile = 0;
		}
	}
	sessionStorage.setItem("RANDOM_INDEX_HOME_UNI_TILE", randomIndexHomeUniTile);
	return imagePath + myImages[randomIndexHomeUniTile];
}

function searchResultsUniRandomImage() {
	var imagePath = "img/unis/images/"
	var myImages = ["sr_uni_bg_img1.png", "sr_uni_bg_img2.png", "sr_uni_bg_img3.png", "sr_uni_bg_img4.png", "sr_uni_bg_img5.png"];
	var randomIndexSrTile = sessionStorage.getItem("RANDOM_INDEX_SR_TILE");
	if (isBlankOrNull(randomIndexSrTile)) {
		randomIndexSrTile = 0;
	} else {
		if (parseInt(randomIndexSrTile) < 4) {
			randomIndexSrTile = parseInt(randomIndexSrTile) + 1;
		} else {
			randomIndexSrTile = 0;
		}
	}
	sessionStorage.setItem("RANDOM_INDEX_SR_TILE", randomIndexSrTile);
	return imagePath + myImages[randomIndexSrTile];
}
//
function checkPostCode(toCheck) {
	var alpha1 = "[abcdefghijklmnoprstuwyz]";                       // Character 1
	var alpha2 = "[abcdefghklmnopqrstuvwxy]";                       // Character 2
	var alpha3 = "[abcdefghjkpmnrstuvwxy]";                         // Character 3
	var alpha4 = "[abehmnprvwxy]";                                  // Character 4
	var alpha5 = "[abdefghjlnpqrstuwxyz]";                          // Character 5
	var BFPOa5 = "[abdefghjlnpqrst]";                               // BFPO alpha5
	var BFPOa6 = "[abdefghjlnpqrstuwzyz]";                          // BFPO alpha6
	var pcexp = new Array();
	pcexp.push(new RegExp("^(bf1)(\\s*)([0-6]{1}" + BFPOa5 + "{1}" + BFPOa6 + "{1})$", "i"));
	pcexp.push(new RegExp("^(" + alpha1 + "{1}" + alpha2 + "?[0-9]{1,2})(\\s*)([0-9]{1}" + alpha5 + "{2})$", "i"));
	pcexp.push(new RegExp("^(" + alpha1 + "{1}[0-9]{1}" + alpha3 + "{1})(\\s*)([0-9]{1}" + alpha5 + "{2})$", "i"));
	pcexp.push(new RegExp("^(" + alpha1 + "{1}" + alpha2 + "{1}" + "?[0-9]{1}" + alpha4 + "{1})(\\s*)([0-9]{1}" + alpha5 + "{2})$", "i"));
	pcexp.push(/^(GIR)(\s*)(0AA)$/i);
	pcexp.push(/^(bfpo)(\s*)([0-9]{1,4})$/i);
	pcexp.push(/^(bfpo)(\s*)(c\/o\s*[0-9]{1,3})$/i);
	pcexp.push(/^([A-Z]{4})(\s*)(1ZZ)$/i);
	pcexp.push(/^(ai-2640)$/i);
	var postCode = toCheck;
	var valid = false;
	for (var i = 0; i < pcexp.length; i++) {
		if (pcexp[i].test(postCode)) {
			pcexp[i].exec(postCode);
			postCode = RegExp.$1.toUpperCase() + " " + RegExp.$3.toUpperCase();
			postCode = postCode.replace(/C\/O\s*/, "c/o ");
			if (toCheck.toUpperCase() == 'AI-2640') { postCode = 'AI-2640' };
			valid = true;
			break;
		}
	}
	if (valid) { return postCode; } else return false;
}
//
function returnKey(key) {
	//alert(key);
	if (Constants.devicePlatform == 'IOS') {
		Keyboard.returnKeyType(key);
	}
}
//
function googleTagManagerPageView(pageUrl) {
	console.log('page view > ' + pageUrl);
	// TODO - Uncomment this when building
	/* alert(Constants.googleTagManagerTrackId);
	var tagManager = cordova.require('com.jareddickson.cordova.tag-manager.TagManager');
	//var trackingId = 'GTM-58FM5GD';
	var trackingId = Constants.googleTagManagerTrackId;
	var intervalPeriod = 10; // seconds
	// Initialize Tag Manager
	tagManager.init(function() {}, null, trackingId, intervalPeriod);
	// Track Page
	tagManager.trackPage(function() {}, function(){}, pageUrl);
	*/
}
function firebaseEventTrack(eventName, parameter) {
	console.log('event log > ', eventName, ' > ', parameter);

	//cordova.plugins.firebase.analytics.logEvent(eventName, parameter);

}
function statusBarFontColor(fontColor) {
	console.log("statusbar color > " + fontColor)
	//
	if (fontColor.toLowerCase() == "black" && Constants.devicePlatform == 'IOS') {
		StatusBar.styleDefault();
	}
	if (fontColor.toLowerCase() == "white" && Constants.devicePlatform == 'IOS') {
		StatusBar.styleLightContent();
	}
	//
}
function statusBarBackgroundColor(backgroundColor) {
	if (backgroundColor != "")
		StatusBar.backgroundColorByName(backgroundColor);
}
function getDate(openDate) {
	if (!isBlankOrNull(openDate)) {
		var openDateArray = openDate.split(" ");
		openDateArray[0] = openDateArray[0].replace(/[^0-9\.]+/g, "");
		var month = getMonth(openDateArray[1]);
		return month + "/" + openDateArray[0] + "/" + openDateArray[2];
	}
}
function getMonth(month) {
	var months = { Jan: 1, Feb: 2, Mar: 3, Apr: 4, May: 5, Jun: 6, Jul: 7, Aug: 8, Sep: 9, Oct: 10, Nov: 11, Dec: 12 };
	var monthNum = months[month];
	return monthNum;
}

function onOffEmail(CheckId) {
	if (CheckId == "permitEmail" && $$D(CheckId).checked == true) {
		$("#marketingEmail").prop('checked', false); $("#marketingEmail").val('N');
		$("#solusEmail").prop('checked', false); $("#solusEmail").val('N');
		$("#receiveNoEmailFlag").prop('checked', false); $("#receiveNoEmailFlag").val('Y');
		$("#surveyFlag").prop('checked', false); $("#surveyFlag").val('N');
		//if($(".email")) { $(".email").addClass("disabled");  }
		//if($(".emailPref")) { $(".emailPref").addClass("disabled");  }
		$('#remainderMailText').show();
	} else if (CheckId == "permitEmail" && $$D(CheckId).checked == false) {
		$("#marketingEmail").prop('checked', true); $("#marketingEmail").val('Y');
		$("#solusEmail").prop('checked', true); $("#solusEmail").val('Y');
		$("#receiveNoEmailFlag").prop('checked', true); $("#receiveNoEmailFlag").val('N');
		$("#surveyFlag").prop('checked', true); $("#surveyFlag").val('Y');
		//if($(".email")) { $(".email").removeClass("disabled");  }
		//if($(".emailPref")) { $(".emailPref").removeClass("disabled");  }
		$('#remainderMailText').hide();
	} else if (CheckId != "permitEmail" && $$D(CheckId).checked == false) {
		//$("#permitEmail").prop('checked', true);
		//$("#permitEmail").val('Y');
	}
}
//
function removeErrorMsg(id) {
	if ($$D(id)) { $$D(id).innerHTML = ""; }
}
//
function saveUserDetailsValidation() {
	var message = true;
	var firstName = trimString($("#first-name").val());
	var lastName = trimString($("#last-name").val());
	var email = trimString($("#email").val());
	var dateOfBirth = trimString($$D("dateOfBirth").value);
	var message = true;
	if (isEmpty(firstName)) {
		message = false; $$D("firstnameErrMsg").innerHTML = ErrorMessage.error_signup_name;
	} else if (!validateUserName(firstName)) {
		message = false; $$D("firstnameErrMsg").innerHTML = ErrorMessage.error_signup_name;
	} else { $$D("firstnameErrMsg").innerHTML = ""; }
	if (isEmpty(lastName)) {
		message = false; $$D("lastNameErrMsg").innerHTML = ErrorMessage.error_signup_name;
	} else if (!validateUserName(lastName)) {
		message = false; $$D("lastNameErrMsg").innerHTML = ErrorMessage.error_signup_name;
	} else { $$D("lastNameErrMsg").innerHTML = ""; }
	if (isEmpty(email)) {
		message = false;
		$$D("emailErrMsg").innerHTML = ErrorMessage.error_user_email;
	} else if (!checkValidEmail(email)) { message = false; $$D("emailErrMsg").innerHTML = ErrorMessage.error_signup_email; }
	else { $$D("emailErrMsg").innerHTML = ""; }
	if (!isEmpty(isValidDate(dateOfBirth))) {
		message = false; $("#loginErrMsgDate").html(isValidDate(dateOfBirth)); $("#loginErrMsgDate").show();
	} else { $("#loginErrMsgDate").hide(); }
	return message;
}
function validatePassword(password, id) {
	if (isEmpty($$D(password).value)) {
		$$D(id).innerHTML = ErrorMessage.error_user_password; $("#" + id).show();
	} else {
		$$D(id).innerHTML = ""; $("#" + id).hide();
	}
}
function reserveopendayPopup() {
	var myappLocal = Constants.myapp;
	myappLocal.popup('#institutionhomeCtrl .reserve_page');

}
function courseDetailsReserveopendayPopup() {
	var myappLocal = Constants.myapp;
	myappLocal.popup('#courseDetailsRootCtrl .reserve_page');
}

function emailUniPopup(num) {
	var myappLocal = Constants.myapp;
	myappLocal.popup('.second_page');
	//$('.snd_lnk').hide();
	if ($("#tab" + num).hasClass("active")) {
		$('.second_page .tabs').addClass("trnsfrm");
	}
	if ($("#tab" + (num + 1)).hasClass("active")) {
		$('.second_page .tabs').addClass("trnsfrm");
	}
}
//
function prospectusPopup(num) {
	var myappLocal = Constants.myapp;
	myappLocal.popup('.req_props');
	//
	if ($("#tab" + num).hasClass("active")) {
		$('.req_props .tabs').addClass("trnsfrm");
	}
	if ($("#tab" + (num + 1)).hasClass("active")) {
		$('.req_props .tabs').addClass("trnsfrm");
	}
	if ($('.done_btn')) { $('.done_btn').show(); }
	//
}
//
function openSearchAdders(num) {
	//myApp.popup('.req_props');
	$('#tab' + num + 'cont').show();
	$('#tab' + (num + 1) + 'cont').hide();
	//$('.search_address .tabs #tab3').addClass("active");
	//$('.search_address .tabs #tab4').removeClass("active");
	var myappLocal = Constants.myapp;
	myappLocal.popup('.req_props');
	if ($("#tab" + (num + 1)).hasClass("active")) {
		$('.search_address .tabs').addClass("trnsfrm");
	}
	if ($("#tab" + num).hasClass("active")) {
		$('.search_address .tabs').addClass("trnsfrm");
	}
	$('.search_address').removeClass("trns_lft");
	$('.search_address').removeClass("trnsfrm_fade");
	$('.search_address').addClass("trnsfrm_fade");
	//if($('.done_btn')) { $('.done_btn').show(); } 
	/*if($$D('addressAjax')){		
		$$D('addressAjax').focus();
	}*/
	clearSearch();
}
//
function clearSearch() {
	if ($$D('addressAjax')) {
		$$D('addressAjax').value = '';
		$$D('xmark').className = '';
		$('#ajaxList').css('display', 'none');
	}
}
//
function backToTab(id, num) {
	if ($$D('mutlipleOpportunities') !== null) {
		$("#mutlipleOpportunities").css('display', 'block');
	}

	returnKey("search");
	if (Constants.devicePlatform == 'IOS') {
		if (Keyboard.isVisible) {
			setTimeout(function () {
				Keyboard.hide();
			}, 100);
		}
	} else {
		setTimeout(function () {
			//cordova.plugins.Keyboard.close();
			Keyboard.close();
		}, 10);


	}
	if (id == "req") {
		//$("#tab1").addClass("active");
		//$("#tab1").removeClass("active");
		$('.req_props .tabs').addClass("trnsfrm");
		$('.cls_tab' + num).hide();
		$('.cls_req_props').show();
		if ($('.prospectus')) { $('.prospectus').show(); }
	} else if (id == "email") {
		$('.second_page .tabs').addClass("trnsfrm");
		$('.cls_tab' + num).hide();
		$('.cls_email').show();
		$('#sendButton').addClass("gry");
		if ($('.enq')) { $('.enq').show(); }
	} else {
		//$("#tab3").addClass("active");
		//$("#tab3").removeClass("active");
		$('.search_address .tabs').addClass("trnsfrm");
		$('#tab' + (num + 1) + 'cont').hide();
		$('#tab' + num + 'cont').show();
	}
	$('.search_address').removeClass('trns_lft');
	$('.search_address').addClass('trnsfrm_fade');
}
function isValidDate(dateString) {
	console.log("came");
	if ($("#date_txt_1").val() != "" && $("#dateOfBirth").val() == "" && Constants.devicePlatform == 'IOS') { //&& $("#dateOfBirth").val()==""
		//$("#dateOfBirth").val($("#date_txt").html());
		var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

		//$("#dateOfBirth").val($scope.userInfoList[0].dateOfBirth); 
		var datePickerValue = $("#date_txt_1").val();
		var datePickerArray = datePickerValue.split(" ");
		var datePickerYear = datePickerArray[2];
		var datePickerMonth = datePickerArray[1];
		var datePickerDate = datePickerArray[0];
		var index = monthNames.indexOf(datePickerMonth);
		//var day = ("0" + date.getDate()).slice(-2) ;
		index = parseInt(index) + parseInt(1);
		index = ("0" + index).slice(-2);
		var dateFormat = datePickerYear + " " + index + " " + datePickerDate;
		//$("#dateOfBirth").val(dateFormat);
		$("#date_txt").html(dateFormat);
		$("#dateOfBirth").val($("#date_txt_1").val());
	}
	var msg = "";
	if ($("#date_txt_1").val() != "") { //&& Constants.devicePlatform == 'ANDROID'
		var d = new Date();
		var minDateAndroid = parseInt(d.getFullYear()) - 100 + "-" + ("0" + (parseInt(d.getMonth()) + 1)).slice(-2) + "-" + ("0" + (parseInt(d.getDate()))).slice(-2);
		var maxDateAndroid = parseInt(d.getFullYear()) - 14 + "-" + ("0" + (parseInt(d.getMonth()) + 1)).slice(-2) + "-" + ("0" + (parseInt(d.getDate()))).slice(-2);

		var checkMinDateAndroid = new Date(minDateAndroid);
		var checkmaxDateAndroid = new Date(maxDateAndroid);
		var timestamp = new Date($("#dateOfBirth").val()).getTime();

		var minDateInMilliSeconds = checkMinDateAndroid.getTime();
		var maxDateInMilliSeconds = checkmaxDateAndroid.getTime();


		if (minDateInMilliSeconds > timestamp) {
			msg = "Sorry, whatuni doesn't allow users over the age of 100 to register";
		}

		if (maxDateInMilliSeconds < timestamp) {
			msg = "Sorry, whatuni does not allow users under the age of 13 to register";
		}
	}
	/* var dateArray = dateString.split("-");        
	 day = dateArray[2];
	 month = dateArray[1];
	 year = dateArray[0];  
	 if(year < 1900) { msg = "Sorry, whatuni doesn't allow users over the age of 110 to register"; }
	 var today = new Date();                 
	 var age = today.getFullYear() - year;
	 var m = today.getMonth() - month;
	 if (m < 0 || (m === 0 && today.getDate() < day)) { age--; }
	 if((age == 13 && month <= today.getMonth() && day <= today.getDate()) || age < 14){
	   msg = "Sorry, whatuni does not allow users under the age of 13 to register";
	   return msg;
	 }*/
	return msg;
};
function getCurrentDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1;
	var yyyy = today.getFullYear();
	if (dd < 10) { dd = '0' + dd }
	if (mm < 10) { mm = '0' + mm }
	today = yyyy + '-' + mm + '-' + dd; return today;
}
function validateDate(id) {
	if (!isEmpty(isValidDate($("#" + id).val()))) {
		message = false; $("#loginErrMsgDate").html(isValidDate($("#" + id).val())); $("#loginErrMsgDate").show();
	} else { $("#loginErrMsgDate").hide(); }
}
function clearDOBErrMsg(id) { $("#" + id).html(""); }

function clearFilter() {
	localStorage.removeItem('recentSearchCriteria');
	localStorage.removeItem('subject');
	/*localStorage.removeItem('categoryId'); 
	localStorage.removeItem('categoryCode');
	localStorage.removeItem('subject');*/
}

function getCourseList(searchId) {
	angular.element($$D('quizCtrl')).scope().getCourseList(searchId);
}
function getJobOrIndustryList(searchId) {
	angular.element($$D('quizCtrl')).scope().getJobOrIndustryList(searchId);
}
function saveSubject(divId) {
	//$("#mainSection").removeAttr("style");
	var index = divId.split("_")[1];
	var value = $("#course_list_" + index).val();
	var courseDetails = value.split("|");
	var subjectName = courseDetails[0];
	var questionId = $("#questionId").val();
	var nextQuestionId = $("#nextQuestionId").val();
	$(".subjectInpAjx").hide();
	$("#searchCategoryCode").val(courseDetails[2]);
	$("#searchCategoryId").val(courseDetails[1]);
	$("#keywordSearch").val(subjectName);
	$("#searchsubject").hide();
	$('#searchResultList').remove();
	if (!isBlankOrNull($("#filterName").val())) {
		var subDiv = $("#filterName").val().toLowerCase();
		$("#" + subDiv).val(courseDetails[1]);
	}
	setTimeout(function () { $("#mainSection").append('<div class="cht_usr" id="answer_' + questionId + '">' + subjectName + '</div>'); subSaveSubject(questionId, subjectName); }, 500);
}
function subSaveSubject(questionId, subjectName, jacsSubject) {
	if (!isBlankOrNull($("#answerOpt_0").html())) {
		setTimeout(function () {
			$("#mainSection").append('<div class="cht_bot"><div class="cht_ico"></div><div id="searchLoad' + questionId + '" class="cht_msg cht_min"><img src="img/cht_loader.gif" alt="" width="40"></div></div>');
			$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
		}, 500);
		var timeout = 2000;
		setTimeout(function () {
			$("#searchLoad" + questionId).html('<span">' + subjectName + '</span>' + $("#answerOpt_0").html());
			if (jacsSubject != "jacsSubject") {
				for (var i = 1; i < parseInt($("#answerOptLength").val()); i++) {
					timeout += 1000;
					$("#mainSection").append('<div class="cht_bot cht_snd"><div class="cht_msg cht_min">' + $("#answerOpt_" + i).html() + '</div></div>');
					$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
				}
			}
		}, timeout);
	}
	setTimeout(function () { angular.element($$D('quizCtrl')).scope().getQuizDetails($("#nextQuestionId").val(), '', '', questionId); }, timeout);
}
//Added for Nov-01-2017 REL
function saveJobOrIndustry(divId) {
	//$("#mainSection").removeAttr("style");
	var index = divId.split("_")[1];
	var value = $("#job_list_" + index).val();
	var jobOrIndustryDetails = value.split("|");
	var jobOrIndustryName = jobOrIndustryDetails[0];
	var questionId = $("#questionId").val();
	var nextQuestionId = $("#nextQuestionId").val();
	if (jobOrIndustryDetails[1] == "0") { nextQuestionId = $("#multipleNextQuestionId").val(); }
	$(".subjectInpAjx").hide();
	$('#jobOrIndustryResultList').remove();
	$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
	$("#jobOrIndustryName").val(jobOrIndustryName);
	setTimeout(function () { $("#mainSection").append('<div class="cht_usr" id="answer_' + questionId + '">' + jobOrIndustryName + '</div>'); angular.element($$D('quizCtrl')).scope().getQuizDetails(nextQuestionId, jobOrIndustryDetails[1] + ',' + jobOrIndustryDetails[2], '', questionId); }, 500);
	//	
}
//
var tempId = "";
function selectQualification(divId, flag) {
	$("#selectAns").removeAttr("disabled");
	if (flag == "locType" && $("#filterName").val().toLowerCase() == 'your preference') {
		//$('#'+divId).find('input').prop("checked", true);				
		setTimeout(function () {
			var typeLength = $("input[name='locationType']:checked").length;
			if (typeLength > 3) { $('#' + divId).find('input').prop("checked", false); }
			if (typeLength == 0) { $("#selectAns").attr("disabled", "disabled"); }
		}, 5);
		$("#filterFlag").val('locType');
	} else if (tempId != divId) {
		tempId = divId;
		var qualDetails = "";
		if (flag == "location") {
			$("#filterFlag").val('location');
		} else if (flag == "locType") {
			$("#filterFlag").val('locType');
			setTimeout(function () {
				var typeLength = $("input[name='locationType']:checked").length;
				if (typeLength == 0) { $("#selectAns").attr("disabled", "disabled"); }
			}, 5);
		} else if (flag == "region") {
			var sval = $("#" + divId).val();
			if (!isBlankOrNull(sval)) { $("#region").val($("#" + divId).val().split("|")[1]); $("#regionFlag").val("Y"); }
			else { $("#region").val("ENGLAND"); $("#regionFlag").val("N"); }
			$("#" + divId).hide();
			if (!isBlankOrNull(sval)) { $("#mainSection").append('<div class="cht_usr" id="answer_' + $("#questionId").val() + '">' + $("#" + divId).val().split("|")[1] + '</div>'); }
			else { $("#mainSection").append('<div class="cht_usr">All England</div>'); }
			angular.element($$D('quizCtrl')).scope().getQuizDetails($("#nextQuestionId").val(), '', '', $("#questionId").val());
		} else if (divId == 'single_select') {
			$("#filterFlag").val('single_select');
		}
		if ($("#filterName").val() == "Qualification") {
			var index = divId.split("_")[1];
			var value = $("#qualHidden_" + index).val();
			qualDetails = value.split("|");
			$("#qualification").val(qualDetails[1]);
			$("#qualificationName").val(qualDetails[0]);
			$("#nextQuestionId").val(qualDetails[2]);
			$("#mainSection").append('<div class="cht_usr" id="answer_' + $("#questionId").val() + '">' + qualDetails[0] + '</div>');
			angular.element($$D('quizCtrl')).scope().getQuizDetails($("#nextQuestionId").val(), '', '', $("#questionId").val());
			$("#question_" + $("#questionId").val() + "_q").remove();
		}
	}
}
//
function selectSubjectGrade(divId, flag) {
	$("#selectAns").removeAttr("disabled");
	if (tempId != divId) {
		tempId = divId;
		if (flag == "grade") {
			var sval = $("#" + divId).val();
			$("#" + divId).hide();
			if (!isBlankOrNull(sval)) {
				$("#mainSection").append('<div class="cht_usr" id="answer_' + $("#questionId").val() + '">' + $("#" + divId).val().split("|")[1] + '</div>');

			}
			previousQualGradeCalculation($("#" + divId).val().split("|")[1]);
			$("#yesNoDiv").show();
			var height = $("#mainSection").outerHeight() + $("#yesNoDiv").outerHeight();
			$("#mainSection").css({ "height": height });
			$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
			if (!isBlankOrNull($("#filterName").val())) {
				var subDiv = $("#filterName").val().toLowerCase();
				$("#" + subDiv).val($("#" + divId).val().split("|")[1]);
			}
		}
	}
}
//
function onChageQualGradesQuiz(qualification, gradeStr, gradeLevel) {
	if ($$D("dynamicQualList")) {
		$$D("dynamicQualList").innerHTML = '';
		var noOfGrades = gradeStr.split(',').length;
		var qualGrades = gradeStr.split(',');
		var grade = 0;
		$$D("numberOfGrades").value = noOfGrades;
		for (var z = 0; z < noOfGrades; z++) {
			if (!isBlankOrNull(gradeStr)) {
				var newFieldValue = '<li><h3 id="gradeTitle_' + z + '">' + qualGrades[z] + '</h3><span class="toggle"><span class="minus" onclick="increaseDecreaseGrades(' + z + ',\'decrease\', \'\', \'\');"> - </span><span class="blu_box" id="gradeValue_' + z + '">' + grade + '</span><span class="plus" onclick="increaseDecreaseGrades(' + z + ',\'increase\', \'\', \'\');"> + </span></span>';
				newFieldValue += '<input type="hidden" name="points_' + z + '" id="points_' + z + '" maxlength="3" value="' + gradeLevel + '"></li>';
				$$D('dynamicQualList').innerHTML += newFieldValue;
				$('#previousQualSavBtn').attr("disabled", "disabled");
			}
		}
		if (isBlankOrNull(gradeStr)) {
			var newFieldValue = '<li class="tps">';
			if (preSelect == 'preselect' && !isBlankOrNull($$D("pqTariffSavedValue").value)) {
				newFieldValue += '<input type="number" placeholder="Enter tariff point" name="points_TP" id="points_TP" maxlength="3" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" onblur="validateSelectedGradesShow()" value="' + $$D("pqTariffSavedValue").value + '"></li>';
			} else {
				newFieldValue += '<input type="number" placeholder="Enter tariff point" name="points_TP" id="points_TP" value="" maxlength="3" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" onblur="validateSelectedGradesShow()"></li>';
			}
			$$D('dynamicQualList').innerHTML = newFieldValue;
			$('#previousQualSavBtn').removeAttr("disabled");
		}
	}
}
//
function skipQuestionFn() {
	$("#skipQuesDiv").hide(); $("#skipAndViewResults").show();
	var height = $("#mainSection").height();
	$("#mainSection").css({ "height": height + 100 });
	$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
}
//
function openAndCloseTrackingDiv(id) {
	if ($$D(id)) {
		if ($$D(id).style.display == 'none') {
			$$D(id).style.display = 'block';
		} else {
			$$D(id).style.display = 'none';
		}
	}
}
//
function undoFacebookFlag() {
	if (!isBlankOrNull(localStorage.getItem('SIGNUP_FROM_FACEBOOK'))) {
		localStorage.removeItem('SIGNUP_FROM_FACEBOOK');
	}
}
//
function removeDisabledProfileSaveButton() {
	if ($('#edit-profile-save')) { $('#edit-profile-save').removeAttr("disabled"); }
}
//
function addDisabledProfileSaveButton() {
	if ($('#edit-profile-save')) { $('#edit-profile-save').attr("disabled"); }
}
//
function removeViewAllFlag() {
	if (!isBlankOrNull(localStorage.getItem('VIEW_COURSES_FLAG'))) { localStorage.removeItem('VIEW_COURSES_FLAG'); }
}
//
function goToAppStore() {
	if (Constants.devicePlatform == 'IOS') {
		window.open(encodeURI("https://itunes.apple.com/gb/app/whatuni-university-degrees-uk/id1267341390?mt=8"), '_system');

	}
	if (Constants.devicePlatform == 'ANDROID') {
		window.open(encodeURI("https://play.google.com/store/apps/details?id=com.hotcourses.group.wuapp"), '_system');
	}
}
//
function goToWhatuni() {
	window.open(encodeURI("https://www.whatuni.com"), '_system');
}
//
function mailTo() {
	window.open(encodeURI("mailto:support.team@whatuni.com"), '_system');
}
//
function goToWhatuniPartner() {
	window.open(encodeURI("https://www.whatuni.com/partner/scouts/"), '_system');
}
//
function mutlipleOpportunities(opportunityName, opportunityId) {
	angular.element($$D('courseDetailsRootCtrl')).scope().getCourses('', opportunityId, opportunityName);
	$("#multiOppName").val(opportunityName);
	$("#multiOppId").val(opportunityId);
}
//
function otherOpendays(eventId) {
	//console.log(eventId);
	angular.element($$D('institutionhomeCtrl')).scope().reservePlace(eventId, null);
}
function otherCourseDetailsOpendays(eventId) {
	//console.log(eventId);
	angular.element($$D('courseDetailsRootCtrl')).scope().reservePlace(eventId, null);
}
//
//
function showVideo(id) {
	var video = $$D('video_' + id);
	var playIcon = $$D('play_' + id);
	if (playIcon) { playIcon.style.display = 'none'; }
	if (video) { video.play(); }
}
//
function showVideoClearing(id) {
	var video = $$D('video_clearing_' + id);
	var playIcon = $$D('play_clearing_' + id);
	if (playIcon) { playIcon.style.display = 'none'; }
	if (video) { video.play(); }
}
//
function applyIconOnPause() {
	console.log($('video'));
	$('video').each(function () {
		var vid = $(this).attr('id');
		$$D(vid).onpause = function () {
			var id = (vid).replace('video_', '');
			if ($$D('play_' + id)) { $$D('play_' + id).style.display = 'block'; }
		}
		//
	});
}
//
function applyIconOnPauseOpenday() {
	console.log($('video'));
	$('video').each(function () {
		var vid = $(this).attr('id');
		$$D(vid).onpause = function () {
			var id = (vid).replace('opendayvideo_', '');
			if ($$D('opendayplay_' + id)) { $$D('opendayplay_' + id).style.display = 'block'; }
		}
		//
	});
}
//
function showOpendayVideo(id) {
	var video = $$D('opendayvideo_' + id);
	var playIcon = $$D('opendayplay_' + id);
	if (playIcon) { playIcon.style.display = 'none'; }
	if (video) { video.play(); }
}
//
function previousQualGradeCalculation(grade1) {
	var tempQualGrades = $("#tempQualGrades").val() + "-";
	var gradeIndex = tempQualGrades.indexOf(grade1 + "-");
	var newGradeIndex = gradeIndex - 1;
	var incrementValue = parseInt(tempQualGrades.charAt(newGradeIndex)) + 1;
	var replacedString = tempQualGrades.substr(0, newGradeIndex) + incrementValue + tempQualGrades.substr(newGradeIndex + 1);
	replacedString = replacedString.substring(0, replacedString.length - 1);
	$("#tempQualGrades").val(replacedString);
}
//
function decodeUsingTextArea(label) {
	var textArea = document.createElement('textarea');
	textArea.innerHTML = label;
	return textArea.value;
}

function otherUniNullCheck(suggestionArray) {
	for (var prop in suggestionArray) {
		if (isBlankOrNull(suggestionArray[prop].collegeId)) {
			suggestionArray.splice(prop, 1);
		}
	}
	return suggestionArray;
}
//
function reviewAndRating(isReview) {
	if (localStorage.getItem("reviewAndRatingCount") > 2 && isReview) {
		var currentCountDown = JSON.parse(localStorage.counter).countdown;
		var applicationVersion = JSON.parse(localStorage.counter).applicationVersion;
		AppRate.preferences = {
			displayAppName: 'Whatuni',
			usesUntilPrompt: 2,
			promptAgainForEachNewVersion: true,
			inAppReview: true,
			simpleMode: true,
			storeAppURL: {
				ios: '1267341390',
				android: 'https://play.google.com/store/apps/details?id=com.hotcourses.group.wuapp',
				windows: '',
				blackberry: '',
				windows8: ''
			},
			callbacks: {
				onButtonClicked: function (buttonIndex) {

					//alert("onButtonClicked -> " + buttonIndex+" countdown -> " + JSON.parse(localStorage.counter).countdown);
					var counter = { countdown: 0, applicationVersion: applicationVersion };
					counter.countdown = currentCountDown;
					localStorage.setItem("counter", JSON.stringify(counter));
					//alert("onButtonClicked -> " + buttonIndex+" countdown -> " + JSON.parse(localStorage.counter).countdown);
				}
			},
			customLocale: {
				title: "Would you mind rating %@?",
				message: "It won't take more than a minute and helps to promote our app. Thanks for your support!",
				cancelButtonLabel: "No, Thanks",
				laterButtonLabel: "Remind Me Later",
				rateButtonLabel: "Rate It Now",
			}
		};

		AppRate.promptForRating(true)
	}
	if (!isReview) {
		AppRate.preferences = {
			displayAppName: 'Whatuni',
			usesUntilPrompt: 2,
			promptAgainForEachNewVersion: true,
			inAppReview: true,
			simpleMode: true,
			storeAppURL: {
				ios: '1267341390',
				android: 'https://play.google.com/store/apps/details?id=com.hotcourses.group.wuapp',
				windows: '',
				blackberry: '',
				windows8: ''
			},
			customLocale: {
				title: "Would you mind rating %@?",
				message: "It won't take more than a minute and helps to promote our app. Thanks for your support!",
				cancelButtonLabel: "No, Thanks",
				laterButtonLabel: "Remind Me Later",
				rateButtonLabel: "Rate It Now",
			}
		};
		AppRate.promptForRating(false);
	}
}
//
function getTranslateX(obj) {
	var matrix = obj.css("-webkit-transform") ||
		obj.css("-moz-transform") ||
		obj.css("-ms-transform") ||
		obj.css("-o-transform") ||
		obj.css("transform");
	if (matrix !== 'none' && typeof (matrix) != "undefined") {
		var matrix = matrix.replace(/[^0-9\-.,]/g, '').split(',');
		var x = matrix[12] || matrix[4];//translate x
	} else { var x = 0; }
	//return (angle < 0) ? angle + 360 : angle;
	//return "7";
	return x;
}
//
function goToWhatuniPage(url) {
	window.open(encodeURI(url), '_system');
}
//
function goToProdiverPage(url) {

	console.log(url);
	var option = '';
	if (Constants.devicePlatform == 'IOS') {
		option = 'clearcache=yes,location=no,hardwareback=no,closebuttoncaption=close,hidenavigationbuttons=yes,toolbarposition=top';
	} else {
		//option = 'clearcache=yes,location=no,hardwareback=no,closebuttoncaption=close,footer=yes,footercolor=#333333';
		option = 'clearcache=yes,location=yes,hardwareback=no,closebuttoncaption=close,footer=no,hidenavigationbuttons=yes,hideurlbar=yes,toolbarcolor=#333333';
	}
	window.open(encodeURI(url), '_blank', option);

}
function inAppBrowser(url) {
	var inAppOption = '';
	if ((device.platform).toUpperCase() == 'IOS') {
		inAppOption = 'clearcache=yes,location=no,hardwareback=no,closebuttoncaption=close,hidenavigationbuttons=yes,toolbarposition=top';
	} else {
		inAppOption = 'clearcache=yes,location=yes,hardwareback=no,closebuttoncaption=close,footer=no,hidenavigationbuttons=yes,hideurlbar=yes,toolbarcolor=#333333';
	}
	window.open(encodeURI(url), '_blank', inAppOption);
}
function inAppBrowserPdf(url) {
	var inAppOption = '';
	if ((device.platform).toUpperCase() == 'IOS') {
		inAppOption = 'clearcache=yes,location=no,hardwareback=no,closebuttoncaption=close,hidenavigationbuttons=yes,toolbarposition=top';
		window.open(encodeURI(url), '_blank', inAppOption);
	} else {
		//inAppOption = 'clearcache=yes,location=yes,hardwareback=no,closebuttoncaption=close,footer=no,hidenavigationbuttons=yes,hideurlbar=yes,toolbarcolor=#333333';
		window.open(encodeURI(url), '_system');
	}
	
}
